package digital.vee.valepay.data.remote;

import digital.vee.valepay.model.PaymentTokenTTL;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface TokenService {
    //http://www.mocky.io/v2/59947f9c11000013037230ec
    //@GET("rest/private/generatePaymentToken")
    @GET("rest/private/generatePaymentToken")
    Call<PaymentTokenTTL> getToken(@Header("Authorization") String authorization,
                                   @Header("Accept-Language") String language,
                                   @Header("accept") String accept,
                                   @Header("Content-Type") String content);
}

