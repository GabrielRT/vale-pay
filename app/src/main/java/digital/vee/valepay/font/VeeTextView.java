package digital.vee.valepay.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class VeeTextView extends android.support.v7.widget.AppCompatTextView {

    public VeeTextView(Context context) {
        super(context);
        if (isInEditMode()) return;
        parseAttributes(null);
    }

    public VeeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    public VeeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    private static Typeface getVee(Context context, int typeface) {
        switch (typeface) {
            case Vee.VEE:
                if (Vee.sVee == null) {
                    Vee.sVee = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BoldItalic.ttf");
                }
                return Vee.sVee;
            default:
                return Vee.sVee;
        }
    }

    private void parseAttributes(AttributeSet attrs) {
        int typeface = 0;
        if (attrs == null) { //Not created from xml
            typeface = Vee.VEE;
        }
        setTypeface(getVee(typeface));
    }

    public void setVeeTypeface(int typeface) {
        setTypeface(getVee(typeface));
    }

    private Typeface getVee(int typeface) {
        return getVee(getContext(), typeface);
    }

    public static class Vee {
        public static final int VEE = 0;

        private static Typeface sVee;
    }
}
