package digital.vee.valepay.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.util.List;

/**
 * Created by brunocesar on 20/03/2017.
 */

public class Network {

    /**
     * Return if the device is online
     *
     * @param tag the class tag
     * @param context the current app context
     * @return true if the device is online
     */
    public static boolean isOnline(String tag, Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Return true if connected successfully
     *
     * @param context the application context
     * @param SSID the WiFi SSID
     * @param password the WiFi password
     * @return tru if connected
     */
    public static boolean forceWifiConnection(Context context, String SSID, String password) {
        boolean connected = false;
        if (SSID == null) {
            return false;
        }

        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> wifiList = wifiManager != null ? wifiManager.getConfiguredNetworks() : null;
        if (wifiList == null) {
            return false;
        }

        boolean notFound = true;
        for( WifiConfiguration currentWifi : wifiList ) {
            try {
                if (currentWifi.SSID != null && currentWifi.SSID.equals("\"" + SSID + "\"")) {
                    notFound = false;

                    WifiConfiguration targetWifi = currentWifi;
                    if (password.length() > 0) {
                        updateWPAConnection(targetWifi, password);
                    } else {
                        updateOpenConnection(targetWifi);
                    }
                    wifiManager.updateNetwork(targetWifi);

                    connected = wifiManager.enableNetwork(targetWifi.networkId, true);
                    wifiManager.reconnect();

                    if (connected) {
                        break;
                    }
                }
            } catch (Exception e) {
                // Not this type!
            }
        }


        if (notFound) {
            WifiConfiguration targetWifi = new WifiConfiguration();
            targetWifi.SSID = "\"" + SSID + "\"";

            if (password.length() > 0) {
                updateWPAConnection(targetWifi, password);
            } else {
                updateOpenConnection(targetWifi);
            }

            targetWifi.networkId = wifiManager.addNetwork(targetWifi);

            connected = wifiManager.enableNetwork(targetWifi.networkId, true);
            wifiManager.reconnect();
        }

        return connected;
    }


    private static void updateOpenConnection(WifiConfiguration conf) {
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
    }

    private static void updateWPAConnection(WifiConfiguration conf, String networkPass) {
        conf.preSharedKey = "\""+ networkPass +"\"";
    }

    private static void updateWEPConnection(WifiConfiguration conf, String networkPass) {
        conf.wepKeys[0] = "\"" + networkPass + "\"";

        conf.wepTxKeyIndex = 0;
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
    }

}
