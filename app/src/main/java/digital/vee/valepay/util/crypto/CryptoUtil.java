package digital.vee.valepay.util.crypto;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.yakivmospan.scytale.ErrorListener;
import com.yakivmospan.scytale.Options;
import com.yakivmospan.scytale.Store;

import org.apache.commons.codec.android.binary.Hex;
import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.SecretKey;

/**
 * Created by Sérgio Brocchetto on 20/10/2016.
 */

public class CryptoUtil {
    private static final String ALGORIZHM = "EC";
    private static final String SignareturAlgorizhm = "SHA256withECDSA";

    private static final String AES_MODE = "AES/ECB/PKCS7Padding";
    private static final String RSA_MODE = "RSA/ECB/PKCS1Padding";
    private static final String AndroidKeyStore = "AndroidKeyStore";
    private static final String SHARED_PREFENCE_NAME = "MyPrefs";
    private static KeyStore keyStore;

    private static String TAG = "createKeys";
    private static String KEY_ALIAS = "deviceKeys";
    private static String ENCRYPTED_KEY = "encryptedKey";
    private static Store store;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public CryptoUtil(Context context) throws KeyStoreException {
        // Create and save key
        if (store == null) {
            store = new Store(context);
            store.setErrorListener(new ErrorListener() {
                @Override
                public void onError(Exception e) {
                    // handle error
                    e.printStackTrace();
                }
            });
        }
        if (!store.hasKey(KEY_ALIAS)) {
            SecretKey key = store.generateSymmetricKey(KEY_ALIAS, null);
        }
    }

    /**
     * @return String[0] Base Hex encode Public key with NO_WRAP
     * String [1] Base Hex encode Private key with NO_WRAP
     * String [2] Base 64 encode Public key with NO_WRAP
     * String [3] Base 64 encode Private key with NO_WRAP
     * @throws NoSuchAlgorithmException
     */
    public String[] generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORIZHM, "SC");
        keyPairGenerator.initialize(256);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        ECPublicKey publicKey = (ECPublicKey) keyPair.getPublic();
        ECPrivateKey privateKey = (ECPrivateKey) keyPair.getPrivate();

        String[] result = new String[4];
        result[0] = new String(Hex.encodeHex(publicKey.getEncoded()));
        result[1] = new String(Hex.encodeHex(privateKey.getEncoded()));
        result[2] = Base64.encodeToString(publicKey.getEncoded(), Base64.NO_WRAP);
        result[3] = Base64.encodeToString(privateKey.getEncoded(), Base64.NO_WRAP);
        return result;
    }

    /**
     * @param data
     * @param base64EncodePrivateKeyStr : Base64 encode with NO_WRAP
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public String[] signature(byte[] data, String base64EncodePrivateKeyStr) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, SignatureException {
        String[] result = new String[2];
        byte[] pbyte = Base64.decode(base64EncodePrivateKeyStr, Base64.NO_WRAP);
        PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(pbyte);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORIZHM);
        PrivateKey privateKey = keyFactory.generatePrivate(encodedKeySpec);

        Signature ecdsaVerify = Signature.getInstance(SignareturAlgorizhm);
        ecdsaVerify.initSign(privateKey);
        ecdsaVerify.update(data);
        byte[] signature = ecdsaVerify.sign();
        result[0] = new String(Hex.encodeHex(signature));
        result[1] = Base64.encodeToString(signature, Base64.NO_WRAP);
        return result;
    }

    /**
     * @param data
     * @param signData
     * @param base64EncodePublicKeyStr: Base64 encode with NO_WRAP
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean verify(byte[] data, byte[] signData, String base64EncodePublicKeyStr) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, SignatureException {
        byte[] pbyte = Base64.decode(base64EncodePublicKeyStr, Base64.NO_WRAP);
        X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(pbyte);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORIZHM);
        PublicKey publicKey = keyFactory.generatePublic(encodedKeySpec);

        Signature ecdsaVerify = Signature.getInstance(SignareturAlgorizhm);
        ecdsaVerify.initVerify(publicKey);
        ecdsaVerify.update(data);
        return ecdsaVerify.verify(signData);
    }

    public String stringify(byte[] bytes) {
        return stringify(new String(bytes));
    }

    private String stringify(String str) {
        String aux = "";
        for (int i = 0; i < str.length(); i++) {
            aux += str.charAt(i);
        }
        return aux;
    }

    public String encrypt(String input) {
        // Get key
        SecretKey key = store.getSymmetricKey(KEY_ALIAS, null);
        // Encrypt/Decrypt data
        com.yakivmospan.scytale.Crypto crypto = new com.yakivmospan.scytale.Crypto(Options.TRANSFORMATION_SYMMETRIC);
        crypto.setErrorListener(new ErrorListener() {
            @Override
            public void onError(Exception e) {
                // handle error
                e.printStackTrace();            }
        });
        String encryptedData = crypto.encrypt(input, key);
        //Log.i("Scytale", "Encrypted data: " + encryptedData);
        return encryptedData;
    }

    public String decrypt(String input) {
        // Get key
        SecretKey key = store.getSymmetricKey(KEY_ALIAS, null);
        com.yakivmospan.scytale.Crypto crypto = new com.yakivmospan.scytale.Crypto(Options.TRANSFORMATION_SYMMETRIC);
        crypto.setErrorListener(new ErrorListener() {
            @Override
            public void onError(Exception e) {
                // handle error
                e.printStackTrace();
            }
        });
        //Log.i("Scytale", "Decrypted data: " + decryptedData);
        return crypto.decrypt(input, key);
    }
}