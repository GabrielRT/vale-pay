package digital.vee.valepay.util;/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

/**
 * Small helper class to manage text/icon around fingerprint authentication UI.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerPrintHelper extends FingerprintManager.AuthenticationCallback {

    private final FingerprintManager fingerprintManager;
    private final FingerPrintListener fingerPrintListener;
    private CancellationSignal cancellationSignal;

    private Context context;

    public interface FingerPrintListener {
        /**
         * Method called when finger print was read successfully
         */
        void onSuccess();
    }

    /**
     * Constructor for {@link FingerPrintHelper}.
     *
     * @param fingerprintManager  the finger print manager
     * @param context             the current context
     * @param fingerPrintListener listener of finger print actions
     */
    public FingerPrintHelper(FingerprintManager fingerprintManager, Context context, FingerPrintListener fingerPrintListener) {
        this.fingerprintManager = fingerprintManager;
        this.fingerPrintListener = fingerPrintListener;
        this.context = context;
    }

    public boolean isFingerprintAuthAvailable() {
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        return fingerprintManager.isHardwareDetected()
                && fingerprintManager.hasEnrolledFingerprints();
    }

    public void startListening(FingerprintManager.CryptoObject cryptoObject) {
        if (!isFingerprintAuthAvailable()) {
            return;
        }
        cancellationSignal = new CancellationSignal();
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        fingerprintManager
                .authenticate(cryptoObject, cancellationSignal, 0 /* flags */, this, null);
    }

    public void stopListening() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        Toast.makeText(this.context, errString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
        Toast.makeText(this.context, helpString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        this.fingerPrintListener.onSuccess();
    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        Toast.makeText(this.context, "Impressão digital não reconhecida", Toast.LENGTH_SHORT).show();
    }


}
