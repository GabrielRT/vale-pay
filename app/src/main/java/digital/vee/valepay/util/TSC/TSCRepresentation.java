/*
 * 2016 - All Rights Reserved.
 * Created by Fabio Covolo Mazzo
 */
package digital.vee.valepay.util.TSC;

/**
 * The Class TSCRepresentation.
 */
public class TSCRepresentation {

    /**
     * The transaction.
     */
    private String transaction;

    /**
     * The signature.
     */
    private String signature;

    /**
     * Gets the transaction.
     *
     * @return the transaction
     */
    public String getTransaction() {
        return transaction;
    }

    /**
     * Sets the transaction.
     *
     * @param transaction the transaction to set
     */
    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    /**
     * Gets the signature.
     *
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the signature.
     *
     * @param signature the signature to set
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }


}
