/*
 * 2016 - All Rights Reserved.
 * Created by Fabio Covolo Mazzo
 */
package digital.vee.valepay.util.TSC;

import android.util.Base64;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

/**
 * The Class TransactionSignedCode.
 */
public class TransactionSignedCode {

    /**
     * The gson.
     */
    private final Gson gson;

    /**
     * The transactionTSC.
     */
    private final TransactionTSC transactionTSC;

    /**
     * The signature.
     */
    private String signature;

    /**
     * Instantiates a new transactionTSC signed code.
     *
     * @param entityCode the entity code
     * @param walletCode the wallet code
     * @param amount     the amount
     * @param pin        the pin
     */
    public TransactionSignedCode(String entityCode, String walletCode, BigDecimal amount, String pin) {
        this(entityCode, walletCode, amount, pin, null);
    }

    public TransactionSignedCode(String entityCode, String walletCode, BigDecimal amount, String pin,
                                 String uuid) {
        this.gson = new Gson();
        this.transactionTSC = new TransactionTSC();
        if (uuid == null) {
            this.transactionTSC.setUuid(UUID.randomUUID().toString());
        }
        else {
            this.transactionTSC.setUuid(uuid);
        }
        transactionTSC.setEntity(entityCode);
        transactionTSC.setWallet(walletCode);
        transactionTSC.setAmount(amount.toString());
        transactionTSC.setTimestamp(Long.toString(System.currentTimeMillis()));
        String saltedPin = pin + entityCode;
        transactionTSC.setPinsec(hashPin(saltedPin));
    }

    /**
     * Hash pin.
     *
     * @param saltedPin the salted pin
     * @return the string
     */
    public static String hashPin(String saltedPin) {
        String hexHash;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(saltedPin.getBytes("UTF-8"));
            byte[] digest = md.digest();
            hexHash = String.format("%064x", new BigInteger(1, digest));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        return hexHash;
    }

    /**
     * Gets the signable transactionTSC string.
     *
     * @return the signable transactionTSC string
     */
    public String getSignableTransactionString() {
        String sts;
        try {
            sts = this.transactionTSC.getTransactionString();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return sts;

    }


    /**
     * Gets the tsc.
     *
     * @return the tsc
     */
    public String getTSC() {
        TSCRepresentation tscr = new TSCRepresentation();
        tscr.setSignature(this.signature);
        tscr.setTransaction(this.getSignableTransactionString());
        String json = gson.toJson(tscr);
        String tsc;

        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream(json.length());
            GZIPOutputStream gos = new GZIPOutputStream(os);
            gos.write(json.getBytes());
            gos.close();
            byte[] compressed = os.toByteArray();
            os.close();
            tsc = Base64.encodeToString(compressed, Base64.NO_WRAP);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return tsc;
    }

    /**
     * Gets the signature.
     *
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the signature.
     *
     * @param signature the new signature
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getUuid() {
        return transactionTSC.getUuid();
    }

}
