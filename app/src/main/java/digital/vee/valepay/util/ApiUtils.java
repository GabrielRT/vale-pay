package digital.vee.valepay.util;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.data.remote.RetrofitClient;
import digital.vee.valepay.data.remote.TokenService;

public class ApiUtils {

    public static final String BASE_URL = BuildConfig.SERVER_URL;

    public static TokenService getTokenService() {
        return RetrofitClient.getClient(BASE_URL).create(TokenService.class);
    }
}