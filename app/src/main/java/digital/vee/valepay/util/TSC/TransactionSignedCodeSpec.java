/*
 * 2016 - All Rights Reserved.
 * Created by Fabio Covolo Mazzo
 */
package digital.vee.valepay.util.TSC;

import android.util.Base64;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * This class load compressed string in a readble object.
 * Don't use this class to generate TSC, use only to convert TSC to a reable object.
 *
 * @author Fabio Covolo MAzzo
 * @date 2016-10-22
 */
class TransactionSignedCodeSpec {

    /**
     * The transactionTSC.
     */
    private TransactionTSC transactionTSC;

    /**
     * The signature.
     */
    private String signature;

    /**
     * The representation.
     */
    private TSCRepresentation representation;

    /**
     * The signable string.
     */
    private String signableString;


    /**
     * Instantiates a new transactionTSC signed code spec.
     *
     * @param tscString the tsc string
     * @throws TSCDecodeException the TSC decode exception
     */
    public TransactionSignedCodeSpec(String tscString) throws TSCDecodeException {
        try {
            /* The gson. */
            Gson gson = new Gson();
            String jsonTSCRepresentation = this.decompress(tscString);
            this.representation = gson.fromJson(jsonTSCRepresentation, TSCRepresentation.class);
            this.signableString = representation.getTransaction();
            this.signature = representation.getSignature();
            String jsonTransaction = this.decompress(representation.getTransaction());
            this.transactionTSC = gson.fromJson(jsonTransaction, TransactionTSC.class);
        } catch (IOException e) {
            throw new TSCDecodeException(e.getMessage());
        }
    }


    /**
     * Decompress.
     *
     * @param compressed the compressed
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private String decompress(String compressed) throws IOException {
        byte[] compressedBytes = Base64.decode(compressed, Base64.NO_WRAP);
        GZIPInputStream gZipInputStream = new GZIPInputStream(new ByteArrayInputStream(compressedBytes));
        return IOUtils.toString(gZipInputStream, "UTF-8");
    }


    /**
     * Gets the transactionTSC.
     *
     * @return the transactionTSC
     */
    public TransactionTSC getTransactionTSC() {
        return transactionTSC;
    }


    /**
     * Sets the transactionTSC.
     *
     * @param transactionTSC the transactionTSC to set
     */
    public void setTransactionTSC(TransactionTSC transactionTSC) {
        this.transactionTSC = transactionTSC;
    }


    /**
     * Gets the signature.
     *
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }


    /**
     * Sets the signature.
     *
     * @param signature the signature to set
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }


    /**
     * Gets the representation.
     *
     * @return the representation
     */
    public TSCRepresentation getRepresentation() {
        return representation;
    }


    /**
     * Sets the representation.
     *
     * @param representation the representation to set
     */
    public void setRepresentation(TSCRepresentation representation) {
        this.representation = representation;
    }


    /**
     * Gets the signable string.
     *
     * @return the signableString
     */
    public String getSignableString() {
        return signableString;
    }


    /**
     * Sets the signable string.
     *
     * @param signableString the signableString to set
     */
    public void setSignableString(String signableString) {
        this.signableString = signableString;
    }

}
