package digital.vee.valepay.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class to wrap SharedPreferences to save Vee Pay preferences.
 *
 * @author Sérgio Brocchetto
 */
public class Preferences {

    public static final String ECDSA_GENERATED = "com.digiv.ECDSA_GENERATED";
    public static final String ECDSA_PUBLIC_KEY = "com.digiv.ECDSA_PUBLIC_KEY";
    public static final String ECDSA_PRIVATE_KEY = "com.digiv.ECDSA_PRIVATE_KEY";
    public static final String ENCRYPTED_MESSAGE = "com.digiv.ENCRYPTED_MESSAGE";
    private static final String SHARED_PREFERENCES = "MyPrefs";
    public static final String GENERATED_PASSWORD = "generatedPassword";
    private static SharedPreferences mPreferences;
    public static String HEADER_NAME = "headerName";

    public static void init(Context context) {
        mPreferences = context.getSharedPreferences(SHARED_PREFERENCES, 0);
    }

    public static boolean getBoolean(String key) {
        return mPreferences.getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return mPreferences.getBoolean(key, defaultValue);
    }

    public static void putBoolean(String key, boolean bool) {
        mPreferences.edit().putBoolean(key, bool).apply();
    }

    public static void putString(String key, String s) {
        mPreferences.edit().putString(key, s).apply();
    }

    public static void putInteger(String key, Integer integer) {
        mPreferences.edit().putInt(key, integer).apply();
    }

    public static void putDouble(String key, double value) {
        mPreferences.edit().putLong(key, Double.doubleToLongBits(value)).apply();
    }

    public static void clear() {
        mPreferences.edit().clear().apply();
    }

    public static void remove(String key) {
        mPreferences.edit().remove(key).apply();
    }

    public static String getString(String key) {
        return mPreferences.getString(key, null);
    }

    public static int getInteger(String key) {
        return mPreferences.getInt(key, 0);
    }

    public static double getDouble(String key) {
        return Double.longBitsToDouble(mPreferences.getLong(key, 0));
    }

    public static boolean contains(String key) {
        return mPreferences.contains(key);
    }

}
