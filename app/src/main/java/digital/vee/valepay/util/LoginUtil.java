package digital.vee.valepay.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.crash.FirebaseCrash;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.activity.ActivateActivity;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.helper.LoginUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.model.Employee;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.model.EntityData;

/**
 * Class that control LoginUtil actions and behaviors.
 * Created by Sérgio Brocchetto
 */
public class LoginUtil {
    private static final String URL = BuildConfig.SERVER_URL + "rest/authenticate";
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";
    private static final String TAG = "LoginUtil";

    public static final String TOKEN_KEY = "tokenKey";
    public static final String USERNAME = "userName";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_ID = "userId";
    public static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    public static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    public static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";
    public static final String WALLET_ID = "walletId";
    private ProgressDialog progressDialog;
    private Context context;
    private static Employee employee = new Employee();
    private static EntityData entityData = new EntityData();
    private static ArrayList<Wallet> walletList;

    /**
     * Validate data input and call login service on server.
     *
     * @param userId
     * @param password
     * @param context
     * @param getData tell if need to fetch cards data or not
     * @throws RuntimeException
     * @throws JSONException
     */

    public void login(String userId, String password, final Context context, final boolean getData) throws RuntimeException, JSONException {

        this.context = context;

        Preferences.init(context);

        String login = userId;
        if (LoginUtils.isValidCPF(login)) {
            // user input a CPF, so remove extra chars before send to server
            login = login.replaceAll("[ .-]", "");
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("userId", login);
            jsonBody.put("password", password);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }

        final String mRequestBody = jsonBody.toString();

        final Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String type = json.getString("type"), token = json.getString("token"),
                            subject = json.getString("subject");
                    SnappyDBHelper.putEncrypted(context, LoginUtil.TOKEN_KEY, token);
                    //Log.v(TAG, "Response: " + response);


                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }


                    if (token == null || token.isEmpty()) {
                        FirebaseCrash.log("---LoginUtil: error on login invalid token: " + token);
                        //Log.d(TAG,"---LoginUtil: error on login invalid token: " + token);
                    }


                    if (getData) {
                        onLoginSuccess(context);
                    }
                } catch (Exception e) {
                    FirebaseCrash.log("---LoginUtil: error on login " + e);
                    //Log.d(TAG,"---LoginUtil: error on login " + e);
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.v(TAG, "Exception: " + error);
                if (error != null) {
                    //Log.v(TAG, "Exception: " + error);
                }
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener, errorListener,
                mRequestBody) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-authc-username-password+json";
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //Log.v(TAG, " +parseNetworkError()");
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    try {
                        onErrorResponse(volleyError);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                //Log.v(TAG, TAG + " -parseNetworkError()");
                return volleyError;
            }
        };
        Volley.newRequestQueue(context).add(postRequest);
    }

    /**
     * Process error on response from server and log or show it to user.
     *
     * @param error Error received from server.
     * @throws UnsupportedEncodingException
     */
    private void onErrorResponse(VolleyError error) throws UnsupportedEncodingException {

        //Log.v(TAG, " +onErrorResponse()");
        dismissDialog();
        String json = null;

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    json = trimMessage(json, "message");

                    if (!json.isEmpty())
                        displayMessage(json);
                    break;
                default:

                    //Log.v(TAG, "error: " + json);
            }

        }
        //Log.v(TAG, " -onErrorResponse()");
    }

    /**
     * Get message from JSON response received from server
     *
     * @param json JSON response text
     * @param key Key to find message in JSON response
     * @return Message from JSON or <code>null</code> in case of no message in JSON.
     * @throws UnsupportedEncodingException
     */
    private String trimMessage(String json, String key) throws UnsupportedEncodingException {
        String trimmedString;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        trimmedString = Arrays.toString(trimmedString.getBytes("UTF-8"));
        return trimmedString;
    }

    /**
     * Display a toast message to user in UTF-8 format
     *
     * @param toastString Message to show
     */
    private void displayMessage(String toastString) {
        String s = null;
        try {
            s = new String(toastString.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Toast.makeText(this.context, s, Toast.LENGTH_LONG).show();
    }

    /**
     * Process success on login
     */
    private void onLoginSuccess(final Context context) {

        //Log.d(TAG, "+OnloginSuccess");
        dismissDialog();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            String response;

            @Override
            public void onResponse(String response) {
                try {
                    this.response = response;
                    JSONObject jsonResponse = new JSONObject(response);

                    //Log.v(TAG, "Response: " + jsonResponse.toString());

                    // SnappyDB
                    try {
                        //DB snappydb = DBFactory.open(context); //create or open an existing database using the default name
                        // salva o json

                        //snappydb.put("CARDS", response);
                        //String responseReturned = snappydb.get("CARDS");

                        SnappyDBHelper.put(context, "CARDS", response);

                        String responseReturned = SnappyDBHelper.get(context, "CARDS");

                        //Log.v(TAG, "responseReturned 1 " + responseReturned);

                        // caso retorne online a lista atualiza

                        //Faz o parse e carrega no models

                        //snappydb.close();

                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }

                    employee = Employee.fromJson(jsonResponse);

                    JSONObject abstractEntity = jsonResponse.getJSONObject("abstractEntity");
                    entityData = EntityData.fromJson(abstractEntity);
                    SnappyDBHelper.put(context, "idEntityNumber", entityData.getIdEntity());

                    walletList = Wallet.fromJson(abstractEntity.getJSONArray("walletList"));

                    SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.VEE_DATE_TIME_FORMAT);
                    String dateFormatted = formatter.format(System.currentTimeMillis());
                    SnappyDBHelper.put(context, "lastUpdated", dateFormatted);

                    //Log.v(TAG, "dateFormatted: " + dateFormatted);

                } catch (Exception e) {
                    //Log.v(TAG, "2 Exception: " + e.getMessage());


                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Log.v(TAG, "error: " + error);
                //TODO: implement
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, USER_DATA_URL,
                responseListener, errorListener, null) {
            Request.Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");

                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(context, LoginUtil.TOKEN_KEY));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Content-Type", "application/json");
                params.put("apiVersion", "2");
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
            }
        };

        Volley.newRequestQueue(context).add(postRequest);


        //Log.d(TAG, "-OnloginSuccess");
    }

    /**
     * Dissmiss current progress dialog in case it is running.
     */
    private void dismissDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

}
