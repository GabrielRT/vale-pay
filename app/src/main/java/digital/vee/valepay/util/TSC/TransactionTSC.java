/*
 * 2016 - All Rights Reserved.
 * Created by Fabio Covolo Mazzo
 */
package digital.vee.valepay.util.TSC;


import android.util.Base64;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;


// TODO: Auto-generated Javadoc

/**
 * The Class TransactionOld.
 */
public class TransactionTSC {

    /**
     * The uuid.
     */
    private String uuid;

    /**
     * The entity.
     */
    private String entity;

    /**
     * The wallet.
     */
    private String wallet;

    /**
     * The amount.
     */
    private String amount;

    /**
     * The timestamp.
     */
    private String timestamp;

    /**
     * The pinsec.
     */
    private String pinsec;

    /**
     * Gets the uuid.
     *
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the uuid.
     *
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets the entity.
     *
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the entity.
     *
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * Gets the wallet.
     *
     * @return the wallet
     */
    public String getWallet() {
        return wallet;
    }

    /**
     * Sets the wallet.
     *
     * @param wallet the wallet to set
     */
    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    /**
     * Gets the amount.
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp.
     *
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets the pinsec.
     *
     * @return the pinsec
     */
    public String getPinsec() {
        return pinsec;
    }

    /**
     * Sets the pinsec.
     *
     * @param pinsec the pinsec to set
     */
    public void setPinsec(String pinsec) {
        this.pinsec = pinsec;
    }

    /**
     * Gets the transaction string.
     *
     * @return the transaction string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String getTransactionString() throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        ByteArrayOutputStream os = new ByteArrayOutputStream(json.length());
        GZIPOutputStream gos = new GZIPOutputStream(os);
        gos.write(json.getBytes());
        gos.close();
        byte[] compressed = os.toByteArray();
        os.close();
        return Base64.encodeToString(compressed, Base64.NO_WRAP);

    }

}
