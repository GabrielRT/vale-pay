package digital.vee.valepay.util.TSC;

class TSCDecodeException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -8170583463831994967L;

    /**
     * Instantiates a new TSC Decode Exception generic exception.
     */
    public TSCDecodeException() {
    }

    /**
     * Instantiates a new TSC Decode Exception generic exception.
     *
     * @param message the message
     */
    public TSCDecodeException(String message) {
        super(message);
    }

    /**
     * Instantiates a new TSC Decode Exception generic exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public TSCDecodeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new TSC Decode Exception generic exception.
     *
     * @param cause the cause
     */
    public TSCDecodeException(Throwable cause) {
        super(cause);
    }
}
