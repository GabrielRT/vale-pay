package digital.vee.valepay.util;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import org.apache.commons.codec.android.CharEncoding;

import java.io.UnsupportedEncodingException;

/**
 * Created by Sérgio Brocchetto on 21/09/2016.
 */

public class StringRequestCustom extends StringRequest {
    private final String mRequestBody;
    private Priority mPriority;

    public StringRequestCustom(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener,
                               String mRequestBody) {
        super(method, url, listener, errorListener);
        this.mRequestBody = mRequestBody;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            // Force UTF-8 as default, in case of no charset set in headers
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers,
                    CharEncoding.UTF_8));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(CharEncoding.UTF_8);

        } catch (UnsupportedEncodingException uee) {
            //Volleyandroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s",
            //mRequestBody, "utf-8");
            return null;
        }
    }

    @Override
    public Priority getPriority() {
        return mPriority != null ? mPriority : Priority.IMMEDIATE;
    }

    public void setPriorityImmediate() {
        mPriority = Priority.IMMEDIATE;
    }

}
