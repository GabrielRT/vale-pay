package digital.vee.valepay.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import digital.vee.valepay.R;

/**
 * Class used to handle Vee standard transitions whenever starting or finishing activities
 *
 * Created by silvio on 11/07/18.
 */

public class ActivityUtils {

    public static void newActivity(Context context, Intent intent) {
        Activity activity = (Activity) context;

        context.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public static void newActivityForResult(Context context, Intent intent, int requestCode) {
        Activity activity = (Activity) context;

        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public static void exitActivity(Context context) {
        Activity activity = (Activity) context;
        activity.finish();
        activity.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public static void exitActivity(Context context, Intent intent) {
        Activity activity = (Activity) context;

        context.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
