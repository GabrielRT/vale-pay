package digital.vee.valepay.util;

/**
 * Created by Sérgio Brocchetto on 02/05/2017.
 */

public class NamesUtil {

    public static String trimFirstName(String firstName) {
        String displayName;

        if (firstName != null && !firstName.isEmpty() && firstName.contains(" ")) {
            displayName = firstName.substring(0, firstName.indexOf(" "));
        } else {
            displayName = firstName;
        }

        if (displayName != null && displayName.length() > 25) {
            displayName = displayName.substring(0, 25);
        }
        return displayName;
    }

}
