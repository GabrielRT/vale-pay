package digital.vee.valepay.util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class App extends Application {
    public static final String BALANCE_WALLET = "balance";
    public static final String BALANCE_HOME = "balanceHome";
    public static final String TAG = "App";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //TestFairy.begin(this, "7f07e19073118add0c1584e397184c994f140659");
    }

}