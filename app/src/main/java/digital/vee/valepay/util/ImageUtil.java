package digital.vee.valepay.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import static java.security.AccessController.getContext;

/**
 * Class that holds image related methods like screen size or resize bitmap
 *
 * Created by silvio on 05/04/18.
 */

public abstract class ImageUtil{
    /**
     * Transform the file in a byte array, file is used because we don't have other way of passing
     * the array through activities.
     * @param file File containing the image
     * @return byte array of the image
     * @throws IOException
     */
    public static byte[] makeByteArray(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    /**
     * Compress a bitmap to a JPEG, the return it's byte array.
     * @param bm bitmap to be compressed
     * @return
     */
    public static byte[] bitmapToByteArray(Bitmap bm) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, os);
        return os.toByteArray();
    }

    /**
     * Return an array with size 2 containing height and width of the device screen
     *
     * @return array[0] width, array[1] height
     */
    public static int[] getDeviceScreenSize(){
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;

        int response[] = new int[2];
        response[0] = width;
        response[1] = height;

        return response;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Method that calculates the correct bitmap size so it doesn't allocate to much memory and get OOM
     * Exception
     */
    public static Bitmap getResizedBitmap(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        int size[] = ImageUtil.getDeviceScreenSize(); // [0] width [1] height

        // Calculate inSampleSize
        options.inSampleSize = ImageUtil.calculateInSampleSize(options, size[0], size[1]);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(imagePath, options);
    }
}
