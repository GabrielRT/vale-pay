package digital.vee.valepay.util.TSC;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.snappydb.SnappydbException;

import org.apache.commons.codec.android.CharEncoding;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Helper class to check transactions
 *
 * @author Rafael O. Magalhaes
 * @since 29/01/2017.
 */
public class TSCHelper {

    /**
     * Success Callback interface of Transactions server calls
     */
    public interface Success {
        /**
         * Process server success response
         *
         * @param jsonResponse JSON response from server
         */
        void onSuccess(JSONObject jsonResponse);
    }

    /**
     * Failure Callback interface of Transactions server calls
     */
    public interface Failure {
        /**
         * Process server failure response
         *
         * @param error Error received from server
         */
        void onFailure(String error);
    }

    public static final String TAG = "TSCHelper";
    private static final String TRANSACTION_URL_FIND = BuildConfig.SERVER_URL + "rest/private/transaction/find";

    /**
     * Check if the transaction passed (transactionUUID) is valid on server.
     *
     * @param context Context that need to check the transaction
     * @param transactionUUID UUID of the transaction to check
     * @param successCallback Callback that process the success return from server
     * @param failureCallback Callback that process the failure return from server
     * @return
     */
    public static boolean checkTransaction(final Context context, String transactionUUID,
                                           final Success successCallback, final Failure failureCallback) {
        //Log.v(TAG, "+checkTransaction transactionUUID: " + transactionUUID);
        final JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("transactionUUID", transactionUUID);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        final String requestBody = jsonBody.toString();

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String dtConfirmed = jsonResponse.getString("dtConfirmed");
                    String rejectReason = jsonResponse.getString("rejectReason");
                    String dtCanceled = jsonResponse.getString("dtCanceled");

                    if (jsonResponse.getBoolean("usedVoucher")) {
                        String amountSpentFromWallet = jsonResponse.getJSONObject("voucherResponse").getString("amountSpentFromWallet");
                        String amountSpentFromVoucher = jsonResponse.getJSONObject("voucherResponse").getString("amountSpentFromVoucher");
                        SnappyDBHelper.put(context, "amountSpentFromWallet", amountSpentFromWallet);
                        SnappyDBHelper.put(context, "amountSpentFromVoucher", amountSpentFromVoucher);
                    }

                    //Log.v(TAG, "check *** *** dtConfirmed: " + dtConfirmed + "\nrejectReason: " + rejectReason + "\n dtCanceled: " + dtCanceled);

                    if (!dtCanceled.equals("null") || !rejectReason.equals("null") || !dtConfirmed.equals("null")) {
                        //Log.v(TAG, "*** *** dtConfirmed: " + dtConfirmed + " rejectReason: " + rejectReason + " dtCanceled: " + dtCanceled);
                        if (!dtCanceled.isEmpty() || !rejectReason.isEmpty() || !dtConfirmed.isEmpty()) {
                            //Log.v(TAG, " 2 FIND()");
                            //Log.v(TAG, "*** *** dtConfirmed: " + dtConfirmed + " rejectReason: " + rejectReason + " dtCanceled: " + dtCanceled);
                            if (dtConfirmed!= null && !dtConfirmed.isEmpty() && !dtConfirmed.equals("null")) {
                                //Log.v(TAG, "*** *** dtConfirmed: " + dtConfirmed + " rejectReason: " + rejectReason + " dtCanceled: " + dtCanceled);
                                //Log.v(TAG, " 3 FIND()");
                                successCallback.onSuccess(jsonResponse);
                            } else if ((!dtCanceled.isEmpty() && !dtCanceled.equals("null")
                                    || (!rejectReason.isEmpty() && !rejectReason.equals("null")))) {
                                failureCallback.onFailure(rejectReason);
                            }
                        }
                    } else if ((!dtCanceled.isEmpty() && !dtCanceled.equals("null")
                            || (rejectReason.isEmpty() && !rejectReason.equals("null")))) {
                        failureCallback.onFailure(rejectReason);
                    }
                } catch (Exception e) {
                    //Log.v(TAG, "4 Exception: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.v(TAG, "18 Exception: " + error + " " + error.getCause() + " " + error.getMessage());
                if (error.getMessage() != null && !error.getMessage().isEmpty()) {
                    //Log.v(TAG, "5 Exception: " + error + " " + error.getCause() + " " + error.getMessage());
                    failureCallback.onFailure(null);
                } else {
                    failureCallback.onFailure(null);
                }
                error.printStackTrace();
            }
        };
        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, TRANSACTION_URL_FIND, listener,
                errorListener, requestBody) {
            Request.Priority priority;

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(context, "tokenKey"));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Accept-Language", "pt_br");
                //Log.v(TAG, TAG + " Header token: " + token);
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //Log.v(TAG, " +parseNetworkError()");
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers,
                                        CharEncoding.UTF_8));
                        //Log.v(TAG, " +json(): " + json);

                    } catch (UnsupportedEncodingException e) {
                        //Log.v(TAG, " +UnsupportedEncodingException(): " + json);
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                    //Log.v(TAG, "UnsupportedEncodingException: " + uee + " " + uee.getCause() + " " + uee.getMessage());
                    //VolleyLog.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Request.Priority getPriority() {
                // If you didn't use the setPriority method,sharedpreferences
                return priority != null ? priority : Request.Priority.IMMEDIATE;
            }

        };

        Volley.newRequestQueue(context).add(postRequest);
        //Log.v(TAG, "-checkTransaction transactionUUID: " + transactionUUID);
        return true;
    }
}
