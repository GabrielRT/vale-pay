package digital.vee.valepay.util;

/**
 * Created by Sérgio Brocchetto on 21/12/14.
 */
class Strings {

    public static boolean isNullOrEmpty(String str) {
        return str != null && !str.isEmpty();
    }

}
