package digital.vee.valepay.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import digital.vee.valepay.R;
import digital.vee.valepay.activity.BaseActivity;
import digital.vee.valepay.activity.TransactionsListActivity;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.model.Transaction;
import digital.vee.valepay.util.LoginUtil;

/**
 * Refactored version of the old Transaction Adapter, where the transaction objects are turned into
 * Views for the Transaction list activity.
 * <p>
 * Created by silvio on 28/05/18.
 */

public class TransactionAdapter extends ArrayAdapter<Transaction> {
    private final Context context;

    public TransactionAdapter(Context context, ArrayList<Transaction> transactions) {
        super(context, 0, transactions);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup viewGroup) {
        // Get the data item for this position
        Transaction transaction = getItem(position);

        ViewHolder holder;

        // Check if an existing view is being reused, otherwise inflate the view.
        // Sets a view holder to the view so we spare processing power on the findView method.
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_transaction, viewGroup, false);
            holder = new ViewHolder(view);

        } else {
            holder = (ViewHolder) view.getTag();
        }


        assert transaction != null;
        if (isPaymentRejected(transaction)) {
            // if transaction used voucher, then, it will also have a voucher background
            int id = (transaction.usedVoucher) ? R.color.voucher_item_transaction : R.color.white;
            view.setBackgroundColor(context.getResources().getColor(id));
            makePaymentRejectedView(holder, transaction);
        } else if (isPayment(transaction)) {
            // if transaction used voucher, then, it will also have a voucher background
            int id = (transaction.usedVoucher) ? R.color.voucher_item_transaction : R.color.white;
            view.setBackgroundColor(context.getResources().getColor(id));
            makePaymentView(holder, transaction);
        } else if (transaction.isVoucher) {
            // voucher has different background from the rest it has a grey background
            view.setBackgroundColor(context.getResources().getColor(R.color.voucher_item_transaction));
            makeCreditView(holder, transaction, context.getString(R.string.text_voucher), transaction.description);
        } else if (isRefund(transaction)) {
            // white background
            view.setBackgroundColor(context.getResources().getColor(R.color.white));
            makeRefundView(holder, transaction);
        } else if (isDeposit(transaction)) {
            // white background
            view.setBackgroundColor(context.getResources().getColor(R.color.white));
            makeCreditView(holder, transaction, context.getString(R.string.deposit_text), transaction.entityNameTo);
        } else if (isGiftCard(transaction)) {
            // white background
            view.setBackgroundColor(context.getResources().getColor(R.color.white));
            makeCreditView(holder, transaction, context.getString(R.string.deposit_text), transaction.typeDescription);
        } else if (isChargeBack(transaction)) {
            // white background
            view.setBackgroundColor(context.getResources().getColor(R.color.white));
            makeCreditView(holder, transaction, context.getString(R.string.credit_payment_text), transaction.entityNameFrom);
        }

        /*
            The code below make a click listener for when
         */
        LinearLayout btButton = view.findViewById(R.id.item_transaction_click);
        // Cache row position inside the button using `setTag`

        btButton.setTag(position);
        // Attach the click event handler
        final String finalDtConfirmed = transaction.dtConfirmed;
        btButton.setOnClickListener(view1 -> {

            // Access the row position here to getEncrypted the correct data item
            Transaction transaction1 = getItem(position);
            boolean refund = false;
            try {
                if (transaction1.movementType.equalsIgnoreCase("refund")) {
                    refund = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            assert transaction1 != null;
            if ((!transaction1.isVoucher && !transaction1.usedVoucher &&
                    finalDtConfirmed != null && !finalDtConfirmed.isEmpty() && context instanceof TransactionsListActivity &&
                    transaction1.isDebitTransaction()) || refund) {
                ((TransactionsListActivity) context).callQRcode(transaction1);
            }
        });
        view.setTag(holder);
        return view;
    }

    private void makeUndoneView(ViewHolder holder, Transaction transaction, String textType, String details) {
        holder.fillView(transaction.dtUndone,
                transaction.amount,
                ContextCompat.getColor(context, R.color.iron),
                String.format("%s - %s", textType, transaction.getMovementCode()),
                View.VISIBLE,
                details,
                true);
    }

    /**
     * Make green text on the amount, because user will be receiving money, set transaction date and
     * put the details bottom text:
     * <ul>
     * <li> in a deposit, the name of the corporate will be on 'entityNameTo'</li>
     * <li> in a credit payment, the name of merchant is on 'entityNameTo'</li>
     * <li> in a voucher, where the name of the restaurant is supposed to be, it says "consumption $value",
     * it is on transaction description</li>
     * <li> in a GiftCard, the code used is on movement type description</li>
     * </ul>
     */
    private void makeCreditView(ViewHolder holder, Transaction transaction, String textType, String details) {
        boolean pendingTransaction = !hasDtConfirmedOrCanceled(transaction);
        String date = "";
        if (transaction.dtConfirmed != null && !transaction.dtConfirmed.isEmpty()) {
            date = transaction.dtConfirmed;
        } else {
            date = transaction.dtStarted;
        }
        holder.fillView(date,
                BaseActivity.setDecimal(Double.valueOf(transaction.amount)),
                ContextCompat.getColor(context, pendingTransaction ? R.color.md_orange_700 : R.color.green),
                String.format("%s - %s", textType, transaction.getMovementCode()),
                View.VISIBLE,
                details);
    }

    private void  makeRefundView(ViewHolder holder, Transaction transaction) {
        String date;
        int amountColor;
        String amount;
        String amountPattern;


        // DATE: it first looks for dtConfirmed, then canceled, then lastly dtStarted
        if (transaction.dtConfirmed != null && !transaction.dtConfirmed.isEmpty()) {
            date = transaction.dtConfirmed;
        } else if (transaction.dtCanceled != null && !transaction.dtCanceled.isEmpty()) {
            date = transaction.dtCanceled;
        } else {
            date = transaction.dtStarted;
        }

        /*
          If transaction does not have amount, use 'N/D', if rejected or error use orange color
         */
        if (transaction.rejectReason != null && !transaction.rejectReason.isEmpty()) {
            amountColor = R.color.md_orange_700;
            amountPattern = "%s";
        } else {
            amountColor = R.color.red;
            amountPattern = "- %s";
        }
        try {
            amount = String.format(amountPattern, BaseActivity.setDecimal(Double.valueOf(transaction.amount)));
        } catch (NumberFormatException e) {
            amount = "N/D";
        }

        holder.fillView(date,
                amount,
                ContextCompat.getColor(context, amountColor),
                String.format("%s - %s", context.getString(R.string.menu_text_refund), transaction.getMovementCode()),
                View.VISIBLE,
                // Set the status of the refund
                transaction.description);
    }

    /**
     * Create the payment view by setting values for confirmed or reject payment and the name of
     * the restaurant
     */
    private void makePaymentRejectedView(ViewHolder holder, Transaction transaction) {
        // for some reason android was hiding bottomText when scrolling fast
        holder.bottomText.setVisibility(View.VISIBLE);

        int bottomVisibility = View.VISIBLE;
        String bottomText;

        // Set the name of the restaurant
        if (transaction.entityNameTo == null || transaction.entityNameTo.isEmpty()
                || transaction.entityNameTo.equalsIgnoreCase("null")) {
            bottomText = "N/D";
        } else {
            bottomText = transaction.entityNameTo;
        }

        /*
           If transaction was rejected then there will be a dtCanceled, and the
           amount should be orange, because that amount will have been withdrawn of the users
           balance.
         */
        holder.fillView(transaction.dtCanceled,
                BaseActivity.setDecimal(Double.valueOf(transaction.amount)),
                ContextCompat.getColor(context, R.color.md_orange_700),
                String.format("%s - %s", transaction.rejectReason, transaction.getMovementCode()),
                bottomVisibility, bottomText);
    }


    /**
     * Create the payment view by setting values for confirmed or reject payment and the name of
     * the restaurant
     */
    private void makePaymentView(ViewHolder holder, Transaction transaction) {
        // for some reason android was hiding bottomText when scrolling fast
        holder.bottomText.setVisibility(View.VISIBLE);

        int bottomVisibility = View.VISIBLE;
        String bottomText;
        boolean pendingTransaction = !hasDtConfirmedOrCanceled(transaction);

        // Set the name of the restaurant
        if (transaction.entityNameTo == null || transaction.entityNameTo.isEmpty()
                || transaction.entityNameTo.equalsIgnoreCase("null")) {
            bottomText = "N/D";
        } else {
            bottomText = transaction.entityNameTo;
        }

        /*
          If transaction was not rejected then there will be a dtConfirmed, and the
          amount should be red, because that amount will have been withdrawn of the users
          balance.
        */
        holder.fillView(pendingTransaction ? transaction.dtStarted : transaction.dtConfirmed,
                String.format("- %s", BaseActivity.setDecimal(Double.valueOf(transaction.amount))),
                ContextCompat.getColor(context, pendingTransaction ? R.color.md_orange_700 : R.color.red),
                String.format("%s - %s", context.getString(R.string.tv_typemsg_payment), transaction.getMovementCode()),
                bottomVisibility, bottomText);
    }

    /**
     * Check if transaction is payment or charge back rejected.
     *
     * @param transaction
     * @return true if transaction is a rejection
     */
    private boolean isPaymentRejected(Transaction transaction) {
        // For rejection in payment, we check its status
        boolean isRejected = transaction.status.equals(Transaction.REJECTED);

        // Movement type do not exist in Payment/Charge back rejected transactions
        boolean paymentMovementType = transaction.movementType == null
                || transaction.movementType.isEmpty();

        return isRejected && paymentMovementType;
    }

    /**
     * Check if transaction is payment by validating conditions that only a payment would have
     *
     * @param transaction
     * @return true if transaction is a payment
     */
    private boolean isPayment(Transaction transaction) {
        // Must be debit, so must be true
        boolean isDebit = transaction.debitOrCredit != null
                && transaction.debitOrCredit.equalsIgnoreCase("debit");

        // Movement type do not exist in Payment transactions
        boolean paymentMovementType = transaction.movementType == null
                || transaction.movementType.isEmpty();

        /* A payment can be confirmed only after its status changes to processed,
             so we may have any of the two on the list */
        boolean notUndone = transaction.status.equals(Transaction.PROCESSED)
                || transaction.status.equals(Transaction.CONFIRMED);

        return isDebit && paymentMovementType;
    }

    /**
     * Check if transaction has dtConfirmed or dtCanceled. If not, a server error happened and
     * did not change transaction status (transaction is pending on server).
     *
     * @param transaction
     * @return <code>true</code> if has dtConfirmed or dtCanceled.
     */
    private boolean hasDtConfirmedOrCanceled(Transaction transaction) {
        return transaction.dtConfirmed != null
                    && !transaction.dtConfirmed.isEmpty()
                    || transaction.dtCanceled != null
                    && !transaction.dtCanceled.isEmpty();
    }

    /**
     * Check if transaction is Deposit by validating conditions that only a Deposit would have
     *
     * @param transaction
     * @return true if transaction is a Deposit
     */
    private boolean isDeposit(Transaction transaction) {
        // For Credit, 'debitOrCredit' must equals 'credit'
        boolean isCredit = transaction.debitOrCredit != null
                && transaction.debitOrCredit.equalsIgnoreCase("credit");

        // In order to be Deposit, movement type must equals 'deposit'
        boolean depositMovementType = transaction.movementType != null &&
                transaction.movementType.equalsIgnoreCase(context.getString(R.string.deposit_text));

        return isCredit && depositMovementType;
    }

    /**
     * Check if transaction is GiftCard credit by validating conditions.
     *
     * @param transaction
     * @return true if transaction is a Deposit
     */
    private boolean isGiftCard(Transaction transaction) {
        // For Credit, 'debitOrCredit' must equals 'credit'
        boolean isCredit = transaction.debitOrCredit != null
                && transaction.debitOrCredit.equalsIgnoreCase("credit");

        // In order to be Deposit, movement type must equals 'deposit'
        boolean giftCardMovementType = transaction.movementType != null &&
                transaction.movementType.equalsIgnoreCase(context.getString(R.string.giftcard_text));

        return isCredit && giftCardMovementType;
    }

    /**
     * Check if transaction is a charge back by validating conditions
     *
     * @param transaction
     * @return true if transaction is a Deposit
     */
    private boolean isChargeBack(Transaction transaction) {
        // For Credit, 'debitOrCredit' must equals 'credit'
        boolean isCredit = transaction.debitOrCredit != null
                && transaction.debitOrCredit.equalsIgnoreCase("credit");

        // In order to be ChargeBack, movement type must be null
        boolean chargeBackMovementType = transaction.movementType == null
                || transaction.movementType.isEmpty();

        return isCredit && chargeBackMovementType;
    }

    /**
     * Check if transaction is Refund by validating conditions that only a Refund would have
     *
     * @param transaction
     * @return true if transaction is a Refund
     */
    private boolean isRefund(Transaction transaction) {
        // For refund movement type must be equal to refund
        return transaction.movementType != null
                && transaction.movementType.equalsIgnoreCase("refund");
    }

    /**
     * Check if transaction is Undone by validating conditions
     *
     * @param transaction
     * @return true if transaction was undone
     */
    private boolean isUndonePayment(Transaction transaction) {
        // Check for undone status
        return transaction.status.equals(Transaction.UNDONE);
    }




    private static class ViewHolder {
        private TextView date;
        private TextView type;
        private TextView amount;
        private TextView symbol;
        private TextView bottomText;

        ViewHolder(View view) {
            this.amount = view.findViewById(R.id.tvAmount);
            this.date = view.findViewById(R.id.tvDtConfirmed);
            this.bottomText = view.findViewById(R.id.tvRestaurant);
            this.symbol = view.findViewById(R.id.v_symbol_transactions);
            this.type = view.findViewById(R.id.tvType);
        }

        public void fillView(String date, String amount, int amountColor, String type, int bottomVisibility, String bottomText,
                             boolean undone) {
            this.date.setText(DateUtils.getDateTimeFromUTC(date));
            this.amount.setText(amount);
            this.amount.setTextColor(amountColor);
            this.type.setText(type);
            this.bottomText.setVisibility(bottomVisibility);
            if (bottomVisibility == View.VISIBLE) {
                this.bottomText.setText(bottomText);
            }


        }

        public void fillView(String date, String amount, int amountColor, String type, int bottomVisibility, String bottomText) {
            fillView(date, amount, amountColor, type, bottomVisibility, bottomText, false);
        }
    }
}
