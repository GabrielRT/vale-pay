package digital.vee.valepay.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import digital.vee.valepay.R;
import digital.vee.valepay.databinding.ItemViewBinding;
import digital.vee.valepay.helper.BitmapHelper;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.model.WalletType;

/**
 * Class to manage data of carousel of cards.
 *
 * Created by Rafael on 13/03/17.
 */
public class CarouselCardsAdapter extends RecyclerView.Adapter<CarouselViewHolder> {

    private static final String TAG = "CarouselCardsAdapter";

    private ArrayList<HashMap<String,Object>> mCardItens;
    private Context con;

    public CarouselCardsAdapter(Context context, ArrayList<Wallet> walletList, String nameOnCards) {
        this.con = context;
        mCardItens = new ArrayList<>();

        updateWallets(walletList, nameOnCards);
    }

    /**
     * Update carousel cards according to wallets data and putEncrypted a Add New Card at the end.
     * @param walletList Wallet list to fill cards of the carousel
     */
    public void updateWallets(ArrayList<Wallet> walletList, String nameOnCards) {
        //Log.v(TAG, "walletList.size: " + walletList.size());
        mCardItens.clear();
        Drawable card;
        HashMap<String, Object> cardMap;
        BitmapHelper bitmapHelper = BitmapHelper.getInstance(con);
        for (Wallet w : walletList) {
            cardMap =  new HashMap<>();
            card = bitmapHelper.getCardDrawableFront(w, nameOnCards);
            if (card != null) {
                cardMap.put("card", card);
            }
            cardMap.put("color", Color.BLACK);
            cardMap.put("wallet", w);
            mCardItens.add(cardMap);
        }

        card = con.getResources().getDrawable(R.drawable.card_v_add);
        cardMap =  new HashMap<>();
        cardMap.put("card", card);
        cardMap.put("color", Color.WHITE);
        Wallet walletAdd = new Wallet();
        WalletType walletType = new WalletType();
        walletType.setCode("vAdd");
        walletAdd.setWalletType(walletType);
        cardMap.put("wallet", walletAdd);
        mCardItens.add(cardMap);

        //Log.v(TAG, "cards.size: " + mCardItens.size());
        notifyDataSetChanged();
    }

    @Override
    public CarouselViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new CarouselViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final CarouselViewHolder holder, final int position) {
        HashMap<String, Object> cardMap = mCardItens.get(position);
        holder.itemView.setBackgroundColor((Integer) cardMap.get("color"));
        if (cardMap.containsKey("card")) {
            Drawable background = (Drawable) cardMap.get("card");
            holder.itemView.setBackground(background);
        }
    }

    @Override
    public int getItemCount() {
        return mCardItens.size();
    }

    /**
     * Get wallet of a card at specific position
     * @param position Position of the wallet in carousel
     * @return wallet of a card at specific position
     */
    public Wallet getItemWallet(int position) {
        return (Wallet) mCardItens.get(position).get("wallet");
    }
}

/**
 * Extends Recycler.ViewHolder, belong to the adapter
 */
class CarouselViewHolder extends RecyclerView.ViewHolder {

    private final ItemViewBinding mItemViewBinding;

    CarouselViewHolder(final ItemViewBinding itemViewBinding) {
        super(itemViewBinding.getRoot());

        mItemViewBinding = itemViewBinding;
    }

}
