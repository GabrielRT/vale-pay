package digital.vee.valepay.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import digital.vee.valepay.R;
import digital.vee.valepay.model.RestaurantCategory;
import digital.vee.valepay.view.RestaurantCategoryItemView;
import digital.vee.valepay.view.RestaurantCategoryItemView_;

/**
 * Adapter that controls the list of categories of restaurants to be used in filter.
 *
 * @author Rafael O. Magalhaes
 * @since  31/01/2017
 */
@EBean
public class RestaurantFilterCategoriesAdapter extends BaseAdapter {

    @RootContext
    Context context;

    private final List<RestaurantCategory> categoriesList;

    public RestaurantFilterCategoriesAdapter() {
        categoriesList = new ArrayList<>();
    }

    @AfterInject
    void afterInject() {
        categoriesList.clear();
        String[] categoriesNames = context.getResources().getStringArray(R.array.restaurant_categories_names);
        String[] categoriesCodes = context.getResources().getStringArray(R.array.restaurant_categories_codes);

        if (categoriesNames.length == categoriesCodes.length) {
            for (int i = 0; i < categoriesNames.length; i++) {
                RestaurantCategory category = new RestaurantCategory();
                category.setName(categoriesNames[i]);
                category.setCode(categoriesCodes[i]);
                category.setSelected(false);
                categoriesList.add(category);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return categoriesList.size();
    }

    @Override
    public RestaurantCategory getItem(int i) {
        return categoriesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        RestaurantCategoryItemView categoryView;
        if (view == null) {
            categoryView = RestaurantCategoryItemView_.build(context);
        } else {
            categoryView = (RestaurantCategoryItemView) view;
        }

        categoryView.bind(getItem(i));

        return categoryView;
    }

    /**
     * @return List of {@link RestaurantCategory} selected in filter view
     */
    public List<RestaurantCategory> getSelectedCategories() {
        List<RestaurantCategory> selected = new ArrayList<>();

        for (RestaurantCategory category : categoriesList) {
            if (category.isSelected()) {
                selected.add(category);
            }
        }

        return selected;
    }

    /**
     * Update the categories that are previously selected by a list of code of categories separated
     * by coma, e. g. 'brazilian,japanese,french'
     * @param selected List of code of categories separated by coma ('brazilian,japanese,french')
     */
    public void setSelectedCategories(String selected) {
        if (selected != null && !selected.isEmpty()) {
            List<String> selectedCategories = Arrays.asList(selected.split("[,]"));

            for (RestaurantCategory category : categoriesList) {
                String[] codes = category.getCode().split("[,]");
                for (String code : codes) {
                    if (selectedCategories.contains(code)) {
                        category.setSelected(true);
                        break;
                    }
                }
            }
        }
    }

}
