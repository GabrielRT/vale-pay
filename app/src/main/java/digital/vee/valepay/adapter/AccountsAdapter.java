package digital.vee.valepay.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.view.AccountItemView;
import digital.vee.valepay.view.AccountItemView_;

/**
 * Adapter to control data of accounts activity.
 *
 * Created by rafael on 13/03/17.
 */
@EBean
public class AccountsAdapter extends BaseAdapter {

    @RootContext
    Context context;

    private final List<Wallet> walletList;
    private String name;

    public AccountsAdapter() {
        walletList = new ArrayList<>();
    }

    public void updateWalletList(List<Wallet> walletList, String name) {
        this.walletList.clear();
        this.walletList.addAll(walletList);
        this.name = name;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return walletList.size();
    }

    @Override
    public Wallet getItem(int i) {
        return walletList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        AccountItemView accountItemView;
        if (view == null) {
            accountItemView = AccountItemView_.build(context);
        } else {
            accountItemView = (AccountItemView) view;
        }

        accountItemView.bind(getItem(i), name);

        return accountItemView;
    }

}
