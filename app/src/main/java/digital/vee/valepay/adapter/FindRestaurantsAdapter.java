package digital.vee.valepay.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.Location;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import digital.vee.valepay.model.Restaurant;
import digital.vee.valepay.view.RestaurantItemView;
import digital.vee.valepay.view.RestaurantItemView_;

/**
 * Adapter to manage the list of restaurants to show.
 *
 * @author Rafael O. Magalhaes
 */
@EBean
public class FindRestaurantsAdapter extends BaseAdapter {

    @RootContext
    Context context;

    private final List<Restaurant> restaurantList;

    public FindRestaurantsAdapter() {
        restaurantList = new ArrayList<>();
    }

    /**
     * Process and update list of restaurants using the YELP and server responses.
     *
     * @param businesses List of restaurants found in YELP

     */
    public void proccessYelpResponse(ArrayList<Business> businesses) {
        restaurantList.clear();

        for (Business business : businesses) {

            Restaurant restaurant = new Restaurant();

            restaurant.setName(business.name());

            Location location = business.location();
            ArrayList<String> addresses = location.address();
            String address = "";
            if (addresses != null && !addresses.isEmpty()) {
                address = addresses.get(0);
                if (addresses.size() > 1) {
                    address += "\n" +addresses.get(1);
                }
            } else if (location.address1() != null) {
                address += location.address1();
            }
            restaurant.setAddress(address);

            restaurant.setImageUrl(business.imageUrl());
            restaurant.setRating(business.rating().floatValue());
            restaurant.setRatingImgUrl(business.ratingImgUrlLarge());

            if( location.coordinate() != null ) {
                String mapUrl = String.format(Locale.ENGLISH,
                        "geo:0,0?q=%f,%f(%s)",
                        location.coordinate().latitude(),
                        location.coordinate().longitude(),
                        business.name());
                restaurant.setMapUrl(mapUrl);
            } else {
                String mapUrl = String.format(Locale.ENGLISH,
                        "geo:0,0?q=%f,%f(%s)",
                        0.0,
                        0.0,
                        business.name());
                restaurant.setMapUrl(mapUrl);
            }

            restaurant.setPhoneNumber(business.displayPhone());
            restaurant.setPartnerCode(business.id());
            restaurant.setInvitedByUser( business.already_invited() );
            restaurant.setVeeRestaurant( business.accept_vee() );

            restaurantList.add(restaurant);
        }
        notifyDataSetChanged();
    }

    /**
     * Check if the restaurantID is on list of invited restaurants by the user.
     *
     * @param restaurantId Restaurant ID to check
     * @param invitedRestaurants List of restaurants invited by the user (response from server)
     * @return <code>true</code> if the restaurantId passed is on invited restaurants
     */
    private boolean isRestaurantInvited(String restaurantId, ArrayList<Restaurant> invitedRestaurants) {
        for (Restaurant restaurant:
             invitedRestaurants) {
            if (restaurantId != null && restaurantId.equalsIgnoreCase(restaurant.getPartnerCode())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getCount() {
        return restaurantList.size();
    }

    @Override
    public Restaurant getItem(int i) {
        return restaurantList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        RestaurantItemView restaurantItemView;
        if (view == null) {
            restaurantItemView = RestaurantItemView_.build(context);
        } else {
            restaurantItemView = (RestaurantItemView) view;
        }

        restaurantItemView.bind(getItem(i));

        return restaurantItemView;
    }

}
