package digital.vee.valepay.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import digital.vee.valepay.R;
import digital.vee.valepay.model.DrawerMenuItem;

/**
 *
 * Manage data shown at Navigation Drawer Menu
 *
 */
public class DrawerMenuAdapter extends ArrayAdapter<DrawerMenuItem> {

    private final Context context;
    private final List<DrawerMenuItem> drawerItemList;

    public static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    public static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    public static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";

    public DrawerMenuAdapter(Context context, List<DrawerMenuItem> listItems) {
        super(context, R.layout.drawer_item, listItems);
        this.context = context;
        this.drawerItemList = listItems;
    }

    // Return the id of that item
    public int getDrawerMenuItemId(int position){
        return drawerItemList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder(context);
            view = inflater.inflate(R.layout.drawer_item, parent, false);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        DrawerMenuItem item = this.drawerItemList.get(position);
        drawerHolder.bind(view, item);

        return view;
    }

    private static class DrawerItemHolder {
        private Context context;
        private TextView itemName;
        private ImageView icon;
        private TextView helpIcon;
        private TextView helpText;

        public DrawerItemHolder(Context context) {
            this.context = context;
        }

        public void bind(View view, DrawerMenuItem item) {

            // fonte roboto light com simbolo V (vee) como $ FIXME: alterar para outro caracter não utilizado
            Typeface type_roboto_light = Typeface.createFromAsset(context.getAssets(), FONTS_ROBOTO_LIGHT_TTF);
            // fonte roboto regular
            Typeface type_roboto_regular = Typeface.createFromAsset(context.getAssets(), FONTS_ROBOTO_REGULAR_TTF);

            icon = view.findViewById(R.id.icon);
            itemName = view.findViewById(R.id.title);
            helpIcon = view.findViewById(R.id.icon_help);
            helpText = view.findViewById(R.id.explain_text);

            icon.setImageDrawable(ContextCompat.getDrawable(context, item.getDrawableResourceId()));
            itemName.setText(item.getTitle());
            itemName.setTypeface(type_roboto_regular);
            helpText.setTypeface(type_roboto_light);
            helpText.setVisibility(View.GONE);

            if (item.getHelpText() == null) {
                helpIcon.setVisibility(View.GONE);
                helpIcon.setEnabled(false);
            }
            else {
                helpIcon.setVisibility(View.VISIBLE);
                helpIcon.setEnabled(item.isEnabled());
                helpText.setText(Html.fromHtml(item.getHelpText()));
                helpText.setMovementMethod(LinkMovementMethod.getInstance());
                helpIcon.setOnClickListener(helpIconView -> toggleText());
                helpText.setOnClickListener(helpTextView -> toggleText());
            }

            view.setEnabled(item.isEnabled());
            icon.setEnabled(item.isEnabled());
            itemName.setEnabled(item.isEnabled());
            helpText.setEnabled(item.isEnabled());
        }

        private void toggleText() {
            if (helpText.getVisibility() == View.VISIBLE) {
                helpText.setVisibility(View.GONE);
            }
            else {
                helpText.setVisibility(View.VISIBLE);
            }
        }
    }
}