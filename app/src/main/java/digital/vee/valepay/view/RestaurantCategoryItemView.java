package digital.vee.valepay.view;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import digital.vee.valepay.R;
import digital.vee.valepay.model.RestaurantCategory;

/**
 * Class to manage restaurant item view in restaurant list.
 *
 * @author Rafael O. Magalhaes
 * @since  31/01/2017
 */
@EViewGroup(R.layout.restaurant_category_item)
public class RestaurantCategoryItemView extends LinearLayout {

    @ViewById
    CheckBox categoryCheckBox;
    private RestaurantCategory category;

    public RestaurantCategoryItemView(Context context) {
        super(context);
    }

    /**
     * Update view using {@link RestaurantCategory}
     *
     * @param category {@link RestaurantCategory} used as model of view
     */
    public void bind(RestaurantCategory category) {
        this.category = category;
        categoryCheckBox.setText(category.getName());
        categoryCheckBox.setChecked(category.isSelected());
    }

    /**
     * Update model when check box is clicked.
     */
    @Click(R.id.categoryCheckBox)
    void categoryBtnClicked() {
        category.setSelected(categoryCheckBox.isChecked());
    }
}
