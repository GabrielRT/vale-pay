package digital.vee.valepay.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import digital.vee.valepay.R;
import digital.vee.valepay.activity.WalletActivity;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;

/**
 * View Holder of Account item.
 */
@EViewGroup(R.layout.account_item)
public class AccountItemView extends LinearLayout {

    @ViewById
    TextView corporateTV;

    @ViewById
    TextView lastUpdateTV;

    @ViewById
    TextView cardTypeTV;

    @ViewById
    TextView balanceTV;

    private Context context;
    private Wallet wallet;
    private String nameOnCard;

    public AccountItemView(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Update view with wallet and name to put on card
     * @param wallet Wallet data
     * @param nameOnCard Name to put on card
     */
    public void bind(Wallet wallet, String nameOnCard) {
        this.wallet = wallet;
        // to be used in action "View card"
        this.nameOnCard = nameOnCard;
        Typeface type_roboto_regular = Typeface.createFromAsset(context.getAssets(),
                LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        corporateTV.setText(wallet.getCompanyName());

        String formattedDtlastRecharged = wallet.getFormattedDtlastRecharged();

        if (formattedDtlastRecharged == null || formattedDtlastRecharged.isEmpty()) {
            lastUpdateTV.setText(R.string.no_charge);
        } else {
            lastUpdateTV.setText(String.format(Locale.getDefault(),
                    "%s %s", context.getString(R.string.last_charge), formattedDtlastRecharged));
        }
        cardTypeTV.setText(String.format("%s - Saldo: ", wallet.getWalletType().getDescription()));
        balanceTV.setTypeface(type_roboto_regular);
        balanceTV.setText(String.format(Locale.getDefault(), "R$ %s", wallet.getValueBalanceLocale()));
    }

    @Click({R.id.itemContainer})
    void showWallet() {
        Intent intentW = new Intent(context, WalletActivity.class);
        intentW.putExtra("wallet", wallet);
        intentW.putExtra("nameOnCard", nameOnCard);
        //TODO: passar wallet no intent
        ActivityUtils.newActivity(context, intentW);
    }

}

