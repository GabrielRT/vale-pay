package digital.vee.valepay.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import digital.vee.valepay.R;
import digital.vee.valepay.activity.FreeMealActivity;
import digital.vee.valepay.model.Restaurant;

/**
 * Class to manage restaurant item view in restaurant list.
 */
@EViewGroup(R.layout.find_restaurant_item)
public class RestaurantItemView extends LinearLayout {

    @ViewById
    RoundedImageView restaurantLogo;

    @ViewById
    TextView restaurantName;

    @ViewById
    TextView restaurantAddress;

    @ViewById
    TextView textInvite;

    @ViewById
    RatingBar ratingBar;

    @ViewById
    ImageView restaurantVee;


    private Uri mapUrl;
    private String restaurantPhone;
    private String partnerCode;
    private Context context;


    public RestaurantItemView(Context context) {
        super(context);
        this.context = context;
    }


    public void bind(Restaurant restaurant) {
        //add handling when image url is empty so th eapp doesn't crash
        if (restaurant.getImageUrl() != null && !restaurant.getImageUrl().isEmpty()) {
            Picasso.get().load(restaurant.getImageUrl()).into(restaurantLogo);
        }
        else {
            restaurantLogo.setImageDrawable( getResources().getDrawable(R.drawable.restaurant_placeholder) );
        }

        restaurantName.setText(restaurant.getName());
        restaurantAddress.setText(restaurant.getAddress());

        ratingBar.setRating(restaurant.getRating());

        /*
        if (restaurant.getRatingImgUrl() != null) {
            Picasso.with(this.context).load(restaurant.getRatingImgUrl()).into(restaurantRatingImg);
        }
        else {
            restaurantRatingImg.setImageDrawable(null);
        }
*/
        mapUrl = Uri.parse(restaurant.getMapUrl());
        restaurantPhone = restaurant.getPhoneNumber();

        if (restaurant.isVeeRestaurant()) {
            restaurantVee.setVisibility(VISIBLE);
        }
        else {
            restaurantVee.setVisibility(GONE);
        }

        if (restaurant.isInvitedByUser()) {
            textInvite.setVisibility(GONE);
        }
        else {
            textInvite.setVisibility(VISIBLE);
        }

        partnerCode = restaurant.getPartnerCode();

    }

    @Click(R.id.restaurantMap)
    void restaurantMapButtonClicked() {
        // open mapUrl
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(mapUrl);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
        //opçao com url marcando o destino pra traçar rota, não é a recomendação da google
//        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, "Where the party is at");
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        // opcao abaixo pra forcar abrir o google maps
//        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//        startActivity(intent);
    }

    @Click(R.id.text_invite)
    void inviteToVeeClicked() {
        Intent intent = new Intent(context, FreeMealActivity.class);
        intent.putExtra(FreeMealActivity.RESTAURANT_PHONE_EXTRA, restaurantPhone);
        intent.putExtra(FreeMealActivity.RESTAURANT_NAME_EXTRA, restaurantName.getText().toString());
        intent.putExtra(FreeMealActivity.RESTAURANT_CODE_EXTRA, partnerCode);
        context.startActivity(intent);
    }

}
