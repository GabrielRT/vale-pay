package digital.vee.valepay.helper;

import android.util.Patterns;

/**
 * Created by rafael on 19/01/17.
 */

public class LoginUtils {

    /**
     * Validate if login is a valid CPF pattern.
     *
     * @param loginCPF Login to be validated
     * @return <code>true</code> if login is a valid CPF, <code>false</code> otherwise
     */
    public static boolean isValidCPF(String loginCPF) {
        // remove all . - and spaces of login
        loginCPF = loginCPF.replaceAll("[ .-]", "");

        // check basics invalid CPFs
        if (loginCPF.equals("00000000000") || loginCPF.equals("11111111111") ||
                loginCPF.equals("22222222222") || loginCPF.equals("33333333333") ||
                loginCPF.equals("44444444444") || loginCPF.equals("55555555555") ||
                loginCPF.equals("66666666666") || loginCPF.equals("77777777777") ||
                loginCPF.equals("88888888888") || loginCPF.equals("99999999999") ||
                (loginCPF.length() != 11))
            return false;

        char dig10, dig11;
        int sm, i, r, num, peso;

        // Calculate 1st verifier digit
        sm = 0;
        peso = 10;
        for (i=0; i<9; i++) {
            // convert char to numeric
            num = (loginCPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }

        r = 11 - (sm % 11);
        if ((r == 10) || (r == 11)) {
            dig10 = '0';
        }
        else {
            // convert char to numeric
            dig10 = (char)(r + 48);
        }

        // Calculate 2nd verifier digit
        sm = 0;
        peso = 11;
        for (i = 0; i < 10; i++) {
            num = (loginCPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }

        r = 11 - (sm % 11);
        if ((r == 10) || (r == 11)) {
            dig11 = '0';
        }
        else {
            dig11 = (char)(r + 48);
        }

        // Verify if all digits calculated matches digits of login
        if ((dig10 == loginCPF.charAt(9)) && (dig11 == loginCPF.charAt(10))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Check if login is a valid email pattern
     * @param loginEmail Login to be validated
     * @return <code>true</code> if login is a valid email, <code>false</code> otherwise
     */
    public static boolean isValidEmail(CharSequence loginEmail) {
        return loginEmail != null && Patterns.EMAIL_ADDRESS.matcher(loginEmail).matches();

    }

}
