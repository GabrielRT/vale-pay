package digital.vee.valepay.helper;

/**
 * Created by Sérgio Brocchetto on 04/11/2016.
 */

public interface VolleyResponseListener {
    void onError(String message);

    void onResponse(Object response);
}
