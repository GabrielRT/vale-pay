package digital.vee.valepay.helper;


import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Verdant Clock Web on 29-Apr-16.
 */
public class VolleyUtils {

    public static final String PT_BR = "pt_br";
    public static final String EN = "en";
    private static final String TAG = "VolleyUtils";
    private static ProgressDialog progressDialog;
    private boolean auth;
    private final Context context;

    public VolleyUtils(Context context) {
        this.context = context;
    }

    public static ProgressDialog getProgressDialog() {
        return progressDialog;
    }
/*------------------------------------------------------*
*             Server request Functions                  *
*-------------------------------------------------------*/

    public void serverPostRequest(String URL, JSONObject params, String language,
                                  String token,
                                  String progressMessage,
                                  ServerCallback callback, boolean auth) {
        this.auth = auth;
        serverPostRequest(URL, params, language, token, progressMessage, callback);
    }

    //---- Volley Custom Request Methods for all POST requests
    private void serverPostRequest(String URL, final JSONObject params, final String language,
                                   final String token,
                                   final String progressMessage,
                                   final ServerCallback callback) {
        //----Prepare Progress Dialog Here
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(progressMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        //--- Show Prepared Progress dialog
        if (!progressMessage.equals(""))
            progressDialog.show();

        //--- Creating Volley Request to process URL request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //--- Call Back on Response of data
                        try {
                            callback.onSuccess(response);
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        if (!progressMessage.equals(""))
                            progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //--- Call Back on Error of data
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            callback.onError(error);
                        } else {
                            callback.onError(error);
                        }

                        if (!progressMessage.equals(""))
                            progressDialog.dismiss();
                    }
                }) {
            /*@Override
            protected Map<String, String> getParams() {
                return params.toString();
            }*/
            @Override
            public byte[] getBody() {
                try {
                    return params == null ? null : params.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    //Volleyandroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s",
                    //params, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", language);
                if (token != null) {
                    params.put("Authorization", "Token " + token);
                }
                return params;
            }

            @Override
            public String getBodyContentType() {
                if (auth) {
                    return "application/x-authc-username-password+json";
                } else {
                    return "application/json";
                }
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //--- Creating Volley Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        //--- Adding request in Queue
        requestQueue.add(stringRequest);
    }

    //---- Volley Custom Request Methods for all GET requests
    public void serverGetRequest(String URL, final JSONObject params, final String language,
                                 final String token,
                                 String progressMessage,
                                 final ServerCallback callback) {
        //----Prepare Progress Dialog Here
/*        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(progressMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        //--- Show Prepared Progress dialog
        if (!progressMessage.equals(""))
            progressDialog.show();*/

        //--- Creating Volley Request to process URL request
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        //--- Call Back on Response of data
                        try {
                            callback.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            throw new RuntimeException(e);
                        }
/*                        if (!progressMessage.equals(""))
                            progressDialog.dismiss();*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //--- Call Back on Error of data
                        callback.onError(error);
/*                        if (!progressMessage.equals(""))
                            progressDialog.dismiss();*/
                    }
                }) {
            @Override
            public byte[] getBody() {
                try {
                    return params == null ? null : params.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    //Volleyandroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s",
                    //params, "utf-8");
                    return null;
                }
            }
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", language);
                if (token != null) {
                    params.put("Authorization", "Token " + token);
                }
                return params;
            }
            @Override
            public String getBodyContentType() {
                if (auth) {
                    return "application/x-authc-username-password+json";
                } else {
                    return "application/json";
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //--- Creating Volley Request Queue
        //RequestQueue requestQueue = Volley.newRequestQueue(context);
        //--- Adding request in Queue
        //requestQueue.add(stringRequest);
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    //--- Server Callback interface for responses of Volley
    public interface ServerCallback {
        void onSuccess(String response) throws JSONException;

        void onError(VolleyError error);
    }

    /**
     *
     */
    /*public static void makeJsonObjectRequest(Context context, String url, JSONObject map, String locale,
                                             String s, boolean b,
                                             VolleyResponseListener listener1) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                    return Response.success(new JSONObject(jsonString),
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }
        };

        // Access the RequestQueue through singleton class.
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }*/
}