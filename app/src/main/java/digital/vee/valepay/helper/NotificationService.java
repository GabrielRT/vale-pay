package digital.vee.valepay.helper;

import android.content.Intent;
import android.support.annotation.MainThread;
import android.util.Base64;
import android.util.Log;

import com.pushwoosh.notification.NotificationServiceExtension;
import com.pushwoosh.notification.PushMessage;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import digital.vee.valepay.activity.MainActivity;
import digital.vee.valepay.activity.TransactionsListActivity;
import digital.vee.valepay.util.Preferences;

public class NotificationService extends NotificationServiceExtension {
    private static final int PUSH_VOUCHER = 1;
    private static final int PUSH_PAYMENT = 2;
    private static final int PUSH_REFUND = 3;
    private static final int PUSH_PROCESSED = 4;
    private static final int PUSH_REJECTED = 5;

    @Override
    public boolean onMessageReceived(final PushMessage message) {
        Log.d("---NotificationService", "onMessageReceived: " + message.toJson().toString());

        JSONObject messageJson;
        if (!message.toJson().toString().isEmpty()) {
            try {
                messageJson = message.toJson();
                JSONObject userdata = new JSONObject(messageJson.getString("userdata"));
                //0 notification, 1 voucher, 2 payment
                int type = userdata.getInt("type");
                String base64 = new String(decoded64(userdata.getString("data")));
                JSONObject base64Json = new JSONObject(base64);
                Log.d("---NotificationService", "type: " + type);
                if (type == PUSH_PAYMENT) {
                    return processPaymentReceived(userdata);

                }
                else if (type == PUSH_REFUND) {
                    processRefundReceived(base64Json);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
        Log.d("---NotificationService", "indicates that notification should be displayed");
        return false;
    }

    /**
     * Process push of payment request message received.
     *
     * @param userdata the data of push message
     * @return indicates if the push will be hidden
     * @throws JSONException
     * @throws SnappydbException
     */
    private boolean processPaymentReceived(JSONObject userdata) throws JSONException, SnappydbException {
        //get payload by 'u' key
        String transaction = userdata.getString("data");
        byte[] decode = decoded64(transaction);


        String qrCode = new String(decode);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody = new JSONObject(qrCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //get payment token
        String paymentToken = null;
        try {
            paymentToken = jsonBody.getString("paymentToken");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if token is the same being shown on GenerateToken Activity and app is in background, so show push
        Log.d("---1 NS paymentToken ", paymentToken + " " + SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken"));

        //indicates if the push will be hidden
        boolean showPaymentToken = (isAppOnBackground() &&
                paymentToken != null &&
                paymentToken.equals(SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken")));
        if (showPaymentToken) {
            // app is in background and will set flag to be read on SplashActivity so it WONT call Main again
            Preferences.init(getApplicationContext());
            Preferences.putBoolean("isPush", true);
        }
        return !showPaymentToken;
    }

    /**
     * Process push of refund message received.
     *
     * @param refundPayload the json of push payload
     * @throws JSONException
     * @throws SnappydbException
     */
    private void processRefundReceived(JSONObject refundPayload) throws JSONException, SnappydbException {
        Preferences.init(getApplicationContext());
        Preferences.putBoolean(MainActivity.REFUND_CONFIRM, true);
        SnappyDBHelper.putRefundPayload(getApplicationContext(), refundPayload);
    }

    private boolean isAppOnBackground() {
        return !isAppOnForeground();
    }

    @Override
    protected void startActivityForPushMessage(PushMessage message) {
        super.startActivityForPushMessage(message);
        //Log.d("PushwooshNotification", "NotificationService.startActivityForPushMessage: " + message.toJson().toString());

        handlePush(message);
    }

    @MainThread
    private void handlePush(PushMessage message) {
        Log.d("PushwooshNotification", "NotificationService.handlePush: " + message.toJson().toString());

        JSONObject messageJson;

        if (!message.toJson().toString().isEmpty()) {
            try {
                messageJson = message.toJson();
                JSONObject userdata = new JSONObject(messageJson.getString("userdata"));
                //0 notification, 1 voucher, 2 payment
                int type = userdata.getInt("type");

                // 1 voucher, 2 transaction, 3 refund, 4 & 5 refund notifications
                switch (type) {
                    case PUSH_VOUCHER:
                        redirVoucher();
                        break;
                    case PUSH_PAYMENT:
                        redirTransaction(userdata);
                        break;
                    case PUSH_REFUND:
                        redirRefundConfirmation();
                        break;
                    case PUSH_PROCESSED:
                        redirRefundTransactionList(messageJson);
                        break;
                    case PUSH_REJECTED:
                        redirRefundTransactionList(messageJson);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Whenever the push is a notification of success or rejection, user is sent to the transaction
     * list.
     */
    private void redirRefundTransactionList(JSONObject jsonObject) throws JSONException {
        Preferences.init(getApplicationContext());
        Preferences.putBoolean("isPush", true);
        Intent intent = new Intent(getApplicationContext(), TransactionsListActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (jsonObject.has("walletFrom")){
            intent.putExtra("walletFrom", jsonObject.getString("walletFrom"));
        }
        getApplicationContext().startActivity(intent);

    }

    /**
     * User is sent to the main activity. The preferences will already be true for refund at this point.
     * In the main activity the user will be redirected to the Confirm Refund Activity.
     */
    private void redirRefundConfirmation() {
        Preferences.putBoolean("isPush", true);

        getApplicationContext().startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    /**
     * User is sent to Main activity with a flag for voucher, so it's redirected to VoucherActivity
     */
    private void redirVoucher() {
        try {
            SnappyDBHelper.put(getApplicationContext(), "voucher", "true");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        getApplicationContext().startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .putExtra("voucher", true)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    /**
     * User redirection to PIN for transactions activity for whenever there's a token transaction.
     *
     * @param userdata Push payload
     * @throws JSONException
     * @throws SnappydbException
     */
    private void redirTransaction(JSONObject userdata) throws JSONException, SnappydbException {
        String transaction;

        //get payload by 'u' key
        transaction = userdata.getString("data");

        //decode from base 64
        byte[] decode = new byte[0];
        try {
            decode = Base64.decode(transaction, Base64.NO_WRAP);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        String qrCode = new String(decode);
        JSONObject jsonBody = new JSONObject(qrCode);

        //get payment token
        String paymentToken;

        paymentToken = jsonBody.getString("paymentToken");


        Log.d("---NS paymentToken ", paymentToken + " " + SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken"));
        if (paymentToken != null &&
                paymentToken.equals(SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken"))) {

            //save
            SnappyDBHelper.getDatabase(getApplicationContext());
            SnappyDBHelper.put(getApplicationContext(), "transaction", transaction);
        }

        getApplicationContext().startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        // Send broadcast to close the generate token activity
        getApplicationContext().sendBroadcast(new Intent("CloseActivity"));
    }

    private byte[] decoded64(String transaction) {
        //decode from base 64
        byte[] decode = new byte[0];
        try {
            decode = Base64.decode(transaction, Base64.NO_WRAP);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return decode;
    }
}
