package digital.vee.valepay.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.widget.ImageView;

import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import digital.vee.valepay.R;
import digital.vee.valepay.activity.BaseActivity;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.util.LoginUtil;

/**
 * Created by rafael on 13/03/17.
 */
public class BitmapHelper {

    private static final String TAG = "BitmapHelper";
//    private static final float BITMAP_WIDTH = 1308f;
    private static final float CARD_NUMBER_TEXT_SIZE = 84f;
    private static final float CARD_TEXT_SIZE = 36f;
    private static final float WARNING_TEXT_SIZE = 40f;

    private static BitmapHelper instance;
    private Context context;
    private final Typeface cardNumberFont;
    private final Typeface cardTextFont;
    private final HashMap<String, BitmapDrawable> cardFrontCache;
    private final HashMap<String, BitmapDrawable> cardBackCache;

    private BitmapHelper(Context context) {
        this.context = context;
        this.cardNumberFont = Typeface.createFromAsset(context.getAssets(), "fonts/afbattersea.ttf");
        this.cardTextFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        this.cardFrontCache = new HashMap<>();
        this.cardBackCache = new HashMap<>();
    }

    public static BitmapHelper getInstance(Context context) {
        if (instance == null) {
            instance = new BitmapHelper(context);
        }
        return instance;
    }

    private void setCardImage(ImageView imageView) {
        String card;
        int walletArraySize;
        try {
            card = SnappyDBHelper.get(instance.context, LoginUtil.WALLET_ID);
            JSONObject jsonObject = new JSONObject(SnappyDBHelper.get(instance.context, "CARDS"));

            JSONArray walletArray = jsonObject.getJSONArray("walletList");
            walletArraySize = walletArray.length();

            Wallet e;

            if(!card.isEmpty()){
                for(int i = 0; i < walletArraySize ; i++){
                    e = Wallet.fromJson(walletArray.getJSONObject(i)); // get the wallet from the wallet array
                    // if the identifier of the card currently selected matches the on we got from
                    // wallet array
                    if(card.equalsIgnoreCase(e.getIdentifier())){
                        switch (e.getWalletType().getCode()){
                            case "v1":
                                //refeição
                                break;
                            case "v2":
                                // alimentação
                                break;
                            case "v3":
                                //combustível
                                break;
                            case "v4":
                                // cultura
                                break;
                            case "v5":
                                //multicard
                                break;
                        }
                    }
                }
            }
        } catch (SnappydbException | JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a drawable of front face of the card using wallet data.
     * @param wallet Wallet to fill card data
     * @param nameOnCards Name to show on card
     * @return Drawable of back face of the card
     */
    public BitmapDrawable getCardDrawableFront(Wallet wallet, String nameOnCards) {
        Bitmap card;
        if (!this.cardFrontCache.containsKey(wallet.getIdWallet())) {
            //Log.d(TAG, "Creating drawable for FRONT card: " + wallet.getIdentifier());
            switch (wallet.getWalletType().getCode()) {
                case "v1": {
                    //# Create bitmap of card
                    card = getBitmapFront(
                            wallet, R.drawable.card_v1,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.card_font_purple),
                            nameOnCards);
                    break;
                }
                case "v2": {
                    //# Create bitmap of card
                    card = getBitmapFront(
                            wallet, R.drawable.card_v2,
                            ContextCompat.getColor(context, R.color.card_font_orange),
                            ContextCompat.getColor(context, R.color.white),
                            nameOnCards);
                    break;
                }
                case "v3":
                    //# Create bitmap of card
                    card = getBitmapFront(
                            wallet, R.drawable.card_v3,
                            ContextCompat.getColor(context, R.color.white),
                            ContextCompat.getColor(context, R.color.white),
                            nameOnCards);
                    break;
                case "v4":
                    //# Create bitmap of card
                    card = getBitmapFront(
                            wallet, R.drawable.card_v4,
                            ContextCompat.getColor(context, R.color.white),
                            ContextCompat.getColor(context, R.color.orange),
                            nameOnCards);
                    break;
                //TODO: CHANGE TO V5?
                default:
                    //# Create bitmap of card
                    card = getBitmapFront(
                            wallet, R.drawable.card_v5,
                            ContextCompat.getColor(context, R.color.white),
                            ContextCompat.getColor(context, R.color.white),
                            nameOnCards);
                    break;
            }
            //# Convert Bitmap to Drawable
            this.cardFrontCache.put(wallet.getIdWallet(), new BitmapDrawable(context.getResources(), card));
        }

        return this.cardFrontCache.get(wallet.getIdWallet());
    }

    @NonNull
    private Bitmap getBitmapFront(Wallet wallet, int resource, int colorNumber, int colorText,
                                  String nameOnCards) {
        // bitmap of background
        Bitmap bitmapResource = BitmapFactory.decodeResource(context.getResources(), resource);

        // new bitmap - use fixed aspect ratio to prevent differences between devices resolutions
//        float reason = 1f;
        Bitmap bitmap = Bitmap.createBitmap(bitmapResource.getWidth(), bitmapResource.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        // paint for background and ohter images
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        // paint for texts
        TextPaint textPaint = new TextPaint();
        textPaint.setAntiAlias(true);

//        reason = bitmapResource.getWidth() / BITMAP_WIDTH;

        //Log.d(TAG, "resource size (h,w): " + bitmapResource.getHeight() + ", " + bitmapResource.getWidth());
        //Log.d(TAG, "canvas size (h,w): " + canvas.getHeight() + ", " + canvas.getWidth());// + " | r = " + reason);

        // print background filling bitmap space
        canvas.drawBitmap(bitmapResource,
                new Rect(0, 0, bitmapResource.getWidth(), bitmapResource.getHeight()),
                new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), paint);
        bitmapResource.recycle();

        // print card number
        textPaint.setTypeface(this.cardNumberFont);
        textPaint.setTextSize(CARD_NUMBER_TEXT_SIZE);// * reason);
        textPaint.setColor(colorNumber);

        float yPos = canvas.getHeight()*0.46f + textPaint.getFontSpacing();
        float xPos = 0.09f*canvas.getWidth();

        canvas.drawText(wallet.getIdentifierCardNumberFormat().replaceAll("[-]", " "),
                xPos, yPos, textPaint);

        // print name of employee
        textPaint.setTypeface(this.cardTextFont);
        textPaint.setTextSize(CARD_TEXT_SIZE);// * reason);
        textPaint.setColor(colorText);
        yPos = canvas.getHeight()*0.75f;
        canvas.drawText(nameOnCards.toUpperCase(), xPos, yPos, textPaint);

        // print corporate of employee
        yPos += textPaint.getFontSpacing()*1.1f;
        canvas.drawText(wallet.getCompanyName().toUpperCase(), xPos, yPos, textPaint);

        // print expiration date of the card
        yPos += textPaint.getFontSpacing()*1.1f;
        canvas.drawText(wallet.getFormattedDtExpiration().toUpperCase(), xPos, yPos, textPaint);

        canvas.save();

        return bitmap;
    }

    /**
     * Create a drawable of back face of the card using wallet data.
     * @param wallet Wallet to fill card data
     * @return Drawable of back face of the card
     */
    public BitmapDrawable getCardDrawableBack(Wallet wallet) {
        Bitmap card;
        if (!this.cardBackCache.containsKey(wallet.getIdWallet())) {
            //Log.d(TAG, "Creating drawable for BACK card: " + wallet.getIdentifier());
            switch (wallet.getWalletType().getCode()) {
                case "v1": {
                    //# Create bitmap of card
                    card = getBitmapBack(
                            wallet, R.drawable.card_back_purple,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.black),
                            context.getString(R.string.card_v1_waring_txt));
                    break;
                }
                case "v2": {
                    //# Create bitmap of card
                    card = getBitmapBack(
                            wallet, R.drawable.card_back_yellow,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.white),
                            context.getString(R.string.card_v2_waring_txt));
                    break;
                }
                case "v3":
                    //# Create bitmap of card
                    card = getBitmapBack(
                            wallet, R.drawable.card_back_purple,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.black),
                            context.getString(R.string.card_v3_waring_txt));
                    break;
                case "v4":
                    //# Create bitmap of card
                    card = getBitmapBack(
                            wallet, R.drawable.card_back_yellow,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.white),
                            context.getString(R.string.card_v4_waring_txt));
                    break;
                default:
                    //# Create bitmap of card
                    card = getBitmapBack(
                            wallet, R.drawable.card_back_yellow,
                            ContextCompat.getColor(context, R.color.black),
                            ContextCompat.getColor(context, R.color.white),
                            context.getString(R.string.card_v5_waring_txt));
                    break;
            }
            //# Convert Bitmap to Drawable
            this.cardBackCache.put(wallet.getIdWallet(), new BitmapDrawable(context.getResources(), card));
        }

        return this.cardBackCache.get(wallet.getIdWallet());
    }

    @NonNull
    private Bitmap getBitmapBack(Wallet wallet, int resource, int colorNumber, int colorWarning,
                                 String warningTxt) {
        // bitmap of background
        Bitmap bitmapResource = BitmapFactory.decodeResource(context.getResources(), resource);
        // new bitmap - use fixed aspect ratio to prevent differences between devices resolutions
        Bitmap bitmap = Bitmap.createBitmap(bitmapResource.getWidth(), bitmapResource.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        // paint for background and ohter images
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        // paint for texts
        TextPaint textPaint = new TextPaint();
        textPaint.setAntiAlias(true);

        //Log.d(TAG, "canvas back size (h,w): " + canvas.getHeight() + ", " + canvas.getWidth());

        // print background filling bitmap space
        canvas.drawBitmap(bitmapResource,
                new Rect(0, 0, bitmapResource.getWidth(), bitmapResource.getHeight()),
                new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), paint);
        bitmapResource.recycle();

        // print card number
        textPaint.setTypeface(this.cardNumberFont);
        textPaint.setTextSize(CARD_NUMBER_TEXT_SIZE);
        textPaint.setColor(colorNumber);
        textPaint.setTextAlign(Paint.Align.CENTER);

        float yPos = 0.32f * canvas.getHeight();
        float xPos = 0.5f * canvas.getWidth();

        canvas.drawText(wallet.getIdentifierCardNumberFormat().replaceAll("[-]", " "),
                xPos, yPos, textPaint);

        // print name of employee
        yPos = 0.48f * canvas.getHeight();
        textPaint.setTypeface(this.cardTextFont);
        textPaint.setTextSize(WARNING_TEXT_SIZE);
        textPaint.setColor(colorWarning);

        //FIXME: depois que o servidor "responder corretamente, usar o campo do waring text
        canvas.drawText(warningTxt, xPos, yPos, textPaint);

        canvas.save();

        return bitmap;
    }

}
