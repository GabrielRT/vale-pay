package digital.vee.valepay.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Sérgio Brocchetto on 04/11/2016.
 */

public class VolleySingleton {

    // Request patterns used by Vee
    public static final int REST_DEFAULT_TIMEOUT = 10000;
    public static final int REST_LONG_TIMEOUT = 30000;
    public static final int REST_NO_RETRY = -1;

    // Singleton object...
    private static VolleySingleton instance;
    private static ImageLoader imageLoader;
    private final RequestQueue requestQueue;

    //Constructor...
    private VolleySingleton(Context context) {

        requestQueue = Volley.newRequestQueue(context);

        imageLoader = new ImageLoader(requestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<>(100000000);


            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    // Singleton method...
    public static VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    public static ImageLoader getImageLoader(Context context) {
        if (imageLoader != null) {
            return imageLoader;
        } else {
            getInstance(context);
            return imageLoader;
        }
    }

    public RequestQueue getRequestQueue(Context context) {
        if (requestQueue != null) {
            return requestQueue;
        } else {
            getInstance(context);
            return requestQueue;
        }
    }

    private RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag("App");
        getRequestQueue().add(req);
    }

    /**
     * The default request time is 10 seconds, no retry and 1 as multiplier
     *
     * @return Retry policy with the configuration above
     */
    public static DefaultRetryPolicy noRetryDefaultTimeRequest() {
        return new DefaultRetryPolicy(VolleySingleton.REST_DEFAULT_TIMEOUT,
                VolleySingleton.REST_NO_RETRY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    /**
     * The Long request time is 30 seconds, no retry and 1 as multiplier, used to big files like receipt image
     *
     * @return Retry policy with the configuration above
     */
    public static DefaultRetryPolicy noRetryLongTimeRequest() {
        return new DefaultRetryPolicy(VolleySingleton.REST_LONG_TIMEOUT,
                VolleySingleton.REST_NO_RETRY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

}