package digital.vee.valepay.helper;

import android.content.Context;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStoreException;

import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Created by Sérgio Brocchetto on 18/05/2017.
 *
 * Manage local SnappyDB database
 *
 */

public class SnappyDBHelper {

    private static DB snappydb;
    private static CryptoUtil cryptoUtil;

    public static DB getDatabase(Context context) {
        try {
            if (snappydb == null || !snappydb.isOpen()) {
                try {
                    snappydb = DBFactory.open(context);
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        if (cryptoUtil == null) {
            try {
                cryptoUtil = new CryptoUtil(context);
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        }
        return snappydb;
    }

    public static void del(Context context, String key) throws SnappydbException{
        getDatabase(context).del(key);
    }

    public static void putEncrypted(Context context, String key, String value) throws SnappydbException {
        String encrypted = cryptoUtil.encrypt(value);
        getDatabase(context).put(key, encrypted);
    }

    public static String getEncrypted(Context context, String key) throws SnappydbException {
        if (getDatabase(context).exists(key)) {
            return cryptoUtil.decrypt(getDatabase(context).get(key));
        }
        return "";
    }

    public static void put(Context context, String key, String value) throws SnappydbException {
        getDatabase(context).put(key, value);
    }

    public static void put(Context context, String key, Long value) throws SnappydbException {
        getDatabase(context).putLong(key, value);
    }

    /**
     * Put all data related to a refund payload on SnappyDB (receipt, refundUUID and walletFrom)
     *
     * @param context the context
     * @param refundPayload the refund payload to save on snappy
     * @throws JSONException
     * @throws SnappydbException
     */
    public static void putRefundPayload(Context context, JSONObject refundPayload) throws JSONException, SnappydbException {
        String refundUuid = refundPayload.getString("refundUUID");
        String receiptPayload = refundPayload.getString("refundReceiptPayload");

        put(context, "receipt", receiptPayload);
        put(context, "refundUUID", refundUuid);

        if (refundPayload.has("walletFrom")) {
            put(context, LoginUtil.WALLET_ID,
                    refundPayload.getString("walletFrom"));
        }
    }

    public static String get(Context context, String key) throws SnappydbException {
        if (getDatabase(context).exists(key)) {
            return getDatabase(context).get(key);
        }
        return "";
    }

    public static Long getLong(Context context, String key) throws SnappydbException {
        if (getDatabase(context).exists(key)) {
            return getDatabase(context).getLong(key);
        }
        return 0l;
    }

    public static boolean exists(Context context, String key) throws SnappydbException {
        boolean exists;
        exists = getDatabase(context).exists(key);
        return exists;
    }

    public static void destroy(Context context) {
        // Clear SnappyDB
        try {
            getDatabase(context).destroy();
            getDatabase(context).close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public static void close(Context context) {
        try {
            getDatabase(context).close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

}