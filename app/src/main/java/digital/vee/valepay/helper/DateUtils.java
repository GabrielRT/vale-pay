package digital.vee.valepay.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class to manage dates received from server to show to user.
 *
 * @author Rafael O. Magalhães
 */
public class DateUtils {

    public static final String VEE_DATE_FORMAT = "dd/MM/yyyy";
    public static final String VEE_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String UTC_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    /**
     * Read a String in UTC date and format it to VEE_DATE_FORMAT
     *
     * @param utcDate String of date in UTC_DATE_TIME_FORMAT
     * @return String in VEE_DATE_FORMAT or null in case of invalid utcDate
     */
    public static String getDateFromUTC(String utcDate) {

        SimpleDateFormat utcFormatter = new SimpleDateFormat(UTC_DATE_TIME_FORMAT);
        utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        if (utcFormatter != null) {
            try {
                return getDateFromUTC(utcFormatter.parse(utcDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Transform a date in format VEE_DATE_FORMAT
     *
     * @param utcDate Date in UTC_DATE_TIME_FORMAT
     * @return String in VEE_DATE_FORMAT or null in case of invalid utcDate
     */
    private static String getDateFromUTC(Date utcDate) {
        SimpleDateFormat formatter = new SimpleDateFormat(VEE_DATE_FORMAT);
        return formatter.format(utcDate);
    }

    /**
     * Read a String in UTC datetime and format it to VEE_DATE_TIME_FORMAT
     *
     * @param utcDateTime String of date in UTC_DATE_TIME_FORMAT
     * @return String in VEE_DATE_TIME_FORMAT or null in case of invalid utcDate
     */
    public static String getDateTimeFromUTC(String utcDateTime) {

        SimpleDateFormat utcFormatter = new SimpleDateFormat(UTC_DATE_TIME_FORMAT);
        utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return getDateTimeFromUTC(utcFormatter.parse(utcDateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Transform a date in format VEE_DATE_TIME_FORMAT
     *
     * @param utcDateTime Date in UTC_DATE_TIME_FORMAT
     * @return String in VEE_DATE_TIME_FORMAT or null in case of invalid utcDate
     */
    private static String getDateTimeFromUTC(Date utcDateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(VEE_DATE_TIME_FORMAT);
        return formatter.format(utcDateTime);
    }

}
