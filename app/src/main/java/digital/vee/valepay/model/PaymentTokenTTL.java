package digital.vee.valepay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentTokenTTL {

    @SerializedName("paymentToken")
    @Expose
    private String paymentToken;
    @SerializedName("ttl")
    @Expose
    private String ttl;

    public String getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }
}