package digital.vee.valepay.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/*
 * Created by Sérgio Brocchetto on 29/04/2017.
 */

public class VoucherResponse implements Serializable {
    private String voucherId;
    private String description;
    private String amountspentFromWallet;
    private String amountSpentFromVoucher;

    // Decode voucher response json into voucher response model object
    public static VoucherResponse fromJson(JSONObject jsonObject) {
        VoucherResponse e = new VoucherResponse();
        // Deserialize json into object fields
        try {
            e.voucherId = jsonObject.getString("voucherId");
            e.description = jsonObject.getString("description");
            e.description = jsonObject.getString("description");
            e.amountspentFromWallet = jsonObject.getString("amountspentFromWallet");
            e.amountSpentFromVoucher = jsonObject.getString("amountSpentFromVoucher");

        } catch (JSONException error) {
            error.printStackTrace();
            return null;
        }
        // Return new object
        return e;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}