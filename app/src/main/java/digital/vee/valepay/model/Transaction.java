package digital.vee.valepay.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Class that represent a Transaction model
 *
 * Created by Sérgio Brocchetto on 30/11/2016.
 */
public class Transaction {

    // Possible values for transaction status
    public static final String NEW = "NEW";
    public static final String PROCESSING = "PROCESSING";
    public static final String PROCESSED = "PROCESSED";
    public static final String CONFIRMED = "CONFIRMED";
    public static final String CANCELED = "CANCELED";
    public static final String REJECTED = "REJECTED";
    public static final String UNDONE = "UNDONE";

    public String getMovementCode() {
        return movementCode;
    }

    public void setMovementCode(String movementCode) {
        this.movementCode = movementCode;
    }

    // Constructor to convert JSON object into a Java class instance
    public String movementCode;
    public String transactionUUID;
    public String dtStarted;
    public String dtConfirmed;
    public String dtCanceled;
    public String dtExpiration;
    public String walletIdentifierTo;
    public String debitOrCredit;
    public String amount;
    public String rejectReason;
    public String dtLastRecharge;
    public String entityNameTo;
    public String entityNameFrom;
    public boolean usedVoucher;
    public boolean isVoucher;
    public String description;
    public String typeDescription;
    public String dtUndone;
    public String status;

    public String movementType;

    public VoucherResponse voucherResponse;

    private final static String TAG = "Transaction";

    private Transaction(JSONObject object) {
        try {
            this.transactionUUID = object.getString("transactionUUID");
            this.dtStarted = object.getString("dtStarted");
            this.dtConfirmed = object.getString("dtConfirmed");
            this.dtCanceled = object.getString("dtCanceled");
            this.dtExpiration = object.getString("dtExpiration");
            this.walletIdentifierTo = object.getString("walletIdentifierTo");
            this.debitOrCredit = object.getString("debitOrCredit");
            this.amount = object.getString("amount");
            this.rejectReason = object.getString("rejectReason");

            if (object.has("movementType")) {
                this.movementType = object.getString("movementType");
            } else {
                this.movementType = "null";
            }

            if (object.has("movementCode")) {
                this.movementCode = object.getString("movementCode");
            } else {
                this.movementCode = "null";
            }

            if (object.has("dtLastRecharge")) {
                this.dtLastRecharge = object.getString("dtLastRecharge");
            } else {
                this.dtLastRecharge = "";
            }
            if (object.has("entityNameTo")) {
                this.entityNameTo = object.getString("entityNameTo");
            } else {
                this.entityNameTo = "";
            }
            if (object.has("entityNameFrom")) {
                this.entityNameFrom = object.getString("entityNameFrom");
            } else {
                this.entityNameFrom = "";
            }
            if(object.has("movementTypeDescription")){
                this.typeDescription = object.getString("movementTypeDescription");
            } else {
                this.typeDescription = "";
            }
            if (object.has("dtUndone")) {
                dtUndone = object.getString("dtUndone");
            } else {
                dtUndone = "";
            }
            if (object.has("status")) {
                status = object.getString("status");
            } else {
                status = "";
            }

            String nullString = "null";
            if (this.dtConfirmed.equalsIgnoreCase(nullString)) {
                this.dtConfirmed = null;
            }
            if (this.dtCanceled.equalsIgnoreCase(nullString)) {
                this.dtCanceled = null;
            }
            if (this.dtExpiration.equalsIgnoreCase(nullString)) {
                this.dtExpiration = null;
            }
            if (this.rejectReason.equalsIgnoreCase(nullString)) {
                this.rejectReason = null;
            }
            if (this.movementType.equalsIgnoreCase(nullString)) {
                this.movementType = null;
            }

            this.usedVoucher = object.getBoolean("usedVoucher");

            this.isVoucher = object.getBoolean("voucher");

            this.description = object.getString("description");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Factory method to convert an array of JSON objects into a list of objects
    // Transaction.fromJson(jsonArray);
    public static ArrayList<Transaction> fromJson(JSONArray jsonObjects) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                transactions.add(new Transaction(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return transactions;
    }

    public boolean isDebitTransaction() {
        return (this.debitOrCredit.compareTo("DEBIT") == 0);
    }

}