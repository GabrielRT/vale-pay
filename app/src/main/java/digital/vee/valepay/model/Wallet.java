package digital.vee.valepay.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Sérgio Brocchetto on 27/02/2017.
 */

public class Wallet implements Serializable {

    private String idWallet;
    private String walletNumber;
    private String goodThru;
    private String flDefault;
    private String idUser;
    private String identifier;
    private String identifierCardNumberFormat;
    private String name;
    private String companyName;
    private String valueBalance;
    private WalletType walletType;
    private String dtExpiration;
    private String dtlastRecharged;
    private String dtFormated;

    public void setDtlastRecharged(String dtlastRecharged) {
        this.dtlastRecharged = dtlastRecharged;
    }

    public String getFormattedDtlastRecharged() {
        if (dtlastRecharged.equals("null") || dtlastRecharged == null) {
            return "";
        }
        dtFormated = "";
        try {
            JSONObject dateRecharge = new JSONObject(this.dtlastRecharged);
            dtFormated = String.format("%d/%d/%d", dateRecharge.getInt("dayOfMonth"), dateRecharge.getInt("monthOfYear"), dateRecharge.getInt("year"));
            return dtFormated;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dtFormated;
    }

    public WalletType getWalletType() {
        return walletType;
    }

    public void setWalletType(WalletType walletType) {
        this.walletType = walletType;
    }

    public String getDtExpiration() {
        return dtExpiration;
    }

    public void setDtExpiration(String dtExpiration) {
        this.dtExpiration = dtExpiration;
    }

    public String getFormattedDtExpiration() {
        try {
            JSONObject dateExp = new JSONObject(this.dtExpiration);
            return String.format("VAL. %02d/%d", dateExp.getInt("monthOfYear"), dateExp.getInt("year"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.dtExpiration;
    }

    // Decodes wallet json into wallet model object
    public static Wallet fromJson(JSONObject jsonObject) {
        Wallet e = new Wallet();
        // Deserialize json into object fields
        try {
            e.idWallet = jsonObject.getString("idWallet");
            e.identifier = jsonObject.getString("identifier");
            e.name = jsonObject.getString("name");
            e.companyName = jsonObject.getString("companyName");
            e.valueBalance = jsonObject.getString("valueBalance");
            e.identifierCardNumberFormat = jsonObject.getString("identifierCardNumberFormat");
            e.dtExpiration = jsonObject.getString("dtExpiration");
            e.dtlastRecharged = jsonObject.getString("dtLastRecharge");
            JSONObject json = jsonObject.getJSONObject("walletType");
            e.walletType = WalletType.fromJson(json);

        } catch (JSONException error) {
            error.printStackTrace();
            return null;
        }
        // Return new object
        return e;
    }

    // Decodes array of wallet json results into wallet model objects
    public static ArrayList<Wallet> fromJson(JSONArray jsonArray) {
        JSONObject walletJson;
        ArrayList<Wallet> wallets = new ArrayList<>(jsonArray.length());
        // Process each result in json array, decode and convert to wallet object
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                walletJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Wallet wallet = Wallet.fromJson(walletJson);
            if (wallet != null) {
                wallets.add(wallet);
            }
        }
        return wallets;
    }

    public String getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(String idWallet) {
        this.idWallet = idWallet;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getGoodThru() {
        return goodThru;
    }

    public void setGoodThru(String goodThru) {
        this.goodThru = goodThru;
    }

    public String getFlDefault() {
        return flDefault;
    }

    public void setFlDefault(String flDefault) {
        this.flDefault = flDefault;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierCardNumberFormat() {
        return identifierCardNumberFormat;
    }

    public void setIdentifierCardNumberFormat(String identifierCardNumberFormat) {
        this.identifierCardNumberFormat = identifierCardNumberFormat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getValueBalance() {
        return valueBalance;
    }

    public String getValueBalanceLocale() {
        Double balanceD = Double.parseDouble(valueBalance);
        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);
        return dFormat.format(balanceD);
    }

    public void setValueBalance(String valueBalance) {
        this.valueBalance = valueBalance;
    }

    @Override

    public String toString() {
        return "Wallet{" +
                "idWallet='" + idWallet + '\'' +
                ", walletNumber='" + walletNumber + '\'' +
                ", goodThru='" + goodThru + '\'' +
                ", flDefault='" + flDefault + '\'' +
                ", idUser='" + idUser + '\'' +
                ", identifier='" + identifier + '\'' +
                ", identifierCardNumberFormat='" + identifierCardNumberFormat + '\'' +
                ", name='" + name + '\'' +
                ", companyName='" + companyName + '\'' +
                ", valueBalance='" + valueBalance + '\'' +
                ", walletType=" + walletType +
                '}';
    }
}



