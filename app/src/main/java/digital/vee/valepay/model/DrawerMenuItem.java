package digital.vee.valepay.model;

public class DrawerMenuItem {
    private int mId;

    private String mTitle;
    private String mHelpText;

    private boolean enabled;

    private int mDrawableResourceId;

    public  DrawerMenuItem() {
        this.enabled = true;
    }

    public DrawerMenuItem(int id, String title, int drawableResourceId) {
        this();
        this.mId = id;
        this.mTitle = title;
        this.mDrawableResourceId = drawableResourceId;
    }

    public DrawerMenuItem(int id, String title, int drawableResourceId, String helpText) {
        this(id, title, drawableResourceId);
        this.mHelpText = helpText;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getDrawableResourceId() {
        return mDrawableResourceId;
    }

    public void setDrawableResourceId(int drawableResourceId) {
        this.mDrawableResourceId = drawableResourceId;
    }

    public String getHelpText() {
        return mHelpText;
    }
}