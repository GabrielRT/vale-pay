package digital.vee.valepay.model;

import digital.vee.valepay.activity.RestaurantFilterActivity;

/**
 * Model of Restaurant Category used in RestaurantFilterActivity.
 *
 * @author Rafael O Magalhaes
 * @see RestaurantFilterActivity
 */
public class RestaurantCategory {

    private String name;
    private String code;
    private boolean selected;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
