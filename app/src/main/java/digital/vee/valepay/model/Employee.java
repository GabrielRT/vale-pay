package digital.vee.valepay.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Sérgio Brocchetto on 27/02/2017.
 */

public class Employee {

    private String idEmployee;
    private String enabled;
    private String loginName;
    private String firstName;
    private String lastName;
    private String email;

    static Employee e = new Employee();

    public static Employee getE() {
        return e;
    }

    public void setE(Employee e) {
        this.e = e;
    }

    // Decodes employee json into employee model object
    public static Employee fromJson(JSONObject jsonObject) {

        // Deserialize json into object fields
        try {
            e.loginName = jsonObject.getString("loginName");
            e.firstName = jsonObject.getString("firstName");
            e.lastName = jsonObject.getString("lastName");
            e.email = jsonObject.getString("email");
        } catch (JSONException error) {
            error.printStackTrace();
            return null;
        }
        // Return new object
        return e;
    }

    // Decodes array of employee json results into employee model objects
    public static ArrayList<Employee> fromJson(JSONArray jsonArray) {
        JSONObject employeeJson;
        ArrayList<Employee> employees = new ArrayList<>(jsonArray.length());
        // Process each result in json array, decode and convert to employee object
        for (int i=0; i < jsonArray.length(); i++) {
            try {
                employeeJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Employee employee = Employee.fromJson(employeeJson);
            if (employee != null) {
                employees.add(employee);
            }
        }
        return employees;
    }

    public void setId(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdEmployee() { return idEmployee; }

    public void setIdEmployee(String idEmployee) { this.idEmployee = idEmployee; }

    @Override
    public String toString() {
        return "Employee{" +
                "email='" + email + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", loginName='" + loginName + '\'' +
                ", enabled='" + enabled + '\'' +
                ", idEmployee='" + idEmployee + '\'' +
                '}';
    }
}
