package digital.vee.valepay.model;

public class TransactionListItem {
    private String transactionUUID, dtStarted, dtConfirmed, dtCanceled, dtExpiration, walletIdentifierTo;
    private Double amount;

    public TransactionListItem(String transactionUUID, String dtStarted, String dtConfirmed, String dtCanceled,
                               String dtExpiration, String walletIdentifierTo, Double amount) {
        this.transactionUUID = transactionUUID;
        this.dtStarted = dtStarted;
        this.dtConfirmed = dtConfirmed;

        this.dtCanceled = dtCanceled;
        this.dtExpiration = dtExpiration;
        this.walletIdentifierTo = walletIdentifierTo;
        this.amount = amount;
    }

    public String getTransactionUUID() {
        return transactionUUID;
    }

    public void setTransactionUUID(String transactionUUID) {
        this.transactionUUID = transactionUUID;
    }

    public String getDtStarted() {
        return dtStarted;
    }

    public void setDtStarted(String dtStarted) {
        this.dtStarted = dtStarted;
    }

    public String getDtConfirmed() {
        return dtConfirmed;
    }

    public void setDtConfirmed(String dtConfirmed) {
        this.dtConfirmed = dtConfirmed;
    }

    public String getDtCanceled() {
        return dtCanceled;
    }

    public void setDtCanceled(String dtCanceled) {
        this.dtCanceled = dtCanceled;
    }

    public String getDtExpiration() {
        return dtExpiration;
    }

    public void setDtExpiration(String dtExpiration) {
        this.dtExpiration = dtExpiration;
    }

    public String getWalletIdentifierTo() {
        return walletIdentifierTo;
    }

    public void setWalletIdentifierTo(String walletIdentifierTo) {
        this.walletIdentifierTo = walletIdentifierTo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
