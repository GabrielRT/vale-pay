package digital.vee.valepay.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import digital.vee.valepay.activity.FindRestaurantsListActivity;

/**
 * Model of Restaurant used in FindRestaurantListActivity.
 * @see FindRestaurantsListActivity
 */
public class Restaurant {

    // REST result

    /**
     * Partner Code (Yelp, TripAdvisor).
     */
    private String partnerCode;

    /**
     * Restaurant Name
     */
    private String name;

    /**
     * Restaurant Email
     */
    private String email;

    /**
     * Restaurant PhoneNumber
     */
    private String phoneNumber;

    /**
     * Person in Chage Name
     */
    private String personInChargeName;

    // YELP result

    /**
     * Restaurant address
     */
    private String address;

    /**
     * Restaurant mapURL link
     */
    private String mapUrl;

    /**
     * Restaurant logo image URL
     */
    private String imageUrl;

    /**
     * Restaurant rating
     */
    private float rating;

    /**
     * Restaurante ratingImgUrl;
     */
    private String ratingImgUrl;

    // internal
    /**
     * Flag to mark if the restaurant was invited by current user
     */
    private boolean invitedByUser;

    /**
     * Flag to mark if the restaurant is on Vee.
     */
    private boolean veeRestaurant;

    /**
     * Default constructor
     */
    public Restaurant() {
        this.invitedByUser = false;
        this.veeRestaurant = false;
    }

    /**
     * Construct a Restaurant using a json return (from Vee server)
     * @param restaurantJSON JSON return from Vee server
     */
    public Restaurant(JSONObject restaurantJSON) {
        this();

        try {

            if (restaurantJSON.has("partnerCode")) {
                this.partnerCode = restaurantJSON.getString("partnerCode");
            }
            this.name = restaurantJSON.getString("name");
            this.email = restaurantJSON.getString("email");
            this.phoneNumber = restaurantJSON.getString("phoneNumber");
            this.personInChargeName = restaurantJSON.getString("personInChargeName");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonInChargeName() {
        return personInChargeName;
    }

    public void setPersonInChargeName(String personInChargeName) {
        this.personInChargeName = personInChargeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getRatingImgUrl() {
        return ratingImgUrl;
    }

    public void setRatingImgUrl(String ratingImgUrl) {
        this.ratingImgUrl = ratingImgUrl;
    }

    public boolean isInvitedByUser() {
        return invitedByUser;
    }

    public void setInvitedByUser(boolean invitedByUser) {
        this.invitedByUser = invitedByUser;
    }

    public boolean isVeeRestaurant() {
        return veeRestaurant;
    }

    public void setVeeRestaurant(boolean veeRestaurant) {
        this.veeRestaurant = veeRestaurant;
    }

    /**
     * Create a list of restaurants from a json array
     *
     * @param jsonObjects List of json of restaurants
     * @return List of restaurants
     */
    public static ArrayList<Restaurant> fromJson(JSONArray jsonObjects) {
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                Restaurant restaurant = new Restaurant(jsonObjects.getJSONObject(i));
                restaurants.add(restaurant);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return restaurants;
    }
}
