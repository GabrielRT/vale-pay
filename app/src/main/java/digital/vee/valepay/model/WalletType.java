package digital.vee.valepay.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/*
 * Created by Sérgio Brocchetto on 27/02/2017.
 */

public class WalletType implements Serializable {
    private String idWalletType;
    private String code;
    private String description;

    public void setIdWallet(String idWalletType) {
        this.idWalletType = idWalletType;
    }

    // Decodes wallet json into wallet model object
    public static WalletType fromJson(JSONObject jsonObject) {
        WalletType e = new WalletType();
        // Deserialize json into object fields
        try {
            e.idWalletType = jsonObject.getString("idWalletType");
            e.code = jsonObject.getString("code");
            e.description = jsonObject.getString("description");

        } catch (JSONException error) {
            error.printStackTrace();
            return null;
        }
        // Return new object
        return e;
    }

    public String getIdWalletType() { return idWalletType; }

    public void setIdWalletType(String idWalletType) {
        this.idWalletType = idWalletType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}