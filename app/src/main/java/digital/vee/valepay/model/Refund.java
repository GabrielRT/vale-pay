package digital.vee.valepay.model;

import org.json.JSONException;
import org.json.JSONObject;

import digital.vee.valepay.helper.DateUtils;

/**
 * Created by silvio on 27/04/18.
 */

public class Refund {

    public String uuid;
    public double value;
    public String dtCanceled;
    public String dtConfirmed;
    public String refundStatus;
    public String rejectReason;
    public String observation;
    public String walletFrom;

    private Refund(JSONObject jsonObject){
        try {
            uuid = jsonObject.getString("refundUUID");
            value = jsonObject.getDouble("value");
            refundStatus = jsonObject.getString("refundStatus");
            walletFrom = jsonObject.getString("walletFrom");
            if(jsonObject.has("dtCanceled") && jsonObject.getString("dtCanceled") != null){
                dtCanceled = DateUtils.getDateFromUTC(jsonObject.getString("dtCanceled"));
            } else {
                dtCanceled = "";
            }
            if(jsonObject.has("dtConfirmed") && jsonObject.getString("dtConfirmed") != null){
                dtConfirmed = DateUtils.getDateFromUTC(jsonObject.getString("dtConfirmed"));
            } else {
                dtConfirmed = "";
            }
            if(jsonObject.has("rejectReason") && jsonObject.getString("rejectReason") != null){
                rejectReason = jsonObject.getString("rejectReason");
            }
            if(jsonObject.has("observation") && jsonObject.getString("observation") != null){
                observation = jsonObject.getString("observation");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Refund fromJson(JSONObject jsonObject){
        return new Refund(jsonObject);
    }

}
