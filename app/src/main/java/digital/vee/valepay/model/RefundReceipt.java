package digital.vee.valepay.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * Created by silvio on 27/04/18.
 */

public class RefundReceipt {

    public double value;
    public String date;
    public String document;
    public String receiptNumber;
    public String receiptType;

    private RefundReceipt(JSONObject jsonObject){
        try {
            value = jsonObject.getDouble("value");
            date = jsonObject.getString("dtReceipt");
            document = jsonObject.getString("merchantDocument");
            receiptNumber = jsonObject.getString("receiptIdentifier");
            receiptType = jsonObject.getString("receiptType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static RefundReceipt fromJson(JSONObject jsonObject){
        return new RefundReceipt(jsonObject);
    }
}
