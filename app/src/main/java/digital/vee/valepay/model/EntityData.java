package digital.vee.valepay.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 * Created by Sérgio Brocchetto on 27/02/2017.
 *
 */

public class EntityData {
    private Long idEntity;
    private String corporate;
    private String name;

    private String externalIdentify;
    private String defaultWalletCode;
    private String activationCode;
    private String activationSent;
    private String dtExpireActivationCode;

    private String totalBalance;

    ArrayList<Wallet> wallets;

    public ArrayList<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(ArrayList<Wallet> wallets) {
        this.wallets = wallets;
    }

    public String getTotalBalance() {
            return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    // Decodes entityData json into entityData model object
    public static EntityData fromJson(JSONObject jsonObject) {
        EntityData e = new EntityData();
        // Deserialize json into object fields
        try {
            e.idEntity = jsonObject.getLong("idEntity");
            e.totalBalance = jsonObject.getString("totalBalance");
            e.name = jsonObject.getString("name");
            e.externalIdentify = jsonObject.getString("externalIdentify");
            e.defaultWalletCode = jsonObject.getString("defaultWalletCode");

            JSONArray jsonArray = jsonObject.getJSONArray("walletList");

            ArrayList<Wallet> walletsEntity = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = (JSONObject) jsonArray.get(i);
                Wallet wallet = new Wallet();
                wallet = Wallet.fromJson(object);
                walletsEntity.add(wallet);
            }
            e.wallets = walletsEntity;
        } catch (JSONException error) {
            error.printStackTrace();
            return null;
        }
        // Return new object
        return e;
    }

    // Decodes array of entityData json results into entityData model objects
    public static ArrayList<EntityData> fromJson(JSONArray jsonArray) {
        JSONObject entityDataJson;
        ArrayList<EntityData> entitiesData = new ArrayList<>(jsonArray.length());
        // Process each result in json array, decode and convert to entityData object
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                entityDataJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            EntityData entityData = EntityData.fromJson(entityDataJson);
            if (entityData != null) {
                entitiesData.add(entityData);
            }
        }
        return entitiesData;
    }

    public Long getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(Long idEntity) {
        this.idEntity = idEntity;
    }

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalIdentify() {
        return externalIdentify;
    }

    public void setExternalIdentify(String externalIdentify) {
        this.externalIdentify = externalIdentify;
    }

    public String getDefaultWalletCode() {
        return defaultWalletCode;
    }

    public void setDefaultWalletCode(String defaultWalletCode) {
        this.defaultWalletCode = defaultWalletCode;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getActivationSent() {
        return activationSent;
    }

    public void setActivationSent(String activationSent) {
        this.activationSent = activationSent;
    }

    public String getDtExpireActivationCode() {
        return dtExpireActivationCode;
    }

    public void setDtExpireActivationCode(String dtExpireActivationCode) {
        this.dtExpireActivationCode = dtExpireActivationCode;
    }

    @Override
    public String toString() {
        return "EntityData{" +
                "idEntity='" + idEntity + '\'' +
                ", corporate='" + corporate + '\'' +
                ", name='" + name + '\'' +
                ", externalIdentify='" + externalIdentify + '\'' +
                ", defaultWalletCode='" + defaultWalletCode + '\'' +
                ", activationCode='" + activationCode + '\'' +
                ", activationSent='" + activationSent + '\'' +
                ", dtExpireActivationCode='" + dtExpireActivationCode + '\'' +
                ", wallets=" + wallets +
                '}';
    }
}
