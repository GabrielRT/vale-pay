package digital.vee.valepay.fragment;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * New type of WebView where we know when the user has reached the final of the WebView to be able to
 * accept the terms.
 *
 * Created by silvio on 09/07/18.
 */

public class TermWebView extends WebView {
    private OnScrollChangedCallback mOnScrollChangedCallback;

    public TermWebView(final Context context)
    {
        super(context);
    }

    public TermWebView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TermWebView(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
    }

    /**
     * onOverScrolled let android know if the user reached the end of the WebView and there is nothing
     * more to be scrolled.
     *
     * @param scrollX New X scroll value in pixels
     * @param scrollY New Y scroll value in pixels
     * @param clampedX True if scrollX was clamped to an over-scroll boundary
     * @param clampedY True if scrollY was clamped to an over-scroll boundary
     */
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY)
    {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        if(mOnScrollChangedCallback != null) mOnScrollChangedCallback.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
    }

    public OnScrollChangedCallback getOnScrollChangedCallback()
    {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback)
    {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Implement in the activity/fragment/view that you want to listen to the WebView
     */
    public interface OnScrollChangedCallback
    {
        void onScroll(int l, int t, int oldl, int oldt);
        void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY);
    }

}
