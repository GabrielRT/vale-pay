package digital.vee.valepay.fragment;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import digital.vee.valepay.R;
import digital.vee.valepay.activity.RefundInformationActivity;
import digital.vee.valepay.util.Preferences;

/**
 * this class inflates the layout for the bottom sheet used in the refund information activity
 * where the user can choose to cancel refund or send to manual evaluation
 * Created by silvio on 18/04/18.
 */

public class RefundDialog extends BottomSheetDialogFragment {

    public RefundDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        RelativeLayout cancel = (RelativeLayout) getView().findViewById(R.id.cancel_refund_layout);
//        RelativeLayout manualRefund = (RelativeLayout) getView().findViewById(R.id.manual_refund_layout);
//
//        cancel.setOnClickListener(v ->{
//                Preferences.putBoolean("refundConfirm", false);
//                RefundInformationActivity.refuseRefund()});

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bottomsheet_refund, container, false);
    }

}
