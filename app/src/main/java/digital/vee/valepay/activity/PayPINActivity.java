package digital.vee.valepay.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.FingerPrintHelper;
import digital.vee.valepay.util.LoginUtil;

/**
 * Get the PIN for the reversal transaction to take place
 */
public class PayPINActivity extends AppCompatActivity {
    private static final String TAG = "PayPINActivity";
    @Bind(R.id.input_confirmation_code)
    EditText _pinCodeText;
    @Bind(R.id.btn_submit)
    Button _confirmPaymentButton;
    @Bind(R.id.finger_print_text)
    LinearLayout fingerPrintText;



    private String amount;
    private long lastClickTime = 0;
    private FingerPrintHelper fingerPrintHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pay_pin);

        ButterKnife.bind(this);

        setCustomActionBar();

        //get transaction data from PayBeingScannedActivity intent
        Intent intent = getIntent();

        amount = intent.getStringExtra("amount");

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();

                String pinCode = _pinCodeText.getText().toString();

                if (pinCode.isEmpty() || pinCode.length() < 4) {
                    //_pinCodeText.requestFocus();
                    _pinCodeText.setError(getString(R.string.error_pin_code));
                } else {
                    _pinCodeText.setError(null);

                    Intent intent = new Intent(PayPINActivity.this, PayQRActivity.class);
                    intent.putExtra("amount", amount);
                    intent.putExtra("pinCode", pinCode);

                    ActivityUtils.newActivity(PayPINActivity.this, intent);
                    finish();
                }
            }
        };
        _confirmPaymentButton.setOnClickListener(listener);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        _confirmPaymentButton.setTypeface(type_roboto_bold);

        TextView tv_pin_text = (TextView) findViewById(R.id.pin_text);
        tv_pin_text.setTypeface(type_roboto_light);
        _pinCodeText.setTypeface(type_roboto_light);
    }

    /**
     * Checks if the user has internet connection and opens a dialog to warn him
     */
    private void enableFingerPrintListening() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            fingerPrintHelper = new FingerPrintHelper(getSystemService(FingerprintManager.class), this,
                    () -> fillPass());

            //get pin
            if (fingerPrintHelper.isFingerprintAuthAvailable()) {
                String pin = null;
                try {
                    pin = SnappyDBHelper.getEncrypted(this, "pin");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                //if pin has already saved
                if (pin != null && !pin.isEmpty()) {
                    //show fingerprint input info
                    fingerPrintText.setVisibility(View.VISIBLE);
                    //wait for fingerprint information
                    fingerPrintHelper.startListening(null);
                }

            } else {
                showKeyboard();
            }
        } else {
            showKeyboard();
        }
    }

    private void fillPass() {
        String pin = null;
        try {
            pin = SnappyDBHelper.getEncrypted(this, "pin");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        _pinCodeText.setText(pin);
        _confirmPaymentButton.callOnClick();
    }

    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }
    }

    /**
     * shows keyboard
     *
     */
    private void showKeyboard() {
        _pinCodeText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(_pinCodeText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirLastActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        redirLastActivity();
    }
    @Override
    public void onResume() {
        super.onResume();
        enableFingerPrintListening();
    }

    void redirLastActivity() {
        ActivityUtils.exitActivity(this, new Intent(PayPINActivity.this, PayBeingScannedActivity.class));
        finish();
    }
}