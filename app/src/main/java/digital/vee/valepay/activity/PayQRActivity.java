package digital.vee.valepay.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.snappydb.SnappydbException;

import org.apache.commons.codec.android.CharEncoding;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;
import digital.vee.valepay.util.ZXingQRCode.QRCodeEncoder;
import digital.vee.valepay.util.crypto.AES;
import digital.vee.valepay.util.crypto.CryptoUtil;

import static java.lang.Math.abs;

/**
 * Class that creates QR code of the reverse transaction
 */
public class PayQRActivity extends BaseActivity {
    private static final String TAG = "PayQRActivity";
    private static final String URL = BuildConfig.SERVER_URL + "rest/private/transaction/find";
    public static final int SHOW_QR_FRAME_TIME = 300;
    public static final int TIMES_TO_CHECK_TRANSACTION = 20;
    public static final String PAY_VALUE_KEY = "value";

    @Bind(R.id.totalAmountText)
    TextView _totalAmountText;
    @Bind(R.id.totalAmount)
    TextView _totalAmount;
    @Bind(R.id.text_cancel)
    TextView _cancelText;
    @Bind(R.id.offline_box)
    RelativeLayout _confirmPaymentButton;
    @Bind(R.id.scan_here2)
    TextView _scanHere;

    private ProgressDialog progressDialog;
    private String amount;
    private String transactionUuid;
    private String token;
    private int runTimer1;
    private boolean confirmed2;
    private Timer t;
    private Timer t1;
    private int runTimer2 = 0;
    private int runTimer3 = 0;
    private Timer t2;
    //TODO: testar com mais frames e tempo menor para facilitar a leitura
    private int PARTS = 4;
    private Intent intentPayOK;
    private long lastClickTimeCancel = 0;
    private long lastClickTimeConfirm = 0;
    private CryptoUtil cryptoUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Preferences.init(this);

        setContentView(R.layout.activity_pay_qr);

        ButterKnife.bind(this);
        setCustomActionBar();

        doLogin();
        try {
            token = SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        //getEncrypted transaction data from Preferences
        Intent intent = getIntent();
        //String entityId = Preferences.getString("entityId");
        //String walletId = Preferences.getString("walletId");

        String entityId = null;
        try {
            entityId = SnappyDBHelper.get(getApplicationContext(), "entityId");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        String walletId = null;
        try {
            walletId = SnappyDBHelper.get(getApplicationContext(), "walletId");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        //getEncrypted transaction data from PayBeingScannedActivity intentPayOK
        amount = intent.getStringExtra("amount");
        // set on screen before convert to number
        _totalAmountText.setText(String.format("%s", getString(R.string.pay_qr_total_amount)));
        _totalAmount.setText(String.format("R$ %s", amount));

        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        _totalAmount.setTypeface(type_roboto_bold);

        String amountTSC = amount;
        amountTSC = amountTSC.replace(".", "");
        amountTSC = amountTSC.replace(",", ".");

        //Log.v(TAG, "*** amount: " + amount);
        BigDecimal bgAmount = new BigDecimal(0);
        if (amountTSC != null) {
            bgAmount = new BigDecimal(amountTSC);
        }

        amount = amountTSC;

        String pinCode = intent.getStringExtra("pinCode");

        //Log.v(TAG, " entityId " + entityId + " walletId " + walletId + " bgAmount " + bgAmount + " pinCode " + pinCode);
        TransactionSignedCode transactionSignedCode = new TransactionSignedCode(entityId, walletId, bgAmount, pinCode);

        transactionUuid = transactionSignedCode.getUuid();
        //Log.v(TAG, "*** transactionUuid: " + transactionUuid);

        String stringParaAssinar = transactionSignedCode.getSignableTransactionString();

        String signature = null;
        try {
            cryptoUtil = new CryptoUtil(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            signature = cryptoUtil.signature(stringParaAssinar.getBytes(),
                    SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.ECDSA_PRIVATE_KEY))[0];
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException | SnappydbException e) {
            e.printStackTrace();
        }
        transactionSignedCode.setSignature(signature);

        final String[] tscProntoParaTransmitir = {transactionSignedCode.getTSC()};

        int length = tscProntoParaTransmitir[0].length();
        //Log.v(TAG, " final tsc: " + tscProntoParaTransmitir[0] + " size: " + length);

        final String[] chunksSeparetedByBreakLine = {""};
        HashMap<Integer, String> integerStringHashMap;
        int tam = 0;

        t2 = new Timer();

        final int rest = length - (length % PARTS);

        try {
            integerStringHashMap = chunk_split(tscProntoParaTransmitir[0], length / PARTS, "\n");
            tam = integerStringHashMap.keySet().iterator().next();
            chunksSeparetedByBreakLine[0] = integerStringHashMap.get(tam);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Log.v(TAG, "Chunks: " + tam + " chunks: \n" + chunksSeparetedByBreakLine[0]);

        //generate QR image signed with public key
        //Find screen size
        WindowManager manager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager != null ? manager.getDefaultDisplay() : null;
        Point point = new Point();
        if (display != null) {
            display.getSize(point);
        }
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        final int endIndex = chunksSeparetedByBreakLine[0].indexOf("\n");
        final int beginIndex = 0;

        final int finalSmallerDimension = smallerDimension;

        final ArrayList<String> arrayList = splitTSCintoQRcodes(tam, chunksSeparetedByBreakLine, beginIndex, endIndex, rest);

        final int size = arrayList.size();

        final int[] i = {0};

        final String[] data = new String[1];

        ImageView imageview = (ImageView) findViewById(R.id.qr_code);

        // Create the AnimationDrawable in which we will store all frames of the animation
        final AnimationDrawable animationDrawable = new AnimationDrawable();
        for (int j = 0; j < size; ++j) {
            //set frame to show and for how much time in miliseconds
            animationDrawable.addFrame(getDrawableForFrameNumber(j, arrayList, finalSmallerDimension), SHOW_QR_FRAME_TIME);
        }
        // Run until we say stop
        animationDrawable.setOneShot(false);

        imageview.setImageDrawable(animationDrawable);
        animationDrawable.start();

        _cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 2000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeCancel < 3000) {
                    return;
                }
                lastClickTimeCancel = SystemClock.elapsedRealtime();

                if (t != null) {
                    t.cancel();
                    t.purge();

                }
                if (t1 != null) {
                    t1.cancel();
                    t1.purge();
                }

                if (t2 != null) {
                    t2.purge();
                    t2.cancel();

                }

                Intent intent = new Intent(PayQRActivity.this, UseActivity.class);
                intent.putExtra("entityNameTo", "");

                ActivityUtils.exitActivity(PayQRActivity.this, intent);
                finish();
            }
        });


        _confirmPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 2000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeConfirm < 3000) {
                    return;
                }
                lastClickTimeConfirm = SystemClock.elapsedRealtime();

                if (t != null) {
                    t.cancel();
                    t.purge();

                }
                if (t1 != null) {
                    t1.cancel();
                    t1.purge();
                }

                if (t2 != null) {
                    t2.purge();
                    t2.cancel();

                }
                //checkTransactionConfirm();

                /*Intent intent = new Intent(PayQRActivity.this, PayOKActivity.class);
                intent.putExtra(PAY_VALUE_KEY, amount);
                intent.putExtra("entityNameTo", "");
                try {
                    intent.putExtra("lastUpdated", SnappyDBHelper.get(getApplicationContext(), "lastUpdated"));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }*/

                try {
                    updateEntities();
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                redirUseOpenMenu();
            }
        });


        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        _totalAmountText.setTypeface(type_roboto_light);
        _scanHere.setTypeface(type_roboto_light);

        //if online set offline text visivility to gone and start timer to check transaction
        if (Network.isOnline(PayQRActivity.TAG, getApplicationContext())) {
            _confirmPaymentButton.setVisibility(View.GONE);
            t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          checkTransaction();
                                          if (t != null) {
                                              t.cancel();
                                              t.purge();
                                          }
                                      }
                                  },
//Set how long before to start calling the TimerTask (in milliseconds)
                    300,
//Set the amount of time between each execution (in milliseconds)
                    3000);

            checkTransaction();
        } else { // else show offline text
            showOfflineBox();
        }
    }

    /**
     * Update entities when offline to show correct values
     *
     * @throws SnappydbException
     */
    private void updateEntities() throws SnappydbException {
        JSONObject jsonSnappy = null;
        // SnappyDB

        try {
            if (SnappyDBHelper.exists(getApplicationContext(), "CARDS")) {
                jsonSnappy = new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonSnappy == null) {
            return;
        }


        //Faz o parse e carrega no models
        try {
            JSONObject jsonEntity = jsonSnappy.getJSONObject("abstractEntity");

            amount = amount.replace(".", "");
            amount = amount.replace(",", ".");

            Double amountD = Double.valueOf(amount.replace(",", "."));
            Double totalBalance = jsonEntity.getDouble("totalBalance");

            jsonEntity.put("totalBalance", totalBalance - amountD);

            JSONArray walletList = jsonEntity.getJSONArray("walletList");

            String walletId = SnappyDBHelper.get(getApplicationContext(), "walletId");

            for (int i = 0; i < walletList.length(); i++) {
                JSONObject jsonWallet = walletList.getJSONObject(i);
                if (jsonWallet.getString("identifier").equalsIgnoreCase(walletId)) {
                    Double walletBalance = jsonWallet.getDouble("valueBalance");
                    jsonWallet.put("valueBalance", walletBalance - amountD);
                    break;
                }
            }

            SnappyDBHelper.put(getApplicationContext(), "CARDS", jsonSnappy.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Take the transaction info and create the animated image of the QRCode
     *
     * @param iterate size of the string in the chunk
     * @param chunksSeparatedbyLineBreak Chunks for each frame of the QRCode
     * @param beginIndex First frame of the QR code
     * @param endIndex Last frame of the QR code
     * @param rest if there's any left, create another QRCode with the remaining strings
     * @return All the frames of the QR Code
     */
    private ArrayList<String> splitTSCintoQRcodes(int iterate, String[] chunksSeparatedbyLineBreak, int beginIndex, int endIndex,
                                                  int rest) {
        ArrayList<String> qrCodes = new ArrayList<>();
        //Log.v(TAG, "splitTSCintoQRcodes runTimer3 " + runTimer3 + " iterate " + iterate);
        String tsc;
        tsc = chunksSeparatedbyLineBreak[0].substring(beginIndex, endIndex);

        String key = "";
        for (int i = 0; i < 64; i++) {
            int c = abs((int) (Math.cos(i) * 10));
            key += (char) (c % 2 == 0 ? c + 97 : c + 48);
        }
        byte[] encodedBytes = Base64.encode(key.getBytes(), Base64.NO_WRAP);
        key = new String(encodedBytes);

        while (runTimer3 < iterate - 1) {
            // getEncrypted chunk from begin of last to first line break

            String qrcode = null;

            AES aES = null;
            try {
                aES = new AES(key);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                qrcode = aES.encrypt("VAQR" + runTimer3 + PARTS + tsc);
                //Log.v(TAG, "*** qrCode: " + qrcode);
            } catch (Exception e) {
                e.printStackTrace();
            }

            qrCodes.add(qrcode);

            // subtrain o chunk recuperado
            chunksSeparatedbyLineBreak[0] = chunksSeparatedbyLineBreak[0].substring(endIndex + 1, chunksSeparatedbyLineBreak[0].length());

            endIndex = chunksSeparatedbyLineBreak[0].indexOf("\n");
            tsc = chunksSeparatedbyLineBreak[0].substring(0,
                    endIndex);

            runTimer3++;
        }
        if (rest > 0) {
            String tmp;
            if (chunksSeparatedbyLineBreak[0] != null &&
                    !chunksSeparatedbyLineBreak[0].isEmpty() && chunksSeparatedbyLineBreak[0].contains("\n")) {
                tmp = chunksSeparatedbyLineBreak[0].substring(0, chunksSeparatedbyLineBreak[0].indexOf("\n"));
            } else {
                tmp = chunksSeparatedbyLineBreak[0];
            }
            if (tmp.length() > 0) {
                tsc = chunksSeparatedbyLineBreak[0].substring(0, chunksSeparatedbyLineBreak[0].indexOf("\n"));

                String qrcode = null;

                AES aES = null;
                try {
                    aES = new AES(key);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    qrcode = aES.encrypt("VAQR" + runTimer3 + PARTS + tsc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                qrCodes.add(qrcode);
            }
        }

        return qrCodes;
    }

    private void changeImageQR(QRCodeEncoder qrCodeEncoder) throws WriterException {
        Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
        ImageView myImage = (ImageView) findViewById(R.id.qr_code);
        //Log.v(TAG, "setando imagem..................................\n..........");
        myImage.setImageBitmap(bitmap);
    }



    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setDisplayShowHomeEnabled(false);
            supportActionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * Start the payment confirmation activity with information on the transaction
     * @param entityNameTo entity on the other end of the transaction
     */
    private void onTransactionSuccess(String entityNameTo) {
        boolean confirmed = true;
        if (t != null) {
            t.cancel();
            t.purge();

        }
        if (t1 != null) {
            t1.cancel();
            t1.purge();
        }

        if (t2 != null) {
            t2.purge();
            t2.cancel();

        }
        //intentPayOK = new Intent(this, PayOKActivity.class);
        intentPayOK.putExtra(PAY_VALUE_KEY, amount);
        intentPayOK.putExtra("entityNameTo", entityNameTo);
        intentPayOK.putExtra(PayOKActivity.PAY_RECEIVER_KEY, entityNameTo);

        ActivityUtils.newActivity(this, intentPayOK);
        finish();
    }

    private void onTransactionFailed() {
        //Toast.makeText(getBaseContext(), R.string.error_login_failed, Toast.LENGTH_LONG).show();
    }

    private void onLoadFailed() {
        dismissDialog();
        //Toast.makeText(getBaseContext(), R.string.load_failed, Toast.LENGTH_LONG).show();
    }

    /**
     * Check if transaction was successful or not
     * case yes: calls onTransactionSuccess with the name of the Merchant as entityNameto
     *          and extra information via intent
     */
    private void checkTransaction() {
        //Log.v(TAG, "+checkTransaction transactionUUID: " + this.transactionUuid);
        intentPayOK = new Intent(this, PayOKActivity.class);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("transactionUUID", this.transactionUuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Log.v(TAG, "2 Response: FIND" + response);

                    JSONObject jsonResponse = new JSONObject(response);
                    String dtConfirmed = jsonResponse.getString("dtConfirmed");
                    String rejectReason = new String(jsonResponse.getString("rejectReason").getBytes(), CharEncoding.UTF_8);

                    String dtCanceled = jsonResponse.getString("dtCanceled");

                    String entityNameTo = jsonResponse.getString("entityNameTo");

                    // puts that the user used Voucher on the intent
                    if (jsonResponse.getBoolean("usedVoucher")) {
                        String amountSpentFromWallet = jsonResponse.getJSONObject("voucherResponse").getString("amountSpentFromWallet");
                        String amountSpentFromVoucher = jsonResponse.getJSONObject("voucherResponse").getString("amountSpentFromVoucher");
                        intentPayOK.putExtra("amountSpentFromWallet", amountSpentFromWallet);
                        intentPayOK.putExtra("amountSpentFromVoucher", amountSpentFromVoucher);
                    }

                    //Log.v(TAG, "check *** *** dtConfirmed: " + dtConfirmed + "\nrejectReason: " + rejectReason + "\n dtCanceled: " + dtCanceled);

                    if (!dtCanceled.equals("null") || !rejectReason.equals("null") || !dtConfirmed.equals("null")) {
                        //Log.v(TAG, "*** *** dtConfirmed: " + dtConfirmed + "\nrejectReason: " + rejectReason + "\n dtCanceled: " + dtCanceled);// + "\nbalance: " + balance +
                        if (!dtCanceled.isEmpty() || !rejectReason.isEmpty() || !dtConfirmed.isEmpty()) {
                            dismissDialog();
                            //check if there is any confirmation data to validate the transaction
                            if (!dtConfirmed.isEmpty() && !dtConfirmed.equals("null")) {
                                confirmed2 = true;
                                if (t != null) {
                                    t.cancel();
                                    t.purge();
                                }
                                if (t1 != null) {
                                    t1.cancel();
                                    t1.purge();
                                }
                                if (t2 != null) {
                                    t2.cancel();
                                    t2.purge();
                                }
                                onTransactionSuccess(entityNameTo);
                            } else {
                                if (!rejectReason.equals("null") || !dtCanceled.equals("null")) {

                                    //Set the schedule function and rate
                                    if (!confirmed2 && t == null) {
                                        t = new Timer();
                                        t.scheduleAtFixedRate(new TimerTask() {
                                                                  @Override
                                                                  public void run() {
                                                                      //Log.v(TAG, "runtimer " + runTimer1);
                                                                      runTimer1++;
                                                                      if (runTimer1 > TIMES_TO_CHECK_TRANSACTION || confirmed2) {
                                                                          //Log.v(TAG, "runtimer  cancel");
                                                                          if (t != null) {
                                                                              t.cancel();
                                                                              t.purge();
                                                                          }
                                                                          if (t1 != null) {
                                                                              t1.cancel();
                                                                              t1.purge();
                                                                          }
                                                                          showOfflineBox();
                                                                      } else {
                                                                          //Log.v(TAG, "checkTransaction ");
                                                                          checkTransaction();
                                                                      }
                                                                  }
                                                              },
//Set how long before to start calling the TimerTask (in milliseconds)
                                                500,
//Set the amount of time between each execution (in milliseconds)
                                                3000);
                                    }

                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    //Log.v(TAG, "4 Exception: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Set the schedule function and rate
                if (!confirmed2 && t1 == null) {
                    t1 = new Timer();
                    t1.scheduleAtFixedRate(new TimerTask() {
                                               @Override
                                               public void run() {
                                                   //Log.v(TAG, " run: " + runTimer2);
                                                   runTimer2++;
                                                   if (runTimer2 > TIMES_TO_CHECK_TRANSACTION || confirmed2) {
                                                       t1.cancel();
                                                       t1.purge();
                                                       if (t != null) {
                                                           t.cancel();
                                                           t.purge();
                                                       }
                                                       if (t2 != null) {
                                                          /* t2.cancel();
                                                           t2.purge();*/
                                                       }
                                                       showOfflineBox();
                                                       //Log.v(TAG, " cancel: ");
                                                   } else {
                                                       //Log.v(TAG, " run: check " + runTimer2);
                                                       checkTransaction();
                                                   }
                                               }
                                           },
//Set how long before to start calling the TimerTask (in milliseconds)
                            900,
//Set the amount of time between each execution (in milliseconds)
                            3000);
                }

                error.printStackTrace();
            }
        };
        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener,
                errorListener, mRequestBody) {
            Request.Priority mPriority;

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Token " + token);
                params.put("Accept-Language", "pt_br");
                //android.util.//Log.v(TAG, TAG + " Header token: " + token);
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //Log.v(TAG, " +parseNetworkError()");
                //Fills a string with parsed error response
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers,
                                        CharEncoding.UTF_8));
                        //Log.v(TAG, " +json(): " + json);

                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                    //Log.v(TAG, "UnsupportedEncodingException: " + uee + " " + uee.getCause() + " " + uee.getMessage());
                    //Volleyandroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Request.Priority getPriority() {
                // If you didn't use the setPriority method,sharedpreferences
                return mPriority != null ? mPriority : Request.Priority.IMMEDIATE;
            }

        };

        System.setProperty("http.keepAlive", "false");
        postRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
        //Log.v(TAG, "-checkTransaction transactionUUID: " + this.transactionUuid);
    }

    private void showOfflineBox() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _confirmPaymentButton.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Get the value of a String and Separates it on chunks
     *
     * @param original String to be spliced
     * @param length length of the spliced chunks
     * @param separator what will be between the chunks
     * @return Returns a HashMap with the integer value of the spliced chunks and the strings value
     *          already with the separator
     */
    private static HashMap<Integer, String> chunk_split(String original, int length, String separator) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(original.getBytes());
        int n = 0;
        int chunks = 0;
        byte[] buffer = new byte[length];
        StringBuilder result = new StringBuilder();
        while ((n = bis.read(buffer)) > 0) {
            for (byte b : buffer) {
                result.append((char) b);
            }
            Arrays.fill(buffer, (byte) 0);
            result.append(separator);
            chunks++;
        }
        HashMap<Integer, String> retur = new HashMap<>();
        Integer integer = chunks;
        retur.put(integer, result.toString());

        //Log.v(TAG, "chunk_split " + integer + " result " + result);
        return retur;
    }

    /**
     * Generates a bitmap image to reduce the size of a QRCode with accuracy
     * @param frameNumber
     * @param arrayList
     * @param finalSmallerDimension
     * @return
     */
    private BitmapDrawable getDrawableForFrameNumber(int frameNumber, ArrayList<String> arrayList,
                                                     int finalSmallerDimension) {

        String data = arrayList.get(frameNumber);
        final QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(
                data,
                BarcodeFormat.QR_CODE.toString(),
                finalSmallerDimension);
        Bitmap bitmap = null;
        try {
            bitmap = qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return new BitmapDrawable(getResources(), bitmap);
    }
}