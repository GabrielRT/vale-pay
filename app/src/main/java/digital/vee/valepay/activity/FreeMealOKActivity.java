package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;


/**
 * Show a message after the user indicates a new Merchant to be part of our client roster
 *
 * Created by Sérgio Brocchetto on 26/09/2016.
 */

public class FreeMealOKActivity extends AppCompatActivity {

    private static final String TAG = "FreeMealOKActivity";
    @Bind(R.id.btn_ok)
    Button _okButton;
    private long lastClickTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_meal_ok);
        ButterKnife.bind(this);

        setTextView();

        setClickListener();
    }

    private void setTextView() {
        TextView tv = (TextView) findViewById(R.id.invite_join_text);
        tv.setText(Html.fromHtml(getString(R.string.congrats) + getString(R.string.invite_join_vee)));
    }

    private void setClickListener() {
        _okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();

                finish();
                startActivity(new Intent(FreeMealOKActivity.this, UseActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed(); intentional comment to do not back
    }
}
