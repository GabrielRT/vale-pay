package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.snappydb.SnappydbException;

import org.hashids.Hashids;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.StringRequestCustom;

public class GiftCardActivity extends BaseActivity{

    private static final String DATA_GIFT_CARD = BuildConfig.SERVER_URL + "rest/private/giftcard/use";
    private static final String CODE_FROM = "BCDFGHJKLMNPQRST";
    private static final String CODE_TO = "0123456789ABCDEF";
    @Bind(R.id._code)
    EditText _code;
    @Bind(R.id.tv_instruct)
    TextView tv_instruct;
    @Bind(R.id.btn_redeem)
    Button btn_redeem;
    @Bind(R.id.layout_invisible)
    RelativeLayout layout_invisible;
    @Bind(R.id.v_symbol)
    TextView v_symbol;
    @Bind(R.id.v_symbol_2)
    TextView v_symbol_2;
    @Bind(R.id.tv_redeemed_balance)
    TextView redeemed_balance;
    @Bind(R.id.tv_redeemed)
    TextView tv_redeemed_text;
    @Bind(R.id.tv_balance)
    TextView current_balance;
    @Bind(R.id.tv_current_balance)
    TextView tv_current_balance;

    private int statusCode;
    private String message;
    private double amountCharged;
    private double currentBalance;
    private long lastClickTimeFindRes = 0;
    private String token;
    private boolean shortcut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gift_card);

        ButterKnife.bind(this);

        doLogin();

        setFonts();

        setToolbar();

        setClickListener();
    }

    private void setClickListener() {
        btn_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeFindRes < 3000) {
                    return;
                }
                lastClickTimeFindRes = SystemClock.elapsedRealtime();

                createProgressDialog(GiftCardActivity.this);

                if(validate()) {
                    makeRequest();
                } else {
                    showHexError();
                }
            }
        });
    }

    private void makeRequest() {
        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, DATA_GIFT_CARD,
                getResponse(), getError(), mRequestBody()) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();

                if (getToken().isEmpty() || getToken() == null) {
                    doLogin();
                    try {
                        token = SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY);
                    } catch (SnappydbException e){
                        e.printStackTrace();
                    }
                } else{
                    token = getToken();
                }

                params.put("Authorization", "Token " + token);

                params.put("Accept-Language", "pt_br");

                return params;
            }
        };

        postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());

        VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);

    }

    private void setFonts(){
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        MaskEditTextChangedListener maskGift = new MaskEditTextChangedListener(
                "####-####-####", _code);

        tv_instruct.setTypeface(type_roboto_bold);

        tv_redeemed_text.setTypeface(type_roboto_regular);

        _code.setTypeface(type_roboto_regular);
        _code.addTextChangedListener(maskGift);

        layout_invisible.setVisibility(View.GONE);

        tv_current_balance.setTypeface(type_roboto_bold);
        current_balance.setTypeface(type_roboto_bold);
        v_symbol.setTypeface(type_roboto_bold);
        v_symbol_2.setTypeface(type_roboto_bold);

        redeemed_balance.setTypeface(type_roboto_bold);
    }

    private Response.Listener<String> getResponse(){
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    message = jsonObject.getString("message");
                    statusCode = jsonObject.getInt("statusCode");
                    if (statusCode != 1) {
                        showFail();
                    } else{
                        amountCharged = jsonObject.getDouble("amountCharged");
                        currentBalance = jsonObject.getDouble("currentBalance");
                        showSucess();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dismissDialog();
            }
        };
        return responseListener;
    }

    private Response.ErrorListener getError(){
        return error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                dismissDialog();
                showTimeoutDialog(this);
            } else {
                showMessage(getString(R.string.error_processing_request));
                dismissDialog();
            }
        };
    }

    private String mRequestBody(){
        JSONObject jsonObject = new JSONObject();
        try{

            // get code user typed and personal id ( same as entityId)
            final String code = _code.getText().toString().replaceAll("-", "");
            final String personalId = SnappyDBHelper.get(getApplicationContext(), "entityId");

            //create new hash object with personal id
            Hashids hashIds = new Hashids(personalId);
            String codex = charToHex(code);

            // create a long type variable with the code the user typed
            long codeLong = Long.valueOf(codex, 16).longValue();

            // generate the hash to be sent
            String giftCardCodeHash = hashIds.encode(codeLong);

            // put hash on json to be sent
            jsonObject.put("giftCardCode", giftCardCodeHash);

        } catch (JSONException | SnappydbException e){
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    /**
     * Mount a new String on the string builder with the hex characters matching the index of the typed
     * code when it matches the CODE_FROM variable.
     *
     * if a digit if invalid, it will show error to the user
     * @param code
     * @return
     */
    private String charToHex(String code) {
        int index;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < code.length(); i++) {
            index = CODE_FROM.indexOf(Character.toUpperCase(code.charAt(i)),0);
            stringBuilder.append(CODE_TO.charAt(index));
        }
        return stringBuilder.toString();
    }

    private boolean validate(){
        if(_code.getText().toString().length() < 14){
            return false;
        }

        for(char c: _code.getText().toString().replaceAll("-","").toCharArray()){
            if(!CODE_FROM.contains(String.valueOf(Character.toUpperCase(c)))){
                return false;
            }
        }
        return true;
    }

    /**
     * Tell user the code is invalid when a digit is non existent in our CODE_TO variable
     */
    private void showHexError() {
        _code.requestFocus();
        _code.setError(getString(R.string.invalid_giftcard));
        dismissDialog();
    }


    private void showSucess() {
        setInfo();
        showMessage(message);
        dismissDialog();
    }

    private void setInfo() {
        layout_invisible.setVisibility(View.VISIBLE);
        redeemed_balance.setText(setDecimal(amountCharged));
        current_balance.setText(setDecimal(currentBalance));
        btn_redeem.setText(R.string.go_back);

        setSecondClickListener();
    }

    private void setSecondClickListener() {
        btn_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void showFail() {
        showMessage(message);
    }

    private void redirRecharge() {
        startActivity(new Intent(GiftCardActivity.this, RechargeActivity.class));
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
