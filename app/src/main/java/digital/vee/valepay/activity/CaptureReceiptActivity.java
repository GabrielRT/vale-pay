package digital.vee.valepay.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import digital.vee.valepay.R;
import digital.vee.valepay.barcode.BarcodeGraphic;
import digital.vee.valepay.barcode.BarcodeGraphicTracker;
import digital.vee.valepay.barcode.BarcodeTrackerFactory;
import digital.vee.valepay.barcode.CameraSourcePreview;
import digital.vee.valepay.barcode.GraphicOverlay;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;

/**
 * Use Google Vision API to scan and save QR Code content on SnappyDB and take pictures and save the
 * bytes on a file in the internal storage directory. A small image containing the selected card is
 * displayed to the user, being one of the following: meal, grocery, fuel or culture.
 * <p>
 * Created by silvio on 03/04/18.
 */

public class CaptureReceiptActivity extends BaseActivity implements BarcodeGraphicTracker.BarcodeDecoder {
    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private static final int QR_CODE_SCANNED = 2490;
    private static final int QR_MODE = 0;
    private static final int CAPTURE_MODE = 1;

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private String imagePath;
    private RelativeLayout instructions;
    private int controlVar;
    private boolean instructionsAreGone = false;
    private CountDownTimer countDownTimer;
    private android.support.v7.app.AlertDialog alertDialog;
    /**
     * Method called when the barcode is decoded, the code is saved on snappyDB for data persistence
     * reasons. After scanning and saving it once, it will no longer be saved.
     *
     * @param itemId
     * @param barcode
     */
    @Override
    public void decodeBarcode(int itemId, Barcode barcode) {
        if (instructionsAreGone) {
            if (controlVar != QR_CODE_SCANNED) {
                if (barcode.rawValue != null && !barcode.rawValue.isEmpty()) {

                    String qrcode = barcode.rawValue;

                    try {
                        SnappyDBHelper.put(this, "qrcode", qrcode);

                        runOnUiThread(() -> {
                            showQRcapturedInfo();
                            countDownTimer.cancel();
                            setCaptureMode(CAPTURE_MODE);
                        });

                        controlVar = QR_CODE_SCANNED;
                    } catch (SnappydbException e) {
                        showMessage(getString(R.string.qr_failure));
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * This method switches between QR scanning and photo capturing.
     *
     * @param mode One of two available modes to be used
     */
    private void setCaptureMode(int mode) {
        RelativeLayout proceedLayout = findViewById(R.id.proceed_layout);
        TextView btnText = findViewById(R.id.bottom_scanner_label_generate_token_text);
        ImageView qrAim;

        switch (mode) {
            case QR_MODE:
                qrAim = findViewById(R.id.qr_aim);
                qrAim.setVisibility(View.VISIBLE);
                btnText.setText(R.string.cancel_qr);
                enableQRInstruction();

                proceedLayout.setOnClickListener(view -> {
                    setCaptureMode(CAPTURE_MODE);
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                });

                break;
            case CAPTURE_MODE:
                qrAim = findViewById(R.id.qr_aim);

                if (qrAim.getVisibility() == View.VISIBLE) {
                    qrAim.setVisibility(View.GONE);
                }

                enableReceiptInstruction();

                btnText.setVisibility(View.VISIBLE);
                btnText.setText(R.string.capture);
                proceedLayout.setOnClickListener(v ->
                        mCameraSource.takePicture(null, bytes -> {
                            // Write picture to internal storage so we can use later
                            FileOutputStream outputStream;
                            File image = new File(getFilesDir(), "FiscalCupom.jpeg");
                            // this image path will be passed on through the intent so the file can be found
                            imagePath = image.getAbsolutePath();
                            try {
                                outputStream = openFileOutput("FiscalCupom.jpeg", Context.MODE_PRIVATE);
                                outputStream.write(bytes);
                                outputStream.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            startPreview();
                        })
                );
                break;
        }
    }

    private void enableReceiptInstruction() {
        TextView instruction = findViewById(R.id.tv_scan_receipt);
        instruction.setVisibility(View.VISIBLE);
        instruction.setText(getString(R.string.frame_receipt));
    }
    private void enableQRInstruction() {
        TextView instruction = findViewById(R.id.tv_scan_receipt);
        instruction.setVisibility(View.VISIBLE);
        instruction.setText(getString(R.string.frame_qr));
        ImageView qrAim = findViewById(R.id.qr_aim);
        qrAim.bringToFront();
        qrAim.setVisibility(View.VISIBLE);
    }

    private void showQRcapturedInfo() {
        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(CaptureReceiptActivity.this).create();

        alertDialog.setTitle(getString(R.string.menu_text_refund));
        alertDialog.setMessage(getString(R.string.qr_success));
        alertDialog.setCanceledOnTouchOutside(false);
        // Alert dialog button
        alertDialog.setCancelable(false);
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK);
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                (dialog, which) -> {
                    //enable Capture button
                    dialog.dismiss();// use dismiss to cancel alert dialog
                });
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_capture_receipt);

        setCustomActionBar();

        loadInstructions();

        mPreview = findViewById(R.id.preview);
        //noinspection unchecked
        mGraphicOverlay = findViewById(R.id.graphicOverlay);

        makeListener();

        // thread that initiate the camera
        new Thread(() -> {
            deleteQrCode(this);
            // Check for the camera permission before accessing the camera.  If the
            // permission is not granted yet, request permission.
            int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            if (rc == PackageManager.PERMISSION_GRANTED) {
                createCameraSource();
            } else {
                requestCameraPermission();
            }
        }).start();
    }

    private void showQRdialog() {
        alertDialog = new android.support.v7.app.AlertDialog.Builder(CaptureReceiptActivity.this).create();

        alertDialog.setTitle(getString(R.string.menu_text_refund));
        alertDialog.setMessage(getString(R.string.does_receipt_has_qr));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        alertDialog.setOnKeyListener((dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK);

        // Alert dialog button
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                (dialog, which) -> {
                    //start disabled/hidden Capture button, so re-enable once QR is read
                    //show QR instructions
                    enableQRInstruction();
                    setCaptureMode(QR_MODE);
                    dialog.dismiss();// use dismiss to cancel alert dialog
                    instructionsAreGone = true;

                    countDownTimer = new CountDownTimer(30000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {}

                        // When count reaches 0 load
                        public void onFinish() {
                            instructionsAreGone = false;
                            showQRdetectionTimeExpired();
                        }
                    };
                    countDownTimer.start();
                });

        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                (dialog, which) -> {
                    //enable Capture button
                    setCaptureMode(CAPTURE_MODE);
                    dialog.dismiss();// use dismiss to cancel alert dialog
                });
        alertDialog.show();
    }

    private void showQRdetectionTimeExpired() {
        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(CaptureReceiptActivity.this).create();

        alertDialog.setTitle(getString(R.string.menu_text_refund));
        alertDialog.setMessage(getString(R.string.qr_code_not_found));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        // Alert dialog button
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK);
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                (dialog, which) -> {
                    //enable Capture button
                    setCaptureMode(CAPTURE_MODE);
                    dialog.dismiss();// use dismiss to cancel alert dialog
                });
        alertDialog.show();
    }

    /**
     * This method set a drawable switching the selected card.
     */
    private void setCardImage() {
        try {
            String cardSelected = SnappyDBHelper.get(this, "walletCode");
            ImageView cardImage = findViewById(R.id.card_imageView);
            cardImage.bringToFront();
            switch (cardSelected) {
                // meal
                case "v1":
                    cardImage.setImageDrawable(getResources().getDrawable(R.drawable.refund_icon_meal));
                    break;
                // grocery
                case "v2":
                    cardImage.setImageDrawable(getResources().getDrawable(R.drawable.refund_icon_grocery));
                    break;
                // fuel
                case "v3":
                    cardImage.setImageDrawable(getResources().getDrawable(R.drawable.refund_icon_fuel));
                    break;
                // culture
                case "v4":
                    cardImage.setImageDrawable(getResources().getDrawable(R.drawable.refund_icon_culture));
                    break;
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set an imageView in front on the camera source and load the PNG containing refund instructions
     */
    private void loadInstructions() {
        instructions = findViewById(R.id.instructions_layout);
        ImageView imageView = findViewById(R.id.instructions_view);
        instructions.bringToFront();
        instructions.bringChildToFront(imageView);

        Picasso.get().load(R.drawable.instructions).into(imageView);
    }

    /**
     * Make the click listener for the buttons of the screen, while the instructionsfullsize image is showing
     * the capture button won't be available even so is being shown to the user.
     */
    private void makeListener() {
        instructions.setOnClickListener(view -> {
                    view.setVisibility(View.GONE);
                    showQRdialog();
                }
        );
    }

    /**
     * Start the activity responsible for the preview of the picture taken, the imagepath
     * will be sent to the activity can find the image on the internal storage.
     */
    private void startPreview() {
        ActivityUtils.newActivity(this, new Intent(CaptureReceiptActivity.this, ConfirmImageActivity.class)
                .putExtra("path", imagePath));
        finish();
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
    }

    @WorkerThread
    @SuppressLint("InlinedApi")
    private void createCameraSource() {
        Context context = getApplicationContext();

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay,
                this);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            //Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, "Low storage", Toast.LENGTH_LONG).show();
                //Log.w(TAG, "Low storage");
            }
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        mCameraSource = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setRequestedPreviewSize(width, height)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .setRequestedFps(15.0f)
                .build();

        startCameraSource();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    @WorkerThread
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
                runOnUiThread(() -> {
                    try {
                        mPreview.start(mCameraSource, mGraphicOverlay);
                    } catch (IOException e) {
                        e.printStackTrace();
                        //Log.e(TAG, "Unable to start camera source.", e);
                        mCameraSource.release();
                        mCameraSource = null;
                    }
                    setCardImage();
                });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (mPreview != null) {
            mPreview.release();
        }
        System.gc();
    }

    // Get the result of the permission request
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // we have permission, so create the camerasource

            new Thread(this::createCameraSource).start();

            return;
        }

        DialogInterface.OnClickListener listener = (dialog, id) -> ActivityUtils.exitActivity(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setMessage(R.string.permission_camera_denied)
                .setPositiveButton(R.string.ok, listener)
                .setCancelable(false)
                .setOnKeyListener((dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK)
                .show();
    }

    /**
     * Set ActionBar with custom back arrow drawable
     */
    public void setCustomActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }
    }

    @Override
    public void onBackPressed() {
        ActivityUtils.exitActivity(this);
    }


    @Override
    public boolean onSupportNavigateUp() {
        ActivityUtils.exitActivity(this);
        return true;
    }



}
