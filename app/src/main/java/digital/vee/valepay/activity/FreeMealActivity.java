package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.snappydb.SnappydbException;

import org.hashids.Hashids;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;


/**
 * Take input from user wanting to invite new Merchants to be part of our client roster
 *
 * Created by Sérgio Brocchetto on 22/09/2016.
 */

public class FreeMealActivity extends BaseActivity {

    private static final String TAG = "FreeMealActivity";

    public static final String RESTAURANT_NAME_EXTRA = "restaurantName";
    public static final String RESTAURANT_PHONE_EXTRA = "restaurantPhone";
    public static final String RESTAURANT_CODE_EXTRA = "restaurantCode";

    public String token;

    @Bind(R.id.share_button)
    Button shareButton;

    private long lastClickTimeLoginSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_meal);

        ButterKnife.bind(this);

        Preferences.init(getApplicationContext());

        doLogin();

        token = getToken();

        setToolbar();

        setTextView();

        setClickListener();
    }

    /**
     * Call for submit method when user clicks the submit button
     */
    private void setClickListener() {
        shareButton.setOnClickListener(v -> {
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastClickTimeLoginSubmit < 3000) {
                return;
            }

            lastClickTimeLoginSubmit = SystemClock.elapsedRealtime();
            shareAction();
        });
    }

    /**
     * Set text on screen
     */
    private void setTextView(){
        TextView tv = findViewById(R.id.text_invite);
        tv.setTypeface(Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF));
        TextView tvB = findViewById(R.id.text_invite_b);
        tvB.setText(Html.fromHtml(getString(R.string.invite_restaurant_part_b)));

        String identify = "";
        long idEntity;
        try {
            idEntity = SnappyDBHelper.getLong(getApplicationContext(), "idEntityNumber");
            if (idEntity > 0L) {
                Hashids hashids = new Hashids("invite", 8, "0123456789bcdfghjlmnkpqrstvxzBCDFGHJLMNKPQRSTVXZ");
                identify = hashids.encode(idEntity);
            }
        } catch (SnappydbException e) {
            Log.w(TAG, e.getMessage());
        }

        this.shareButton.setText(identify);
    }

    private void shareAction() {
        String identify = this.shareButton.getText().toString();
        String message = String.format(getString(R.string.invite_share_message), identify);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.invite_share_send)));
    }

    @Override
    public void onBackPressed() {
        ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
