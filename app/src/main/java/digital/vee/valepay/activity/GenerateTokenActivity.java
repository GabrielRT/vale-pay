package digital.vee.valepay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.data.remote.TokenService;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.ApiUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.StringRequestCustom;

import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * Generates the 6 digit token used in transactions and keep track of the 60 second timer a token
 * has before it expires
 *
 * Created by sergiohlb on 16/08/2017.
 */

public class GenerateTokenActivity extends BaseActivity {
    private static final String TAG = "GenerateToken";
    private static final int PAYMENT_REQUEST_RESPONSE_OK = 1;
    private static final int PAYMENT_REQUEST_RESPONSE_TOKEN_INVALID = 2;
    private static final int PAYMENT_REQUEST_RESPONSE_TOKEN_EXPIRED = 3;
    private static final int PAYMENT_REQUEST_RESPONSE_NOT_FOUND = 4;
    private static final int PERIOD = 3000;

    private String token;
    private static final String URL = BuildConfig.SERVER_URL + "rest/private/payment/generatePaymentToken";
    private String urlUpdate = BuildConfig.SERVER_URL + "rest/private/payment/findByToken";
    private volatile Timer t = null;
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    @Bind(R.id.n1)
    TextView n1;

    @Bind(R.id.n2)
    TextView n2;

    @Bind(R.id.n3)
    TextView n3;

    @Bind(R.id.n4)
    TextView n4;

    @Bind(R.id.n5)
    TextView n5;

    @Bind(R.id.n6)
    TextView n6;

    @Bind(R.id.timer)
    TextView timer;

    @Bind(R.id.displayNumberText)
    TextView displayNumberText;

    @Bind(R.id.countdownbar)
    ProgressBar countdownbar;

    @Bind(R.id.cancelText)
    TextView cancelText;

    private static final int DELAY = 5000;
    private static final long MILLISSECONDS = 1000;
    private long lastClickTimeLogin = 0;
    private CountDownTimer countDownTimer;
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_token);

        createProgressDialog(GenerateTokenActivity.this, true, dialog -> {
            ActivityUtils.exitActivity(this, new Intent(GenerateTokenActivity.this, ContinuousCaptureActivity.class));
            finish();
        });

        ButterKnife.bind(this);

        setToolbar();

        registerReceiver(mMessageReceiver, new IntentFilter("CloseActivity"));

        TokenService paymentTokenService = ApiUtils.getTokenService();

        token = getToken();

        loadToken();

        setClickListener();
    }

    private void checkTransaction() {
        Log.e(TAG, "checkTransaction");
        if (t != null) {
            t.cancel();
            t.purge();
        }
        if(errorListener == null){
            makeError();
        }
        if (responseListener == null) {
            makeResponse();
        }
        t = new Timer();

        t.scheduleAtFixedRate(getTimerTask(), DELAY, PERIOD);
    }

    /**
     * make the body inside a string builder putting a json with the key token and the value being
     * the current displayed token.
     */

    private String makeBody() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("paymentToken", grabToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    /**
     * Take the payment token to append in a string to be returned.
     */
    private String grabToken() {
        String returnToken = "";

        returnToken += n1.getText().toString();
        returnToken += n2.getText().toString();
        returnToken += n3.getText().toString();
        returnToken += n4.getText().toString();
        returnToken += n5.getText().toString();
        returnToken += n6.getText().toString();

        return returnToken;
    }

    private void makeResponse() {
        Log.e(TAG, "makeResponse");
        responseListener = response -> {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("statusCode")) {
                    switch (jsonObject.getInt("statusCode")) {
                        case PAYMENT_REQUEST_RESPONSE_OK:
                            t.cancel();
                            t.purge();
                            startActivity(new Intent(GenerateTokenActivity.this,
                                    PayScanningActivity.class)
                                    .putExtra("transaction", jsonObject.getString("payment")));
                            finish();
                            removePaymentToken();
                            break;
                        case PAYMENT_REQUEST_RESPONSE_TOKEN_EXPIRED:
                            //loadToken();
                            //checkTransaction();
                            break;
                        case PAYMENT_REQUEST_RESPONSE_TOKEN_INVALID :
                            //loadToken();
                            //checkTransaction();
                            break;
                        default:
                            // do nothing so the requests keep going
                            break;
                    }

                }
            } catch (JSONException e) {
                showMessage(e.getMessage());
            }
        };
    }

    private void makeError() {
        errorListener = volleyError -> {
            /* when trying to find a transaction an error should not stop the process, just have
               the timer task going*/
        };
    }

    private TimerTask getTimerTask() {
        Log.e(TAG, "getTimerTask");
        return new TimerTask() {
            @Override
            public void run() {
                StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, urlUpdate,
                        responseListener, errorListener, makeBody())  {
                        Priority mPriority;

                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Accept-Language", "pt_br");
                        params.put("Authorization", "Token " + token);
                        params.put("apiVersion", "2");
                        return params;
                    }

                    @Override
                    public Priority getPriority() {
                        return mPriority != null ? mPriority : Priority.NORMAL;
                    }

                };
                postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
                VolleySingleton.getInstance(GenerateTokenActivity.this).addToRequestQueue(postRequest);
            }
        };
    }

    private void setClickListener() {
        cancelText.setOnClickListener(v -> {

            if (SystemClock.elapsedRealtime() - lastClickTimeLogin < 3000) {
                return;
            }

            lastClickTimeLogin = SystemClock.elapsedRealtime();
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            redirUse();
        });
    }

    /**
     * Get the token from the server in a JSONObject and puts it to a String to be displayed
     * Manages the 60 second timer before loading another token
     */
    public void loadToken() {
        Log.e(TAG, "loadToken");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("walletFrom", SnappyDBHelper.get(getApplicationContext(), LoginUtil.WALLET_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL,
                new com.android.volley.Response.Listener<String>() {
                    String response;

                    @Override
                    public void onResponse(String response) {
                        try {
                            this.response = response;
                            JSONObject jsonResponse = null;
                            try {
                                jsonResponse = new JSONObject(response);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                            String paymentToken = null;
                            try {
                                if (jsonResponse != null) {
                                    paymentToken = jsonResponse.getString("paymentToken");
                                    //save to snappydb
                                    String pt = SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken");
                                    Log.e(TAG,
                                            "PaymentToken before: " + pt);
                                    SnappyDBHelper.putEncrypted(getApplicationContext(), "paymentToken", paymentToken);
                                    Log.e(TAG, "PaymentToken put: " + paymentToken);
                                    String pt2 = SnappyDBHelper.getEncrypted(getApplicationContext(), "paymentToken");
                                    Log.e(TAG,
                                            "PaymentToken after: " + pt2);
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            //Log.d(TAG, "--- paymentToken: "+ paymentToken);

                            char[] digits = new char[0];
                            if (paymentToken != null) {
                                digits = paymentToken.toCharArray();
                            }

                            n1.setText(valueOf(digits[0]));
                            n2.setText(valueOf(digits[1]));
                            n3.setText(valueOf(digits[2]));
                            n4.setText(valueOf(digits[3]));
                            n5.setText(valueOf(digits[4]));
                            n6.setText(valueOf(digits[5]));

                            long ttl = 0;
                            try {
                                ttl = jsonResponse.getInt("ttl") * MILLISSECONDS;
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                            //dismiss the dialog when to token is displayed to the user
                            dismissDialog();

                            //Log.d(TAG, "--- response.body().getTtl(): "+ ttl);

                            timer.setText(valueOf(ttl));

                            final long finalTtl = ttl;
                            countDownTimer = new CountDownTimer(finalTtl, MILLISSECONDS) {

                                public void onTick(long millisUntilFinished) {
                                    timer.setText(format("%d", millisUntilFinished / MILLISSECONDS));

                                    int progress = 100 - (int) ((100 * millisUntilFinished / 1000) / (finalTtl / 1000));
                                    countdownbar.setProgress(100 - progress);
                                }

                                // When count reaches 0 load another token
                                public void onFinish() {
                                    timer.setText("0");
                                    countdownbar.setProgress(0);
                                    //removePaymentToken();
                                    loadToken();
                                    //checkTransaction();
                                }
                            };
                            countDownTimer.start();
                        } catch (Exception e) {
                            Log.v(TAG, "Exception: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, error -> {
                    /*Handle Timeout errors*/
                    if (error.getClass().equals(TimeoutError.class)) {
                        // dismiss progress dialog
                        dismissDialog();
                        // show dialog indicating error while talking to server
                        showTimeoutDialog(this, new Intent(GenerateTokenActivity.this, UseActivity.class));
                    }
                    Log.v(TAG, "Exception: " + error);
                    error.printStackTrace();
                }, jsonObject.toString()) {
            Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                params.put("Authorization", "Token " + token);
                params.put("apiVersion", "2");
                return params;
            }

            @Override
            public Priority getPriority() {
                return mPriority != null ? mPriority : Priority.NORMAL;
            }

        };
        postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
    }

    /**
     * Cancel the token and finishes this activity, returning to the use activity then
     */
    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed");
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        redirUse();
    }

    /**
     * Cancel the token and finishes this activity, returning to the use activity then
     *
     * @param item Menu item pressed, the only one being the white arrow
     * @return true if arrow was pressed and user wants to go back
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Log.e(TAG, "onOptionsItemSelected home");
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
                redirUse();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Go back to use activity with the drawerMenu closed
     */
    public void redirUse() {
        Log.e(TAG, "redirUse");
        removePaymentToken();
        ActivityUtils.exitActivity(this, new Intent(this, ContinuousCaptureActivity.class));
        finish();
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
        if (t != null) {
            t.cancel();
            t.purge();
        }
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        checkTransaction();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        removePaymentToken();
        if (t != null) {
            t.cancel();
            t.purge();
        }
        unregisterReceiver(mMessageReceiver);
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    private void removePaymentToken() {
        Log.e(TAG, "removePaymentToken");
        try {
            SnappyDBHelper.putEncrypted(getApplicationContext(), "paymentToken", "");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
}
