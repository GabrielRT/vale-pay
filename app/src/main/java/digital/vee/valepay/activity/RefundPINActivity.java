package digital.vee.valepay.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.google.firebase.crash.FirebaseCrash;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.FingerPrintHelper;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;
import digital.vee.valepay.util.crypto.CryptoUtil;


import static digital.vee.valepay.util.LoginUtil.FONTS_ROBOTO_BOLD_TTF;
import static digital.vee.valepay.util.LoginUtil.FONTS_ROBOTO_REGULAR_TTF;

/**
 * This class use the reactivate pin xml from another activity and just changes it's texts.
 * Called when the user confirms the refund data and has to send a TSC.
 * <p>
 * Created by silvio on 16/04/18.
 */

public class RefundPINActivity extends BaseActivity {

    @Bind(R.id.btn_thankyou_ok)
    Button submitButton;
    @Bind(R.id.input_confirmation_code)
    EditText tvPin;
    @Bind(R.id.finger_print_text)
    LinearLayout fingerPrintText;
    @Bind(R.id.balance_value_purchase_text)
    TextView transactionValue;
    @Bind(R.id.balance_header)
    TextView balanceHeader;

    // These two text views will be Gone for this activity
    @Bind(R.id.restaurant_name)
    TextView restaurantName;
    @Bind(R.id.last_update)
    TextView lastUpdate;
    @Bind(R.id.last_update_text)
    TextView lastUpdateText;
    private static final String CONFIRM_URL = BuildConfig.SERVER_URL + "rest/private/refund/confirm";
    private static final String FIND_URL = BuildConfig.SERVER_URL + "rest/private/refund/find";
    private static final int REFUND_RESPONSE_OK = 1;
    private static final int REFUND_RESPONSE_NOT_FOUND = 2;
    private static final int CONFIRM_REFUND_RESPONSE_TRANSACTION_REJECTED = 3;
    private static final int CONFIRM_REFUND_RESPONSE_TRANSACTION_NOT_CREATED = 4;

    private CryptoUtil cryptoUtil;
    private String value = "";
    private String pinCode;
    private static String uuid;
    private static volatile Timer t = null;

    private FingerPrintHelper fingerPrintHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pay_reversal);

        ButterKnife.bind(this);

        restaurantName.setVisibility(View.GONE);
        lastUpdate.setVisibility(View.GONE);
        lastUpdateText.setVisibility(View.GONE);
        displayInfo();

        makeListeners();

        setToolbar();

        enableFingerPrintListening();
    }

    private void displayInfo(){
        Typeface typeRobotoBold = Typeface.createFromAsset(getAssets(), FONTS_ROBOTO_BOLD_TTF);
        Typeface typeRobotoRegular = Typeface.createFromAsset(getAssets(), FONTS_ROBOTO_REGULAR_TTF);
        TextView veeSymbol = findViewById(R.id.v_symbol_header);

        String value = String.valueOf(getIntent().getDoubleExtra("value", 0.0));
        transactionValue.setText(String.format("%s %s", "$", setDecimal(Double.parseDouble(value))));
        transactionValue.setTypeface(typeRobotoBold);
        String balance = "";
        try {
            balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        balanceHeader.setText(setDecimal(Double.parseDouble(balance)));
        balanceHeader.setTypeface(typeRobotoRegular);
        veeSymbol.setTypeface(typeRobotoRegular);
        submitButton.setTypeface(typeRobotoBold);
        tvPin.requestFocus();
    }


    /**
     * The layout used in this activity is recycled from another activity, so the text must be changed
     * to match the activity's need
     */

    private void makeListeners() {
        MaskEditTextChangedListener maskGift = new MaskEditTextChangedListener(
                "####", tvPin);

        tvPin.addTextChangedListener(maskGift);

        submitButton.setOnClickListener(v -> {
            if (validate()) {
                makeTSC();
                createProgressDialog(this);
            }
        });
    }

    private boolean validate() {
        pinCode = tvPin.getText().toString();
        int pinCodeLength = pinCode.length();

        if (pinCodeLength < 4) {
            tvPin.setError(getString(R.string.error_pin_code));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Make the tsc string and pass it on to send transaction method to be put in request
     */
    private void makeTSC() {

        String walletEmployeeFrom = "", entityId = "";
        uuid = "";

        try {
            entityId = SnappyDBHelper.get(this, "entityId");
            walletEmployeeFrom = SnappyDBHelper.get(getApplicationContext(), LoginUtil.WALLET_ID); //default wallet code
            value = String.valueOf(getIntent().getDoubleExtra("value", 0.0));
            uuid = getIntent().getStringExtra("uuid");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        if (!walletEmployeeFrom.equals("") && !entityId.equals("")
                && !value.equals("") && !uuid.equals("")) {
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(value));
            TransactionSignedCode tsc = new TransactionSignedCode(entityId, //entityCode
                    walletEmployeeFrom, //default wallet code
                    amount, //amount
                    pinCode, //pin
                    uuid);

            String toSign = tsc.getSignableTransactionString();

            String[] signature = null;
            try {
                cryptoUtil = new CryptoUtil(getApplicationContext());
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
            try {
                signature = cryptoUtil.signature(toSign.getBytes(),
                        SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.ECDSA_PRIVATE_KEY));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException | SnappydbException e) {
                e.printStackTrace();
            }

            assert signature != null;
            tsc.setSignature(signature[0]);

            sendTransaction(tsc.getTSC());
        }
    }

    private void sendTransaction(String tsc) {
        String token = "";
        try {
            token = SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        String finalToken = token;

        // create request
        StringRequestCustom request = new StringRequestCustom(Request.Method.POST, CONFIRM_URL,
                makeResponse(), makeError(), getBody(tsc)) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();

                if (finalToken.isEmpty()) {
                    FirebaseCrash.log("---ChangePINCodeActivity: invalid token: " + finalToken);
                }

                params.put("Authorization", "Token " + finalToken);

                params.put("Accept-Language", "pt_br");

                return params;
            }
        };
        request.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private Response.ErrorListener makeError() {
        return error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                showTimeoutDialog(this);
            }
            error.printStackTrace();
            dismissDialog();
        };
    }

    /**
     * Same response for find and confirm, but code is written so that in the future it's easier to change
     */
    private Response.Listener<String> makeResponse() {
        return response -> {
            try {
                takeAction(response);
            } catch (JSONException e) {
                e.printStackTrace();
                dismissDialog();
                ActivityUtils.exitActivity(this);
            }
        };

    }

    /**
     * If it is not a find operation, 'find' variable will be false. The same method is called
     * for bot cases and each has it's own switch.
     *
     * @param response Json response in string format to be read
     * @throws JSONException Exceptions may occur while getting status code or message
     */
    private void takeAction(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);

        String statusCode = jsonObject.getString("statusCode");
        String message = jsonObject.getString("message");

        switch (Integer.valueOf(statusCode)) {
            case REFUND_RESPONSE_OK:
                redirRefundOk();
                Preferences.putBoolean(MainActivity.REFUND_CONFIRM, false);
                break;
            case REFUND_RESPONSE_NOT_FOUND:
                Preferences.putBoolean(MainActivity.REFUND_CONFIRM, false);
                destroySnappyRefund();
                dismissDialog();
                ActivityUtils.exitActivity(this);
                showMessage(message);
                break;
            case CONFIRM_REFUND_RESPONSE_TRANSACTION_REJECTED:
                Preferences.putBoolean(MainActivity.REFUND_CONFIRM, false);
                destroySnappyRefund();
                dismissDialog();
                ActivityUtils.exitActivity(this);
                showMessage(message);
                break;
            case CONFIRM_REFUND_RESPONSE_TRANSACTION_NOT_CREATED:
                destroySnappyRefund();
                Preferences.putBoolean(MainActivity.REFUND_CONFIRM, false);
                dismissDialog();
                ActivityUtils.exitActivity(this);
                showMessage(message);
                break;
        }
    }

    private void redirRefundOk() {
        destroySnappyRefund();

        new AsyncFindRefund(this).execute();
    }

    private void destroySnappyRefund() {
        try {
            SnappyDBHelper.del(this, "receipt");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the user has internet connection and opens a dialog to warn him
     */
    private void enableFingerPrintListening() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            fingerPrintHelper = new FingerPrintHelper(getSystemService(FingerprintManager.class), this,
                    () -> fillPass());

            //get pin
            if (fingerPrintHelper.isFingerprintAuthAvailable()) {
                String pin = null;
                try {
                    pin = SnappyDBHelper.getEncrypted(this, "pin");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                //if pin has already saved
                if (pin != null && !pin.isEmpty()) {
                    //show fingerprint input info
                    fingerPrintText.setVisibility(View.VISIBLE);
                    //wait for fingerprint information
                    fingerPrintHelper.startListening(null);
                }

            } else {
                showKeyboard();
            }
        } else {
            showKeyboard();
        }
    }

    private void showKeyboard() {
        tvPin.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(tvPin, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void fillPass() {
        String pin = null;
        try {
            pin = SnappyDBHelper.getEncrypted(this, "pin");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        tvPin.setText(pin);
        submitButton.callOnClick();
    }

    private String getBody(String tcs) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("refundUUID", uuid);
            jsonObject.put("tscString", tcs);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityUtils.exitActivity(this, new Intent(RefundPINActivity.this, RefundInformationActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this, new Intent(RefundPINActivity.this,
                        RefundInformationActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Format the uuid to be sent in a request
     *
     * @return JSON with the refund uuid in the body
     */
    public static String getUuid() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("refundUUID", uuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (t != null) {
            t.purge();
            t.cancel();
        }
    }


    /**
     * Tries to find the transaction referring to the previous made refund. It has a 60 second limit.
     * Once we are able to check if the transaction is successful or ultimately rejected, we redirect
     * to RefundOK or to home.
     */
    private static class AsyncFindRefund extends AsyncTask<String, Void, String> {
        int control = 0; // after 20 backed calls it will have elapsed 60 seconds

        private WeakReference<Context> contextRef;

        // constructor
        private AsyncFindRefund(Context context) {
            this.contextRef = new WeakReference<>(context);
        }

        @Override
        protected String doInBackground(String... params) {
            findRefund();

            return "EXECUTED";
        }

        /**
         * start the times to Make requests to the find url to check the status of the refund transaction
         */
        private void findRefund() {
            if (t != null) {
                t.cancel();
                t.purge();
            }

            t = new Timer();

            String token = "";
            try {
                token = SnappyDBHelper.getEncrypted(contextRef.get(), LoginUtil.TOKEN_KEY);
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            String finalToken = token;

            StringRequestCustom post = new StringRequestCustom(Request.Method.POST, FIND_URL,
                    responseFind(), errorFind(), getUuid()) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();

                    if (finalToken.isEmpty()) {
                        FirebaseCrash.log("---ChangePINCodeActivity: invalid token: " + finalToken);
                    }

                    params.put("Authorization", "Token " + finalToken);

                    params.put("Accept-Language", "pt_br");

                    return params;
                }
            };

            t.scheduleAtFixedRate(getTimerTask(post), 5000, 3000);

        }

        /**
         * The timer waits 5 seconds before firing first request and after that, fires one every 3 sec
         * after 18 requests, we know it elapsed 60 seconds
         *
         * @return TimerTask handling the requests
         */
        private TimerTask getTimerTask(StringRequestCustom post) {
            return new TimerTask() {
                @Override
                public void run() {
                    if (control < 18) {
                        post.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
                        VolleySingleton
                                .getInstance(contextRef.get())
                                .addToRequestQueue(post);
                        control++;
                    } else {
                        t.cancel();
                        t.purge();
                    }
                }
            };
        }

        private Response.Listener<String> responseFind() {
            return response -> {
                try {
                    findAction(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            };
        }

        private Response.ErrorListener errorFind() {
            return error -> {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(contextRef.get(),
                            contextRef.get().getString(R.string.internet_connection),
                            Toast.LENGTH_SHORT).show();
                }
                error.printStackTrace();
                t.cancel();
                t.purge();
                dismissDialog();
                redirHome(contextRef.get());
            };
        }

        private void findAction(String response) throws JSONException {
            String statusCode;
            String refundStatus;
            JSONObject jsonObject = new JSONObject(response);
            statusCode = jsonObject.getString("statusCode");

            switch (Integer.valueOf(statusCode)) {
                case REFUND_RESPONSE_OK:
                    JSONObject refund = jsonObject.getJSONObject("refund");
                    refundStatus = refund.getString("refundStatus");

                    // Check if refund was rejected
                    if (refundStatus.equalsIgnoreCase("REJECTED")) {
                        // cancel timer so it doesn't try to find the transaction more than once
                        if (t != null) {
                            t.cancel();
                            t.purge();
                        }
                        ActivityUtils.exitActivity(contextRef.get());
                        rejectionToast(refund.getString("rejectReason"));


                    } else if (refundStatus.equalsIgnoreCase("PROCESSING_AUTO_TRANSFER")
                            || refundStatus.equalsIgnoreCase("WAITING_AUTO_TRANSFER")) {
                        // cancel timer so it doesn't try to find the transaction more than once
                        if (t != null) {
                            t.cancel();
                            t.purge();
                        }
                        // if wasn't rejected will redirect to the OK screen
                        dismissDialog();
                        ActivityUtils.newActivity(contextRef.get(), new Intent(contextRef.get(), RefundOKActivity.class)
                                .putExtra("refund", refund.toString())
                                .putExtra("walletFrom", refund.getString("walletFrom")));

                        // using cast to be able to finish activity from it's context
                        ((Activity) contextRef.get()).finish();
                    }
                    break;
                case REFUND_RESPONSE_NOT_FOUND:
                    // ignore the response and keep searching for refund
                    break;
            }
        }

        private void rejectionToast(String message) {
            Toast toast1 = Toast.makeText(
                    contextRef.get(),
                    message,
                    Toast.LENGTH_LONG);
            toast1.show();
        }

    }

}
