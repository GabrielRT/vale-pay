package digital.vee.valepay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.snappydb.SnappydbException;

import org.androidannotations.annotations.sharedpreferences.Pref;

import java.security.KeyStoreException;

import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Created by Sérgio Brocchetto on 05/05/2017.
 *
 * Checks the preferences of the user and if the app is activated.
 * Starts the useActivity as the home page of the app or
 * starts first activation if needed.
 *
 */
public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity";
    private CryptoUtil cryptoUtil;
    public static final String IS_ACTIVATED = "isActivated";
    public static final String TERMS_ACCEPTED = "termsWasAccepted";
    public static final String PIN_CHANGED = "pinWasChanged";
    public static final String PUT_CPF = "cpfWasPut";
    public static final String IS_REACTIVATION = "isReactivation";
    public static final String PIN_REACTIVATION = "hasInsertedPIN";
    public static final String RH_ACTIVATION = "rhActivation";
    public static final String SENT_INFO = "wasInfoSet";
    public static final String REFUND_CONFIRM = "refundConfirm";

    private String IS_FIRST_RUN = "isNewFirstRun";
    //private ProgressDialog progressDialog;
    //TaskStackBuilder intents;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        Log.d(TAG, "Intent: " + getIntent().toString());

        Preferences.init(getApplicationContext());

        SnappyDBHelper.getDatabase(getApplicationContext());

        //FirebaseCrash.log("---Activity created");

        //initialize keystore when starting after activation

        try {
            cryptoUtil = new CryptoUtil(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        if (Preferences.getBoolean(IS_FIRST_RUN)) {
            checkFirstRun();
        }

        boolean pinChanged = Preferences.getBoolean(MainActivity.PIN_CHANGED, false);
        boolean termsAccepted = Preferences.getBoolean(MainActivity.TERMS_ACCEPTED, false);
        boolean sentInfo = Preferences.getBoolean(MainActivity.SENT_INFO, false);
        boolean reactivation = Preferences.getBoolean(MainActivity.IS_REACTIVATION, false);
        boolean activation = Preferences.getBoolean(MainActivity.RH_ACTIVATION, false);
        boolean refundConfirm = Preferences.getBoolean(MainActivity.REFUND_CONFIRM, false);
        if(pinChanged){
            String transaction = getTransaction();
            if(refundConfirm){
                startActivity(new Intent(MainActivity.this, RefundInformationActivity.class));
                finish();
            }
            else if (!transaction.isEmpty()) {
                Intent intent = new Intent(this, PayScanningActivity.class);
                intent.putExtra("transaction", transaction);
                finish();
                startActivity(intent);

            } else {
                String voucher = getVoucher();
                //Log.d(TAG, "voucher: " + voucher);
                if (!voucher.isEmpty()) {
                    finish();
                    startActivity(new Intent(this, VouchersActivity.class));
                }
                else {
                    finish();
                    startActivity(new Intent(this, UseActivity.class));
                }
            }
        }
        else if(!termsAccepted){
            TaskStackBuilder.create(MainActivity.this)
                    .addNextIntentWithParentStack(new Intent(this, AcceptTermsActivity.class))
                   .startActivities();
        }
        else if(sentInfo){
            finish();
            startActivity(new Intent(MainActivity.this, ConfirmPINCodeActivity_.class));
        }
        else if (reactivation){
            reactivationFlow();
        }
        else if (activation){
            activationFlow();
        }
        else {
            TaskStackBuilder.create(MainActivity.this).
                    addNextIntent(new Intent(this, SignupCPFActivity.class))
                    .startActivities();
        }
    }

    private void activationFlow() {
        boolean isActivated = Preferences.getBoolean(MainActivity.IS_ACTIVATED, false);

        if (!isActivated){
            finish();
            startActivity(new Intent(MainActivity.this, ActivateActivity.class));
        }
        else{
            finish();
            startActivity(new Intent(MainActivity.this, ConfirmPINCodeActivity_.class));
        }
    }

    private void reactivationFlow(){
        boolean reactivationPIN = Preferences.getBoolean(MainActivity.PIN_REACTIVATION, false);

        if (!reactivationPIN){
            finish();
            startActivity(new Intent(this, ReactivatePIN.class));
        } else {
            redirHome();
        }
    }

    private void redirHome() {
        startActivity(new Intent(this, UseActivity.class));
        finish();
    }


    /**
     * Check the extra to see if app is being initiated after clicking on a PUSH notification, meaning
     * a transaction is taking place
     * @return true if there is a string called "transaction"
     */
    private String getTransaction() {
        SnappyDBHelper.getDatabase(getApplicationContext());
        try {
            return SnappyDBHelper.get(getApplicationContext(),"transaction");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Check if along the intent there's a Boolean tagged 'voucher' to check if user received a voucher

     * @return true if there's a boolean tagged voucher and with value true.
     */
    private String getVoucher() {
        SnappyDBHelper.getDatabase(getApplicationContext());
        try {
            String toBeReturned = SnappyDBHelper.get(getApplicationContext(),"voucher");
            SnappyDBHelper.put(getApplicationContext(), "voucher", "");
            return toBeReturned;
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Check if it's the first run by going on the preferences e checking the boolean variables.
     * Checks also for upgrade.
     */
    private void checkFirstRun() {
        final String PREFS_NAME = "MyPrefsFile";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;
        int currentVersionCode = -1;

        Preferences.putBoolean(IS_FIRST_RUN, false);

        //retro compatibility with old versions activation preferences
        String pinChanged = Preferences.getString("pinChanged");

        if (pinChanged != null && pinChanged.equals("true")) {
            Preferences.putBoolean(IS_ACTIVATED, true);
            Preferences.putBoolean(TERMS_ACCEPTED, true);
            Preferences.putBoolean(PIN_CHANGED, true);
        }

        // Get current version code
        PackageInfo packageInfo;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            currentVersionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Get saved version code
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {
            // This is just a normal run
            return;
        } else if (savedVersionCode == DOESNT_EXIST) {
            // TODO This is a new install (or the user cleared the shared preferences)
            //se for a versão 2, apaga as preferências e o banco para gerar o novo esquema
            Preferences.init(this);
            Preferences.clear();
            SnappyDBHelper.destroy(getApplicationContext());

        } else if (currentVersionCode > savedVersionCode) {
            // TODO This is an upgrade
        }

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
    }
}
