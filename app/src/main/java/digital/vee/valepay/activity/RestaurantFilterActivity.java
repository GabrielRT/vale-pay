package digital.vee.valepay.activity;

import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import digital.vee.valepay.R;
import digital.vee.valepay.adapter.RestaurantFilterCategoriesAdapter;
import digital.vee.valepay.model.RestaurantCategory;

/**
 * Class used to filter restaurants by sort type and categories
 *
 * @author Rafael O. Magalhaes
 * @since  31/01/2017
 */
@EActivity(R.layout.activity_restaurant_filter)
public class RestaurantFilterActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    RadioButton sortTypeBtn0;
    @ViewById
    RadioButton sortTypeBtn1;
    @ViewById
    RadioButton sortTypeBtn2;

    @ViewById
    ScrollView scrollView;

    @ViewById
    ListView listCategories;

    @Bean
    RestaurantFilterCategoriesAdapter categoriesAdapter;

    @Extra(FindRestaurantsListActivity.SORT_KEY)
    String initialSort;

    @Extra(FindRestaurantsListActivity.CATEGORY_FILTER_KEY)
    String initialCategories;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }

        String[] sortTypes = getApplicationContext().getResources().getStringArray(R.array.restaurant_sort_types);
        int index = 0;
        sortTypeBtn0.setText(sortTypes[index++]);
        sortTypeBtn1.setText(sortTypes[index++]);
        sortTypeBtn2.setText(sortTypes[index++]);

        // update selected sort button
        if (initialSort != null) {
            switch (initialSort) {
                case "0":
                    sortTypeBtn0.setChecked(true);
                    break;
                case "1":
                    sortTypeBtn1.setChecked(true);
                    break;
                case "2":
                    sortTypeBtn2.setChecked(true);
                    break;
                default:
                    break;
            }
        }

        // update selected categories
        categoriesAdapter.setSelectedCategories(initialCategories);
        listCategories.setAdapter(categoriesAdapter);

        categoriesAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                // update size of listview after change items
                setListViewHeightBasedOnItems(listCategories);
                scrollView.scrollTo(0,0);
            }

        });
        // update size of listview
        setListViewHeightBasedOnItems(listCategories);
        scrollView.scrollTo(0,0);
    }

    /**
     * Process the filter button action and return selected data to parent activity.
     */
    @Click (R.id.filterBtn)
    void filterBtnClicked() {
        Intent intent = new Intent();

        // get categories selected
        String selectedCategories = "";
        for (RestaurantCategory category :
             categoriesAdapter.getSelectedCategories()) {
            selectedCategories += category.getCode() + ",";
        }
        if (!selectedCategories.isEmpty()) {
            selectedCategories = selectedCategories.substring(0, selectedCategories.length()-1);
            intent.putExtra(FindRestaurantsListActivity.CATEGORY_FILTER_KEY, selectedCategories);
        }

        // get sort type selected
        String sort = null;
        if (sortTypeBtn0.isChecked()) {
            sort = "0";
        }
        else if (sortTypeBtn1.isChecked()) {
            sort = "1";
        }
        else if (sortTypeBtn2.isChecked()) {
            sort = "2";
        }

        if (sort != null) {
            intent.putExtra(FindRestaurantsListActivity.SORT_KEY, sort);
        }

        if ((intent.getExtras() == null || intent.getExtras().isEmpty()) &&
                (initialCategories == null || initialCategories.isEmpty())) {
            setResult(RESULT_CANCELED);
        }
        else {
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    private boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);

            return true;

        } else {
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
