package digital.vee.valepay.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yelp.clientlib.connection.YelpAPI;
import com.yelp.clientlib.connection.YelpAPIFactory;
import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.options.CoordinateOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.adapter.FindRestaurantsAdapter;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Show a list of restaurants using YELP API and filter options.
 *
 * @author Rafael O. Magalhaes
 * @since  31/01/2017
 */
@EActivity(R.layout.activity_find_restaurants_list)
//@OptionsMenu(R.menu.find_restaurant_menu)
public class FindRestaurantsListActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Runnable {

    private static final String BASE_URL = BuildConfig.SERVER_URL + "rest/yelpdataenrich";;
    private static final String TAG = "FindRestaurantsList";
    private static final int PERMISSION_LOCATION_REQUEST_CODE = 101;
    private static final int REQUEST_ID = 102;

    /**
     *
     * NEW (Vee)
     * Consumer Key:	EfavJs3Niej-EhoL6j4WNA
     * Consumer Secret:	gsW2tDoo_LhaHJ7LPVKweT9nmqU
     * Token:	        HtxtvO9e8m9pCc4PUn8Xw7zgAJkmqqU3
     * Token Secret:	0xOHYsxwFXy1dnRVAr_M7crriQ8
     *
     * OLD (Sergio)
     consumer_key: crnZKbpKyhbLUZzxs0ERkw
     consumer_secret: SET1R-d9OZk9avLriRjsF1CE3co
     token: 8oy-At7_cM0r1zHvuMVCuQai3iH_OFf3
     token_secret: 74NNuMYTrP5sLCLYOL8JULFZSvs
     */
    private static final String YELP_CONSUMER_KEY = "EfavJs3Niej-EhoL6j4WNA";
    private static final String YELP_CONSUMER_SECRET = "gsW2tDoo_LhaHJ7LPVKweT9nmqU";
    private static final String YELP_TOKEN = "HtxtvO9e8m9pCc4PUn8Xw7zgAJkmqqU3";
    private static final String YELP_TOKEN_SECRET = "0xOHYsxwFXy1dnRVAr_M7crriQ8";

    public static final String ZIP_CODE_EXTRA_KEY = "zipCode";
    public static final String LOCATION_EXTRA_KEY = "location";
    public static final String LOCATION_LAT_KEY = "locationLatitude";
    public static final String LOCATION_LON_KEY = "locationLongitude";

    private static final int FILTER_REQUEST_CODE = 10001;

    public static final String CATEGORY_FILTER_KEY = "category_filter";
    public static final String SORT_KEY = "sort";
    private static final String DEFAULT_TERM = ""; //do not search since default categorie/card will be send

    private ArrayList<String> backEndCategoryFilter = new ArrayList<>();// = getBackEndCategoriesWithCardTypes(CategoryType.ALL.toString()); // default starting values include all ategories
    private ArrayList<String> yelpCategoryFilter = new ArrayList<>();// = getCategoriesWithCardTypes(CategoryType.ALL.toString()); // default starting values include all ategories
    //private final String DEFAULT_YELP_CATEGORY_FILTER;// = convertArrayListToCsv(yelpCategoryFilter); //TODO: method to set all categories

    public static final String SELECTED_CARD = "selected_card";

    private boolean filterButtonRestaurantState;
    private boolean filterButtonMarketState;
    private boolean filterButtonGasStationState;
    private boolean filterButtonCultureState;
    private long l;

    public enum CategoryType {
        v1, v2, v3, v4, ALL
    }

    private static final String SEPARATOR = ",";

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView searchRestText;

    @ViewById
    ListView list;

    @ViewById
    ImageView filterRestaurantsButton;

    @ViewById
    ImageView filterMarketsButton;

    @ViewById
    ImageView filterGasStationsButton;

    @ViewById
    ImageView filterCultureButton;

    @Bean
    FindRestaurantsAdapter adapter;

    @Extra(LOCATION_EXTRA_KEY)
    Location yelpLocation;

    //private ProgressDialog progressDialog;
    private YelpAPI yelpAPI;
    private String sort;
    // categories used to consult Yelp service. must be set by
    private String yelpCategories;
    private String backEndCategories;
    private static final String NUMBER_OF_RESULTS = "40";

    private GoogleApiClient client;
    private Location location;
    private boolean checkPermission;

    @AfterViews
    void afterViews() {
        client = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        checkPermission = true;

        Preferences.init(this);

        doLogin();

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }

        //Log.v(TAG, "=> apiFactory");
        YelpAPIFactory apiFactory = new YelpAPIFactory(YELP_CONSUMER_KEY, YELP_CONSUMER_SECRET, YELP_TOKEN, YELP_TOKEN_SECRET);
        //Log.v(TAG, "=> apiFactory.createAPI");
        yelpAPI = apiFactory.createAPI();

        //Log.v(TAG, "=> requestRestaurants");
        Bundle extras = getIntent().getExtras();
        String card = extras != null ? extras.getString(SELECTED_CARD) : "";
        Pair<ArrayList<String>, ArrayList<String>> categoriesWithCardTypes = getCategoriesWithCardTypes(card);
        ArrayList<String> yelpCategories = categoriesWithCardTypes.second;
        this.yelpCategories = convertArrayListToCsv(yelpCategories);

        ArrayList<String> backendCategories = categoriesWithCardTypes.first;
        this.backEndCategories = convertArrayListToCsv(backendCategories);

        //Log.v(TAG, "=> adapter");
        list.setAdapter(adapter);
    }

    /**
     * Handle clicks on filter category buttons: restaurants, markets, gas stations and culture
     *
     * Must change yelpCategories
     *
     */
    @Click(R.id.filterRestaurantsButton)
    void filterButtonRestaurantsClicked() {
        doSearchYelpCategoryAndKeywords(CategoryType.v1);
    }

    @Click(R.id.filterMarketsButton)
    void filterButtonMarketsClicked() {
        doSearchYelpCategoryAndKeywords(CategoryType.v2);
    }

    @Click(R.id.filterGasStationsButton)
    void filterButtonGasStationsClicked() {
        doSearchYelpCategoryAndKeywords(CategoryType.v3);
    }

    @Click(R.id.filterCultureButton)
    void filterButtonCultureClicked() {
        doSearchYelpCategoryAndKeywords(CategoryType.v4);
    }

    /**
     * Receive the card/merchant type clicked and add or remove it from yelpCategories ArrayList
     * Request merchants(restaurants) passing all yelpCategories as csv string
     *
     * @param cardType
     */
    private void doSearchYelpCategoryAndKeywords(CategoryType cardType) {
        getCategoriesWithCardTypes(cardType.toString());

        backEndCategories = convertArrayListToCsv(backEndCategoryFilter);
        yelpCategories = convertArrayListToCsv(yelpCategoryFilter);

        String text = searchRestText.getText().toString();
        requestRestaurants(text, false);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            View currentFocus = activity.getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        currentFocus.getWindowToken(), 0);
            }
        }
    }

    private String convertArrayListToCsv(ArrayList<String> categoryFilter) {
        if (categoryFilter.size() <= 0) {
            return "";
        }
        StringBuilder csvBuilder = new StringBuilder();

        for(String category : categoryFilter){
            csvBuilder.append(category);
            csvBuilder.append(SEPARATOR);
        }

        String csv = csvBuilder.toString();
        csv = csv.substring(0, csv.length() - SEPARATOR.length());
        return csv;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /**
     * Request using YELP API a list of restaurants using the term, sort and this.yelpCategories filters.
     *
     * @author Daniel O. Basconcello Filho
     * @since  06/06/2017
     *
     * @param term Term to be used in search
     * @param initial <code>true</code> if it used in initialization of view and use sort by best
     *                match when sort was not set in filter, <code>false</code> that used sort by
     *                distance (better results when putEncrypted restaurant name in search)
     */
    private void requestRestaurants(String term, boolean initial) {
        Call<ResponseBody> yelpCall = null;

        Map<String, String> params = new HashMap<>();

        // general params
        params.put("term", term);

        params.put("limit", NUMBER_OF_RESULTS);
        if (sort != null) {
            // sort selected on filter
            params.put(SORT_KEY, sort);
        }
        else if (initial) {
            // sort by best match
            params.put(SORT_KEY, "0");
        }
        else {
            // sort by distance
            params.put(SORT_KEY, "1");
        }

        assert yelpCategories != null;
        // use yelpCategories from filter
        params.put(CATEGORY_FILTER_KEY, yelpCategories);
        //else {
            //params.put(CATEGORY_FILTER_KEY, DEFAULT_YELP_CATEGORY_FILTER);
        //}
        String latitude = null, longitude = null;
        ArrayList<String> searchNames = new ArrayList<>();
        ArrayList<String> typeNames = new ArrayList<>();
        if (yelpLocation != null) {
            //Log.v(TAG, "=> yelpLocation != null");
            // save to preferences
            Preferences.putDouble(LOCATION_LAT_KEY, yelpLocation.getLatitude());
            Preferences.putDouble(LOCATION_LON_KEY, yelpLocation.getLongitude());
            Preferences.remove(ZIP_CODE_EXTRA_KEY);

            //Log.v(TAG, "=> CoordinateOptions");
            // use location to find restaurants and fill adapter
            latitude = String.valueOf(yelpLocation.getLatitude());
            longitude = String.valueOf(yelpLocation.getLongitude());

            //CoordinateOptions coordinate = CoordinateOptions.builder()
              //      .latitude()
                //    .longitude().build();
            //Log.v(TAG, "=> yelpCall");
/*            try {
                yelpCall = yelpAPI.searchRaw(coordinate, params);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        } else {
            double lat = Preferences.getDouble(LOCATION_LAT_KEY);
            double lon = Preferences.getDouble(LOCATION_LON_KEY);
            if (lat == 0.0 &&
                    lon == 0.0) {
                latitude = "-23.598492";
                longitude = "-46.687895";
            }
/*            CoordinateOptions coordinate = CoordinateOptions.builder()
                    .latitude(lat)
                    .longitude(lon)
                    .build();*/
            /*try {
                yelpCall = yelpAPI.searchRaw(coordinate, params);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }

        Callback<ResponseBody> callback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {

                    String rawResponse = response.body().string();

                    /*
                     * @author Daniel O. Basconcello Filho
                     * @since  06/06/2017
                     * Compatibilização da resposta do Yelp API 2 para Yelp API 3 esperada pelo backend
                     * varre os business e troca a categoria de array para array chave/valor
                    */
                    //TODO: Atualizar para consumir a API 3.0 do Yelp quando existir uma biblioteca estável.
                    JSONObject rawJson = new JSONObject(rawResponse);

                    JSONObject region = rawJson.getJSONObject("region");
                    region.remove("span");
                    rawJson.remove("region");
                    rawJson.put("region", region);

                    JSONArray businesses = (JSONArray) rawJson.get("businesses");
                    for( int b=0; b < businesses.length(); b++ ){
                        JSONObject bus = (JSONObject) businesses.get(b);

                        if( bus.has("categories") ) {
                            JSONArray cats = (JSONArray) bus.get("categories");
                            JSONArray nova_cats = new JSONArray();
                            for (int c = 0; c < cats.length(); c++) {
                                JSONObject nova_cat = new JSONObject();
                                JSONArray cat = (JSONArray) cats.get(c);
                                nova_cat.put("alias", cat.get(0));
                                nova_cat.put("title", cat.get(1));
                                nova_cats.put(nova_cat);
                            }
                            bus.put("categories", nova_cats);
                        }
                    }

                    //add search and accepted merchatn types to backend search
                    String s = searchRestText.getText().toString();
                    searchNames.add(s);

                    JSONArray names = new JSONArray();

                    JSONObject nameObj = new JSONObject();
                    nameObj.put("name", s);
                    names.put(nameObj);

                    JSONObject search = new JSONObject();

                    search.put("names", names);

                    rawJson.put("search", search);

                    JSONArray types = new JSONArray();

                    for (String cat : backEndCategoryFilter) {
                        JSONObject catObj = new JSONObject();
                        catObj.put("name", cat);
                        typeNames.add(cat);
                        types.put(catObj);
                    }

                    JSONObject acceptedTypes = new JSONObject();

                    acceptedTypes.put("types", types);

                    rawJson.put("acceptedTypes", acceptedTypes);

                    String businessesV3 = rawJson.toString();
                    // End of v2>v3 compatibility code --------------------------------------------------

                    //submit yelp json to backend
                    l = System.currentTimeMillis() - l;
                    checkRestaurantsInvited(businessesV3);

                }  catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                // HTTP error happened, do something to handle it.
                t.printStackTrace();
                t.getLocalizedMessage();
                dismissDialog();
                showMessage(getString(R.string.load_failed));
                redirUse();
            }
        };

        l = System.currentTimeMillis();
        searchNames.add(searchRestText.getText().toString());

        for (String cat : backEndCategoryFilter) {
            typeNames.add(cat);
        }

        checkRestaurantsInvited(mockupYelpCallback(latitude, longitude, searchNames, typeNames));
        //Call<ResponseBody> finalYelpCall = yelpCall;
        //createProgressDialog(FindRestaurantsListActivity.this, true, dialog -> {
            //finalYelpCall.cancel();
            //redirHome();
        //});
    }

    /**
     * Method to mock Yelp response after discontinued version 2.0 of API to sendo to backend with
     *  empty business value
     *
     */
    String mockupYelpCallback(String latitude, String longitude, ArrayList<String> searchNames,
                              ArrayList<String> typeNames) {
        JSONObject jsonObj = new JSONObject();
        JSONArray businessesArray = new JSONArray();
        try {

            jsonObj.put("businesses", businessesArray);

            JSONObject centerObj = new JSONObject();

            centerObj.put("latitude", latitude);//-23.593208004341648);
            centerObj.put("longitude", longitude);//-46.6865515);

            JSONObject regionObj = new JSONObject();

            regionObj.put("center", centerObj);

            jsonObj.put("region", regionObj);

            JSONArray namesArray = new JSONArray();

            for (String name: searchNames) {
                JSONObject nameObj = new JSONObject();
                nameObj.put("name", name);
                namesArray.put(nameObj);
            }

            JSONObject search = new JSONObject();
            search.put("names", namesArray);
            jsonObj.put("search", search);

            jsonObj.put("total", 0);

            JSONArray typeName = new JSONArray();

            for (String name: typeNames) {
                JSONObject nameV1 = new JSONObject();
                nameV1.put("name", name);
                typeName.put(nameV1);
            }

            JSONObject acceptedTypes = new JSONObject();
            acceptedTypes.put("types", typeName);

            jsonObj.put("acceptedTypes", acceptedTypes);

            jsonObj.put("locale", "en");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*JSONArray yelpFakeResponse = new JSONArray();
        JSONObject total = new JSONObject();
        try {
            total.put("total",0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        yelpFakeResponse.put(total);

        Log.d(TAG, yelpFakeResponse.toString());

        JSONObject business = new JSONObject();
        yelpFakeResponse.put("business", business);

        JSONObject search = new JSONObject();
        JSONArray names = new JSONArray();
        JSONObject name = new JSONObject();
        names.put(name);
        try {
            search.put("names",names);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject acceptTypes = new JSONObject();*/
        return jsonObj.toString();
    }

    /**
     * Check if the restaurants received from YELP are invited by the user and update the adapter.
     * Send the original JSON response from yelp to vee backend enrich yelp data (rest/yelpenrich)
     *
     *      * @author Daniel O. Basconcello Filho
     * @since  06/06/2017
     *
     * @param yelpJson List of restaurants found in YELP
     */
    private void checkRestaurantsInvited(String yelpJson) {
        Log.v(TAG, "*** " + yelpJson);
        l = System.currentTimeMillis();
        final String mRequestBody = yelpJson;

        //Deserialize response
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Log.v(TAG, "Response: " + response);

                    //Deserialize bussiness tree to business object
                    JSONObject jsonObj = new JSONObject(response);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
                    ArrayList<Business> businesses = mapper.readValue(jsonObj.get("businesses").toString(),
                            new TypeReference<ArrayList<Business>>() {});

                    //Convert json response to Restaurant object and bind to view
                    adapter.proccessYelpResponse(businesses);
                    dismissDialog();
                    l = System.currentTimeMillis() - l;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissDialog();
                if (error != null) {
                    error.printStackTrace();
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showTimeoutDialog(FindRestaurantsListActivity.this,
                                    new Intent(FindRestaurantsListActivity.this, UseActivity.class));
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            showMessage(getString(R.string.no_connection));
                        } else {
                            showMessage(getString(R.string.unknow_error));
                        }
                    }
                }
            }
        };

        StringRequestCustom getRequest = new StringRequestCustom(Request.Method.POST, BASE_URL,
                listener, errorListener, mRequestBody) {
            Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("accept", "application/json");
                params.put("Accept-Language", "pt_BR");
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return this.mPriority != null ? this.mPriority : Priority.NORMAL;
            }
        };

        System.setProperty("http.keepAlive", "false");
        getRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(getRequest);
    }

    /**
     * Update search after input a new term value in search field
     *
     * @param editText EditText used to input the term to search
     * @param actionId ActionID of EditText that will be processed
     */
    @EditorAction(R.id.searchRestText)
    void onSearchRestaurantEditorAction(EditText editText, int actionId) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
            String text = editText.getText().toString();
            hideSoftKeyboard(this);
            requestRestaurants(text, false);
        }
    }

    /**
     * Process the result of Filter and update the list.
     *
     * @param resultCode ResultCode received from Filter view.
     * @param data Data result received from Filter view (may contain sort and/or category)
     */
    @OnActivityResult(FILTER_REQUEST_CODE)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            sort = null;
            yelpCategories = null;
            if (data.hasExtra(SORT_KEY)) {
                sort = data.getStringExtra(SORT_KEY);
            }
            if (data.hasExtra(CATEGORY_FILTER_KEY)) {
                yelpCategories = data.getStringExtra(CATEGORY_FILTER_KEY);
            }
            String text = searchRestText.getText().toString();

            if (text.isEmpty()) {
                requestRestaurants(DEFAULT_TERM, true);
            }
            else {
                requestRestaurants(text, false);
            }
        }
    }

    /**
     * Receive merchant types/cards required and return an ArrayList String of the correspondent
     *  Yelp categorie values
     * If cardType is empty, return all yelpCategories
     * @param cardType list of required merchant types
     * @return list of String values containing Yelp categories related to cardType
     */
    Pair<ArrayList<String>, ArrayList<String>> getCategoriesWithCardTypes(String cardType) {
        ArrayList<String> localBackEndCategories = new ArrayList<>();
        ArrayList<String> localYelpCategories = new ArrayList<>();

        String allTypes = CategoryType.ALL.toString();
        String mealType = CategoryType.v1.toString();
        String groceryType = CategoryType.v2.toString();
        String fuelType = CategoryType.v3.toString();
        String cultureType = CategoryType.v4.toString();

        if (cardType.equals(mealType) || cardType.equals(allTypes)) { //meal
            filterButtonRestaurantState = !filterButtonRestaurantState;

            localYelpCategories.add("restaurants");
            localYelpCategories.add("food");
            localYelpCategories.add("cafes");
            localYelpCategories.add("bars");
            localYelpCategories.add("hotdogs");
            localYelpCategories.add("diners");
            localYelpCategories.add("candy");
            localYelpCategories.add("chocolate");
            localYelpCategories.add("tea");
            localYelpCategories.add("foodtrucks");
            localYelpCategories.add("fooddeliveryservices");

            localBackEndCategories.add(CategoryType.v1.toString());

            addOrRemoveCategories(filterButtonRestaurantState, localBackEndCategories, localYelpCategories);

        }
        if (cardType.equals(groceryType) || cardType.equals(allTypes)) { //grocery
            filterButtonMarketState = !filterButtonMarketState;
            localYelpCategories.add("markets");
            localYelpCategories.add("gourmet");
            localYelpCategories.add("grocery");

            localBackEndCategories.add(CategoryType.v2.toString());

            addOrRemoveCategories(filterButtonMarketState, localBackEndCategories, localYelpCategories);

        }
        if (cardType.equals(fuelType) || cardType.equals(allTypes)) { //fuel
            filterButtonGasStationState = !filterButtonGasStationState;
            localYelpCategories.add("servicestations");

            localBackEndCategories.add(CategoryType.v3.toString());

            addOrRemoveCategories(filterButtonGasStationState, localBackEndCategories, localYelpCategories);
        }
        if (cardType.equals(cultureType) || cardType.equals(allTypes)) { //culture
            filterButtonCultureState = !filterButtonCultureState;
            localYelpCategories.add("movietheaters");

            localBackEndCategories.add(CategoryType.v4.toString());

            addOrRemoveCategories(filterButtonCultureState, localBackEndCategories, localYelpCategories);
        }

        filterRestaurantsButton.setImageAlpha(filterButtonRestaurantState ? 255 : 127);
        filterMarketsButton.setImageAlpha(filterButtonMarketState ? 255 : 127);
        filterGasStationsButton.setImageAlpha(filterButtonGasStationState ? 255 : 127);
        filterCultureButton.setImageAlpha(filterButtonCultureState ? 255 : 127);

        return new Pair<>(localBackEndCategories, localYelpCategories);
    }

    private void addOrRemoveCategories(boolean buttonValue, ArrayList<String> backEndCategories, ArrayList<String> localYelpCategories) {
        if (buttonValue) {
            backEndCategoryFilter.addAll(backEndCategories);
            yelpCategoryFilter.addAll(localYelpCategories);

        } else {
            backEndCategoryFilter.removeAll(backEndCategories);
            yelpCategoryFilter.removeAll(localYelpCategories);
        }
    }

    @Override
    public void onBackPressed() {
        redirUse();
    }

    private void redirUse() {
        ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUse();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public void run() {
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(client);
            final Handler handler = new Handler();

            if (loc == null) {
                handler.postDelayed(this, 1000);
            }

            this.location = loc;
            yelpLocation = getLocation();
            //check is location was gotten
            if (yelpLocation == null) {
                double lat = Preferences.getDouble(LOCATION_LAT_KEY);
                double lon = Preferences.getDouble(LOCATION_LON_KEY);
                if (lat == 0.0 && lon == 0.0) {
                    // FIXME: set default coordinates as Vee Digital headquarter
                    lat = -23.598492f;
                    lon = -46.687895f;
                }
                yelpLocation = new Location(LOCATION_EXTRA_KEY);
                yelpLocation.setLatitude(lat);
                yelpLocation.setLongitude(lon);
/*                }
                else {
                   redirUse();
                    Toast.makeText(this, R.string.unable_get_location, Toast.LENGTH_LONG).show();
                    handler.removeCallbacks(this);
                    return;
                }
*/
            }
            handler.removeCallbacks(this);

            requestRestaurants(DEFAULT_TERM, true);
        } catch (SecurityException se) {
            se.printStackTrace();
        }
    }

    private void showMessageOKCancel() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.gps_request_title)
                .setMessage(R.string.request_gps_permission)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                FindRestaurantsListActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION},
                                PERMISSION_LOCATION_REQUEST_CODE);
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Canceled by user
                    }
                })
                .create().show();
    }

    private void checkAndShowPermissionDialog() {
        boolean noPermission = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;

/*
        noPermission = noPermission && ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
*/

        if (checkPermission && noPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel();
                return;
            }
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_LOCATION_REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAndShowPermissionDialog();
        client.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        client.disconnect();
    }

    @Override
    public void onConnected(Bundle undocumented) {
        run();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Log.d(TAG, "onConnectionSuspended");
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        boolean anyLuck = false;

        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_ID);
                anyLuck = true;
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }

        if (!anyLuck) {
            finish();
        }
    }
}
