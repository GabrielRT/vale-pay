package digital.vee.valepay.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.snappydb.SnappydbException;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.yelp.clientlib.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.ImageUtil;
import digital.vee.valepay.util.LoginUtil;
import uk.me.hardill.volley.multipart.MultipartRequest;

/**
 * After taking a picture for refund, these class builds a bitmap with the bytes captured and show it
 * to the user so that he can confirm that he wants to send said picture, after that a multipart volley
 * request is sent to the backend.
 * <p>
 * Created by silvio on 03/04/18.
 */

public class ConfirmImageActivity extends BaseActivity {

    private static final String IMAGE_SEND_URL = BuildConfig.SERVER_URL + "rest/private/refund/send";
    private static final int SEND_REFUND_RESPONSE_OK = 1;
    private static final int SEND_REFUND_RESPONSE_MANUAL_APPROVE = 3;
    private static final int SEND_REFUND_RESPONSE_REFUSED = 4;
    @Bind(R.id.confirm_btn) RelativeLayout confirmBtn;

    private static File imageFile;
    private static Intent informationIntent;
    private static  String imagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_confirm_image);

        ButterKnife.bind(this);

        setToolbar();

        makeClickListener();

        informationIntent = new Intent(ConfirmImageActivity.this,
                RefundInformationActivity.class);
        imagePath = getIntent().getStringExtra("path");
        imageFile = new File(imagePath);

        showImage();
    }

    /**
     * If the image config has the rotate option, this method will rotate it back be
     */
    private void showImage() {
        // if the image has rotation on it's configuration, the method will rotate it back to
        // portrait
        System.gc();

        displayImage();
    }

    /**
     * Display image using picasso.
     */
    private void displayImage() {
        try {
            ImageView imageView = findViewById(R.id.imageView);

            // NO_STORE cause every photo will be different and displayed once
            Picasso.get().load(imageFile).memoryPolicy(MemoryPolicy.NO_STORE).into(imageView);


        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            ActivityUtils.exitActivity(this);
            showLongMessage(getString(R.string.image_error));
        }
    }

    /**
     * Start the async task that will send the image
     */
    private void makeClickListener() {
        confirmBtn.setOnClickListener(v -> {
            confirmBtn.setClickable(false);
            createProgressDialog(ConfirmImageActivity.this);
            new PicassoTask(this).execute("");
        });
    }

    @Override
    public void onBackPressed() {
        ActivityUtils.exitActivity(this, new Intent(ConfirmImageActivity.this,
                CaptureReceiptActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this, new Intent(ConfirmImageActivity.this,
                        CaptureReceiptActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Destroy the QR code saved in the snappy db so it doesn't get confused with the future ones.
     * Delete the image saved on internal storage.
     * Recycles bitmap and calls garbage collector.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        deleteQrCode(this);
        imageFile.delete();
        System.gc();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissDialog();
    }

    /**
     * This method show a dialog informing user when timeout has occurred and redirects him to Home
     *
     * @param intent Whenever an error will change activities, an intent must be passed so it is executed
     */
    public static void showTimeDialog(Context context, @Nullable Intent intent) {
        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog
                .Builder(context)
                .create();
        alertDialog.setTitle(context.getString(R.string.error));
        alertDialog.setMessage(context.getString(R.string.timeout_connection_error));
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL,
                context.getString(R.string.ok),
                (dialog, which) -> {
                    dialog.dismiss();

                    // if there is intent, execute it.
                    if (intent != null) {
                        ActivityUtils.newActivity(context, intent);
                        ((Activity) context).finish();
                    }
                });
        alertDialog.show();
    }

    private static class PicassoTask extends AsyncTask<String, Void, String> {
        private WeakReference<Context> contextRef;
        private int height, width;

        // constructor
        private PicassoTask (Context context) {
            this.contextRef = new WeakReference<>(context);
        }

        private Response.ErrorListener makeErrorListener() {
            return error -> {
                dismissDialog();
                if (error.getClass().equals(TimeoutError.class)) {
                    showTimeDialog(contextRef.get(), null);
                } else {
                    ActivityUtils.exitActivity(contextRef.get());
                    showLongMessage(contextRef.get(), contextRef.get().getString(R.string.image_send_error));
                }
                error.printStackTrace();
            };
        }

        /**
         * Make header used in the request, in the multipart they are not get from an overridden method,
         * they are passed as parameters to the request object constructor.
         *
         * @return HashMap with authorization,language, content type and api version
         */
        private Map<String, String> makeHeader() {
            Map<String, String> params = new HashMap<>();
            params.put("Accept-Language", "pt_br");
            try {
                params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(contextRef.get(), LoginUtil.TOKEN_KEY));
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            params.put("apiVersion", "2");
            return params;
        }

        /**
         * Before sending the user to another activity, clear the qr codes tuple in the snappy. There might
         * be cases where no qr code has been read and we can't leave old qr codes to interfere with future
         * refunds
         */
        private void redirConfirmRefundData() {
            // clear the value of the qr code for snappy
            deleteQrCode(contextRef.get());
            ActivityUtils.newActivity(contextRef.get(), informationIntent);
            ((Activity) contextRef.get()).finish();
        }

        private void saveRefundData(JSONObject jsonObject) throws JSONException, SnappydbException {
            String refundUuid = jsonObject.getString("refundUUID");

            // check if receipt object has been returned
            if (jsonObject.has("receipt")) {

                // every data on the transaction to be displayed on the next screen is saved in snappy for
                // data persistence in case the user closes the app
                SnappyDBHelper.put(contextRef.get(), "receipt",
                        jsonObject.getJSONObject("receipt").toString());
                SnappyDBHelper.put(contextRef.get(), "refundUUID",
                        refundUuid);
            }
        }

        /**
         * take different courses of action depending on the statusCode received from the backend
         *
         * @param statusCode Code returned from server after sending image
         */
        private void takeAction(int statusCode, JSONObject jsonObject) throws JSONException, SnappydbException {
            switch (statusCode) {
                case SEND_REFUND_RESPONSE_OK:
                    saveRefundData(jsonObject);
                    // user is sent to RefundInformationActivity
                    redirConfirmRefundData();
                    dismissDialog();
                    break;
                case SEND_REFUND_RESPONSE_MANUAL_APPROVE:
                    dismissDialog();
                    ActivityUtils.exitActivity(contextRef.get());
                    showLongMessage(contextRef.get(),contextRef.get().getString(R.string.manual_refund));
                    break;
                case SEND_REFUND_RESPONSE_REFUSED:
                    // in case of refused refund the user is sent to the home and is shown a Toast
                    dismissDialog();
                    ActivityUtils.exitActivity(contextRef.get());
                    showLongMessage(contextRef.get(),contextRef.get().getString(R.string.refused_refund));
                    break;
            }
        }

        private Response.Listener<NetworkResponse> makeListener() {
            return response -> {
                int statusCode;
                try {
                    JSONObject jsonObject = new JSONObject(new String(response.data));
                    statusCode = jsonObject.getInt("statusCode");
                    takeAction(statusCode, jsonObject);
                } catch (JSONException | SnappydbException e) {
                    e.printStackTrace();
                }
            };
        }

        /**
         * Make async request since picasso needs to be in separated thread.
         *
         * @param params optional params not being used in this class
         *
         * @return result to be passed on to onPostExecute()
         */
        @Override
        protected String doInBackground(String... params) {

            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            MultipartRequest request = new MultipartRequest(Request.Method.POST, IMAGE_SEND_URL,
                    makeHeader(), makeListener(), makeErrorListener());
            //Get bitmap resized and pass his byte array through the request
            try {
                String qrCode = SnappyDBHelper.get(contextRef.get(), "qrcode");
                String walletFrom = SnappyDBHelper.get(contextRef.get(), LoginUtil.WALLET_ID);
                Bitmap bitmap = getBitmap();

                request.addPart(new MultipartRequest.FilePart("fileInput", "image/jpg",
                        "Receipt.jpg", ImageUtil.bitmapToByteArray(bitmap)));

                request.addPart(new MultipartRequest.FormPart("walletFrom", walletFrom));

                if (!qrCode.isEmpty()) {
                    request.addPart(new MultipartRequest.FormPart("qrData", qrCode));
                }

                // send local timezone
                Calendar calendar = Calendar.getInstance();
                String timeZoneName = TimeZone.getDefault().getID();
                request.addPart(new MultipartRequest.FormPart("timeZoneName", timeZoneName));

                bitmap.recycle();
                // set request policy so the image is not sent twice
                System.setProperty("http.keepAlive", "false");
                request.setRetryPolicy(VolleySingleton.noRetryLongTimeRequest());

                VolleySingleton.getInstance(contextRef.get()).addToRequestQueue(request);

            } catch (IOException | SnappydbException e) {
                e.printStackTrace();
                ActivityUtils.exitActivity(contextRef.get());
                showLongMessage(contextRef.get(), contextRef.get().getString(R.string.picasso_exception));
            } catch (OutOfMemoryError e) {
                doInBackground();
            }

            return "EXECUTED";
        }

        private void showLongMessage(Context context, String message) {
            if (message != null && !message.isEmpty()) {
                Toast toast1 = Toast.makeText(context, message, Toast.LENGTH_LONG);
                toast1.show();
            }
        }

        /**
         * Go back to use activity with the drawerMenu open
         */
        public static void redirUseOpenMenu(Context context) {
            ActivityUtils.exitActivity(context);
        }

        private Bitmap getBitmap() throws IOException {
            //makeMeasure();
            height = Picasso.get().load(imageFile).get().getHeight();
            width = Picasso.get().load(imageFile).get().getWidth();

            if (width < 1024) {
                return Picasso.get()
                        .load(imageFile)
                        .get();
            }
            int oldWidth = width;
            float scaleFactor = 1024f / oldWidth;
            float newHeight = height * scaleFactor;
            float newWidth = oldWidth * scaleFactor;

            return Picasso.get()
                    .load(imageFile)
                    .resize((int) newWidth, (int) newHeight)
                    .get();
        }

        private void makeMeasure() {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imagePath, options);
            if (width > height) {
                //get inverted measures from phone image info
                height = options.outHeight;
                width = options.outWidth;
            } else {
                height = options.outWidth;
                width = options.outHeight;
            }
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }
}
