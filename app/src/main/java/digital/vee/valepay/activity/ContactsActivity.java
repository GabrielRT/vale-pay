package digital.vee.valepay.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
/**
 * Show the contact info like phone, Whatsapp and E-mail
 * When clicked, the button calls for another application like E-mail button opening G-mail
 */
public class ContactsActivity extends BaseActivity {
    public static final String TAG = "ContactsActivity";
    public static final int VEE_PHONE = 1130453608;

    @Bind(R.id.contact_phone)
    RelativeLayout contactPhone;

    @Bind(R.id.contact_whatsapp)
    RelativeLayout contact_whatsapp;

    @Bind(R.id.contact_email)
    RelativeLayout contact_email;

    @Bind(R.id.hello_contact)
    TextView helloContact;

    @Bind(R.id.hello_text_contact)
    TextView helloTextContact;

    @Bind(R.id.phone_text)
    TextView phone_text;

    @Bind(R.id.phone)
    TextView phone;

    @Bind(R.id.whatsapp_text)
    TextView whatsapp_text;

    @Bind(R.id.whatsapp)
    TextView whatsapp;

    @Bind(R.id.email_text)
    TextView email_text;

    @Bind(R.id.email)
    TextView email;
    private long lastClickTime = 0;
    private long lastClickTimeLoginWA = 0;
    private long lastClickTimeLoginE = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contacts);

        ButterKnife.bind(this);

        Preferences.init(getApplicationContext());

        setToolbar();

        setClickListeners();

        setFonts();
    }

    private void setClickListeners() {
        contactPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + VEE_PHONE));

                if (ActivityCompat.checkSelfPermission(ContactsActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        contact_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }
                //TODO: apenas abrir a aplicação
                lastClickTimeLoginWA = SystemClock.elapsedRealtime();

            }
        });
        // when pressed opens default Email app
        contact_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTimeLoginE = SystemClock.elapsedRealtime();

                String mailto = "mailto:vitor@vee.digital";

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(mailto));

                try {
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
                }

            }
        });
    }

    private void setFonts() {
        //TODO: set fonts
        Typeface typeRobotoLight = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        // font roboto bold
        Typeface typeRobotoBold = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        helloContact.setTypeface(typeRobotoLight);
        helloTextContact.setTypeface(typeRobotoLight);
        phone_text.setTypeface(typeRobotoLight);
        phone.setTypeface(typeRobotoBold);
        whatsapp_text.setTypeface(typeRobotoLight);
        whatsapp.setTypeface(typeRobotoBold);
        email_text.setTypeface(typeRobotoLight);
        email.setTypeface(typeRobotoBold);
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
