package digital.vee.valepay.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import digital.vee.valepay.R;

@EActivity(R.layout.activity_find_restaurant)
public class FindRestaurantActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Runnable {

    private static final String TAG = "FindRestaurantActivity";
    private static final int PERMISSION_LOCATION_REQUEST_CODE = 101;
    private static final int REQUEST_ID = 102;
    @ViewById
    EditText zipCodeEditText;
    @ViewById
    Button userLocationButton;
    @ViewById
    Toolbar toolbar;
    @ViewById
    Button zipCodeOkButton;

    private GoogleApiClient client;
    private Location location;
    private boolean checkPermission;
    private long lastClickTimeLogin = 0;
    private long lastClickTimeLoginZip = 0;

    @AfterViews
    void afterViews() {
        client = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        checkPermission = true;

        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }

        zipCodeOkButton.setEnabled(false);

        /**
         * Creates a TextWatcher to validate CEP format
         */
        final TextWatcher inputTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (zipCodeEditText.getText().toString().length() < 8) {
                    zipCodeOkButton.setEnabled(false);
                } else {
                    zipCodeOkButton.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeLogin < 3000) {
                    return;
                }

                lastClickTimeLogin = SystemClock.elapsedRealtime();
            }
        };

        zipCodeEditText.addTextChangedListener(inputTextWatcher);

    }

    @Override
    protected void onResume() {
        checkAndShowPermissionDialog();
        client.connect();
        super.onResume();
    }

    @Override
    public void onPause() {
        client.disconnect();
        super.onPause();
    }

    @Override
    public void onConnected(Bundle undocumented) {
        run();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        boolean anyLuck = false;

        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_ID);
                anyLuck = true;
            } catch (IntentSender.SendIntentException e) {
                //Log.e(TAG, "Exception trying to startResolutionForResult()");
            }
        }

        if (!anyLuck) {
            finish();
        }
    }

    private void checkAndShowPermissionDialog() {
        boolean noPermission = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        //noPermission = noPermission && ContextCompat.checkSelfPermission(
                //this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;

        if (checkPermission && noPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel();
                return;
            }
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_LOCATION_REQUEST_CODE);
        }
    }

    private void showMessageOKCancel() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.gps_request_title)
                .setMessage(R.string.request_gps_permission)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                FindRestaurantActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION},
                                PERMISSION_LOCATION_REQUEST_CODE);
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Canceled by user
                        userLocationButton.setEnabled(false);
                        userLocationButton.setTextColor(Color.LTGRAY);
                    }
                })
                .create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOCATION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Location permission granted!
                    userLocationButton.setEnabled(true);
                    userLocationButton.setTextColor(getResources().getColor(R.color.aluminum));
                } else {
                    userLocationButton.setEnabled(false);
                    userLocationButton.setTextColor(Color.LTGRAY);
                    checkPermission = false;
                }
        }
    }

    @Override
    public void run() {
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(client);

            if (loc == null) {
                new Handler().postDelayed(this, 1000);
            } else {
                this.location = loc;
            }
        } catch (SecurityException se) {

        }
    }

    public Location getLocation() {
        return location;
    }

    @Click(R.id.zipCodeOkButton)
    void zipCodeOkButtonClicked() {
        // mis-clicking prevention, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTimeLogin < 3000) {
            return;
        }

        lastClickTimeLoginZip = SystemClock.elapsedRealtime();
        String zipCode = this.zipCodeEditText.getText().toString();
        //Log.i(TAG, "ZipCode inserted: " + zipCode);

        Intent intent = new Intent(this, FindRestaurantsListActivity_.class);
        intent.putExtra(FindRestaurantsListActivity.ZIP_CODE_EXTRA_KEY, zipCode);
        startActivity(intent);
    }

    @Click(R.id.userLocationButton)
    void userLocationButtonClicked() {
        //Log.i(TAG, getString(R.string.unable_to_find) + this.location);
        if (this.location != null) {
            Intent intent = new Intent(this, FindRestaurantsListActivity_.class);
            intent.putExtra(FindRestaurantsListActivity.LOCATION_EXTRA_KEY, getLocation());
            startActivity(intent);
        }
        else {
            Toast.makeText(this, R.string.unable_get_location, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, UseActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent intent = new Intent(this, UseActivity.class);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
