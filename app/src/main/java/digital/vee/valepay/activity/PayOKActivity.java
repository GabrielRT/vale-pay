package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.model.EntityData;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Created by Sérgio Brocchetto on 29/09/2016.
 * Show the OK Screen after a successful transaction
 * Updates the user current balance and adds the transaction to the history
 */

public class PayOKActivity extends BaseActivity {

    private static final String TAG = "PayOKActivity";
    public static final String PAY_VALUE_KEY = "value";
    public static final String PAY_DATE_KEY = "date";
    public static final String PAY_PAYER_KEY = "payer";
    public static final String PAY_RECEIVER_KEY = "receiver";
    
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";

    // Theses variable are used when there's a voucher transaction to know if anything will come out of the wallet
    private String spentFromWalletValue;
    private Double spentFromWalletValueD;
    @Bind(R.id.btn_thankyou_ok)
    Button _okButton;
    @Bind(R.id.balance_value_purchase_text)
    TextView tv_balanceAfter;
    @Bind(R.id.thank_you_text)
    TextView _textThankYou;

    @Bind(R.id.payment_date)
    TextView tv_payment_date;
    @Bind(R.id.voucher)
    RelativeLayout _voucher;

    @Bind(R.id.restaurant_name)
    TextView tv_restaurant;
    @Bind(R.id.voucher_value)
    TextView voucher_value;
    @Bind(R.id.balance_payment_text)
    TextView tv_payment_value;
    @Bind(R.id.balance_previous_text)
    TextView tv_previous_balance;
    private String afterAsString;
    private String balanceAfter;
    private long lastClickTimeOK = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pay_ok);

        ButterKnife.bind(this);

        Preferences.init(this);

        setFont();

        createDialog();

        createProgressDialog(PayOKActivity.this);

        doLogin(false);

        updateViewsValues();

        setClickListener();
    }

    /**
     * When the TextView is filled with the values, the dialog is dismissed
     */
    private void createDialog() {
        tv_previous_balance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dismissDialog();
            }
        });
    }

    private void setFont() {
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        TextView tv_balance_previous_text = (TextView) findViewById(R.id.balance_previous_text);
        TextView tv_balance_value_purchase = (TextView) findViewById(R.id.balance_value_purchase_text);
        TextView balance_text = (TextView) findViewById(R.id.balance_text);
        TextView thank_you_text = (TextView) findViewById(R.id.thank_you_text);

        voucher_value.setTypeface(type_roboto_light);
        tv_balance_previous_text.setTypeface(type_roboto_light);
        tv_payment_value.setTypeface(type_roboto_light);
        tv_payment_date.setTypeface(type_roboto_light);
        thank_you_text.setTypeface(type_roboto_bold);

        tv_balance_value_purchase.setTypeface(type_roboto_bold);
        tv_restaurant.setTypeface(type_roboto_bold);
        balance_text.setTypeface(type_roboto_regular);
    }

    private void setClickListener() {
        _okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeOK < 3000) {
                    return;
                }

                lastClickTimeOK = SystemClock.elapsedRealtime();

                Intent intent = new Intent(PayOKActivity.this, UseActivity.class);
                try {
                    SnappyDBHelper.put(getApplicationContext(), App.BALANCE_WALLET, afterAsString.replace("$", "").trim());
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                // UseActivity would be in a logic position where we would be going back from this screen, so used exitActivity()
                ActivityUtils.exitActivity(PayOKActivity.this, intent);
                finish();
            }
        });
    }

    private void updateViewsValues() {
        Intent intent = getIntent();
        //Get the value of the transaction and parses it to double
        Double payment = Double.parseDouble(intent.getStringExtra(PAY_VALUE_KEY));
        // Display the name of the restaurant that got the payment
        tv_restaurant.setText(String.format("Estabelecimento: %s", intent.getStringExtra(PAY_RECEIVER_KEY)));
        // display the date
        tv_payment_date.setText(getDate());
        // Get the how much vee there was in the local database, therefore, before the transaction took place
        Float before = Float.valueOf(getBalance());
        // how much balance there is left after the transaction
        Double balanceLeft = 0d;
        /*
        Check if voucher is being used in the transaction, else, gets the amount of balance after the 
        transaction to be displayed to the user
         */
        try {
            String amountSpentFromWallet = SnappyDBHelper.get(getApplicationContext(), "amountSpentFromWallet");
            if (!amountSpentFromWallet.isEmpty()) {
                String voucherValue = SnappyDBHelper.get(getApplicationContext(), "amountSpentFromVoucher");
                spentFromWalletValue = amountSpentFromWallet;
                _voucher.setVisibility(View.VISIBLE);
                voucher_value.setText(String.format("R$ %s", voucherValue));
                spentFromWalletValueD = Double.valueOf(spentFromWalletValue);
                balanceLeft = before - spentFromWalletValueD;

                SnappyDBHelper.put(getApplicationContext(), "amountSpentFromWallet", "");
                SnappyDBHelper.put(getApplicationContext(), "amountSpentFromVoucher", "");

            } else {
                // remaining balance = balance before transaction - value of transaction
                balanceLeft = before - payment;
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        //set the balance to use offline on the database
        updateBalanceoff(balanceLeft);
        // convert to current locale to show
        afterAsString = setDecimal(balanceLeft);
        try {
            updateOperation();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set balance in the local database
     * @param balanceLeft amount after the latest transaction
     */
    private void updateBalanceoff(Double balanceLeft) {
        try {
            SnappyDBHelper.put(getApplicationContext(), "balance", balanceLeft.toString());
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the date of the transaction contained inside the intent
     * @return String with the date value
     */
    private String getDate() {
        Intent intent = getIntent();
        if (intent.hasExtra(PAY_DATE_KEY)) {
            return DateUtils.getDateTimeFromUTC(intent.getStringExtra(PAY_DATE_KEY));
        }
        return "";
    }

    /**
     * Get Balance of wallet in local database
     * @return balance in String
     */
    private String getBalance() {
        try {
            return SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        } catch (SnappydbException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed(); intentional comment to do not back
    }

    /**
     * Verify if is online fetch wallets data from service, otherwise getEncrypted it from local database
     *
     * @throws SnappydbException
     */
    private void updateOperation() throws SnappydbException {

        if (Network.isOnline(UseActivity.TAG, getApplicationContext())) {
            //Get data from wallet if online

            Response.Listener<String> responseListener = new Response.Listener<String>() {
                String response;

                @Override
                public void onResponse(String response) {
                    try {
                        this.response = response;
                        JSONObject jsonResponse = new JSONObject(response);
                        SnappyDBHelper.put(getApplicationContext(), "CARDS", response);

                        JSONObject abstractEntity = jsonResponse.getJSONObject("abstractEntity");
                        EntityData entityData = EntityData.fromJson(abstractEntity);

                        // Get the balance left after the payment is made
                        balanceAfter = entityData != null ? entityData.getTotalBalance() : null;
                        Double balanceAfterPay = Double.parseDouble(balanceAfter);
                        balanceAfter = setDecimal(balanceAfterPay);


                        // value of the payment being made
                        Double payment = Double.parseDouble(getIntent().getStringExtra(PAY_VALUE_KEY));
                        // how much there was before the payment
                        Double previous = balanceAfterPay + payment;

                        //variable that will be under "Saldo Atual"
                        tv_balanceAfter.setText(String.format("R$ %s", setDecimal(payment)));

                        // display the balance before the payment and how much it was on the TextView
                        tv_previous_balance.setText(String.format("R$ %s", setDecimal(previous)));
                        tv_payment_value.setText(String.format("R$ %s", balanceAfter));

                    } catch (Exception e) {
                        //Log.v(UseActivity.TAG, "2 Exception: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            };
            Response.ErrorListener errorListener = error -> {
                Log.d(TAG, "error: " + error.getMessage());

                updateWalletBalance();

                // Set how much the user has spent on this transaction
                tv_balanceAfter.setText(String.format("R$ %s", setDecimal(Double.parseDouble(getIntent().getStringExtra(PAY_VALUE_KEY)))));

                dismissDialog();
            };

            JSONObject jsonBody = new JSONObject();

            String requestBody = jsonBody.toString();

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, USER_DATA_URL,
                    responseListener, errorListener, requestBody) {
                Request.Priority mPriority;

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept-Language", "pt_br");
                    try {
                        params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    params.put("Content-Type", "application/json");
                    params.put("apiVersion", "2");
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
                }
            };
            postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
            Volley.newRequestQueue(getApplicationContext()).add(postRequest);
        }
    }

    /**
     * This method change the balance of the wallet in the authenticated json saved in Snappy, makes
     * offline calculations to display wallet balance to user.
     */
    private void updateWalletBalance() {
        double valueSpent = Double.parseDouble(getIntent().getStringExtra(PAY_VALUE_KEY));
        double previousBalance = getWalletBalance();
        double currentBalance = previousBalance - valueSpent;

        tv_previous_balance.setText(setDecimal(previousBalance));
        tv_payment_value.setText(setDecimal(currentBalance));

        setWalletBalance(currentBalance);
    }

    private Double getWalletBalance() {
        double value = 0.0;
        try {
            String identifier = SnappyDBHelper.get(this, "walletId");
            JSONObject jsonObject = new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS"));
            value = jsonObject.getJSONObject("abstractEntity")
                    .getJSONArray("walletList")
                    .getJSONObject(getWalletIndex(identifier, jsonObject))
                    .getDouble("valueBalance");
        } catch (SnappydbException | JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Update the value of the wallet used in the transaction
     */
    private void setWalletBalance(double value) {
        try {
            String identifier = SnappyDBHelper.get(this, "walletId");
            JSONObject jsonObject = new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS"));

            // change value in wallet
            jsonObject.getJSONObject("abstractEntity")
                    .getJSONArray("walletList")
                    .getJSONObject(getWalletIndex(identifier, jsonObject))
                    .put("valueBalance", value);

            // save json back in snappy
            SnappyDBHelper.put(getApplicationContext(), "CARDS", jsonObject.toString());

        } catch (SnappydbException | JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * compare wallet on local database with wallet array and return it's index
     *
     * @return index of the used wallet on the json Array that we get from /authenticate
     */
    public int getWalletIndex(String identifier, JSONObject jsonObject) {
        JSONArray walletArray;
        int index = 0;

        try {
            walletArray = jsonObject.getJSONObject("abstractEntity").getJSONArray("walletList");

            for(int i=0; i< walletArray.length(); i++) {
                if (walletArray.getJSONObject(i).getString("identifier").equalsIgnoreCase(identifier)) {
                    index = i;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return index;
    }
}
