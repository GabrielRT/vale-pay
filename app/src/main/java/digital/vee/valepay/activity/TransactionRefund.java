package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.snappydb.SnappydbException;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.model.Transaction;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;

/**
 * Class called whenever the user clicks a refund item on the transaction list
 *
 * Created by silvio on 13/04/18.
 */

public class TransactionRefund extends BaseActivity {

    private static final String TAG = "TransactionRefund";

    @Bind(R.id.name_user_text_header) TextView _nameText;

    @Bind(R.id.date) TextView _date;

    @Bind(R.id.totalAmount) TextView _totalAmount;

    @Bind(R.id.text_cancel) TextView _cancelText;

    @Bind(R.id.balance_header) TextView _balanceHeader;

    @Bind(R.id.v_symbol_header) TextView _vSymbol;

    @Bind(R.id.last_update) TextView _lastUpdatedText;

    @Bind(R.id.refund_information) TextView refundInformation;

    private String amount;
    private long lastClickTimeLoginCancel;
    private Intent intent;
    
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

        setContentView(R.layout.activity_transaction_refund);

        ButterKnife.bind(this);
        
        setToolbar();

        makeListener();

        intent = getIntent();
        
        try{
            initializeVariables();
        }catch (Exception e){
            redirUseOpenMenu();
        }

        setFontAndText();
    }

    private void makeListener() {
        _cancelText.setOnClickListener(v -> {
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastClickTimeLoginCancel < 3000) {
                return;
            }
            lastClickTimeLoginCancel = SystemClock.elapsedRealtime();

            ActivityUtils.exitActivity(this);
        });
    }

    private void setFontAndText() {
        Typeface type_roboto_bold = Typeface.createFromAsset(getApplicationContext().getAssets(),
                LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        _totalAmount.setText(String.format("%s R$ %s", getString(R.string.pay_qr_total_amount), amount));

        _totalAmount.setTypeface(type_roboto_bold);
        _vSymbol.setTypeface(type_roboto_bold);
    }

    private void initializeVariables() {
        if(intent.hasExtra("amount")) {
            try {
                amount = setDecimal(Double.valueOf(intent.getStringExtra("amount")));
            } catch (NumberFormatException e){
                e.printStackTrace();
                amount = "N/D";
            }
        }
        String information = !intent.hasExtra("rejectReason") ? intent.getStringExtra("description") : intent.getStringExtra("rejectReason");

        setDate();

        refundInformation.setText(information);
        try {
            updateUserData();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    private void setDate() {
        String date;
        if(intent.hasExtra("dtConfirmed")) {
            date = DateUtils.getDateFromUTC(intent.getStringExtra("dtConfirmed"));
            _date.setText(getString(R.string.refund_date) + " " + date);
        } else if(intent.hasExtra("dtCanceled")){
            date = DateUtils.getDateFromUTC(intent.getStringExtra("dtCanceled"));
            _date.setText(getString(R.string.rejection_date) + " " + date);
        } else {
            _date.setVisibility(View.GONE);
        }
    }

    private void updateUserData() throws SnappydbException {
        String checkBalance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        String balance = checkBalance != null &&
                !checkBalance.isEmpty() ? checkBalance : "";
        String checkLast = SnappyDBHelper.get(getApplicationContext(), "lastUpdated");
        String lastUpdated = checkLast != null &&
                !checkLast.isEmpty() ? checkLast : "";
        String checkName = SnappyDBHelper.get(getApplicationContext(), "name");
        String firstName = checkName != null &&
                !checkName.isEmpty() ? checkName : "";
        String displayName;
        if (firstName.contains(" ")) {
            displayName = firstName.substring(0, firstName.indexOf(" "));
        } else {
            displayName = firstName;
        }
        if (displayName.length() > 25) {
            displayName = displayName.substring(0, 25);
        }

        _nameText.setText(String.format(" %s :)", displayName));

        Double balanceD = Double.parseDouble(balance);

        balance = setDecimal(balanceD);

        _balanceHeader.setText(balance);

        _lastUpdatedText.setText(lastUpdated);

        if (balanceD.compareTo(0D) < 0) {
            _balanceHeader.setTextColor(Color.RED);
            _vSymbol.setTextColor(Color.RED);
        } else {
            int color = ContextCompat.getColor(getApplicationContext(), R.color.green);
            _balanceHeader.setTextColor(color);
            _vSymbol.setTextColor(color);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                redirTransactionList();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void redirTransactionList() {
        ActivityUtils.exitActivity(this);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        redirTransactionList();
    }
}
