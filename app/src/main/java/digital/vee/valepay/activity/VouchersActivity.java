package digital.vee.valepay.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.Preferences;


/**
 * Displays an webview of the users available vouchers
 * Created by Sérgio Brocchetto on 18/04/2017.
 */

public class VouchersActivity extends BaseActivity {

    private static final String TAG = "VouchersActivity";
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "vouchermanager/displayvoucher.jsf";

    @Bind(R.id.wvVouchers)
    WebView _wvVouchers;

    private StringRequest stringRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vouchers);
        ButterKnife.bind(this);

        // if user click to go back, cancel whole thing instead of just the dialog
        createProgressDialog(VouchersActivity.this, true, dialog -> {
            if (stringRequest != null) {
                stringRequest.cancel();
            }

            redirUseOpenMenu();
            dismissDialog();
        });

        Preferences.init(getApplicationContext());

        doLogin();

        setToolbar();

        _wvVouchers.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                dismissDialog();
            }

            public void onReceivedError(WebView view,
                                        int errorCode,
                                        String description,
                                        String failingUrl) {
                dismissDialog();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue(this).add(getRequest());
    }

    private StringRequest getRequest() {
        JSONObject jsonBody = new JSONObject();
        final String mRequestBody = jsonBody.toString();
        final String finalToken = getToken();

        stringRequest = new StringRequest(Request.Method.GET, USER_DATA_URL,
                new Response.Listener<String>() {
                    String response;

                    @Override
                    public void onResponse(String response) {
                        try {
                            this.response = response;

                            //load content of html into WebView
                            WebSettings settings = _wvVouchers.getSettings();
                            settings.setJavaScriptEnabled(true);
                            // settings.setDefaultTextEncodingName("utf-8");
                            _wvVouchers.loadDataWithBaseURL(null, this.response, "text/html", "UTF-8", null);
                        } catch (Exception e) {
                            Log.v(TAG, "Exception: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, error -> {
                    if (error != null) {
                        //Log.v(TAG, "1 Exception: " + error);
                        error.printStackTrace();
                        if (error.networkResponse == null) {
                            dismissDialog();
                            if (error.getClass().equals(TimeoutError.class)) {
                               showTimeoutDialog(VouchersActivity.this, new Intent(VouchersActivity.this, UseActivity.class)
                                       .putExtra("OpenDrawer", true)); // Home with drawer menu open
                            } else if (error.getClass().equals(NoConnectionError.class)) {
                                Log.v(TAG, error.getClass().getName());
                                showMessage(getString(R.string.no_connection));
                                redirUseOpenMenu();
                            } else {
                                Log.v(TAG, error.getClass().getName());
                                showMessage(getString(R.string.unknow_error));
                                redirUseOpenMenu();
                            }
                        }
                    }
                }
        ) {
            Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json;";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return mRequestBody == null ? null : mRequestBody.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                params.put("Authorization", "Token " + finalToken);
                //params.putEncrypted("Content-Type", "application/json");
                return params;
            }

            @Override
            public Priority getPriority() {
                return mPriority != null ? mPriority : Priority.NORMAL;
            }
        };
        stringRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        return stringRequest;
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (stringRequest != null && !stringRequest.isCanceled()) {
            stringRequest.cancel();
        }
    }
}
