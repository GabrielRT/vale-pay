package digital.vee.valepay.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.FingerPrintHelper;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Created by Sérgio Brocchetto on 29/09/2016.
 * <p>
 * Display the amount being charged and ask for PIN password after scanning the Merchant's QRCode
 */

public class PayScanningActivity extends BaseActivity {

    private static final String TAG = "PayScanningActivity";
    private static final String INFO_URL = BuildConfig.SERVER_URL + "rest/private/wallet/info";

    @Bind(R.id.input_confirmation_code)
    EditText passwordEditText;

    @Bind(R.id.btn_thankyou_ok)
    Button _okButton;

    @Bind(R.id.balance_value_purchase_text)
    TextView _balanceValueAfterText;

    @Bind(R.id.restaurant_name)
    TextView _restaurantName;

    @Bind(R.id.restaurant_document)
    TextView _restaurantDocument;

    @Bind(R.id.finger_print_text)
    LinearLayout fingerPrintText;

    private String tUuid;
    private String walletTo;
    private String totalAmount;
    private String pos;

    private String wifiSSID;
    private String wifiPassword;

    Timer t = null;
    private String transaction;
    private CryptoUtil cryptoUtil;
    private FingerPrintHelper fingerPrintHelper;
    private long lastClickTimeLoginOK;
    private String walletFrom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_reversal);
        ButterKnife.bind(this);
        setCustomActionBar();

        Preferences.init(this);

        doLogin();

        //comes from onActivityResult on UseActivity
        transaction = getIntent().getStringExtra("transaction");

        //decode from base 64
        byte[] decode = new byte[0];

        Intent intent = new Intent(PayScanningActivity.this, UseActivity.class);
        try {
            decode = Base64.decode(transaction, Base64.NO_WRAP);
        } catch (IllegalArgumentException e) {
            intent.putExtra("toast", getString(R.string.reading_error));
            ActivityUtils.exitActivity(this, intent);
            finish();
            e.printStackTrace();
        }
        String qrCode = new String(decode);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody = new JSONObject(qrCode);
        } catch (JSONException e) {
            intent.putExtra("toast", getString(R.string.reading_error));
            ActivityUtils.exitActivity(this, intent);
            finish();
            e.printStackTrace();
            return;
        }

        try {
            tUuid = jsonBody.getString("transactionUUID");
            //tUuid = jsonBody.getString("uuid");
            walletTo = jsonBody.getString("walletTo");
            totalAmount = jsonBody.getString("amount");

            pos = jsonBody.has("pos") ? jsonBody.getString("pos") : "";

            walletFrom = jsonBody.has("walletFrom") ? jsonBody.getString("walletFrom") : "";

            if (tUuid == null || walletTo == null || totalAmount == null || pos == null) {
                intent.putExtra("toast", getString(R.string.reading_error));
                ActivityUtils.exitActivity(this, intent);
                finish();
                throw new NullPointerException("Null parameters");
            }

            wifiSSID = jsonBody.has("wifiSSID") ? jsonBody.getString("wifiSSID") : "GUEST";
            wifiPassword = jsonBody.has("wifiPassword") ? jsonBody.getString("wifiPassword") : "";
        } catch (JSONException e) {
            intent.putExtra("toast", getString(R.string.reading_error));
            ActivityUtils.exitActivity(this, intent);
            finish();
            e.printStackTrace();
        }

        try {
            if (!Network.isOnline(PayScanningActivity.TAG, getApplicationContext())) {
                boolean connected = Network.forceWifiConnection(getApplicationContext(), wifiSSID, wifiPassword);

                if (connected) {
                    waitForInternetConnection();
                } else {
                    askUserForInternetConnection();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            doCreate();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

    }

    /**
     * Checks if the user has internet connection and opens a dialog to warn him
     */
    private void enableFingerPrintListening() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            fingerPrintHelper = new FingerPrintHelper(getSystemService(FingerprintManager.class), this,
                    () -> fillPass());

            //get pin
            if (fingerPrintHelper.isFingerprintAuthAvailable()) {
                String pin = null;
                try {
                    pin = SnappyDBHelper.getEncrypted(this, "pin");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                //if pin has already saved
                if (pin != null && !pin.isEmpty()) {
                    //show fingerprint input info
                    fingerPrintText.setVisibility(View.VISIBLE);
                    //wait for fingerprint information
                    fingerPrintHelper.startListening(null);
                }

            } else {
                showKeyboard();
            }
        } else {
            showKeyboard();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enableFingerPrintListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerPrintHelper.stopListening();
        }
    }

    private void showKeyboard() {
        passwordEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(passwordEditText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Creates and waits for thread to finish
     */
    private void waitForInternetConnection() {
        createProgressDialog(PayScanningActivity.this, true, dialog -> redirHome());

        int counter = 0;
        while (!Network.isOnline(PayScanningActivity.TAG, getApplicationContext())) {
            counter++;
            if (counter > 3) {
                break;
            }
            dismissDialog();
                SystemClock.sleep(3000);
        }
        dismissDialog();
    }

    /**
     * throws user back to home page and asks for internet connection
     */
    private void askUserForInternetConnection() {
        Intent homeIntent = new Intent(this, UseActivity.class);
        ActivityUtils.exitActivity(this, homeIntent);
        this.finish();

        Context baseContext = getBaseContext();
        final Toast toast = Toast.makeText(baseContext, R.string.connect_to_proceed, Toast.LENGTH_LONG);
        toast.show();

        try {
            ActivityUtils.newActivity(this, new Intent(Settings.ACTION_SETTINGS));
        } catch (Exception e) {
            //Log.v(TAG, " Error: " + e.getMessage());
        }
    }

    //set fields with merchant name and document on view
    public void doCreate() throws SnappydbException {

        //generate tsc transaction and send it to Load to send
        final String finalTotalAmount = totalAmount;

        _okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeLoginOK < 3000) {
                    return;
                }
                lastClickTimeLoginOK = SystemClock.elapsedRealtime();

                //validate
                String pinCode = passwordEditText.getText().toString();

                if (pinCode.isEmpty() || pinCode.length() < 4) {
                    passwordEditText.setError(getString(R.string.error_pin_code));
                } else {
                    passwordEditText.setError(null);

                    String identify = null;
                    try {
                        identify = SnappyDBHelper.get(getApplicationContext(), "entityId");
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }

                    BigDecimal amount = new BigDecimal(0);
                    amount = BigDecimal.valueOf(Double.parseDouble(finalTotalAmount));

                    String walletEmployeeFrom = null;
                    if (walletFrom != null && !walletFrom.isEmpty()) {
                        walletEmployeeFrom = walletFrom;
                    } else {
                        try {
                            walletEmployeeFrom = SnappyDBHelper.get(getApplicationContext(), LoginUtil.WALLET_ID);
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                    }

                    TransactionSignedCode tsc = new TransactionSignedCode(identify, //entityCode
                            walletEmployeeFrom, //walletFrom
                            amount, //amount
                            pinCode, //pin
                            tUuid); // uuid lida do qrcode

                    String toSign = tsc.getSignableTransactionString();

                    String[] signature = null;
                    try {
                        cryptoUtil = new CryptoUtil(getApplicationContext());
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    }
                    try {
                        signature = cryptoUtil.signature(toSign.getBytes(),
                                SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.ECDSA_PRIVATE_KEY));
                    } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException | SnappydbException e) {
                        e.printStackTrace();
                    }

                    assert signature != null;
                    tsc.setSignature(signature[0]);

                    String tscSend = tsc.getTSC();

                    Intent intent = new Intent(PayScanningActivity.this, LoadActivity.class);
                    intent.putExtra("transaction", tscSend);
                    intent.putExtra("walletTo", walletTo);
                    intent.putExtra("pos", pos);
                    intent.putExtra("pin", pinCode);

                    ActivityUtils.newActivity(PayScanningActivity.this, intent);
                    finish();
                }
            }
        });

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return actionId == EditorInfo.IME_ACTION_SEND;
            }
        });

        Typeface typeRobotoRegular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoBold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);

        TextView balancePayHeader = (TextView) findViewById(R.id.balance_pay_header);
        TextView balanceHeader = (TextView) findViewById(R.id.balance_header);
        TextView tvSymbolHeader = (TextView) findViewById(R.id.v_symbol_header);

        TextView purchaseSuccessText = (TextView) findViewById(R.id.purchase_success_text);
        TextView restaurantName = (TextView) findViewById(R.id.restaurant_name);
        TextView tvBalanceValuePurchase = (TextView) findViewById(R.id.balance_value_purchase_text);

        TextView btn_thankyou_ok = (TextView) findViewById(R.id.btn_thankyou_ok);

        balancePayHeader.setTypeface(typeRobotoBold);
        balanceHeader.setTypeface(typeRobotoBold);
        tvSymbolHeader.setTypeface(typeRobotoBold);

        purchaseSuccessText.setTypeface(typeRobotoLight);

        restaurantName.setTypeface(typeRobotoBold);

        tvBalanceValuePurchase.setTypeface(typeRobotoBold);

        passwordEditText.setTypeface(typeRobotoLight);

        btn_thankyou_ok.setTypeface(typeRobotoRegular);

        updateViewsValues();

        getMerchantInfo();

    }


    private void getMerchantInfo() {

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("walletIdentifier", walletTo);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String mRequestBody = jsonBody.toString();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            String response;

            @Override
            public void onResponse(String response) {
                try {
                    this.response = response;
                    JSONObject jsonResponse = new JSONObject(response);

                    String name = jsonResponse.getString("name");
                    String document = jsonResponse.getString("document");

                    _restaurantName.setText(String.format("%s%s", getResources().getString(R.string.merchant), name));
                    _restaurantDocument.setText(String.format("%s%s", getString(R.string.document), document));

                } catch (Exception e) {
                    //UseActivity.this.loadDataFailed();
                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                showTimeoutDialog(PayScanningActivity.this);
            } else {
                Toast.makeText(getBaseContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, INFO_URL,
                responseListener, errorListener, mRequestBody) {
            Request.Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
            }
        };
        postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);

    }

    /**
     * Gets the balance the user have and displays it on the screen
     *
     * @throws SnappydbException
     */
    private void updateViewsValues() throws SnappydbException {

        TextView tv_balance_user = (TextView) findViewById(R.id.balance_header);
        TextView tv_last_update = (TextView) findViewById(R.id.last_update);

        String balance = null;
        try {
            balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        // convert to current locale to show
        Double balanceD = Double.parseDouble(balance);
        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);

        String amount = totalAmount;
        Double amountD = Double.parseDouble(amount);

        tv_last_update.setText(SnappyDBHelper.get(getApplicationContext(), "lastUpdated"));

        tv_balance_user.setText(dFormat.format(balanceD));

        _balanceValueAfterText.setText(String.format("$%s", dFormat.format(amountD)));
    }

    /**
     * Set ActionBar with custom drawable and icons
     */
    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.GRAY)
                    .sizeDp(24));
        }
    }

    /**
     * throws the user back to the home page if the home button is pressed
     *
     * @param item menu item selected by the user
     * @return true if the button 'home' was selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerPrintHelper.stopListening();
                }
                redirHome();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerPrintHelper.stopListening();
        }
        redirHome();
    }

    private void fillPass() {
        String pin = null;
        try {
            pin = SnappyDBHelper.getEncrypted(this, "pin");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        passwordEditText.setText(pin);
        _okButton.callOnClick();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //delete transaction data
        try {
            SnappyDBHelper.put(getApplicationContext(), "transaction", "");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
}
