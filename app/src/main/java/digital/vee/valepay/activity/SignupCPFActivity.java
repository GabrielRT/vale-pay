package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.LoginUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;

public class SignupCPFActivity extends BaseActivity {

    public String TAG = "SignupCPFActivity";
    private String personalId = null;
    private String statusCode;
    private static final String URL = BuildConfig.SERVER_URL + "rest/signup/request";

    @Bind(R.id.input_user_id)
    EditText _userID;

    @Bind(R.id.btn_submit)
    Button _submit;
    private long mLastClickTimeLogin = 0;
    private String documentWithoutPoints;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup_cpf);

        ButterKnife.bind(this);

        setClickListener();

        setFonts();

        // Initiate preferences so we have a flag that this is the first usage of the app in a device
        Preferences.init(this);
        Preferences.putBoolean("firstInit", true);
    }

    private void setFonts() {
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        MaskEditTextChangedListener maskCPF = new MaskEditTextChangedListener(
                "###.###.###-##", _userID);
        _userID.setTypeface(type_roboto_regular);
        _userID.addTextChangedListener(maskCPF);
    }

    private void setClickListener() {
        _submit.setOnClickListener(view -> {

            // mis-clicking prevention, using threshold of 1000 ms

            if (SystemClock.elapsedRealtime() - mLastClickTimeLogin < 3000) {
                return;
            }

                mLastClickTimeLogin = SystemClock.elapsedRealtime();
                
                signup();
            });
    }

    private void signup() {
        if (validate()) {
            createProgressDialog(SignupCPFActivity.this);

            try {
                SnappyDBHelper.put(getApplicationContext(), LoginUtil.USERNAME, getDocument());
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            String cpf = _userID.getText().toString();

            String removePoint = cpf.replaceAll("\\.", "");
            documentWithoutPoints = removePoint.replaceAll("-", "");

            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("document", documentWithoutPoints);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, setListener(),
                    setErrorListener(), jsonBody.toString());
            postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
            VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
        }
    }

    private String getDocument() {
       return _userID.getText().toString();
    }

    private void redirect(String statusCode){
        switch (statusCode){
            case "1":
                //Log.d(TAG, " status code 1");
                Preferences.putBoolean(MainActivity.PUT_CPF, true);
                Preferences.putBoolean(MainActivity.IS_REACTIVATION, true);
                ActivityUtils.newActivity(this, new Intent(SignupCPFActivity.this, ReactivatePIN.class));
                finish();
                dismissDialog();
                break;
            case "2":
            case "5":
                //Log.d(TAG, " status code 2, novo cadastro de não employee");
                Preferences.putBoolean(MainActivity.PUT_CPF, true);
                ActivityUtils.newActivity(this, new Intent(SignupCPFActivity.this, SignupActivity.class)
                .putExtra("document", documentWithoutPoints));
                finish();
                dismissDialog();
                break;
            case "3":
                //Log.d(TAG, " status code 3, Ativar com o código do rH");
                Preferences.putBoolean(MainActivity.PUT_CPF, true);
                Preferences.putBoolean(MainActivity.RH_ACTIVATION, true);
                ActivityUtils.newActivity(this, new Intent(SignupCPFActivity.this, ActivateActivity.class));
                finish();
                dismissDialog();
                break;
            default:
                showMessage("CPF inválido");
                dismissDialog();
        }
    }

    private Response.Listener<String> setListener() {

        return response -> {

            JSONObject jsonObject;
            try {

                jsonObject = new JSONObject(response);
                statusCode = jsonObject.getString("statusCode");

                // if there's personal id, it will grab it
                if (statusCode.equals("1")) { // only id if it's a reactivation
                    personalId = jsonObject.getString("personalId");
                    SnappyDBHelper.put(getApplicationContext(), "entityId", personalId);
                }

                redirect(statusCode);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        };
    }

    private Response.ErrorListener setErrorListener(){
        return error -> {
            dismissDialog();
            if (error.getClass().equals(TimeoutError.class)) {
                showTimeoutDialog(this);
            } else {
                showMessage(getString(R.string.error_processing_request));
            }
        };
    }

    /**
     * Validate Sign Up input data.
     * @return <code>true</code> if data is valid
     */
    private boolean validate() {
        boolean valid;

        String userId = _userID.getText().toString();

        valid = isValidUserId(userId);

        return valid;
    }

    /**
     * Validate field of user ID (CPF).
     * @param userId Data input of userId
     * @return <code>true</code> if it is a valid UserID
     */
    private boolean isValidUserId(String userId) {
        boolean valid = true;
        if (userId.isEmpty() || userId.length() < 11 || userId.length() > 14) {
            _userID.requestFocus();

            _userID.setError(getString(R.string.error_userId_signup_min));
            valid = false;
        }
        else if (!LoginUtils.isValidCPF(userId)) {

            _userID.requestFocus();

            _userID.setError(getString(R.string.error_userId_signup_invalid));

            valid = false;
        }
        else {
            _userID.setError(null);
        }
        return valid;
    }

}
