package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.pushwoosh.Pushwoosh;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Login;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;
import digital.vee.valepay.util.crypto.CryptoUtil;

public class ReactivatePIN extends BaseActivity {

    public static final String TAG = "ReactivatePINActivity";
    private static final String URL = BuildConfig.SERVER_URL + "rest/reactivate";
    final String[][] keyPair = {null};
    private CryptoUtil cryptoUtil;
    private String statusCode;
    private String message;
    final String generatedPassword= Login.randomString(128);
    private String devicePushId;
    private long lastClickTimeUse = 0;

    @Bind(R.id.btn_submit)
    Button btn_submit;
    @Bind(R.id.input_confirmation_code)
    EditText _code;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reactivate_pin);

        Preferences.init(getApplicationContext());

        ButterKnife.bind(this);

        iniKeyStore();

        generateKey();

        setClickListener();

        devicePushId = Pushwoosh.getInstance().getHwid();
        try {
            SnappyDBHelper.putEncrypted(getApplicationContext(), "devicePushId", devicePushId);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate string matrix with a key pair generated from the CryptoUtil object
     */
    /**
     * Generate string matrix with a key pair generated from the CryptoUtil object
     */
    private void generateKey() {
        if (!keyExist()) {
            //generate EC key pair and save to preferences
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        keyPair[0] = cryptoUtil.generateKeyPair();
                        try{
                            SnappyDBHelper.putEncrypted(getApplicationContext(),"ECDSA_PUBLIC_KEY_HEX",keyPair[0][0]);
                            SnappyDBHelper.putEncrypted(getApplicationContext(),"ECDSA_PRIVATE_KEY_HEX",keyPair[0][1]);
                            SnappyDBHelper.putEncrypted(getApplicationContext(),Preferences.ECDSA_PUBLIC_KEY,keyPair[0][2]);
                            SnappyDBHelper.putEncrypted(getApplicationContext(),Preferences.ECDSA_PRIVATE_KEY,keyPair[0][3]);
                            SnappyDBHelper.putEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD, generatedPassword);
                        } catch (Exception e){

                            e.printStackTrace();
                        }

                    } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                        e.printStackTrace();
                    }
                }
            });

            t.start(); // spawn thread
        }
    }

    private boolean keyExist() {
        try{
            if(SnappyDBHelper.exists(getApplicationContext(), "ECDSA_PUBLIC_KEY_HEX") &&
                    SnappyDBHelper.exists(getApplicationContext(), "ECDSA_PRIVATE_KEY_HEX") &&
                    SnappyDBHelper.exists(getApplicationContext(), Preferences.ECDSA_PUBLIC_KEY) &&
                    SnappyDBHelper.exists(getApplicationContext(), Preferences.ECDSA_PRIVATE_KEY)){
                return true;
            }
        } catch (SnappydbException e){

            e.printStackTrace();
        }
        return false;
    }

    private void iniKeyStore() {
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cryptoUtil = new CryptoUtil(getApplicationContext());
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                }
            }});

        t2.start(); // spawn thread

        try {
            t2.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeUse < 3000) {
                    return;
                }
                lastClickTimeUse = SystemClock.elapsedRealtime();
                createProgressDialog(ReactivatePIN.this);
                sendRequest();
            }
        });
    }

    private void sendRequest() {
        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, setResponse(),
                setErrorListener(), mRequestBody());
        postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
    }

    private String mRequestBody(){
        String pin = _code.getText().toString();
        String entityId = null;
        String personalId = null;
        String deviceId = null;
        String publicKey = null;
        JSONObject requestBody = new JSONObject();
        try {
            entityId = SnappyDBHelper.get(getApplicationContext(), "entityId");
             SnappyDBHelper.putEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD, generatedPassword);
            publicKey = SnappyDBHelper.getEncrypted(getApplicationContext(), "ECDSA_PUBLIC_KEY_HEX");
            SnappyDBHelper.putEncrypted(getApplicationContext(), "pin", pin);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        try {
            requestBody.put("pinHash", TransactionSignedCode.hashPin(pin + entityId));
            requestBody.put("personalId", entityId);
            requestBody.put("deviceAlias", getAlias());
            requestBody.put("deviceId", devicePushId);
            requestBody.put("password", generatedPassword);
            requestBody.put("publicKey", publicKey);
        } catch (JSONException e){
            e.printStackTrace();
        }
        return requestBody.toString();
    }

    private Response.Listener<String> setResponse(){
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    statusCode = jsonObject.getString("statusCode");
                    message = jsonObject.getString("message");
                    redirect(statusCode, message);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        };

        return listener;
    }

    private Response.ErrorListener setErrorListener(){
        Response.ErrorListener errorListener  = error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                dismissDialog();
                showTimeoutDialog(ReactivatePIN.this);
            } else {
                showMessage(getString(R.string.error_processing_request));
                dismissDialog();
            }
        };

        return errorListener;
    }

    private void redirect(String code, String message){

        if(code.equals("1")){
            Preferences.putBoolean(MainActivity.PIN_CHANGED, true);
            ActivityUtils.newActivity(this, new Intent(ReactivatePIN.this, UseActivity.class));
            showMessage(message);
            finish();
            dismissDialog();
        }
        else {
            showMessage(message);
            dismissDialog();
        }
    }

}

