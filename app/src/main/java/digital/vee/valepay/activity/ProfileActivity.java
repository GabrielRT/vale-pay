package digital.vee.valepay.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.model.Employee;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;

/**
 * Manage the profile screen
 *
 * Created by Sérgio Brocchetto on 22/04/2017.
 */

public class ProfileActivity extends BaseActivity {

    private static final String TAG = "ProfileActivity";

    private static final String URL = BuildConfig.SERVER_URL + "rest/private/manager/changepin";
    private static final String CURRENT_PIN_HASH_KEY = "currentPINHash";
    private static final String NEW_PIN_HASH_KEY = "newPINHash";
    private static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    private static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    private static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";


    @Bind(R.id.hello_profile)
    TextView hello_profile;

    @Bind(R.id.btn_change_pin)
    Button btn_change_pin;

    @Bind(R.id.input_current_password)
    EditText currentPincodeET;

    @Bind(R.id.input_new_password)
    EditText newPincodeET;

    @Bind(R.id.input_repeat_password)
    EditText confirmPincodeET;

    @Bind(R.id.personal_view)
    Button personalView;

    @Bind(R.id.btn_banking)
    Button _banking;

    @Bind(R.id.btn_corporate)
    Button _corporate;

    private String token;

    private long lastClickTimeLoginChangePin = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);

        Preferences.init(getApplicationContext());

        setHello();

        setButtons();

        doLogin();

        token = getToken();

        setToolbar();

        setClickListeners();
    }

    private void setHello() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.hello) + " ");

        try {
            Employee e = new Employee();
            stringBuilder.append(e.fromJson(new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS")))
                    .getFirstName());

        } catch (SnappydbException | JSONException e) {
            e.printStackTrace();
        }

        stringBuilder.append(" :)");

        hello_profile.setText(stringBuilder.toString());
    }

    private void setButtons(){
        AssetManager assets = this.getAssets();
        Typeface typeRobotoRegular = Typeface.createFromAsset(assets, FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(assets, FONTS_ROBOTO_LIGHT_TTF);
        Typeface typeRobotoBold = Typeface.createFromAsset(assets, FONTS_ROBOTO_BOLD_TTF);

        personalView.setText(R.string.profile_personal_data);
        personalView.setTypeface(typeRobotoLight);

        _banking.setText(R.string.profile_bank_data);
        _banking.setTypeface(typeRobotoLight);
        _banking.setVisibility(View.GONE);

        _corporate.setText(R.string.profile_business_data);
        _corporate.setTypeface(typeRobotoLight);
        _corporate.setVisibility(View.GONE);

        personalView.setBackgroundColor(this.getResources().getColor(R.color.white));
        _banking.setBackgroundColor(this.getResources().getColor(R.color.white));
        _corporate.setBackgroundColor(this.getResources().getColor(R.color.white));
    }

    /**
     * Set the listener to the change pin button that gets what the user typed and puts on a Volley
     * Request to change the PIN in the server while showing a dialog
     */
    private void setClickListeners() {
        this.btn_change_pin.setOnClickListener(v -> {
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastClickTimeLoginChangePin < 3000) {
                return;
            }

            if (validate()) {
                createProgressDialog(this);
                // getEncrypted data used in post to change the PIN Code
                String entityId = null;
                try {
                    entityId = SnappyDBHelper.get(getApplicationContext(), "entityId");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                String personalId = null;
                try {
                    personalId = SnappyDBHelper.get(getApplicationContext(), "personalId");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                final String pinCode = currentPincodeET.getText().toString();
                String newPinCode = newPincodeET.getText().toString();

                changePinRequest(entityId, personalId, pinCode, newPinCode);

            } else {
                dismissDialog();
            }
        });

        this._corporate.setOnClickListener(view -> startActivity(new Intent(ProfileActivity.this, CorporateProfileActivity.class)));

        this._banking.setOnClickListener(view -> startActivity(new Intent(ProfileActivity.this, BankingProfileActivity.class)));

        this.personalView.setOnClickListener(view -> ActivityUtils.newActivity(this,
                new Intent(ProfileActivity.this, PersonalProfileActivity.class)));
    }

    /**
     * Make the volley request to change the PIN in the server
     * @param entityId unique id of the device
     * TODO: descobrir o que sao e comentar os parametros abaixo
     * @param personalId
     * @param pinCode
     * @param newPinCode
     */
    private void changePinRequest(String entityId, String personalId, final String pinCode, String newPinCode) {
        // create json object to use in post
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put(ActivateActivity.PERSONAL_ID, entityId);
            jsonBody.put(CURRENT_PIN_HASH_KEY, TransactionSignedCode.hashPin(pinCode + entityId));
            jsonBody.put(NEW_PIN_HASH_KEY, TransactionSignedCode.hashPin(newPinCode + entityId));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String mRequestBody = jsonBody.toString();

        Response.Listener<String> listener = response -> {
            try {

                //save pin
                try {

                    SnappyDBHelper.putEncrypted(ProfileActivity.this, "pin", pinCode );
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                onChangePinSuccess();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        Response.ErrorListener errorListener = error -> {
            if (error != null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    dismissDialog();
                    showTimeoutDialog(ProfileActivity.this);
                } else {
                    onChangePinFailed();
                }
                error.printStackTrace();
            } else {
                onChangePinFailed();
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener, errorListener,
                mRequestBody) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Token " + token);
                params.put("Accept-Language", "pt_br");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    try {
                        onErrorResponse(volleyError);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                return volleyError;
            }
        };
        System.setProperty("http.keepAlive", "false");
        postRequest.setRetryPolicy(new DefaultRetryPolicy(15 * 1000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(ProfileActivity.this).add(postRequest);
    }

    /**
     * Validate if the fields of PIN Code are filled correctly
     * @return <code>true</code> if all input fields are correct
     */
    private boolean validate() {
        String currentPinCode = currentPincodeET.getText().toString();
        String newPinCode = newPincodeET.getText().toString();
        String pinCodeConfirmation = confirmPincodeET.getText().toString();
        boolean valid = true;

        // validate current pin code input
        if (currentPinCode.isEmpty() || currentPinCode.length() < 4) {
            currentPincodeET.requestFocus();
            currentPincodeET.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else {
            currentPincodeET.setError(null);
        }

        // validate new pin code input
        if (newPinCode.isEmpty() || newPinCode.length() < 4) {
            newPincodeET.requestFocus();
            newPincodeET.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else if (newPinCode.equals(currentPinCode)) {
            newPincodeET.requestFocus();
            newPincodeET.setError(getString(R.string.error_new_pass));
            valid = false;
        }
        else {
            newPincodeET.setError(null);
        }

        // validate confirm new pin code input
        if (pinCodeConfirmation.isEmpty() || pinCodeConfirmation.length() < 4) {
            confirmPincodeET.requestFocus();
            confirmPincodeET.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else if (!pinCodeConfirmation.equals(newPinCode)) {
            confirmPincodeET.requestFocus();
            confirmPincodeET.setError(getString(R.string.error_pass_confirm_signup));
            valid = false;
        }
        else {
            confirmPincodeET.setError(null);
        }
        return valid;
    }

    /**
     * Process success on Change PIN Code and starts UseActivity.
     */
    private void onChangePinSuccess() {
        dismissDialog();
        Intent intent = new Intent(this, UseActivity.class);
        intent.putExtra("toast", "Senha alterada com sucesso.");

        ActivityUtils.exitActivity(this, intent);
        finish();
    }

    /**
     * Process error on change PIN Code and shows message to user.
     */
    private void onChangePinFailed() {
        dismissDialog();
        Toast.makeText(getBaseContext(), R.string.toast_signup_failed, Toast.LENGTH_LONG).show();
    }

    /**
     * Process error on response from server and log or show it to user.
     *
     * @param error Error received from server.
     * @throws UnsupportedEncodingException
     */
    private void onErrorResponse(VolleyError error) throws UnsupportedEncodingException {
        Log.v(TAG, TAG + " +onErrorResponse()");
        String json;

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    json = trimMessage(json, "message");
                    displayMessage(json);
                    break;
            }
        }

    }

    /**
     * Get message from JSON response received from server
     *
     * @param json JSON response text
     * @param key Key to find message in JSON response
     * @return Message from JSON or <code>null</code> in case of no message in JSON.
     * @throws UnsupportedEncodingException
     */
    private String trimMessage(String json, String key) throws UnsupportedEncodingException {
        String trimmedString;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        trimmedString = Arrays.toString(trimmedString.getBytes("UTF-8"));
        return trimmedString;
    }

    /**
     * Display a toast message to user in UTF-8 format
     *
     * @param toastString Message to show
     */
    private void displayMessage(String toastString) {
        String s = null;
        try {
            s = new String(toastString.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
