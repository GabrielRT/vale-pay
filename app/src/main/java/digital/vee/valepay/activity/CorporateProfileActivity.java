package digital.vee.valepay.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Created by Silvio Machado on 31/01/2018.
 */

public class CorporateProfileActivity extends BaseActivity {

    @Bind(R.id._company)
    TextView _company;
    @Bind(R.id._email)
    TextView _email;
    @Bind(R.id._tel)
    TextView _tel;

    @Bind(R.id.btn_save)
    Button btn_save;

    @Bind(R.id.tv_tel)
    TextView tv_tel;
    @Bind(R.id.tv_email)
    TextView tv_email;
    @Bind(R.id.tv_company)
    TextView tv_company;

    private static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    private static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    private static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";

    private String company;
    private String email;
    private String tel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_corporate);

        ButterKnife.bind(this);

//        getData();

        setInfo();

        setFonts();

        setClickListener();

        setToolbar();
    }

//    private void getData() {
//        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET,
//                USER_DATA_URL,
//                getResponse(),
//                getError(),
//                mRequestBody()){
//            Request.Priority mPriority;
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept-Language", "pt_br");
//                params.put("Authorization", "Token " + getToken());
//                params.put("Content-Type", "application/json");
//                params.put("apiVersion", "2");
//                return params;
//            }
//
//            @Override
//            public Request.Priority getPriority() {
//                return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
//            }
//        };
//    }

    private Response.Listener<String> getResponse() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject employee = jsonObject.getJSONObject("employeeList");

                    tel = employee.getString("workPhoneNumber");
                    email = employee.getString("workEmail");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private void setFonts() {
        AssetManager assets = this.getAssets();
        Typeface typeRobotoRegular = Typeface.createFromAsset(assets, FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(assets, FONTS_ROBOTO_LIGHT_TTF);
        Typeface typeRobotoBold = Typeface.createFromAsset(assets, FONTS_ROBOTO_BOLD_TTF);

        _company.setTypeface(typeRobotoRegular);
        _email.setTypeface(typeRobotoRegular);
        _tel.setTypeface(typeRobotoRegular);
        tv_company.setTypeface(typeRobotoRegular);
        tv_email.setTypeface(typeRobotoRegular);
        tv_tel.setTypeface(typeRobotoRegular);
    }

    private void setInfo() {
        _tel.setText(tel);
        _email.setText(email);
        _company.setText(company);
    }

    private void setClickListener() {
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // just simulating the flow
                onBackPressed();
                showMessage("Dados Alterados com sucesso!");
            }
        });


    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(CorporateProfileActivity.this, ProfileActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                startActivity(new Intent(CorporateProfileActivity.this, ProfileActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
