package digital.vee.valepay.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.azoft.carousellayoutmanager.DefaultChildSelectionListener;
import com.google.firebase.crash.FirebaseCrash;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyStoreException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.R.id;
import digital.vee.valepay.adapter.CarouselCardsAdapter;
import digital.vee.valepay.adapter.DrawerMenuAdapter;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.helper.LoginUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.model.DrawerMenuItem;
import digital.vee.valepay.model.Employee;
import digital.vee.valepay.model.EntityData;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.NamesUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Class that control the Main activity of Vee Pay
 *
 * @author Sérgio Brocchetto
 */
public class UseActivity extends BaseActivity implements OnNavigationItemSelectedListener {
    public static final String TAG = "UseActivity";
    public static final int ADD_NEW_CARD_REQUEST = 101;
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";
    private static final String URL = BuildConfig.SERVER_URL + "rest/authenticate";
    private static final int ID_MY_PAYMENTS = 0;
    private static final int ID_MY_WALLET = 1;
    private static final int ID_QRPAYMENT = 2;
    private static final int ID_RECHARGE = 3;
    private static final int ID_REFUND = 4;
    private static final int ID_PROFILE = 5;
    private static final int ID_CONTACT = 6;
    private static final String REFUND_ACTIVATED = "refundActivated";
    @Bind(id.v_symbol_total_use_home)
    TextView v_symbol_total;
    @Bind(id.balance_total_home)
    TextView _balanceTotal;
    @Bind(id.free_meal_restaurants)
    LinearLayout app_layer;
    @Bind(id.last_update_home)
    TextView _lastUpdateHome;
    @Bind(id.balance_layout)
    LinearLayout balanceLayout;
    @Bind(id.v_symbol_use_home_card)
    TextView _symbol;
    @Bind(id.balance_home)
    TextView _balanceHome;
    @Bind(id.balance_text_use)
    TextView _balance_text_use;
    @Bind(id.balance_total_text_use)
    TextView balance_total_text_use;
    @Bind(id.last_update_text_use)
    TextView last_update_text_use;
    // symbol on menu header
    @Bind(id.menu_v_symbol_header)
    TextView menu_v_symbol_header;
    // balance header menu
    @Bind(id.menu_balance_header)
    TextView _menuBalanceHeader;
    // user name at home header
    @Bind(id.menu_name_user_text_header)
    TextView _menuNameUserTextHeader;
    // last update date menu header
    @Bind(id.menu_last_update)
    TextView _menuLastUpdate;
    @Bind(id.btn_change_pin)
    Button _useButton;
    @Bind(id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @Bind(id.list_vertical)
    RecyclerView listVertical;
    @Bind(id.find_restaurants)
    LinearLayout findRest_layer;
    // stores the drawer element
    @Bind(id.drawer)
    DrawerLayout drawer;
    @Bind(id.nav_view)

    NavigationView navigationView;
    private Employee employee = new Employee();
    private ArrayList<Wallet> walletList = new ArrayList<>();
    private EntityData entityData = new EntityData();
    private CarouselCardsAdapter adapter;
    private long lastClickTimeUse = 0;
    private long lastClickTimeFreeMeal = 0;
    private long lastClickTimeFindRes = 0;
    private Menu menu;
    private Toolbar toolbar;
    private ActionBar supportActionBar;
    private IconicsDrawable iconicsDrawable;
    private Canvas canvas;
    private CryptoUtil cryptoUtil;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private JSONObject EspressoTeste;
    private Context context;
    private List<DrawerMenuItem> menuItemList;
    private DrawerMenuItem refundMenuItem;
    private DrawerMenuItem myPaymentsMenuItem;
    private DrawerMenuItem reversePayMenuItem;

    public TextView getBalance(){
        return _balanceHome;
    }
    private DrawerMenuAdapter drawerMenuAdapter;

    public JSONObject getEspressoTeste(){
        return EspressoTeste;
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize keystore when starting after activation
        initKeyStore();

        Preferences.init(this);

        createProgressDialog(this);

        callLogin();

        context = getApplicationContext();

        this.setContentView(R.layout.activity_use);

        setCustomActionBar();

        //show message if there's any
        showMessage(this.getIntent().getStringExtra("toast"));

        ButterKnife.bind(this);

        drawReturnicon();

        drawCanvas();

        this.createDrawerMenu(); // call method that creates the drawer menu from right side

        // Create view to manage card carousel
        initRecyclerView(listVertical, new CarouselLayoutManager(CarouselLayoutManager.VERTICAL, true));

/*        //use token to send request to /rest/use API name and balance to show on screen
        try {
            updateOperation();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }*/

        setSwipeRefresh(); // enable swipe refresh

        //set the fonts of textviews from asset font files
        this.setFonts();

        setClickListeners();

        destroySplash();
    }

    private void callLogin() {
        try {
            login(SnappyDBHelper.get(getApplicationContext(), LoginUtil.USERNAME),
                    SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD),
                    getApplicationContext(), true);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    private void initKeyStore() {
        try {
            cryptoUtil = new CryptoUtil(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    /**
     * Action of Image button to read a QRCode of a payment transaction.
     *
     */
    private void scanToolbar() {
        ActivityUtils.newActivityForResult(UseActivity.this,
                new Intent(this, ContinuousCaptureActivity.class),
                RC_BARCODE_CAPTURE);
        finish();
    }

    private void setClickListeners() {
        // Set Threshold to prevent clicking the same button twice under 1 second
        this._useButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeUse < 3000) {
                    return;
                }
                lastClickTimeUse = SystemClock.elapsedRealtime();
                scanToolbar();
            }
        });

        app_layer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeFreeMeal < 3000) {
                    return;
                }

                lastClickTimeFreeMeal = SystemClock.elapsedRealtime();

                ActivityUtils.newActivity(UseActivity.this,
                        new Intent(UseActivity.this, FreeMealActivity.class));
                finish();
            }
        });

        findRest_layer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String walletCode = "";
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeFindRes < 3000) {
                    return;
                }
                lastClickTimeFindRes = SystemClock.elapsedRealtime();
                finish();

                try {
                    walletCode = SnappyDBHelper.get(getApplicationContext(), "walletCode");
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }


                ActivityUtils.newActivity(UseActivity.this,
                        new Intent(UseActivity.this, FindRestaurantsListActivity_.class)
                                .putExtra(FindRestaurantsListActivity.SELECTED_CARD, walletCode));
                finish();
            }
        });
    }

    /**
    * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
    * performs a swipe-to-refresh gesture.
    */
    private void setSwipeRefresh() {
        this.swiperefresh.setOnRefreshListener(
                new OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        try {
                            updateOperation();
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    /**
     * Draw the canvas so we can have drawables, like de drawer menu
     */
    private void drawCanvas() {
        final Bitmap bitmap = Bitmap.createBitmap(iconicsDrawable.getIntrinsicWidth(),
                iconicsDrawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);

        canvas = new Canvas(bitmap);
        iconicsDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        iconicsDrawable.draw(canvas);
    }

    /**
     * Make the left arrow used to return to previous screen
     */
    private void drawReturnicon() {
        GoogleMaterial.Icon gmd_chevron_left = GoogleMaterial.Icon.gmd_chevron_left;
        IconicsDrawable chevron_back_icon = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_chevron_left)
                .color(Color.WHITE)
                .sizeDp(24);

        this.iconicsDrawable = new IconicsDrawable(getApplicationContext(), gmd_chevron_left)
                .color(Color.WHITE)
                .sizeDp(24);
    }

    private void setFonts() {

        Typeface type_roboto_light = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);

        // roboto bold font
        Typeface type_roboto_bold = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        // Use screen

        // name on menu
        this._menuNameUserTextHeader.setTypeface(type_roboto_bold);

        // symbol
        this.v_symbol_total.setTypeface(type_roboto_bold);
        // symbol on menu
        this.menu_v_symbol_header.setTypeface(type_roboto_bold);

        // Balance
        this._balanceTotal.setTypeface(type_roboto_light);
        // Balance on menu
        this._menuBalanceHeader.setTypeface(type_roboto_light);

        //data
        this._menuLastUpdate.setTypeface(type_roboto_light);

        this._balance_text_use.setTypeface(type_roboto_bold);

        this.balance_total_text_use.setTypeface(type_roboto_light);

        this._lastUpdateHome.setTypeface(type_roboto_light);

        this.last_update_text_use.setTypeface(type_roboto_light);
        // Fim do Header do menu


        // Balance bellow cards
        // Balance Text
        TextView tv_balance = (TextView) this.findViewById(id.balance_text_use);
        tv_balance.setTypeface(type_roboto_bold);


        // Vee symbol
        TextView tv_balance_symbol = (TextView) this.findViewById(id.v_symbol_use_home_card);
        tv_balance_symbol.setTypeface(type_roboto_bold);


        // 0,00 balance
        TextView tv_balance_home = (TextView) this.findViewById(id.balance_home);
        tv_balance_home.setTypeface(type_roboto_bold);


        // use button
        this._useButton.setTypeface(type_roboto_bold);

        // use screen end

        _menuBalanceHeader.setTypeface(type_roboto_bold);
    }

    /**
     * Sends app back to first activation after logout
     */
    private void redirStart() {
        ActivityUtils.newActivity(UseActivity.this,
                new Intent(UseActivity.this, SignupCPFActivity.class));
        finish();
    }

    /**
     * Create the menu, sets methods to inflate the menu and control it's behavior.
     * Adds all the options to a list view.
     * The 'view terms' and 'logout' options are outside the list and are set as TextView with
     * custom fonts.
     */
    private void createDrawerMenu() {
        //drawer config starts here
        menuItemList = getDrawerMenuItems();
        drawerMenuAdapter = new DrawerMenuAdapter(this, menuItemList);
        //getEncrypted NavigationView layout inside  navigation drawer
        ListView drawerList = (ListView) this.findViewById(id.list_view);

        //Sets footer, all the menu items via getDrawerMenuItems and a click listener
        drawerList.addFooterView(customFooter());
        drawerList.setAdapter(drawerMenuAdapter);
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        //Set toggle to be used with drawer listener
        this.drawer.addDrawerListener(setToggle(this.getMenuInflater()));

        // Set Width of navigation view
        setNavWidth();
    }

    /**
     * In the menu, logout and terms are in the footer instead of ListView, here are set custom font
     * and clickListeners.
     * Links the variable footer with the xml footer.
     *
     * @return view with the custom footer
     */
    private View customFooter() {
        View footer = getLayoutInflater().inflate(R.layout.menu_footer, null);
        TextView termsTV = (TextView) footer.findViewById(id.terms_title);
        TextView logoutTV = (TextView) footer.findViewById(id.logout_title);

        // Set custom fonts and underlines the logout
        logoutTV.setPaintFlags(logoutTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        termsTV.setTypeface(Typeface.createFromAsset(this.getAssets(),
                LoginUtil.FONTS_ROBOTO_LIGHT_TTF));
        logoutTV.setTypeface(Typeface.createFromAsset(this.getAssets(),
                LoginUtil.FONTS_ROBOTO_BOLD_TTF)); // Bold for the login font is thicker than terms font

        // Set the click listener to both terms and logout
        termsTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.newActivity(UseActivity.this,
                        new Intent(UseActivity.this, TermsActivity.class));
                finish();
            }
        });
        logoutTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutLinkClicked();
            }
        });

        return footer;
    }

    /**
     * Method that will control the slide of the menu whenever is opening or closing by the onDrawerSlide method
     *
     * @param menuInflater instance of menu inflater, to show the menu on screen
     * @return Implementation of the onDrawerSlide of the DrawerListener interface
     */
    @NonNull
    private ActionBarDrawerToggle setToggle(final MenuInflater menuInflater) {
        return new ActionBarDrawerToggle(
                this, this.drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset == 0 &&
                        getSupportActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD) {
                    if (drawer.isDrawerOpen(GravityCompat.END)) {
                        closeDrawerMenu(menu, _menuBalanceHeader);
                    } else {
                        openDrawerMenu(menu, menuInflater);
                    }
                } else if (slideOffset != 0
                        && getSupportActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD) {

                    if (drawer.isDrawerOpen(GravityCompat.END)) {
                        closeDrawerMenu(menu, _menuBalanceHeader);
                    } else {
                        openDrawerMenu(menu, menuInflater);
                    }

                    // Once menu is fully opened, change offset to 1 with super method
                    super.onDrawerSlide(drawerView, slideOffset);
                }
            }
        };
    }

    /**
     * Set Width of navigation view the same as screen width
     */
    private void setNavWidth() {
        // Get parameters
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        // Set the parameter's width with the display's screen width
        params.width = this.getResources().getDisplayMetrics().widthPixels;
        // put it back in the navigation View to be used with the new Width
        navigationView.setLayoutParams(params);
    }

    /**
     * Make a list of every item that will show up in the menu to be set in the ListView
     * 
     * @return ArrayList with the menu items inside
     */
    @NonNull
    private List<DrawerMenuItem> getDrawerMenuItems() {
        List<DrawerMenuItem> itemList = new ArrayList<>();

        this.myPaymentsMenuItem = new DrawerMenuItem(UseActivity.ID_MY_PAYMENTS, this.getString(R.string.menu_text_payments), R.drawable.transaction_list_image_selector);
        this.refundMenuItem = new DrawerMenuItem(UseActivity.ID_REFUND, getString(R.string.menu_text_refund), R.drawable.refund_image_selector);
        this.reversePayMenuItem = new DrawerMenuItem(ID_QRPAYMENT, getString(R.string.generate_payment_code), R.drawable.reversal_pay_image_selector,
                getString(R.string.menu_text_qr_explain));

        itemList.add(myPaymentsMenuItem);
        itemList.add(new DrawerMenuItem(UseActivity.ID_MY_WALLET, this.getString(R.string.menu_text_my_wallet), R.drawable.ic_my_wallet));
        itemList.add(reversePayMenuItem);
        itemList.add(new DrawerMenuItem(UseActivity.ID_RECHARGE, getString(R.string.recharge), R.drawable.icon_recharge));
        // the refund menu item can change it state depends on user properties and wallet selected
        if (Preferences.getBoolean(UseActivity.REFUND_ACTIVATED, false)) {
            itemList.add(UseActivity.ID_REFUND, this.refundMenuItem);
        }
        itemList.add(new DrawerMenuItem(UseActivity.ID_PROFILE, this.getString(R.string.menu_text_profile), R.drawable.ic_profile));
        itemList.add(new DrawerMenuItem(UseActivity.ID_CONTACT, this.getString(R.string.menu_text_contact), R.drawable.ic_contact));

        return itemList;
    }

    private void openDrawerMenu(Menu menu, MenuInflater menuInflater) {
        // Checks if has to change the menu buttons on the toolbar

        if (menu.getItem(0).getTitle().equals(getString(R.string.menu))) {
            menu.clear();
            menuInflater.inflate(R.menu.main2, menu);
            this.menu = menu;
        }

        View viewById = getWindow().getDecorView()
                .findViewById(id.toolbar);

        if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().findViewById(id.toolbar)
                    .setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            getWindow().getDecorView().findViewById(id.logo)
                    .setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        drawer.openDrawer(GravityCompat.END); // .END sets the menu motion from right to left
        swiperefresh.setEnabled(false); // when the menu is open there is no refresh
    }

    private void closeDrawerMenu(Menu menu, TextView menuBalance) {
        try {
            menuBalance.setText(SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET));
        } catch (SnappydbException e) {
            e.printStackTrace();

            menuBalance.setText(null);
        }

        // check if the menu button 'back' was clicked in the toolbar
        if (menu.getItem(0).getTitle().equals("back")) {
            menu.clear();
            getMenuInflater().inflate(R.menu.main, menu); //inflates the layout with the 3 stacked dots
            this.menu = menu;
        }

        View viewById = getWindow().getDecorView().findViewById(id.toolbar);
        if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            viewById.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            getWindow().getDecorView().findViewById(id.logo)
                    .setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        drawer.closeDrawer(GravityCompat.END); // Close the drawerMenu with the gravity on the right side
        swiperefresh.setEnabled(true);
    }

    @Override
    public void onBackPressed() {
        //Log.v(UseActivity.TAG, "+onBackPressed()");
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            closeDrawerMenu(menu, _menuBalanceHeader);
        } else {
            this.finish();
        }
    }


    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) this.findViewById(id.toolbar);
        this.setSupportActionBar(toolbar);
        this.supportActionBar = this.getSupportActionBar();
        if (this.supportActionBar != null) {
            this.supportActionBar.setDisplayHomeAsUpEnabled(false);
            this.supportActionBar.setDisplayShowHomeEnabled(false);
            this.supportActionBar.setDisplayShowTitleEnabled(false);
        }

    }

    /**
     * Create a Dialog to alert the user if he really want to logout, and logs out if he click to
     * confirm.
     */
    private void logoutLinkClicked() {
        AlertDialog alertDialog = new AlertDialog.Builder(UseActivity.this).create();

        alertDialog.setTitle(getString(R.string.wish_to_continue));
        alertDialog.setMessage(getString(R.string.deauthorize1) + " " +
                getString(R.string.deauthorize2));
        alertDialog.setCanceledOnTouchOutside(false);
        // Alert dialog button

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                        //redirStart();
                        dialog.dismiss();// use dismiss to cancel alert dialog
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();// use dismiss to cancel alert dialog
                    }
                });
        alertDialog.show();
    }

    /**
     * Clear preferences so device is no longer used and needs to be activated again
     */
    private void logout() {

        String token = null;

        Preferences.clear();
        SnappyDBHelper.destroy(getApplicationContext());

        redirStart();
    }

    /**
     * Check if backarrow or menuRight has been clicked
     * @param item menu item that can be back arrow  or  menuRight
     * @return true if item has been clicked
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //Log.v(TAG, "onOptionsItemSelected item: " + id);

        if (id == R.id.back_arrow) {

            if (this.drawer.isDrawerOpen(GravityCompat.END)) {
                this.menu.clear();
                this.getMenuInflater().inflate(R.menu.main, this.menu);
                //this.toggle.syncState();

                View viewById = getWindow().getDecorView().findViewById(R.id.toolbar);
                if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                    viewById.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                    getWindow().getDecorView().findViewById(R.id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }
                this.drawer.closeDrawer(GravityCompat.END);
                swiperefresh.setEnabled(true);
            } else {
                TextView tv_balance_menu = (TextView) this.findViewById(R.id.menu_balance_header);

                String balance = null;
                try {
                    balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                tv_balance_menu.setText(balance);

                if (menu.getItem(0).getTitle().equals("Menu")) {
                    menu.clear();
                    getMenuInflater().inflate(R.menu.main2, menu);
                }

                View viewById = getWindow().getDecorView().findViewById(R.id.toolbar);
                if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
                    getWindow().getDecorView().findViewById(R.id.toolbar).setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    getWindow().getDecorView().findViewById(R.id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }

                this.supportActionBar.setDisplayShowHomeEnabled(false);
                this.supportActionBar.setDisplayShowTitleEnabled(false);

                this.drawer.openDrawer(GravityCompat.END);
                swiperefresh.setEnabled(false);
            }
        } else if (id == R.id.menuRight) {
            //Log.v(UseActivity.TAG, "TAG " + "onOptionsItemSelected menuRight");
            if (this.drawer.isDrawerOpen(GravityCompat.END)) {
                this.menu.clear();
                this.getMenuInflater().inflate(R.menu.main, this.menu);

                this.drawer.closeDrawer(GravityCompat.END);
                swiperefresh.setEnabled(true);
            } else {
                TextView tv_balance_menu = (TextView) this.findViewById(R.id.menu_balance_header);

                String balance = null;
                try {
                    balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
                dFormat.setMaximumFractionDigits(2);
                dFormat.setMinimumFractionDigits(2);

                Double balanceD = Double.parseDouble(balance);
                tv_balance_menu.setText(String.format("%s", dFormat.format(balanceD)));

                if (menu.getItem(0).getTitle().equals("Menu")) {
                    menu.clear();
                    getMenuInflater().inflate(R.menu.main2, menu);
                }

                View viewById = getWindow().getDecorView().findViewById(R.id.toolbar);
                if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
                    getWindow().getDecorView().findViewById(R.id.toolbar).setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    getWindow().getDecorView().findViewById(R.id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }

                this.drawer.openDrawer(GravityCompat.END);
                swiperefresh.setEnabled(false);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Treats open and close of inverted Navigation Drawer
     * It opens from right to left
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            View viewById = getWindow().getDecorView().findViewById(R.id.toolbar);
            if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
                getWindow().getDecorView().findViewById(R.id.toolbar).setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                getWindow().getDecorView().findViewById(R.id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
            //finish();
            drawer.closeDrawer(GravityCompat.END);
            swiperefresh.setEnabled(true);
        } else {
            final TextView tv_balance_menu = (TextView) this.findViewById(R.id.menu_balance_header);

            String balance = null;
            try {
                balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
            dFormat.setMaximumFractionDigits(2);
            dFormat.setMinimumFractionDigits(2);

            Double balanceD = Double.parseDouble(balance);
            tv_balance_menu.setText(String.format("%s", dFormat.format(balanceD)));

            View viewById = getWindow().getDecorView().findViewById(R.id.toolbar);
            if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
                getWindow().getDecorView().findViewById(R.id.toolbar).setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                getWindow().getDecorView().findViewById(R.id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
            drawer.openDrawer(GravityCompat.END);
            swiperefresh.setEnabled(false);
        }
        return true;
    }

    /**
     * When option menu is created the drawer is opened and several textviews get data to be
     * displayed to the user
     *
     * @param menu menu items such as the back arrow on the toolbar
     * @return boolean true when finish
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menu = this.menu;
        this.getMenuInflater().inflate(R.menu.main, menu);
        boolean openDrawer = this.getIntent().getBooleanExtra("OpenDrawer", false);
        if (openDrawer) {
            TextView tv_balance_menu = (TextView) this.findViewById(R.id.menu_balance_header);

            String balance;

            if (entityData != null && entityData.getTotalBalance() != null) {
                balance = entityData.getTotalBalance();
                Double balanceD = Double.parseDouble(balance);
                tv_balance_menu.setText(String.format("%s", setDecimal(balanceD)));

            } else {
                tv_balance_menu.setText(String.format("%s", setDecimal(0.0)));
            }

            openDrawerMenu(menu, getMenuInflater());

            View viewById = getWindow().getDecorView().findViewById(id.toolbar);
            if (viewById.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
                getWindow().getDecorView().findViewById(id.toolbar).setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                getWindow().getDecorView().findViewById(id.logo).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
            this.drawer.openDrawer(GravityCompat.END, false);
        }

        this.menu = menu;

        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
            try {
                updateOperation();
            } catch (SnappydbException e) {
                e.printStackTrace();
                //initialize keystore when starting after activation
                try {
                    cryptoUtil = new CryptoUtil(getApplicationContext());
                } catch (KeyStoreException e1) {
                    e1.printStackTrace();
                }
            }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (!Preferences.getBoolean("firstInit")) {
                updateOperation();
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
            //initialize keystore when starting after activation
            try {
                cryptoUtil = new CryptoUtil(getApplicationContext());
            } catch (KeyStoreException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //initialize keystore when starting after activation
        try {
            cryptoUtil = new CryptoUtil(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start activation for a Card, called if it's not the first card of the user.
     */
    private void redirSignup2() {
        ActivityUtils.newActivityForResult(
                UseActivity.this,
                new Intent(this, ActivateActivity.class)
                        .putExtra("addNewCard", true),
                ADD_NEW_CARD_REQUEST);
    }

    public CarouselCardsAdapter getAdapter(){
        return adapter;
    }

    /**
     * Initiates the recyclerView creates the adapter, sets the scroll to scroll up and down the cards,
     *  get the id of the card clicked when the card is clicked and add a clicklistener
     *

     * @param recyclerView Object of the class creating the carousel
     * @param layoutManager Object containing carousel
     */
    private void initRecyclerView(final RecyclerView recyclerView, final CarouselLayoutManager layoutManager) {
        // create the adapter of cards
        adapter = new CarouselCardsAdapter(getApplicationContext(), walletList,
                entityData.getName());

        // enable zoom effect. this line can be customized
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(2);

        recyclerView.setLayoutManager(layoutManager);
        // we expect only fixed sized item for now
        recyclerView.setHasFixedSize(true);
        // sample adapter with random data
        recyclerView.setAdapter(adapter);
        // enable center post scrolling
        recyclerView.addOnScrollListener(new CenterScrollListener());
        // enable center post touching on item and item click listener

        try {
            String index = SnappyDBHelper.get(getApplicationContext(), "index");
            if (!index.isEmpty()) {
                recyclerView.scrollToPosition(Integer.valueOf(index));
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        // enable center post touching on item and item click listener
        DefaultChildSelectionListener.initCenterItemListener(new DefaultChildSelectionListener.OnCenterItemClickListener() {
            @Override
            public void onCenterItemClicked(@NonNull final RecyclerView recyclerView, @NonNull final CarouselLayoutManager carouselLayoutManager, @NonNull final View v) {

                final int position = recyclerView.getChildLayoutPosition(v);

                if (adapter.getItemWallet(position).getWalletType().getCode().equalsIgnoreCase("vAdd")) {
                    //call activation if Add New Card is clicked
                    redirSignup2();
                }
            }

        }, recyclerView, layoutManager);

        // When item changes, the balance is updated to be shown on screen
        layoutManager.addOnItemSelectionListener(new CarouselLayoutManager.OnCenterItemSelectionListener() {

            @Override
            public void onCenterItemChanged(final int adapterPosition) {
                if (CarouselLayoutManager.INVALID_POSITION != adapterPosition) {

                    Wallet wallet = adapter.getItemWallet(adapterPosition);
                    try {
                        updateBalanceOfWallet(wallet);
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    try {
                        SnappyDBHelper.put(getApplicationContext(),"index", String.valueOf(adapterPosition));
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    /**
     * Get how much money the user currently have and display on screen
     * @param wallet contains information on user's wallet
     * @throws SnappydbException
     */
    private void updateBalanceOfWallet(Wallet wallet) throws SnappydbException {
        String entityId = SnappyDBHelper.get(getApplicationContext(), "entityId");
        SnappyDBHelper.put(getApplicationContext(), "walletEntityId", String.valueOf(entityId));
        SnappyDBHelper.put(getApplicationContext(), "walletCode", String.valueOf(wallet.getWalletType().getCode()));

        //TODO:save card position

        if (wallet.getWalletType().getCode().equalsIgnoreCase("vAdd")) {
            balanceLayout.setVisibility(View.INVISIBLE);
            _useButton.setEnabled(false);
        }
        else {
            SnappyDBHelper.put(getApplicationContext(), "walletId", String.valueOf(wallet.getIdentifier()));

            String balance = wallet.getValueBalance();
            balanceLayout.setVisibility(View.VISIBLE);
            _useButton.setEnabled(true);
            // Get the correct format for the balance
            balance = setDecimal(Double.parseDouble(balance));
            SnappyDBHelper.put(getApplicationContext(), App.BALANCE_HOME, String.valueOf(balance));
            _balanceHome.setText(balance);
        }

        updateDrawerMenu();
    }

    /**
     * Check if all the cards activated were already added to the listView.
     * Updates the correct balance after a transaction has changed it.
     * @throws SnappydbException
     */
    public void updateUserData() throws SnappydbException {

        JSONObject responseReturned = null;
        try {

            if (SnappyDBHelper.exists(getApplicationContext(), "CARDS")) {
                try {
                    responseReturned = new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS"));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                if (responseReturned == null) {
                    //null, abort updateUserData!
                    return;
                }
                //parse response and load on models
                doParseResponse(responseReturned);
            }

            if (responseReturned == null) {

                Log.v(TAG, "null responseReturned, abort updateUserData()!");
                return;
            }
            //parse response and load on models
            doParseResponse(responseReturned);

        } catch (SnappydbException e) {
            SnappyDBHelper.put(getApplicationContext(), "CARDS", responseReturned.toString());
            e.printStackTrace();
        }

        if (adapter != null) {
            adapter.updateWallets(walletList, entityData.getName());
            if (!walletList.isEmpty()) {

                int indexOfWallet;

                // change wallet
                RecyclerView listVertical = (RecyclerView) findViewById(id.list_vertical);
                CarouselLayoutManager clm = (CarouselLayoutManager) listVertical.getLayoutManager();
                indexOfWallet = clm.getCenterItemPosition();

                if (indexOfWallet < 0) {
                    // the current wallet must be the Add New Card, that is not in the wallet list
                    indexOfWallet = adapter.getItemCount() - 1;
                }

                // update the balance of the wallet on the selected index
                updateBalanceOfWallet(adapter.getItemWallet(indexOfWallet));

                SnappyDBHelper.put(getApplicationContext(), "index", (String.valueOf(indexOfWallet)));
            }
        }

        displayBalance();

        this.swiperefresh.setRefreshing(false);

        dismissDialog();
        if (Preferences.getBoolean("firstInit")) {
            Preferences.putBoolean("firstInit", false);
        }
    }

    /**
     * Get name, balance and date, format both with trim and decimal digits, color, and puts on the correct
     * TextViews.
     * @throws SnappydbException
     */
    private void displayBalance() throws SnappydbException {
        String firstName = "";
        String balance = "0,00";

        if (employee != null) {
            firstName = employee.getFirstName();
        }

        if (entityData != null) {
            balance = entityData.getTotalBalance();
        }
        String displayName = NamesUtil.trimFirstName(firstName);
        Double balanceD = Double.parseDouble(balance);

        SnappyDBHelper.put(getApplicationContext(), "balance", balance);

        balance = setDecimal(balanceD);


        _menuNameUserTextHeader.setText(String.format(" %s :)", displayName));

        SnappyDBHelper.put(getApplicationContext(), "name", displayName);

        if (balanceD.compareTo(0D) < 0) {
            this._balanceTotal.setTextColor(Color.RED);
            this.v_symbol_total.setTextColor(Color.RED);
        } else {
            int color = ContextCompat.getColor(this, R.color.green);
            this._balanceTotal.setTextColor(color);
            this.v_symbol_total.setTextColor(color);
        }

        _balanceTotal.setText(balance);
        _menuBalanceHeader.setText(balance);

        SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.VEE_DATE_TIME_FORMAT);
        String dateFormatted = formatter.format(System.currentTimeMillis());

        SnappyDBHelper.put(getApplicationContext(), "lastUpdated", dateFormatted);
        //Log.v(UseActivity.TAG, "dateFormatted: " + dateFormatted);
        _lastUpdateHome.setText(dateFormatted);
        _menuLastUpdate.setText(dateFormatted);

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            TextView tv_balance_menu = (TextView) this.findViewById(id.menu_balance_header);
            tv_balance_menu.setText(String.format("%s", balance));
        }
    }

    /**
     * Verify if is online, fetch wallets data from service, otherwise getEncrypted it from local database
     *
     * @throws SnappydbException
     */
    public void updateOperation() throws SnappydbException {
        if (Network.isOnline(UseActivity.TAG, getApplicationContext())) {
            // Get wallet data if online
            Response.Listener<String> responseListener = new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        //Log.d(TAG, "---response: " + response);
                        JSONObject jsonResponse = new JSONObject(response);
                        EspressoTeste = new JSONObject(response);
                        SnappyDBHelper.put(getApplicationContext(), "CARDS", response);
                        employee = Employee.fromJson(jsonResponse);

                        // this preference decides whether or not the refund option will be created in the menu
                        Preferences.putBoolean(UseActivity.REFUND_ACTIVATED, jsonResponse.getBoolean("refundActivated"));
                        JSONObject abstractEntity = jsonResponse.getJSONObject("abstractEntity");
                        entityData = EntityData.fromJson(abstractEntity);
                        SnappyDBHelper.put(context, "idEntityNumber", entityData.getIdEntity());
                        // Get wallet list from array tagged "walletList" on the JSON object
                        walletList = Wallet.fromJson(abstractEntity.getJSONArray("walletList"));

                        SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.VEE_DATE_TIME_FORMAT);
                        String dateFormatted = formatter.format(System.currentTimeMillis());
                        SnappyDBHelper.put(getApplicationContext(), "lastUpdated", dateFormatted);
                        //Log.v(UseActivity.TAG, "dateFormatted: " + dateFormatted);

                        updateUserData();

                        updatePendingRefund(jsonResponse);

                        swiperefresh.setRefreshing(false);
                    } catch (Exception e) {
                        Log.v(UseActivity.TAG, "Exception: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // Update data of wallet if offline or request error
                    try {
                        updateUserData();
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showMessage(getString(R.string.internet_connection));
                            //Log.d(TAG, "error: " + getString(R.string.internet_connection));
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            //Log.v(TAG, "*** error: " + error.getClass().getName());
                            showMessage(getString(R.string.no_connection));

                        } else {
                            //Log.v(TAG, "*** error: " + error.getClass().getName());
                            showMessage(getString(R.string.unknow_error));
                        }
                    }
                }
            };

            JSONObject jsonBody = new JSONObject();
            /*try {
                jsonBody.put("apiVersion", 2);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            String requestBody = jsonBody.toString();

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, USER_DATA_URL,
                    responseListener, errorListener, requestBody) {
                Request.Priority mPriority;

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept-Language", "pt_br");
                    try {
                        params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    params.put("Content-Type", "application/json");
                    params.put("apiVersion", "2");
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    // Check if there's high priority, if not is returned normal priority
                    return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
                }
            };

            Volley.newRequestQueue(getApplicationContext()).add(postRequest);
        }
        // Update wallet data if offline
        updateUserData();
    }

    /**
     * Check if there is a pending refund to show refund activity.
     *
     * @param jsonResponse authenticated JSON response
     * @throws JSONException
     * @throws SnappydbException
     */
    private void updatePendingRefund(JSONObject jsonResponse) throws JSONException, SnappydbException {
        if (jsonResponse.has("pendingRefund")) {
            Preferences.init(getApplicationContext());
            Preferences.putBoolean(MainActivity.REFUND_CONFIRM, true);

            JSONObject refundPayload = jsonResponse.getJSONObject("pendingRefund");
            SnappyDBHelper.putRefundPayload(getApplicationContext(), refundPayload);

            ActivityUtils.newActivity(UseActivity.this,
                    new Intent(UseActivity.this, RefundInformationActivity.class));
            finish();
        }
    }

    /**
     * Update state of items in drawer menu
     */
    private void updateDrawerMenu() {
        this.refundMenuItem.setEnabled(this.isOptionEnabled());

        // Add Card must no have reversepay and payment list
        boolean enabled = !this.isAddCard();
        this.reversePayMenuItem.setEnabled(enabled);
        this.myPaymentsMenuItem.setEnabled(enabled);

        boolean refundActivated = Preferences.getBoolean(UseActivity.REFUND_ACTIVATED, false);
        if (refundActivated && !this.menuItemList.contains(this.refundMenuItem)) {
            // refund is active and not in menu, so add it
            this.menuItemList.add(ID_REFUND, this.refundMenuItem);
        }
        else if (!refundActivated && this.menuItemList.contains(this.refundMenuItem)) {
            // refund is not active and it is in menu, so remove it
            this.menuItemList.remove(this.refundMenuItem);
        }

        this.drawerMenuAdapter.notifyDataSetChanged();
    }

    /**
     * Parse the JSON file to put another wallet on the arraylist walletList
     * @param jsonRoot JSON containing information on the user
     */
    private void doParseResponse(JSONObject jsonRoot) {

        // Employee Data

        employee = Employee.fromJson(jsonRoot);

        // AbstractEntity
        try {
            entityData = EntityData.fromJson(jsonRoot.getJSONObject("abstractEntity"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            walletList = Wallet.fromJson(jsonRoot.getJSONObject("abstractEntity").getJSONArray("walletList"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create mapping of list of items on Navigation Drawer and associate the proper action
     */
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            int selectedId = drawerMenuAdapter.getDrawerMenuItemId(position);

            switch (selectedId) {
                case UseActivity.ID_REFUND:
                    if (isOptionEnabled()) {
                        ActivityUtils.newActivity(UseActivity.this,
                                new Intent(UseActivity.this, CaptureReceiptActivity.class));
                    } else {
                        showLongMessage(getString(R.string.multicard_refund));
                    }
                    break;
                case UseActivity.ID_PROFILE:
                    ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, ProfileActivity.class));
                    finish();
                    break;
                case UseActivity.ID_MY_PAYMENTS:
                    if (!isAddCard()){
                        ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, TransactionsListActivity.class)
                                .putExtra("balance", entityData.getTotalBalance()));
                        finish();
                    }
                    break;
        /*        case UseActivity.ID_VOUCHER:
                    ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, VouchersActivity.class));
                    finish();
                    break;*/
                case UseActivity.ID_MY_WALLET:
                    ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, AccountsActivity_.class)
                                    .putExtra("balance", entityData.getTotalBalance()));
                    finish();
                    break;
                case ID_CONTACT:
                    ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, ContactsActivity.class));
                    finish();
                    break;

                case ID_QRPAYMENT:
                    if (!isAddCard()) {
                        ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, PayBeingScannedActivity.class));
                        finish();
                    }
                    break;
                case ID_RECHARGE:
                    ActivityUtils.newActivity(UseActivity.this,
                            new Intent(UseActivity.this, GiftCardActivity.class)
                                    .putExtra("shortcut", true));
                    finish();
                    break;
            }
        }
    }

    /**
     * Validate if selected wallet accepts refund. Wallets with code v1, v2, v3 and v4 accepts.
     * Other wallets should not enable menu item. Meaning user cannot ask for refund when using
     * Vee Cash.
     *
     * @return <code>true</code> for wallet that accepts refund.
     */
    private boolean isOptionEnabled(){
        String walletCode = "";
        try {
            walletCode = SnappyDBHelper.get(getApplicationContext(), "walletCode");
        } catch (SnappydbException e){
            e.printStackTrace();
        }
        if(!walletCode.isEmpty() && !walletCode.equals("v1") && !walletCode.equals("v2")
                && !walletCode.equals("v3") && !walletCode.equals("v4")){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if Card currently selected is the grey "Add Card" card
     *
     * @return true if Add Card is selected
     */
    private boolean isAddCard() {
        String walletCode = "";
        try {
            walletCode = SnappyDBHelper.get(getApplicationContext(), "walletCode");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        return walletCode.equals("vAdd");
    }

    /**
     * Validate data input and call login service on server.
     *
     * @param userId
     * @param password
     * @param context
     * @param getData tell if need to fetch cards data or not
     * @throws RuntimeException
     * @throws JSONException
     */

    public void login(String userId, String password, final Context context, final boolean getData) throws RuntimeException, JSONException {
        Preferences.init(context);

        String login = userId;
        if (LoginUtils.isValidCPF(login)) {
            // user input a CPF, so remove extra chars before send to server
            login = login.replaceAll("[ .-]", "");
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("userId", login);
            jsonBody.put("password", password);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }

        final String mRequestBody = jsonBody.toString();

        final Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject json = new JSONObject(response);
                    String type = json.getString("type"), token = json.getString("token"),
                            subject = json.getString("subject");
                    //Log.v(TAG, "--- token: + " + token);
                    SnappyDBHelper.putEncrypted(context, LoginUtil.TOKEN_KEY, token);
                    //Log.v(TAG, "Response: " + response);

                    if (token == null || token.isEmpty()) {
                        FirebaseCrash.log("---LoginUtil: error on login invalid token: " + token);
                        //Log.v(TAG, "---LoginUtil: error on login invalid token: " + token);
                    }

                    if (getData) {
                        onLoginSuccess(context);
                    }
                    updateOperation();
                } catch (Exception e) {
                    FirebaseCrash.log("---LoginUtil: error on login " + e);
                    //Log.d(TAG,"---LoginUtil: error on login " + e);
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /* whenever there is a error with login and it is the user first time, we cannot
                 allow app usage because the app will have no card, balance or info. */
                if (Preferences.getBoolean("firstInit")) {
                    try {
                        showLoginErrorDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    callLogin();
                } else {
                    try {
                        updateOperation();
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener, errorListener,
                mRequestBody) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-authc-username-password+json";
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //Log.v(TAG, " +parseNetworkError()");
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    try {
                        onErrorResponse(volleyError);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                //Log.v(TAG, TAG + " -parseNetworkError()");
                return volleyError;
            }
        };
        Volley.newRequestQueue(context).add(postRequest);
    }

    /**
     * Process error on response from server and log or show it to user.
     *
     * @param error Error received from server.
     * @throws UnsupportedEncodingException
     */
    private void onErrorResponse(VolleyError error) throws UnsupportedEncodingException {

        //Log.v(TAG, " +onErrorResponse()");
        dismissDialog();
        String json = null;

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    json = trimMessage(json, "message");

                    if (!json.isEmpty())
                        displayMessage(json);
                    break;
                default:
                    Log.v(TAG, "error: " + json);
            }

        }
        //Log.v(TAG, " -onErrorResponse()");
    }

    /**
     * Display a toast message to user in UTF-8 format
     *
     * @param toastString Message to show
     */
    private void displayMessage(String toastString) {
        String s = null;
        try {
            s = new String(toastString.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Toast.makeText(this.context, s, Toast.LENGTH_LONG).show();
    }

    /**
     * Process success on login
     */
    private void onLoginSuccess(final Context context) {
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            String response;

            @Override
            public void onResponse(String response) {
                try {
                    this.response = response;
                    JSONObject jsonResponse = new JSONObject(response);

                    //Log.v(TAG, "Response: " + jsonResponse.toString());

                    // SnappyDB
                    try {
                        //DB snappydb = DBFactory.open(context); //create or open an existing database using the default name
                        // salva o json

                        //snappydb.put("CARDS", response);
                        //String responseReturned = snappydb.get("CARDS");

                        SnappyDBHelper.put(context, "CARDS", response);

                        String responseReturned = SnappyDBHelper.get(context, "CARDS");

                        //Log.v(TAG, "responseReturned 1 " + responseReturned);

                        // caso retorne online a lista atualiza

                        //Faz o parse e carrega no models

                        //snappydb.close();

                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }

                    employee = Employee.fromJson(jsonResponse);

                    JSONObject abstractEntity = jsonResponse.getJSONObject("abstractEntity");
                    entityData = EntityData.fromJson(abstractEntity);

                    walletList = Wallet.fromJson(abstractEntity.getJSONArray("walletList"));

                    SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.VEE_DATE_TIME_FORMAT);
                    String dateFormatted = formatter.format(System.currentTimeMillis());
                    SnappyDBHelper.put(context, "lastUpdated", dateFormatted);

                    //Log.v(TAG, "dateFormatted: " + dateFormatted);

                } catch (Exception e) {
                    //Log.v(TAG, "2 Exception: " + e.getMessage());

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.v(TAG, "error: " + error);
                //TODO: implement
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, USER_DATA_URL,
                responseListener, errorListener, null) {
            Request.Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");

                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(context, LoginUtil.TOKEN_KEY));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Content-Type", "application/json");
                params.put("apiVersion","2");
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
            }
        };

        Volley.newRequestQueue(context).add(postRequest);

        //Log.d(TAG, "-OnloginSuccess");
    }

    /**
     * Get message from JSON response received from server
     *
     * @param json JSON response text
     * @param key Key to find message in JSON response
     * @return Message from JSON or <code>null</code> in case of no message in JSON.
     * @throws UnsupportedEncodingException
     */
    private String trimMessage(String json, String key) throws UnsupportedEncodingException {
        String trimmedString;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        trimmedString = Arrays.toString(trimmedString.getBytes("UTF-8"));
        return trimmedString;
    }

    /**
     * When the first login throws an error, this message is shown to user informing that we need at least
     * his first login before continuing.
     */
    public void showLoginErrorDialog() {
        try {
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog
                    .Builder(UseActivity.this)
                    .create();
            alertDialog.setTitle(getString(R.string.error));
            alertDialog.setMessage(getString(R.string.login_error));
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL,
                    getString(R.string.try_again),
                    (dialog, which) -> {
                        dialog.dismiss();
                        callLogin();
                    });

            alertDialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called because the splash activity does not call finish() after calling this
     * activity
     */
    private void destroySplash() {
        final long TIME_TO_WAIT_TO_DESTROY_SPLASH = 5000;
        Handler handle = new Handler();
        handle.postDelayed(() ->
            SplashActivity.splashActivity.finish()
            , TIME_TO_WAIT_TO_DESTROY_SPLASH);
    }
}