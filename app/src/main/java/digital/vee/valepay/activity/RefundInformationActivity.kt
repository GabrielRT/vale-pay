package digital.vee.valepay.activity

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import com.android.volley.Request
import com.android.volley.Response
import digital.vee.valepay.R
import digital.vee.valepay.helper.DateUtils
import digital.vee.valepay.helper.SnappyDBHelper
import digital.vee.valepay.helper.VolleySingleton
import digital.vee.valepay.util.Preferences
import digital.vee.valepay.util.StringRequestCustom
import kotlinx.android.synthetic.main.activity_refund_information.*
import org.json.JSONException
import org.json.JSONObject
import android.support.design.widget.BottomSheetDialog
import android.widget.RelativeLayout
import com.android.volley.TimeoutError
import com.google.firebase.crash.FirebaseCrash
import com.snappydb.SnappydbException
import digital.vee.valepay.BuildConfig
import digital.vee.valepay.model.RefundReceipt
import digital.vee.valepay.util.ActivityUtils
import digital.vee.valepay.util.LoginUtil
import java.util.HashMap

/**
 * Created by silvio on 16/03/18.
 *
 * Considerations: Kotlin makes the IDE heavier; Many kotlin features are used in this class so it
 * can be used as example of the language's capacity.
 *
 */
class RefundInformationActivity : BaseActivity() {
    private val FONTS_ROBOTO_REGULAR_TTF: String = "fonts/Roboto-Regular.ttf"
    private val FONTS_ROBOTO_BOLD_TTF: String = "fonts/Roboto-Bold.ttf"
    private val CANCEL_URL = BuildConfig.SERVER_URL + "rest/private/refund/cancel"
    private val REFUSE_URL = BuildConfig.SERVER_URL + "rest/private/refund/refuseData"
    private val RESPONSE_OK: Int = 1

    private lateinit var uuid: String
    private lateinit var receipt: RefundReceipt

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refund_information)

        createProgressDialog(this)

        Preferences.init(applicationContext)

        Preferences.putBoolean("refundConfirm", true)

        getInformation()

        setFonts()

        setListener()
    }

    /**
     * On back, open the menu to take the correct action
     */
    override fun onBackPressed() {
        openMenu()
    }

    /**
     * Create instance of data class with information from intent
     * to be passed on to setValues()
     */
    private fun getInformation() {
        if (Preferences.getBoolean(MainActivity.REFUND_CONFIRM)) {

            if (SnappyDBHelper.exists(this, "receipt")) {
                receipt = RefundReceipt.fromJson(JSONObject(SnappyDBHelper.get(this, "receipt")))

                try {
                    uuid = SnappyDBHelper.get(this, "refundUUID")
                } catch (e: SnappydbException) {
                    e.printStackTrace()
                }

                setValues()
            } else {
                ActivityUtils.exitActivity(this)
                Preferences.putBoolean(MainActivity.REFUND_CONFIRM, false)
            }
        }
    }

    private fun setListener() {
        // nothin to do yet, so just go back to Use.
        accept_btn.setOnClickListener {
            ActivityUtils.newActivity(this, Intent(this, RefundPINActivity::class.java)
                    .putExtra("uuid", uuid)
                    .putExtra("value", receipt.value))
            finish()
        }

        // If options btn clicked, dialog will be opened
        options_btn.setOnClickListener {
            openMenu()
        }
    }

    /**
     * Inflate dialog and make click listeners for the view
     */
    private fun openMenu() {
        val dialog = BottomSheetDialog(this)

        if (!dialog.isShowing) {
            // TODO see alternatives for this null
            val view = layoutInflater.inflate(R.layout.bottomsheet_refund, null)
            val cancel: RelativeLayout = view.findViewById(R.id.cancel_refund_layout)
            val manualRefund: RelativeLayout = view.findViewById(R.id.manual_refund_layout)
            val goBack: RelativeLayout = view.findViewById(R.id.goback_layout)

            cancel.setOnClickListener {
                cancelRefund()
                dialog.dismiss()
            }

            manualRefund.setOnClickListener {
                refuseRefund()
                dialog.dismiss()
            }

            goBack.setOnClickListener {

                dialog.dismiss()
            }
            dialog.setContentView(view)
            dialog.show()
        }
    }

    private fun refuseRefund() {
        createProgressDialog(this)
        val token: String = SnappyDBHelper.getEncrypted(this, LoginUtil.TOKEN_KEY)

        val post = object : StringRequestCustom(Request.Method.POST, REFUSE_URL,
                makeResponse(), makeErrorListener(), getBody()) {
            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()

                if (token.isEmpty()) {
                    FirebaseCrash.log("---ChangePINCodeActivity: invalid token: $token")
                }

                params["Authorization"] = "Token $token"

                params["Accept-Language"] = "pt_br"

                return params
            }
        }
        post.retryPolicy = VolleySingleton.noRetryDefaultTimeRequest()
        VolleySingleton.getInstance(this).addToRequestQueue(post)
    }

    private fun setFonts() {
        val robotoRegular: Typeface? = Typeface.createFromAsset(this.assets, FONTS_ROBOTO_REGULAR_TTF)
        val robotoBold: Typeface? = Typeface.createFromAsset(this.assets, FONTS_ROBOTO_BOLD_TTF)

        tv_cnpj.typeface = robotoRegular
        tv_cnpj_value.typeface = robotoRegular

        tv_receipt.typeface = robotoRegular
        tv_receipt_value.typeface = robotoRegular

        tv_date.typeface = robotoRegular
        tv_date_value.typeface = robotoRegular

        tv_value.typeface = robotoRegular
        tv_value_number.typeface = robotoBold
    }

    /**
     * Put information from the data class instance on the TextView
     */
    private fun setValues() {
        // TODO correct concatenation below
        tv_cnpj_value.text = cnpjFormat(receipt.document)
        tv_receipt_value.text = getSAT(receipt.receiptNumber)
        tv_date_value.text = DateUtils.getDateTimeFromUTC(receipt.date)
        tv_value_number.text = "$ " + setDecimal(receipt.value)
        dismissDialog()
    }

    /**
     * The receipt AccessKey have 44 characters, this method take only the SAT number
     */
    private fun getSAT(number: String?): CharSequence? {
        val charArray = number?.toCharArray()
        var newString = ""
        try {
            if(charArray?.size!! > 10){
                for (i in 22..30) {
                    newString += charArray[i]
                }
            } else {
                newString = number
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
        return newString
    }

    private fun cnpjFormat(document: String?): CharSequence? {
        var newString = ""
        document?.forEach { char ->
            if (newString.length == 2 || newString.length == 6) {
                newString += '.'
                newString += char
            } else if (newString.length == 10) {
                newString += '/'
                newString += char
            } else if (newString.length == 15) {
                newString += '-'
                newString += char
            } else {
                newString += char
            }
        }
        return newString
    }

    /**
     * Make volley request to the cancel url with the refund UUID so it's canceled
     */
    private fun cancelRefund() {
        createProgressDialog(this)
        val token: String = SnappyDBHelper.getEncrypted(this, LoginUtil.TOKEN_KEY)

        val post = object : StringRequestCustom(Request.Method.POST, CANCEL_URL,
                makeResponse(), makeErrorListener(), getBody()) {
            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()

                if (token.isEmpty()) {
                    FirebaseCrash.log("---ChangePINCodeActivity: invalid token: $token")
                }

                params["Authorization"] = "Token $token"

                params["Accept-Language"] = "pt_br"

                return params
            }
        }
        post.retryPolicy = VolleySingleton.noRetryDefaultTimeRequest()
        VolleySingleton.getInstance(this).addToRequestQueue(post)
    }

    private fun getBody(): String? {
        val jsonObject = JSONObject()
        jsonObject.put("refundUUID", uuid)
        return jsonObject.toString()
    }

    private fun makeErrorListener(): Response.ErrorListener? {
        return Response.ErrorListener { error ->
            dismissDialog()
            if (error.javaClass == TimeoutError::class.java) {
                showTimeoutDialog(this)
            } else {
                showLongMessage("Ocorreu um erro com seu pedido.")
            }
            error.printStackTrace()
        }
    }

    private fun makeResponse(): Response.Listener<String>? {
        return Response.Listener { response ->
            try {
                val jsonObject = JSONObject(response)
                takeAction(jsonObject.getString("message"))
            } catch (e: JSONException) {
                e.printStackTrace()
                takeAction("")
            }
        }
    }

    /**
     * Redirects user to home after cancellation request.
     */
    private fun takeAction(message: String?) {
        //puts false so this activity is no longer the first on opening the app
        Preferences.putBoolean("refundConfirm", false)
        dismissDialog()
        ActivityUtils.exitActivity(this)
        showLongMessage(message)
    }
}
