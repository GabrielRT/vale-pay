package digital.vee.valepay.activity;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.Toast;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;
import com.yelp.clientlib.annotation.Nullable;

import org.json.JSONException;

import java.security.KeyStoreException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Used to Manipulate the Login info of the user
 * Often called with extends when login is needed
 * 
 * Created by Sérgio Brocchetto on 17/05/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    static ProgressDialog progressDialog;
    final CryptoUtil[] cryptoUtil = {null};

    /**
     * Do the login again using stored credentials
     */
    public void doLogin() {
        Preferences.init(getApplicationContext());
        LoginUtil loginUtil = new LoginUtil();
        try {
            loginUtil.login(SnappyDBHelper.get(getApplicationContext(), LoginUtil.USERNAME),
                    SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD),
                    getApplicationContext(), true);
        } catch (JSONException | SnappydbException e) {
            e.printStackTrace();
        }
    }
    /**
     * Do the login again using stored credentials but does not overwrite the authenticate json
     * that is stored in the snappyDB
     */
    public void doLogin(boolean update) {
        Preferences.init(getApplicationContext());
        LoginUtil loginUtil = new LoginUtil();
        try {
            loginUtil.login(SnappyDBHelper.get(getApplicationContext(), LoginUtil.USERNAME),
                    SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD),
                    getApplicationContext(), update);
        } catch (JSONException | SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void showMessage(String message) {
        if (message != null && !message.isEmpty()) {
            Toast toast1 = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast1.show();
        }
    }

    public void showLongMessage(String message) {
        if (message != null && !message.isEmpty()) {
            Toast toast1 = Toast.makeText(this, message, Toast.LENGTH_LONG);
            toast1.show();
        }
    }

    public static String setDecimal(double balance){
        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);

        return dFormat.format(balance);
    }

    public void redirHome() {
        ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class));
        dismissDialog();
        finish();
    }

    public static void redirHome(Context context) {
        ActivityUtils.exitActivity(context, new Intent(context, UseActivity.class));
        dismissDialog();
        ((Activity) context).finish();
    }

    public static void deleteQrCode(Context context){
        try {
            SnappyDBHelper.del(context, "qrcode");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public String getToken(){
        try {
            return SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY);
        } catch (SnappydbException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * concatenate the strings necessary to form the device alias
     *
     * @return device alias
     */
    public String getAlias() {
        String deviceAlias = Build.MODEL + "_" + Build.VERSION.CODENAME +
                Build.VERSION.RELEASE + "_" + Build.VERSION.SDK_INT;
        return deviceAlias;
    }

    /**
     * Create a default progress dialog to use in communication with server
     */
    public static void createProgressDialog(Context context, String message, boolean cancelable,
                                            @Nullable DialogInterface.OnCancelListener onCancelListener) {
        progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(cancelable);
        progressDialog.setIndeterminate(true);

        if (onCancelListener != null) {
            progressDialog.setOnCancelListener(onCancelListener);
        }

        if (message == null || message.isEmpty()) {
            progressDialog.setMessage(context.getString(R.string.toast_loading));
        } else {
            progressDialog.setMessage(message);
        }
        showProgressDialog();
    }

    public static void createProgressDialog(Context context, boolean cancelable,
                                            @Nullable DialogInterface.OnCancelListener onCancelListener) {
        createProgressDialog(context, "", cancelable, onCancelListener);
    }

    public static void createProgressDialog(Context context, String message) {
        createProgressDialog(context, message, false, null);
    }

    public static void createProgressDialog(Context context) {
        createProgressDialog(context, "", false, null);
    }

    public static void showProgressDialog() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    /**
     * Dismiss current progress dialog in case it is running.
     */
    public static void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * This method show a dialog informing user when timeout has occurred and redirects him to Home
     *
     * @param intent Whenever an error will change activities, an intent must be passed so it is executed
     */
    public void showTimeoutDialog(Context context, @Nullable Intent intent) {
        try {
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog
                    .Builder(context)
                    .create();
            alertDialog.setTitle(getString(R.string.error));
            alertDialog.setMessage(getString(R.string.timeout_connection_error));
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL,
                    getString(R.string.ok),
                    (dialog, which) -> {
                        dialog.dismiss();

                        // if there is intent, execute it.
                        if (intent != null) {
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        }
                    });
            alertDialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }
    public void showTimeoutDialog(Context context) {
        showTimeoutDialog(context, null);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Put the white arrow on the toolbar and sets it to go back to use activity
     */
    public void setToolbar() {
        Toolbar toolbar = this.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }
    }

    /**
     * Go back to use activity with the drawerMenu open
     */
    public void redirUseOpenMenu() {
        ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class)
                        .putExtra("OpenDrawer", true));
        finish();
    }


    /**
     * Initialize a cryptoutil object that automatically initiate the KeyStore
     */
    public void iniKeyStore(final Context context) {
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cryptoUtil[0] = new CryptoUtil(context);
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                }
            }});

        t2.start(); // spawn thread

        try {
            t2.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
