package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pushwoosh.Pushwoosh;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Login;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.PhoneMaskUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Class that control SignupActivity actions and behaviors.
 * Created by Sérgio Brocchetto
 */
public class SignupActivity extends BaseActivity {
    private static final String URL = BuildConfig.SERVER_URL + "rest/signup";
    private static final String TAG = "SignupActivity";

    @Bind(R.id.input_name)
    EditText _nameText;
    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.btn_submit)
    Button _signupButton;
    @Bind(R.id.input_phone)
    EditText _phone;

    private final String[][] keyPair = {null};
    String devicePushId;
    private CryptoUtil cryptoUtil;
    //generate random 128 char
    final String generatedPassword= Login.randomString(128);
    private long mLastClickTimeLogin = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);

        setToolbar();

        setClickListeners();

        setFonts();

        iniKeyStore();

        generateKey();

        devicePushId = Pushwoosh.getInstance().getHwid();
        try {
            SnappyDBHelper.putEncrypted(getApplicationContext(), "devicePushId", devicePushId);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    private void signup() {

        if (validate()) {
            try {
                SnappyDBHelper.put(getApplicationContext(), LoginUtil.USERNAME, getDocument());
                SnappyDBHelper.putEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD, generatedPassword);
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, setResponse(),
                    setErrorListener(), mRequestBody());
            postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());

            VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
        } else {
            dismissDialog();
        }
    }

    private String mRequestBody() {
        JSONObject jsonObject = new JSONObject();
        String name = _nameText.getText().toString();
        try{
            jsonObject.put("document", getDocument());
            jsonObject.put("name", name);
            jsonObject.put("email", _emailText.getText().toString());
            jsonObject.put("phone", _phone.getText().toString());
            jsonObject.put("deviceAlias", getAlias());
            jsonObject.put("deviceId", SnappyDBHelper.getEncrypted(getApplicationContext(), "devicePushId"));
            jsonObject.put("password", generatedPassword);
            jsonObject.put("publicKey",SnappyDBHelper.getEncrypted(getApplicationContext(),"ECDSA_PUBLIC_KEY_HEX"));

        } catch (JSONException | SnappydbException e){

            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private Response.ErrorListener setErrorListener() {

        return error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                dismissDialog();
                showTimeoutDialog(this);
            } else {
                showMessage("Erro ao processar seu pedido");
                error.printStackTrace();
                onSignupFailed();
            }
        };
    }

    private Response.Listener<String> setResponse(){

        return response -> {
            try{
                JSONObject jsonObject = new JSONObject(response);
                showMessage(jsonObject.getString("message"));
                if(jsonObject.getString("personalId") != null){
                    SnappyDBHelper.put(getApplicationContext(),"entityId", jsonObject.getString("personalId") );
                }
            } catch (JSONException | SnappydbException e){
                e.printStackTrace();
            }
            signupSucces();
            redirPIN();
        };
    }

    private void signupSucces() {
        Thread t = new Thread(() -> {
            // do the login
            LoginUtil loginUtil = new LoginUtil();
            //get username and password stored in local database to do the login
            try {
                loginUtil.login(SnappyDBHelper.get(getApplicationContext(), LoginUtil.USERNAME),
                        SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD),
                        getApplicationContext(), true);

            } catch (JSONException | SnappydbException e) {
                e.printStackTrace();
            }
        });

        t.start(); // spawn thread

        try {
            t.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void redirPIN(){
        finish();
        ActivityUtils.newActivity(this, new Intent(this, ConfirmPINCodeActivity_.class));
        Preferences.putBoolean(MainActivity.SENT_INFO, true);
        dismissDialog();
    }

    private void setClickListeners() {
        _signupButton.setOnClickListener(v -> {

            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTimeLogin < 3000) {
                return;
            }

            mLastClickTimeLogin = SystemClock.elapsedRealtime();

            // if there are no numbers in the name, proceed
            if(isAlpha(_nameText.getText().toString())){
                createProgressDialog(SignupActivity.this);
                if (Network.isOnline(SignupActivity.TAG, getApplicationContext())) {
                    signup();
                } else {
                    onLoadFailed();
                }
            } else{
                _nameText.requestFocus();

                _nameText.setError("O nome não deve conter números");
            }

        });
    }

    private boolean isAlpha(String name) {
        for(int i = 0; i <= name.length()-1; i++) {
            if (Character.isDigit(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }



    private void onLoadFailed() {
        dismissDialog();
        showMessage(getString(R.string.internet_connection));
    }

    /**
     * Process error on SignUp and show it to user.
     */
    private void onSignupFailed() {
        dismissDialog();
        Toast.makeText(getBaseContext(), R.string.toast_signup_failed, Toast.LENGTH_LONG).show();
    }

    /**
     * Set fonts and mask on telephone field
     */
    private void setFonts() {
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_regular = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_REGULAR_TTF);

        TextView tv = (TextView) findViewById(R.id.text_start_using);
        tv.setText(Html.fromHtml(getString(R.string.signup_to_use)));
        tv.setTypeface(type_roboto_light);

        _nameText.setTypeface(type_roboto_regular);
        _emailText.setTypeface(type_roboto_regular);
        _phone.addTextChangedListener(PhoneMaskUtil.insert(_phone));
        _signupButton.setTypeface(type_roboto_bold);

    }

    /**
     * Put the white arrow on the toolbar and sets it to go back to use activity
     */
    public void setToolbar() {
        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }
    }

    @Override
    public void onBackPressed(){
        finish();
        ActivityUtils.exitActivity(this, new Intent(SignupActivity.this, SignupCPFActivity.class));
    }

    /**
     * If user clicks to return, it will call redirUseOpenMenu
     * @param item item clicked by the user, usually the back arrow
     * @return true if user chose to return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                ActivityUtils.exitActivity(this, new Intent(SignupActivity.this, SignupCPFActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Generate string matrix with a key pair generated from the CryptoUtil object
     */
    private void generateKey() {
            if (!keyExist()) {
                //generate EC key pair and save to preferences
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            keyPair[0] = cryptoUtil.generateKeyPair();
                            try{
                                SnappyDBHelper.putEncrypted(getApplicationContext(),"ECDSA_PUBLIC_KEY_HEX",keyPair[0][0]);
                                SnappyDBHelper.putEncrypted(getApplicationContext(),"ECDSA_PRIVATE_KEY_HEX",keyPair[0][1]);
                                SnappyDBHelper.putEncrypted(getApplicationContext(),Preferences.ECDSA_PUBLIC_KEY,keyPair[0][2]);
                                SnappyDBHelper.putEncrypted(getApplicationContext(),Preferences.ECDSA_PRIVATE_KEY,keyPair[0][3]);
                            } catch (Exception e){

                                e.printStackTrace();
                            }

                        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                            e.printStackTrace();
                        }
                    }
                });

                t.start(); // spawn thread
            }
    }

    private boolean keyExist() {
        try{
            if(SnappyDBHelper.exists(getApplicationContext(), "ECDSA_PUBLIC_KEY_HEX") &&
            SnappyDBHelper.exists(getApplicationContext(), "ECDSA_PRIVATE_KEY_HEX") &&
            SnappyDBHelper.exists(getApplicationContext(), Preferences.ECDSA_PUBLIC_KEY) &&
            SnappyDBHelper.exists(getApplicationContext(), Preferences.ECDSA_PRIVATE_KEY)){
                return true;
            }
        } catch (SnappydbException e){

            e.printStackTrace();
        }
        return false;
    }

    private void iniKeyStore() {
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cryptoUtil = new CryptoUtil(getApplicationContext());
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                }
            }});

        t2.start(); // spawn thread

        try {
            t2.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Validate Sign Up input data.
     * @return <code>true</code> if data is valid
     */
    private boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String email = _emailText.getText().toString();
        String phone = _phone.getText().toString();

        if (phone.isEmpty() || phone.length() < 8) {
            _phone.requestFocus();
            _phone.setError(getString(R.string.invalid_phone));
            valid = false;
        } else {
            _phone.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.error_email_signup));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (name.isEmpty() || !name.contains(" ") || name.substring(0, name.indexOf(" ")).length() < 3
                || name.substring(name.indexOf(" "), name.length()).length() < 3
                || name.length() > 255) {

            _nameText.setError(getString(R.string.error_name_signup));
            valid = false;
        } else {
            _nameText.setError(null);
        }

        return valid;
    }


    public String getDocument() {
        return getIntent().getExtras().getString("document");
    }
}