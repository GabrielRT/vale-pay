package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.widget.Button;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.R.id;
import digital.vee.valepay.fragment.TermWebView;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;


/**
 * Class to show terms to be accepted
 *
 * @author Bruno Cesar
 */
public class AcceptTermsActivity extends BaseActivity {
    public static final String TAG = "AcceptTermsActivity";

    @Bind(id.btn_accept) Button _acceptButton;
    @Bind(id.btn_deny) Button _denyButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initiate the class with stored user info
        Preferences.init(this);

        createProgressDialog(AcceptTermsActivity.this);

        // isTermsAccepted false by default unless user accepts it
        boolean isTermsAccepted = Preferences.getBoolean(MainActivity.TERMS_ACCEPTED, false);
        if (isTermsAccepted) {
            ActivityUtils.exitActivity(this, new Intent(AcceptTermsActivity.this, SignupCPFActivity.class));
            this.finish();
        }

        this.setContentView(R.layout.activity_terms_accept);

        ButterKnife.bind(this);

        // Button will not be enabled until user has scrolled WebView to the end
        _acceptButton.getBackground().setAlpha(128);
        _acceptButton.setEnabled(false);

        loadWebView();

        setClickListener();

        //set the fonts of text views from asset font files
        this.setFonts();
    }

    private void setClickListener() {
        this._acceptButton.setOnClickListener(new OnClickListener() {
            long lastClickTimeAccept = 0;

            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeAccept < 3000) {
                    return;
                }
                lastClickTimeAccept = SystemClock.elapsedRealtime();

                redirSignup();
            }
        });
        // sends user back if he does not accept the terms
        this._denyButton.setOnClickListener(new OnClickListener() {
            long lastClickTimeDeny = 0;

            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeDeny < 3000) {
                    return;
                }
                lastClickTimeDeny = SystemClock.elapsedRealtime();

                redirStart();
            }
        });
    }

    /**
     * Send user to activate if he already accepted terms
     */
    private void redirSignup() {
        Preferences.putBoolean(MainActivity.TERMS_ACCEPTED, true);
        ActivityUtils.exitActivity(this, new Intent(this, SignupCPFActivity.class));
        this.finish();
    }

    private void setFonts() {
        // roboto bold font
        Typeface type_roboto_bold = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        // buttons
        this._acceptButton.setTypeface(type_roboto_bold);
        this._denyButton.setTypeface(type_roboto_bold);
    }
    
    private void redirStart() {
        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }

    @Override
    public void onBackPressed() {
        //do not back
    }

    /**
     * Load webview containing the terms
     */
    public void loadWebView(){
        TermWebView contractWebView = findViewById(id.webview);

        contractWebView.setOnScrollChangedCallback(new TermWebView.OnScrollChangedCallback() {
            @Override
            public void onScroll(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
                if (clampedY) {
                    _acceptButton.getBackground().setAlpha(255);
                    _acceptButton.setEnabled(true);
                }
            }
        });
        //load content of html into WebView
        WebSettings settings = contractWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        if(Locale.getDefault().getLanguage().equalsIgnoreCase("en")){
            contractWebView.loadUrl("file:///android_asset/terms/terms-en.html");
        }else {
            contractWebView.loadUrl("file:///android_asset/terms/terms.html");
        }
        dismissDialog();
    }
}