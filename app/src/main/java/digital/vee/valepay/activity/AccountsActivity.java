package digital.vee.valepay.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.adapter.AccountsAdapter;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.model.Employee;
import digital.vee.valepay.model.EntityData;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Network;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Show list of active Cards and option to activate a new one
 */

@EActivity(R.layout.activity_accounts)
public class AccountsActivity extends BaseActivity {
    /**
     * points the requests to the correct server, either PROD or QA
     */
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";


    public static final String TAG = "AccountsActivity";

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById
    ListView list;

    @Bean
    AccountsAdapter adapter;

    private Employee employee = new Employee();
    private ArrayList<Wallet> walletList = new ArrayList<>();
    private EntityData entityData = new EntityData();

    @AfterViews
    void afterViews() {
        Preferences.init(this);

        setToolbar();

        doLogin();

        list.setAdapter(adapter);

        try {
            updateData(getApplicationContext());
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

    }

    public AccountsAdapter getAdapter(){
        return adapter;
    }
    /**
     * Get all the cards registered on local database to be displayed on a list
     * @param context Context from wheres been called
     * @throws SnappydbException
     */
    private void updateData(Context context) throws SnappydbException {
        //FIXME: usar String.format
        JSONObject responseReturned = null;
        //TODO: obter da Base Pegar
        // SnappyDB
        try {
            //DB snappydb = DBFactory.open(getApplicationContext()); //create or open an existing database using the default name

            responseReturned = null;
            try {
                responseReturned = new JSONObject(SnappyDBHelper.get(context, "CARDS"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Log.v(TAG, "responseReturned 1 " + responseReturned);

            //Faz o parse e carrega no models
            doParseResponse(responseReturned);

            //snappydb.close();

        } catch (SnappydbException e) {
            /*DB snappydb = null;
            try {
                snappydb = DBFactory.open(getApplicationContext());
            } catch (SnappydbException e1) {
                e1.printStackTrace();
            }*/
            try {
                SnappyDBHelper.put(context, "CARDS", responseReturned != null ? responseReturned.toString() : null);
            } catch (SnappydbException e1) {
                e1.printStackTrace();
            }

            //snappydb.close();
            e.printStackTrace();
        }
        if (adapter != null) {
            adapter.updateWalletList(walletList, entityData.getName());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @OptionsItem(android.R.id.home)
    void homeClicked() {
        redirUseOpenMenu();
    }

    // goes to activation screen so that the user can activate another card
    @Click(R.id.addCard)
    void addCardClicked() {
        Intent intent = new Intent(this, ActivateActivity.class);
        //informing that this is not this device first acrivation
        intent.putExtra("addNewCard", true);
        ActivityUtils.newActivityForResult(this, intent, UseActivity.ADD_NEW_CARD_REQUEST);
    }

    /**
     * If results are returned positive this methods calls updateCards so that the newly activated card
     * is also shown on the list
     * @param resultCode If the card has been successfully activated
     * @param data on the result
     */
    @OnActivityResult(UseActivity.ADD_NEW_CARD_REQUEST)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // try update cards to getEncrypted new card added
            try {
                updateCards();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send info on new card to be activated to the server, stored the new card on local database also
     * updateData is called so the newly stored card shows on the card list
     * @throws SnappydbException
     */
    private void updateCards() throws SnappydbException {

        if (Network.isOnline(AccountsActivity.TAG, getApplicationContext())) {
            //Obtém os dados das carteiras, caso esteja online
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                String response;

                @Override
                public void onResponse(String response) {
                    try {

                        this.response = response;
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.v(UseActivity.TAG, "Response: " + jsonResponse.toString());
                        // SnappyDB
                        try {
                            //create or open an existing database using the default name
                            //DB snappydb = DBFactory.open(getApplicationContext());
                            // salva o json
                            SnappyDBHelper.put(getApplicationContext(), "CARDS", response);
                            String responseReturned = SnappyDBHelper.get(getApplicationContext(), "CARDS");
                            //Log.v(TAG, "responseReturned 1 " + responseReturned);
                            //snappydb.close();
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                        updateData(getApplicationContext());

                    } catch (Exception e) {
                        //Log.v(UseActivity.TAG, "2 Exception: " + e.getMessage());
                        //UseActivity.this.loadDataFailed();
                        e.printStackTrace();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // Atualiza os dados das carteiras se estiver offline ou erro no request
                    try {
                        updateData(getApplicationContext());
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                }
            };

            JSONObject jsonBody = new JSONObject();

            String requestBody = jsonBody.toString();

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, USER_DATA_URL,
                    responseListener, errorListener, requestBody) {
                Request.Priority mPriority;

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept-Language", "pt_br");
                    try {
                        params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    params.put("Content-Type", "application/json");
                    params.put("apiVersion", "2");
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
                }
            };

            Volley.newRequestQueue(getApplicationContext()).add(postRequest);
        }
        else {
            // Atualiza os dados das carteiras se estiver offline
            updateData(getApplicationContext());
        }
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    private void doParseResponse(JSONObject jsonRoot) {
        // Dados do employee
        employee = Employee.fromJson(jsonRoot);

        // AbstractEntity
        try {
            entityData = EntityData.fromJson(jsonRoot.getJSONObject("abstractEntity"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            walletList = Wallet.fromJson(jsonRoot.getJSONObject("abstractEntity").getJSONArray("walletList"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
