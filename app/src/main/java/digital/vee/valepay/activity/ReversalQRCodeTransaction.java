package digital.vee.valepay.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.DateUtils;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.ZXingQRCode.QRCodeEncoder;

/**
 * Class that show data and QRCode of a specific transaction.
 */
@EActivity(R.layout.activity_reversal_qrcode_transaction)
public class ReversalQRCodeTransaction extends BaseActivity {

    private static final String TAG = "ReversalQRCodeTransact";
    private static final String URL = BuildConfig.SERVER_URL + "rest/private/transaction/find";

    @ViewById(R.id.date)
    TextView _date;

    @ViewById(R.id.totalAmount)
    TextView _totalAmount;

    @ViewById(R.id.text_cancel)
    TextView _cancelText;

    @ViewById(R.id.name_user_text_header)
    TextView _nameText;

    @ViewById(R.id.balance_header)
    TextView _balanceHeader;

    @ViewById(R.id.v_symbol_header)
    TextView _vSymbol;

    @ViewById(R.id.last_update)
    TextView _lastUpdatedText;

    private long lastClickTimeLoginCancel;

    @AfterViews
    void afterViews() {

        Preferences.init(getApplicationContext());

        ButterKnife.bind(this);

        setToolbar();

        String totalAmount = getIntent().getStringExtra("amount");
        String walletMerchant = getIntent().getStringExtra("walletTo");
        String walletEmployee = null;
        try {
            walletEmployee = SnappyDBHelper.get(getApplicationContext(), LoginUtil.WALLET_ID);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        String dtConfirmed = getIntent().getStringExtra("dtConfirmed");
        String transactionUUID = getIntent().getStringExtra("uuid");

        String dateFormatted = DateUtils.getDateFromUTC(dtConfirmed);
        dtConfirmed = dateFormatted != null ? dateFormatted : dtConfirmed;
        _date.setText(getString(R.string.payment_date) + " " + dtConfirmed);

        Typeface type_roboto_bold = Typeface.createFromAsset(getApplicationContext().getAssets(),
                LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        BigDecimal bgAmount = new BigDecimal(totalAmount);

        Double amountD = Double.parseDouble(totalAmount);

        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);

        totalAmount = dFormat.format(amountD);

        _totalAmount.setTypeface(type_roboto_bold);
        _vSymbol.setTypeface(type_roboto_bold);

        _totalAmount.setText(String.format("%s R$ %s", getString(R.string.pay_qr_total_amount), totalAmount));

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("walletEmployee", walletEmployee);
            jsonBody.put("walletTo", walletMerchant);
            jsonBody.put("totalAmount", bgAmount);
            jsonBody.put("uuid", transactionUUID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();

        //generate QR image signed with public key
        //Find screen size
        WindowManager manager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(mRequestBody,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);

        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            ImageView myImage = (ImageView) findViewById(R.id.qr_code);
            myImage.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        _cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimeLoginCancel < 3000) {
                    return;
                }

                lastClickTimeLoginCancel = SystemClock.elapsedRealtime();

                ActivityUtils.exitActivity(ReversalQRCodeTransaction.this);
            }
        });

        try {
            updateUserData();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update data of logged user
     */
    private void updateUserData() throws SnappydbException {
        String checkBalance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        String balance = checkBalance != null &&
                !checkBalance.isEmpty() ? checkBalance : "";
        String checkLast = SnappyDBHelper.get(getApplicationContext(), "lastUpdated");
        String lastUpdated = checkLast != null &&
                !checkLast.isEmpty() ? checkLast : "";
        String checkName = SnappyDBHelper.get(getApplicationContext(), "name");
        String firstName = checkName != null &&
                !checkName.isEmpty() ? checkName : "";
        String displayName;
        if (firstName.contains(" ")) {
            displayName = firstName.substring(0, firstName.indexOf(" "));
        } else {
            displayName = firstName;
        }
        if (displayName.length() > 25) {
            displayName = displayName.substring(0, 25);
        }

        _nameText.setText(String.format(" %s :)", displayName));

        Double balanceD = Double.parseDouble(balance);

        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);

        balance = dFormat.format(balanceD);

        _balanceHeader.setText(balance);

        _lastUpdatedText.setText(lastUpdated);


        if (balanceD.compareTo(0D) < 0) {
            _balanceHeader.setTextColor(Color.RED);
            _vSymbol.setTextColor(Color.RED);
        } else {
            int color = ContextCompat.getColor(getApplicationContext(), R.color.green);
            _balanceHeader.setTextColor(color);
            _vSymbol.setTextColor(color);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityUtils.exitActivity(ReversalQRCodeTransaction.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        ActivityUtils.exitActivity(ReversalQRCodeTransaction.this);
        //super.onBackPressed();
        //NavUtils.navigateUpFromSameTask(this);
    }

    /**
     * Set ActionBar with custom drawable and icons
     */
    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.GRAY)
                    .sizeDp(24));
        }
    }
}
