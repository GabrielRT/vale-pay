package digital.vee.valepay.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.model.Refund;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;

/**
 * Show information when the refund has been found and has not been rejected. This class gets the
 * previous wallet balance from json stored on SnappyDB and the current updated balance from the
 * authenticated REST service.
 * <p>
 * Created by silvio on 20/04/18.
 */

public class RefundOKActivity extends BaseActivity {

    @Bind(R.id.balance_value_purchase_text)
    TextView tvRefundValue;
    @Bind(R.id.restaurant_name)
    TextView tvRestaurantName;
    @Bind(R.id.balance_previous_text)
    TextView tvPreviousBalance;
    @Bind(R.id.balance_payment_text)
    TextView tvCurrentBalance;
    @Bind(R.id.payment_date)
    TextView tvDate;
    @Bind(R.id.balance_text)
    TextView tvPaymentText;
    @Bind(R.id.purchase_success_text)
    TextView tvRefundSuccessText;

    // Auth request variables
    private static final String AUTH_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";
    private String walletFrom;
    private double refundValue;
    private String currentBalance;
    private Refund refund;
    private String date;

    @Override
    public void onCreate(Bundle savedStateInstance) {
        super.onCreate(savedStateInstance);

        setContentView(R.layout.activity_pay_ok);

        ButterKnife.bind(this);

        createProgressDialog(this);

        setFonts();

        // call authenticated to get updated wallet balance
        callAuth();

        // make ok button send user to home
        findViewById(R.id.btn_thankyou_ok).setOnClickListener(v -> ActivityUtils.exitActivity(this));
    }

    private void setFonts() {
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);
        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);

        tvRefundValue.setTypeface(type_roboto_bold);
        tvPreviousBalance.setTypeface(type_roboto_light);
        tvCurrentBalance.setTypeface(type_roboto_light);
        tvDate.setTypeface(type_roboto_light);
        tvPaymentText.setTypeface(type_roboto_light);
    }

    /**
     * Call authenticated url to get fresh wallet balance. If we got to this screen, it means the auth
     * will already return the balance without the refund money, already good to be displayed.
     */
    private void callAuth() {
        getIntentInfo();

        StringRequest request = new StringRequest(Request.Method.GET, AUTH_URL, makeResponse(),
                makeError()) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Content-Type", "application/json");
                params.put("apiVersion", "2");
                return params;
            }
        };
        request.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    /**
     * Information on the refund is passed via Intent. The global variables are initialized in this
     * method by obtaining information from inside de Intent
     */
    private void getIntentInfo() {
        tvRestaurantName.setVisibility(View.GONE); // refund doesn't have merchant name

        try {
            // call model class Refund
            refund = Refund.fromJson(new JSONObject(getIntent().getStringExtra("refund")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        walletFrom = refund.walletFrom;
        refundValue = refund.value;

        if (refund.dtConfirmed == null) {
            tvDate.setVisibility(View.GONE); // If no date, then visibility will be gone
        } else {
            tvDate.setText(String.format("%s %s", getString(R.string.payment_date), refund.dtConfirmed));
        }
    }

    private Response.ErrorListener makeError() {
        return error -> {
            error.printStackTrace();
            setValuesOffline();
        };
    }

    private void setValuesOffline() {
        try {
            JSONObject jsonObject = new JSONObject(SnappyDBHelper.get(this,"CARDS"));
            JSONArray walletList = jsonObject
                    .getJSONObject("abstractEntity")
                    .getJSONArray("walletList");

            // 0 - balance, 1 - index
            double wallet[] = getWallet(walletList);

            /* Previous balance is what we have stored, current balance is previous minus what was
              spent. Values are immediately to TextView */
            double previousBalance = wallet[0];
            currentBalance = setDecimal(previousBalance - refundValue);
            tvPreviousBalance.setText(String.format("%s %s", "$", setDecimal(previousBalance)));
            tvCurrentBalance.setText(String.format("%s %s", "$", currentBalance));
            tvRefundValue.setText(String.format("%s %s", "$", setDecimal(refundValue)));

            /* Update offline the information on the wallets and put it back on snappy to be used
               until a new information can be retrieved from server */
            jsonObject.getJSONObject("abstractEntity")
                    .getJSONArray("walletList")
                    .getJSONObject((int) wallet[1])
                    .put("valueBalance", previousBalance - refundValue);

            SnappyDBHelper.put(this,"CARDS", jsonObject.toString());
        } catch (JSONException | SnappydbException e) {
            e.printStackTrace();
        }
        dismissDialog();
    }

    private Response.Listener<String> makeResponse() {
        return response -> {
            JSONArray walletList;
            try {
                JSONObject jsonObject = new JSONObject(response);
                walletList = jsonObject
                        .getJSONObject("abstractEntity")
                        .getJSONArray("walletList");

                // 0 - balance, 1 - index
                double wallet[] = getWallet(walletList);

                currentBalance = setDecimal(wallet[0]);

                setValues();
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
        };
    }

    private void setValues() {
        // TODO fix text formatting
        tvCurrentBalance.setText("$ " + currentBalance);
        tvPreviousBalance.setText(" $ " + getPreviousBalance());
        tvRefundValue.setText(" $ " + setDecimal(refundValue));
        tvRefundSuccessText.setText(R.string.refund_success);
        dismissDialog();
    }

    /**
     * iterates through json array to find the wallet whose 'identifier' matches the one on the var
     * walletFrom. When the the wallet is found, it's balance and index on the array are returned.
     *
     * @param walletList array with users wallet
     * @return array[0] = wallet balance, array[1] = wallet index
     * @throws JSONException while getting the wallet identifier
     */
    private double[] getWallet(JSONArray walletList) throws JSONException {
        double array[] = new double[2];
        for (int i = 0; i < walletList.length(); i++) {
            // check if that position in the array has the id we're looking for
            if (walletList.getJSONObject(i).getString("identifier").equalsIgnoreCase(walletFrom)) {
                array[0] = walletList.getJSONObject(i).getDouble("valueBalance");
                array[1] = i;
            }
        }
        return array;
    }

    /**
     * Previous balance uses the wallets information stored on SnappyDB. After finding the wallet
     * used in the refund by looking for it's identifier, the balance is returned.
     *
     * @return balance of the previously saved wallet
     */
    private String getPreviousBalance() {
        try {
            // this tuple has the name CARDS but it has all the authenticated REST response
            JSONObject jsonObject = new JSONObject(SnappyDBHelper.get(getApplicationContext(), "CARDS"));
            JSONArray walletList = jsonObject.getJSONObject("abstractEntity").getJSONArray("walletList");

            // 0 - balance , 1 - index
            double wallet[] = getWallet(walletList);

            return setDecimal(wallet[0]);
        } catch (JSONException | SnappydbException e) {
            e.printStackTrace();
        }

        return "";
    }
}
