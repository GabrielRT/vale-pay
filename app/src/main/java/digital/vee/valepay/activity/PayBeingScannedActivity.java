package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.PayTextWatcher;
import digital.vee.valepay.util.Preferences;

/**
 * Insert the value of the transaction, format the user's input
 * and check if sufficient wallet balance
 */
public class PayBeingScannedActivity extends BaseActivity {

    private static final String TAG = "PayBeingScannedActivity";
    @Bind(R.id.hello_pay_header)

    TextView _helloText;
    @Bind(R.id.name_user_text_header)

    TextView _nameText;
    @Bind(R.id.balance_header)
    TextView _balanceHeader;

    @Bind(R.id.v_symbol_header)
    TextView _vSymbol;

    @Bind(R.id.last_update_text)
    TextView _lastUpdatedTextText;

    @Bind(R.id.last_update)
    TextView _lastUpdatedText;

    @Bind(R.id.btn_pay)
    Button _payButton;

    @Bind(R.id.valuePayEdit)
    EditText _valueEdit;

    private long lastChangeTextTime = 0;
    private long lastClickTimePay = 0;
    
    /**
     * Creates a TextWatcher to validate if password input is weak or strong.
     */
    private final TextWatcher inputTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }


        // checks if value on edittext value was typed and if it is valid, so enable the pay button
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastChangeTextTime < 100) {
                return;
            }

            String valueEdit = _valueEdit.getText().toString().replace(".","");
            valueEdit = valueEdit.replace(",",".");

            String amountText = valueEdit;
            amountText = amountText.replace(".","");
            amountText = amountText.replace(",",".");

            Float amountFloat = Float.parseFloat(amountText);

            if (Double.valueOf(valueEdit).compareTo(Double.valueOf("0")) != 0) {
                if (amountFloat <= 0.00) {
                    _payButton.setEnabled(false);
                } else {
                    _payButton.setEnabled(true);
                }
            }

            lastChangeTextTime = SystemClock.elapsedRealtime();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        ButterKnife.bind(this);

        setCustomActionBar();

        Preferences.init(getApplicationContext());

        doLogin();

        updateUserData();

        _valueEdit.addTextChangedListener(inputTextWatcher);

        _valueEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //Log.d(TAG, keyCode + " character(code) to send");
                return false;
            }
        });

        _payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - lastClickTimePay < 3000) {
                    return;
                }

                lastClickTimePay = SystemClock.elapsedRealtime();

                //Log.v(TAG, "+++ payButton");

                Intent intent = new Intent(PayBeingScannedActivity.this, PayPINActivity.class);

                intent.putExtra("amount", _valueEdit.getText().toString());

                ActivityUtils.newActivity(PayBeingScannedActivity.this, intent);
                finish();
            }
        });

        Typeface type_roboto_light = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_bold = Typeface.createFromAsset(getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        PayTextWatcher ptw = new PayTextWatcher(_valueEdit, "%,.2f");
        _valueEdit.addTextChangedListener(ptw);
        _valueEdit.setText("00,00");
        _valueEdit.setTextColor(getResources().getColor(R.color.aluminum));
        _valueEdit.setSelection(_valueEdit.getText().length());
        _valueEdit.requestFocus();

        _valueEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                _valueEdit.onTouchEvent(event);
                _valueEdit.setSelection(_valueEdit.getText().length());
                return true;
            }
        });

        final String amountText = _valueEdit.getText().toString();

        Float amountFloat = Float.parseFloat(amountText.replace(",", "."));

        if (amountFloat <= 0.00) {
            _payButton.setEnabled(false);
        } else {
            _payButton.setEnabled(true);
        }

        _valueEdit.setTypeface(type_roboto_bold);

        TextView tv_v_symbol = (TextView) findViewById(R.id.v_symbol_header);
        tv_v_symbol.setTypeface(type_roboto_bold);

        _helloText.setTypeface(type_roboto_bold);
        _nameText.setTypeface(type_roboto_light);
        _balanceHeader.setTypeface(type_roboto_light);
        TextView tv_balance_pay = (TextView) findViewById(R.id.balance_pay_header);
        tv_balance_pay.setTypeface(type_roboto_light);
        _lastUpdatedText.setTypeface(type_roboto_light);
        TextView tv_totalAmount = (TextView) findViewById(R.id.totalAmount);
        tv_totalAmount.setTypeface(type_roboto_light);
        _payButton.setTypeface(type_roboto_bold);

        _valueEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    String replaced = _valueEdit.getText().toString()
                            .replace(".", "");
                    replaced = replaced.replace(",",".");
                    if (Double.valueOf(replaced).compareTo(Double.valueOf("0"))
                            != 0) {
                        _payButton.performClick();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Check how much balance the user has and set when the last check was done
     */
    private void updateUserData() {
        String firstName = null;
        try {
            firstName = SnappyDBHelper.get(getApplicationContext(), "name");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        String balance = "";
        try {
            balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_WALLET);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        String checkLast = null;
        try {
            checkLast = SnappyDBHelper.get(getApplicationContext(), "lastUpdated");
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        String lastUpdated = checkLast != null &&
                !checkLast.isEmpty() ? checkLast : "";
        //Log.v(TAG, " UpdateUserData - name: " + firstName + " balance: " + balance);
        String displayName = this.trimFirstName(firstName);

        _nameText.setText(" " + displayName + " :)");
        _lastUpdatedText.setText(lastUpdated);

        if (balance != null && !balance.isEmpty()) {

            Double balanceD = Double.valueOf(balance);

            if (balanceD != null && balanceD.compareTo(0D) < 0) {
                _balanceHeader.setTextColor(Color.RED);
                _vSymbol.setTextColor(Color.RED);
            } else {
                int color = ContextCompat.getColor(getApplicationContext(), R.color.green);
                _balanceHeader.setTextColor(color);
                _vSymbol.setTextColor(color);
            }
        } else {
            //Log.v(TAG, "####### Balance vazio #######");
            //TODO: throw an exception
        }

        Double balanceD = Double.parseDouble(balance);

        DecimalFormat dFormat = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        dFormat.setMaximumFractionDigits(2);
        dFormat.setMinimumFractionDigits(2);

        balance = dFormat.format(balanceD);

        _balanceHeader.setText(balance);
    }

    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Format the first name of the user to be displayed on screen
     * @param firstName of the user
     * @return the name with maximum of 25 char length
     */
    private String trimFirstName(String firstName) {
        String displayName;
        //Log.v(TAG, "+trimFirstName() firstName: " + firstName);

        if (firstName != null && !firstName.isEmpty() && firstName.contains(" ")) {
            displayName = firstName.substring(0, firstName.indexOf(" "));
        } else {
            displayName = firstName;
        }
        //Log.v(TAG, "+trimFirstName() displayName: " + displayName);

        if (displayName != null && displayName.length() > 25) {
            displayName = displayName.substring(0, 25);
        }
        //Log.v(TAG, "+trimFirstName() displayName: " + displayName);
        return displayName;
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }
}