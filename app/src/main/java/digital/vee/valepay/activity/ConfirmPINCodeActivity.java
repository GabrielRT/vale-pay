package digital.vee.valepay.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Class that controls the confirmation (without a service) of PIN Code.
 *
 * @author Sérgio Brocchetto
 */
@SuppressLint("Registered")
@EActivity(R.layout.activity_confirm_pin_code)
public class ConfirmPINCodeActivity extends BaseActivity {

    private static final String TAG = "ChangePINCodeActivity";
    private static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    private static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    private static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";
    private static final String URL = BuildConfig.SERVER_URL + "rest/private/pin/resend";

    @ViewById
    Toolbar toolbar;
    @ViewById
    TextView insert_pin_code;
    @ViewById
    EditText input_confirmation_code;
    @ViewById
    TextView did_not_receive_sms_call;

    @ViewById(R.id.btn_submit)
    Button submitBtn;

    private ProgressDialog progressDialog;
    Handler handler;
    Runnable runnable;
    private boolean requestCallClicked = false;
    private Toast toast1;

    @AfterViews
    void afterViews() {
        Preferences.init(getApplicationContext());

        doLogin();

        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }

        setFonts();

        //show error if returning from changepin
        String message = getIntent().getStringExtra("toast");

        //if returning from changepin after error, shows imediately the option to request the
        // confirmation code by a call
        if (getIntent().getBooleanExtra("showCallOption", false)) {
            did_not_receive_sms_call.setVisibility(View.VISIBLE);
        } else {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() { //set option visible only after 30 seconds
                    did_not_receive_sms_call.setVisibility(View.VISIBLE);
                }
            };
            handler.postDelayed(runnable, 30000);
        }

        if (message != null && !message.isEmpty()) {
            Toast toast1 = Toast.makeText(this, message, Toast.LENGTH_LONG);
            toast1.show();
        }
    }

    /**
     * Set Typeface of EditTexts and TextViews
     */
    private void setFonts() {
        AssetManager assets = this.getAssets();
        Typeface typeRobotoBold = Typeface.createFromAsset(assets, FONTS_ROBOTO_BOLD_TTF);
        Typeface typeRobotoRegular = Typeface.createFromAsset(assets, FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(assets, FONTS_ROBOTO_LIGHT_TTF);

        insert_pin_code.setTypeface(typeRobotoBold);
        input_confirmation_code.setTypeface(typeRobotoRegular);

        did_not_receive_sms_call.setText(Html.fromHtml("<font color=#999999>" + "Não recebeu o SMS? " + "</font>" + "<font color=#522B69><u>" + "Toque aqui" + "</u></font>  " +
                        "<font color=#999999>" + " para receber uma ligação com o código." + "</font>"));
    }

    /**
     * Request to the server a call to be made to the user with the information needed to confirm PIN
     */
    @Click(R.id.did_not_receive_sms_call)
    void requestCallClicked() {
        if (!requestCallClicked) {
            //Log.d(TAG, "RequestCall Click");
            createProgressDialog(this);
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                String response;

                @Override
                public void onResponse(String response) {

                    this.response = response;
                    JSONObject jsonResponse = null;
                    try {
                        jsonResponse = new JSONObject(response);
                    } catch (JSONException e) {
                        //Log.v(UseActivity.TAG, "Exception: " + e.getMessage());
                        e.printStackTrace();
                    }

                    try {
                        requestCallClicked = jsonResponse != null && jsonResponse.getBoolean("resent");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (requestCallClicked) {
                        toast1 = Toast.makeText(ConfirmPINCodeActivity.this, "Você receberá uma ligação em breve.", Toast.LENGTH_SHORT);
                        toast1.show();
                    }
                    dismissDialog();
                    //Log.v(UseActivity.TAG, "Response: " + jsonResponse.toString());
                }
            };
            Response.ErrorListener errorListener = error -> {
                if (error != null) {
                    if (error.networkResponse != null && error.networkResponse.statusCode == 401) {
                        dismissDialog();
                        doLogin();
                    } else if (error.getClass().equals(TimeoutError.class)) {
                        showTimeoutDialog(ConfirmPINCodeActivity.this);
                        dismissDialog();
                    } else {
                        Toast.makeText(getBaseContext(), "Erro no servidor.", Toast.LENGTH_LONG).show();
                        dismissDialog();
                    }
                    error.printStackTrace();
                }
            };
            //Send the call request to the server
            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.GET, URL,
                    responseListener, errorListener, null) {
                Request.Priority mPriority;

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept-Language", "pt_br");
                    try {
                        params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
                }
            };
            postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
            VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
        } else {
            Toast toast1 = Toast.makeText(this, "Aguarde a ligação", Toast.LENGTH_LONG);
            toast1.show();
        }
    }

    /**
     * Action of button to confirm change of PIN Code
     */
    @Click(R.id.btn_submit)
    void confirmBtnClicked() {
        //Log.d(TAG, "Submit Click");

        if (validate()) {
            Intent intent = new Intent(this, ChangePINCodeActivity_.class);
            intent.putExtra(ChangePINCodeActivity_.CONFIRMATION_PIN_CODE, input_confirmation_code.getText().toString());
            finish();
            ActivityUtils.newActivity(this, intent);
        } else {
            dismissDialog();
        }
    }

    /**
     * Validate if the fields of PIN Code are filled correctly
     * @return <code>true</code> if all input fields are correct
     */
    private boolean validate() {
        String currentPinCode = input_confirmation_code.getText().toString();
        boolean valid = true;

        // validate current pin code input
        if (currentPinCode.isEmpty() || currentPinCode.length() < 4) {
            input_confirmation_code.requestFocus();
            input_confirmation_code.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else {
            input_confirmation_code.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
