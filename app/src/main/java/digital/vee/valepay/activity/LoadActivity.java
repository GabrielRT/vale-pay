package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.snappydb.SnappydbException;

import org.apache.commons.codec.android.CharEncoding;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TSCHelper;

/**
 * Class that process the result of QRCoded read. Send the transaction read to server and check if
 * the server processed it.
 *
 * @author Sérgio Brocchetto
 */
public class LoadActivity extends BaseActivity {

    private static final String TAG = "LoadActivity";
    /**
     * URL to send transaction to server
     */
    private static final String TRANSACTION_URL = BuildConfig.SERVER_URL + "rest/private/transaction/send";

    private volatile boolean confirmed = false;
    private int runTimer = 0;
    private volatile Timer t = null;
    private String transactionUUID;
    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
        Preferences.init(getApplicationContext());

        doLogin();

        //Log.v(TAG, "onCreate");
        //Declare the timer
        t = new Timer();
        //Set the schedule function and rate
        final String transaction = getIntent().getStringExtra("transaction");
        String walletMerchantTo = getIntent().getStringExtra("walletTo");
        String pos = getIntent().getStringExtra("pos");

        pin = getIntent().getStringExtra("pin");
        //Log.v(TAG, "pin: " + pin);
        //Log.v(TAG, "*** transaction " + transaction);

        sendTransaction(transaction, walletMerchantTo, pos, pin);
    }

    /**
     * Process a return of a valid transaction after send it to server.
     *
     * @param jsonResponse JSON response from server
     */
    private void onTransactionSuccess(JSONObject jsonResponse) {
        //Log.v(TAG, "+onTransactionSuccess()");

        //save pin
        try {
            //Log.v(TAG, "pin: " + pin);
            SnappyDBHelper.putEncrypted(LoadActivity.this, "pin", pin);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        if (t != null) {
            t.cancel();
            t.purge();
        }

        Intent intent = new Intent(LoadActivity.this, PayOKActivity.class);
        try {
            intent.putExtra(PayOKActivity.PAY_DATE_KEY, jsonResponse.getString("dtConfirmed"));
            intent.putExtra(PayOKActivity.PAY_VALUE_KEY, jsonResponse.getString("amount"));
            intent.putExtra(PayOKActivity.PAY_PAYER_KEY, jsonResponse.getString("entityNameFrom"));
            intent.putExtra(PayOKActivity.PAY_RECEIVER_KEY, jsonResponse.getString("entityNameTo"));

            ActivityUtils.newActivity(this, intent);
            finish();
            //Log.v(TAG, "-onTransactionSuccess()");
        } catch (JSONException e) {
            if (t != null ) {
                t.cancel();
                t.purge();
            }
            e.printStackTrace();
            onTransactionFailed(null);
        }
    }

    /**
     * Process a failure on sending transaction to server.
     *
     * @param error Error received from server
     */
    private void onTransactionFailed(String error) {
        Intent intent = new Intent(LoadActivity.this, UseActivity.class);

        if (t != null) {
            t.cancel();
            t.purge();
        }
        if (error != null) {
            intent.putExtra("toast", "Ocorreu um erro com a transação" + ": \n" + error);
            //Toast.makeText(getBaseContext(), getString(R.string.error_transaction) + ": \n" + error, Toast.LENGTH_SHORT).show();
        } else {
            intent.putExtra("toast", "Ocorreu um erro com a transação");
            //Toast.makeText(getBaseContext(), getString(R.string.error_transaction), Toast.LENGTH_SHORT).show();
        }

        ActivityUtils.exitActivity(this, intent);
        finish();
    }

    /**
     * Send the transaction read in QRCode to server.
     *
     * @param transaction Transaction string read in QRCode
     */
    private void sendTransaction(String transaction, String walletMerchantTo, String pos, String pin) {
        //Log.d(TAG, "sendTransaction");

        JSONObject jsonBody = new JSONObject();
        try {
            //Log.v(TAG, "*** *** transaction: \n" + transaction);
            jsonBody.put("tscString", transaction);
            jsonBody.put("walletTo", walletMerchantTo);
            jsonBody.put("pos", pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.d(TAG, "--- sendTransaction: " + jsonBody.toString());

        final String mRequestBody = jsonBody.toString();

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //android.util.Log.v(TAG, "1 Response: FIND " + response);

                    JSONObject jsonResponse = new JSONObject(response);
                    transactionUUID = jsonResponse.getString("transactionUUID");
                    String rejectReason = jsonResponse.getString("rejectReason");
                    String dtCanceled = jsonResponse.getString("dtCanceled");
                    if ((rejectReason != null && !rejectReason.equals("null")
                            || (dtCanceled != null) && !dtCanceled.equals("null"))) {
                        if (t != null ) {
                            t.cancel();
                            t.purge();
                        }
                        onTransactionFailed(rejectReason);
                    }

                    //Log.v(TAG, "---transactionUUID: " + transactionUUID);// + "\nbalance: " + balance +

                } catch (Exception e) {
                    if (t != null ) {
                        t.cancel();
                        t.purge();
                    }                    //android.util.Log.v(TAG, "---20 Exception: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (t != null ) {
                    t.cancel();
                    t.purge();
                }
                if (error.networkResponse != null && error.networkResponse.statusCode == 401) {
                    doLogin();
                }
                //Log.v(TAG, "---22 Exception: " + error + " " + error.getCause() + " " + error.getMessage());
                onTransactionFailed(null);
                error.printStackTrace();
            }
        };

        //Log.d(TAG, "--- body: " + mRequestBody);

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, TRANSACTION_URL, listener,
                errorListener, mRequestBody) {
            Request.Priority mPriority;

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                params.put("Accept-Language", "pt_br");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //android.util.Log.v(TAG, " ---+parseNetworkError()");
                String json = null;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    //if (t != null ) t.cancel();
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers,
                                        CharEncoding.UTF_8));

                        //android.util.Log.v(TAG, "--- +json(): " + json);
                        return volleyError;
                    } catch (UnsupportedEncodingException e) {
                        //android.util.Log.v(TAG, "--- +UnsupportedEncodingException(): " + json);
                        return new VolleyError(e.getMessage());
                    }
                }
                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                    //android.util.Log.v(TAG, "---UnsupportedEncodingException: " + uee + " " + uee.getCause() + " " + uee.getMessage());
                    //Volleyandroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s",mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Request.Priority getPriority() {
                // If you didn't use the setPriority method,sharedpreferences
                return mPriority != null ? mPriority : Request.Priority.IMMEDIATE;
            }

        };

        System.setProperty("http.keepAlive", "false");
        postRequest.setRetryPolicy(new DefaultRetryPolicy(15 * 1000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);

        if (!confirmed) {
            t.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          runTimer++;
                                          if (runTimer > 20 && !confirmed) {
                                              if (t != null ) {
                                                  t.cancel();
                                                  t.purge();
                                              }
                                              onTransactionFailed(null);
                                          } else {
                                              if (transactionUUID != null) {
                                                  TSCHelper.checkTransaction(LoadActivity.this, transactionUUID,
                                                          new TSCHelper.Success() {
                                                              @Override
                                                              public void onSuccess(JSONObject jsonResponse) {
                                                                  if (t != null ) {
                                                                      t.cancel();
                                                                      t.purge();
                                                                  }
                                                                  if (!confirmed) {
                                                                      onTransactionSuccess(jsonResponse);
                                                                  }
                                                                  confirmed = true;
                                                              }
                                                          },
                                                          new TSCHelper.Failure() {
                                                              @Override
                                                              public void onFailure(String error) {
                                                                  if (t != null ) {
                                                                      t.cancel();
                                                                      t.purge();
                                                                  }
                                                                  if (!confirmed) {
                                                                      onTransactionFailed(error);
                                                                  }
                                                                  confirmed = true;
                                                              }
                                                          });
                                              }
                                          }
                                      }
                                  },
                    800, //Set how long before to start calling the TimerTask (in milliseconds)
                    800); //Set the amount of time between each execution (in milliseconds)
        }


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * Finishes the activity and kills the timer
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (t != null) {
            t.cancel();
            t.purge();
        }
    }
}
