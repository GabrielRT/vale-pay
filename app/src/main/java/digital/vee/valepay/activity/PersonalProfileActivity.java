package digital.vee.valepay.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.StringRequestCustom;

/**
 * Created by Silvio Machado on 31/01/2018.
 */

public class PersonalProfileActivity extends BaseActivity {

    private static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    private static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    private static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";
    private static final String USER_DATA_URL = BuildConfig.SERVER_URL + "rest/private/authenticated";
    private static final String USER_UPDATE_URL = BuildConfig.SERVER_URL + "rest/private/user/basicProfile";
    @Bind(R.id._name)
    TextView _name;
    @Bind(R.id._cpf)
    TextView _cpf;
    @Bind(R.id._adress)
    TextView _adress;
    @Bind(R.id._city)
    TextView _city;
    @Bind(R.id._state)
    TextView _state;
    @Bind(R.id._birth)
    TextView _birth;
    @Bind(R.id._cel)
    TextView _cel;
    @Bind(R.id._email)
    TextView _email;
    @Bind(R.id._zipcode)
    TextView _zipcode;
    @Bind(R.id._complement)
    TextView _complement;
    @Bind(R.id._number)
    TextView _number;

    @Bind(R.id.btn_save)
    Button btn_save;
    @Bind(R.id.btn_upload)
    Button btn_upload;

    @Bind(R.id.tv_name)
    TextView tv_name;
    @Bind(R.id.tv_cpf)
    TextView tv_cpf;
    @Bind(R.id.tv_adress)
    TextView tv_adress;
    @Bind(R.id.tv_city)
    TextView tv_city;
    @Bind(R.id.tv_state)
    TextView tv_state;
    @Bind(R.id.tv_birth)
    TextView tv_birth;
    @Bind(R.id.tv_cel)
    TextView tv_cel;
    @Bind(R.id.tv_email)
    TextView tv_email;
    @Bind(R.id.tv_zipcode)
    TextView tv_zipcode;
    @Bind(R.id.tv_complement)
    TextView tv_complement;
    @Bind(R.id.tv_number)
    TextView tv_number;

    private String name;
    private String phone;
    private String email;
    private String city;
    private String state;
    private String cpf;
    private String birth;
    private String address;
    private String zipcode;
    private String complement;
    private String number;
    private JSONObject jsonObject;
    private StringRequest stringRequest;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_personal);

        setToolbar();

        createProgressDialog(PersonalProfileActivity.this, true, dialog -> ActivityUtils.exitActivity(this));

        ButterKnife.bind(this);

        doLogin();

        setClickListener();

        setFonts();

        getUserData();
    }

    private void setFonts() {
        AssetManager assets = this.getAssets();
        Typeface typeRobotoRegular = Typeface.createFromAsset(assets, FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(assets, FONTS_ROBOTO_LIGHT_TTF);
        Typeface typeRobotoBold = Typeface.createFromAsset(assets, FONTS_ROBOTO_BOLD_TTF);

        _name.setTypeface(typeRobotoRegular);
        _cpf.setTypeface(typeRobotoRegular);
        _adress.setTypeface(typeRobotoRegular);
        _city.setTypeface(typeRobotoRegular);
        _state.setTypeface(typeRobotoRegular);
        _birth.setTypeface(typeRobotoRegular);
        _cel.setTypeface(typeRobotoRegular);
        _email.setTypeface(typeRobotoRegular);
        _zipcode.setTypeface(typeRobotoRegular);
        _complement.setTypeface(typeRobotoRegular);
        _number.setTypeface(typeRobotoRegular);

        tv_name.setTypeface(typeRobotoRegular);
        tv_cpf.setTypeface(typeRobotoRegular);
        tv_adress.setTypeface(typeRobotoRegular);
        tv_city.setTypeface(typeRobotoRegular);
        tv_state.setTypeface(typeRobotoRegular);
        tv_birth.setTypeface(typeRobotoRegular);
        tv_cel.setTypeface(typeRobotoRegular);
        tv_email.setTypeface(typeRobotoRegular);
        tv_zipcode.setTypeface(typeRobotoRegular);
        tv_complement.setTypeface(typeRobotoRegular);
        tv_number.setTypeface(typeRobotoRegular);

        _name.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _cpf.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _adress.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _city.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _state.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _birth.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _cel.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _email.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _zipcode.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _complement.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _number.setTextColor(getResources().getColor(R.color.aluminum_darker));

        tv_name.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_cpf.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_adress.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_city.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_state.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_birth.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_cel.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_email.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_zipcode.setTextColor(getResources().getColor(R.color.aluminum_darker));
        tv_complement.setTextColor(getResources().getColor(R.color.aluminum_darker));
        _number.setTextColor(getResources().getColor(R.color.aluminum_darker));
    }

    private void setClickListener() {
        btn_upload.setVisibility(View.GONE);

        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(view -> {
            sendData();
            showMessage("Dados Alterados com sucesso!");
        });
    }

    private void sendData() {
        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, USER_UPDATE_URL,
                //FIXME: colocar endpoint real para post
                getUpdateResponse(), getError(),mRequestBody()) {
                Request.Priority mPriority;

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept-Language", "pt_br");
                    params.put("Authorization", "Token " + getToken());
                    params.put("Content-Type", "application/json");
                    params.put("apiVersion", "2");
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
                }
            };

        VolleySingleton.getInstance(PersonalProfileActivity.this).addToRequestQueue(postRequest);
    }

    private void setInfo(){
        _name.setText(name);
        _cpf.setText(cpfFormat(cpf));
        _adress.setText(address);
        _city.setText(city);
        _state.setText(state);
        _birth.setText(birth);
        _cel.setText(phone);
        _email.setText(email);
        _zipcode.setText(zipcode);
        _complement.setText(complement);
        _number.setText(number);
        dismissDialog();
    }

    private String cpfFormat(String cpf) {
        String newString ="";
        for(char c : cpf.toCharArray()){
            if(newString.length() == 3 || newString.length() == 7){
                newString += "." + c;
            } else if(newString.length() == 11){
                newString += "-" + c;
            } else {
                newString += c;
            }
        }
        return newString;
    }

    @Override
    public void onBackPressed() {
        ActivityUtils.exitActivity(this);
    }

    private void cancelRequest() {
        if (stringRequest != null) {
            try {
                stringRequest.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getUserData() {
        stringRequest = new StringRequest(Request.Method.GET, USER_DATA_URL, getRetrieveResponse(),
                getError()) {
            Request.Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                params.put("Authorization", "Token " + getToken());
                params.put("Content-Type", "application/json");
                params.put("apiVersion", "2");
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return this.mPriority != null ? this.mPriority : Request.Priority.NORMAL;
            }
        };
        stringRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(PersonalProfileActivity.this).addToRequestQueue(stringRequest);
    }

    private Response.ErrorListener getError() {
            return error -> {
                dismissDialog();
                if (error.getClass().equals(TimeoutError.class)) {
                    showTimeoutDialog(this, new Intent(PersonalProfileActivity.this, ProfileActivity.class));
                } else {
                    showMessage("Erro ao processar seu pedido");
                }
                error.printStackTrace();
            };
    }

    private Response.Listener<String> getRetrieveResponse(){
            return response -> {
                try {
                    jsonObject = new JSONObject(response);

                    // get name from abstract entity so it's the full name
                    try {
                        name = new String(jsonObject.getJSONObject("abstractEntity")
                                .getString("name").getBytes("ISO-8859-1"), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    cpf = jsonObject.getString("loginName");
                    setAddress(jsonObject);
                    birth = checkJson(jsonObject,"birthDate");
                    phone = checkJson(jsonObject,"phoneNumber");
                    email = checkJson(jsonObject, "email");
                    setInfo();
                } catch (JSONException e){
                    e.printStackTrace();
                    showMessage("Erro ao processar seu pedido");
                    dismissDialog();
                }
            };
    }

    private Response.Listener<String> getUpdateResponse(){
        return response -> {
            showMessage("Sucesso ao salvar dados");
        };
    }

    private String checkJson(JSONObject jsonObject, String name){
        if (jsonObject.has(name)){
            try {
                return jsonObject.getString(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private String mRequestBody(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", _name.getText().toString());
            jsonObject.putOpt("address", getAdress());
            jsonObject.put("birthDate", _birth.getText().toString());
            jsonObject.put("phoneNumber", _cel.getText().toString());
            jsonObject.put("email", _email.getText().toString());
        } catch (JSONException e){
            e.printStackTrace();
            showMessage("Erro ao processar seu pedido");
        }
        return jsonObject.toString();
    }

    private JSONObject getAdress() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("address", _adress.getText().toString());
            jsonObject.put("number", _number.getText().toString());
            jsonObject.put("complement", _complement.getText().toString());
            jsonObject.put("zipcode", _zipcode.getText().toString());
            jsonObject.put("city", _city.getText().toString());
            jsonObject.put("state", _state.getText().toString());
            jsonObject.put("country", "Brazil");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void setAddress(JSONObject response) {
        try {
            JSONObject addressArray;
            if (response.has("address")) {
                addressArray = response.getJSONObject("address");
                address = checkJson(addressArray, "street");
                number = checkJson(addressArray, "number");
                city = checkJson(addressArray, "city");
                state = checkJson(addressArray, "state");
                zipcode = checkJson(addressArray, "zipcode");
                complement = checkJson(addressArray, "complement");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
}


