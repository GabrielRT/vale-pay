package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.crash.FirebaseCrash;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.notification.NotificationServiceExtension;

import digital.vee.valepay.R;
import digital.vee.valepay.helper.NotificationService;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Created by Sérgio Brocchetto on 21/06/2017.
 *
 * First Java Class to be called.
 * Show the user the splash screen and initiates the main activity.
 */

public class SplashActivity extends AppCompatActivity {

    private static final long TIME_TO_COUNT = 700;
    public static SplashActivity splashActivity;
    private CryptoUtil cryptoUtil;
    private String TAG = "SplashScreen";
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        Pushwoosh.getInstance().registerForPushNotifications(result -> {
            if (result.isSuccess()) {
                Log.d(TAG, "Successfully registered for push notifications with token: " + result.getData());
            } else {
                Log.d(TAG, "Failed to register for push notifications:u " + result.getException().getMessage());
            }
        });

        Preferences.init(getApplicationContext());

        SnappyDBHelper.getDatabase(getApplicationContext());

        FirebaseCrash.log(TAG + " Activity created");

        intent = new Intent(SplashActivity.this, MainActivity.class);

        Preferences.init(getApplicationContext());
        boolean isPush = Preferences.getBoolean("isPush", false);

        splashActivity = this;

        //if the call is from a push handled by NotificationService, so just i should open MainActivity
        Handler handle = new Handler();
        handle.postDelayed(() -> {
            if (!isPush) {
                startActivity(intent);
            } else {
                Preferences.putBoolean("isPush", false);
            }
        }, TIME_TO_COUNT);
    }

}