package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.BitmapHelper;
import digital.vee.valepay.model.Wallet;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Preferences;

/**
 * Class of activity that shows a single card.
 */
public class WalletActivity extends AppCompatActivity {

    private static final String TAG = "WalletActivity";

    @Bind(R.id.cardIV)
    ImageView cardIV;

    private boolean showingFront;
    private BitmapDrawable cardFront;
    private BitmapDrawable cardBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        Preferences.init(this);

        ButterKnife.bind(this);
        setCustomActionBar();

        if (getIntent().hasExtra("wallet") && getIntent().hasExtra("nameOnCard")) {
            Wallet wallet = (Wallet) getIntent().getSerializableExtra("wallet");
            String name = getIntent().getStringExtra("nameOnCard");

            BitmapHelper bitmapHelper = BitmapHelper.getInstance(this);

            cardFront = bitmapHelper.getCardDrawableFront(wallet, name);
            cardBack = bitmapHelper.getCardDrawableBack(wallet);
            cardIV.setBackground(cardFront);
            showingFront = true;
        }

    }

    /**
     * Flip Card to show back/front face.
     * @param v
     */
    public void flipCard(View v) {
        if (showingFront) {
            cardIV.setBackground(cardBack);
            showingFront = false;
        }
        else {
            cardIV.setBackground(cardFront);
            showingFront = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setCustomActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            IconicsDrawable backArrow = new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.GRAY)
                    .sizeDp(24);
            supportActionBar.setHomeAsUpIndicator(backArrow);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityUtils.exitActivity(this);
    }
}
