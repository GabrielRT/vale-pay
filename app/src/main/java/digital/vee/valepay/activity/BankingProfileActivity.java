package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.fasterxml.jackson.databind.ser.Serializers;

import digital.vee.valepay.R;

/**
 * Created by Silvio Machado on 31/01/2018.
 */


public class BankingProfileActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_banking);

        setToolbar();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(BankingProfileActivity.this, ProfileActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                startActivity(new Intent(BankingProfileActivity.this, ProfileActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
