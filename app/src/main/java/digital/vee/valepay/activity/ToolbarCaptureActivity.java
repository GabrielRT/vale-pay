package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import digital.vee.valepay.R;

/**
 * Sets the screen to Scan the QRCode or generate token key. Show the camera image and apply
 * the image mask
 */
public class ToolbarCaptureActivity extends BaseActivity {
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView ;
    private String TAG = "ToolbarCap";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.capture_appcompat);

        setToolbar();
        createCapture(savedInstanceState);
    }

    private void createCapture(Bundle savedInstanceState) {
        barcodeScannerView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    public void redirGenerateToken(View view) {
        finish();
        startActivity(new Intent(this, GenerateTokenActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
