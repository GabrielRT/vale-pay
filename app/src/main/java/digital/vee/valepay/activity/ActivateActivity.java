package digital.vee.valepay.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pushwoosh.Pushwoosh;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.R.layout;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.Login;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 *  Initiate de process of activation of a device, sending the device's info to
 *  the server.
 */
public class ActivateActivity extends BaseActivity {
    private static final String URL = BuildConfig.SERVER_URL + "rest/activate";
    private static final String TAG = "ActivateActivity";
    public static final String PERSONAL_ID = "personalId";
    public static final String PUBLIC_KEY = "publicKey";
    public static final String DEVICE_ALIAS = "deviceAlias";
    private static final String DEVICE_ID = "deviceId";
    public static final String ACTIVATION_CODE = "activationCode";
    private static final String PASSWORD = "password";
    @Bind(R.id.input_confirmation_code)
    EditText _activationCodeText;

    @Bind(R.id.btn_submit)
    Button _signupButton;

    private ProgressDialog progressDialog;
    private String activationCode;

    final String[][] keyPair = {null};
    private Context context;
    //generate random 128 char
    final String generatedPassword=Login.randomString(128); // ver se isso nao vai dar pau
    private String deviceAlias;
    String devicePushId;
    private CryptoUtil cryptoUtil;
    private String personalId;
    private Boolean calledFromAddCard;
    //KeyStore keyStore = null;
    long mLastClickTimeLogin = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();
        calledFromAddCard = getIntent().getBooleanExtra("addNewCard", false);

        //initiate class to data storage
        Preferences.init(this);

        //use KeyStore to generate the device private key that is used to criptograph SecurePreferences database
        iniKeyStore();

        setContentView(layout.activity_activate);

        ButterKnife.bind(this);

        // check if this is the first time this activity is called or if is to activate ANOTHER card
        checkIfNewCard(getToken());

        setActivateToolbar();

        setClickListeners();

        //generate EC key pair and save to preferences
        generateKey();
    }

    private void setActivateToolbar(){
        if(calledFromAddCard) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setDisplayShowTitleEnabled(false);
                supportActionBar.setDisplayHomeAsUpEnabled(true);
                supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_chevron_left)
                        .color(Color.WHITE)
                        .sizeDp(24));
            }
        } else {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            ActionBar supportActionBar = getSupportActionBar();
            if (null != supportActionBar) {
                supportActionBar.setDisplayHomeAsUpEnabled(false);
                supportActionBar.setDisplayShowHomeEnabled(false);
                supportActionBar.setDisplayShowTitleEnabled(false);
            }
        }
    }


    /**
     * Generate string matrix with a key pair generated from the CryptoUtil object
     */
    private void generateKey() {
        try {
            if (!SnappyDBHelper.exists(getApplicationContext(), Preferences.ECDSA_PUBLIC_KEY)) {
                //generate EC key pair and save to preferences
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            keyPair[0] = cryptoUtil.generateKeyPair();
                        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                            e.printStackTrace();
                        }
                    }
                });

                t.start(); // spawn thread
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the threshold and the function to be called when button is clicked
     */
    private void setClickListeners() {
        _signupButton.setOnClickListener(v -> {
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTimeLogin < 3000) {
                return;
            }

            mLastClickTimeLogin = SystemClock.elapsedRealtime();

            signup();
        });
    }

    /**
     * Check if user is coming to this activity through the 'Add New Card' card
     * @param token String get from local database
     */
    private void checkIfNewCard(String token) {
        if (token != null && !token.isEmpty()) {
            // get flag indicating if the activity was called by Add New Card from Home activity
            if (!calledFromAddCard) {
                onLoginSuccess();
            }
        }
    }

    /**
     * Initialize a cryptoutil object that automatically initiate the KeyStore
     */
    private void iniKeyStore() {
        Thread t2 = new Thread(() -> {
            try {
                cryptoUtil = new CryptoUtil(getApplicationContext());
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        });

        t2.start(); // spawn thread

        try {
            t2.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process sign up action. Validate data input and call service on server to validate account.
     */
    private void signup() {
        if (validate()) {
            showDialog();
            // Set devicepush id, generated password and all values used to activate
            getDeviceValues();
            // Make a volley request
            makeRequest();
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Create a jsonbody with activation code, personal id, the crypto key, device alias and ID, and
     * the password previously generated to send to the server, and puts the response on the local database
     */
    private void makeRequest() {
        //Prepares a JSONObjetct with user info to be sent to the server by a request
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put(ActivateActivity.ACTIVATION_CODE, activationCode);
            jsonBody.put(ActivateActivity.PERSONAL_ID, personalId);
            if (keyPair[0] != null) {
                jsonBody.put(ActivateActivity.PUBLIC_KEY, keyPair[0][0]);
            }//sends public key in hexa
            jsonBody.put(ActivateActivity.DEVICE_ALIAS, deviceAlias);
            jsonBody.put(ActivateActivity.DEVICE_ID, SnappyDBHelper.getEncrypted(context, "devicePushId"));
            jsonBody.put(ActivateActivity.PASSWORD, generatedPassword);
        } catch (JSONException | SnappydbException e2) {
            e2.printStackTrace();
        }
        String requestBody = jsonBody.toString();

        final String[] finalKeyPair = keyPair[0];
        Response.Listener<String> listener = response -> {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                String walletId = jsonResponse.getString("walletCode"),
                        entityId = jsonResponse.getString("entityCode"),
                        userId = jsonResponse.getString("document");
                if (!walletId.isEmpty() && !entityId.isEmpty()) {
                    SnappyDBHelper.put(context, LoginUtil.WALLET_ID, walletId);
                    SnappyDBHelper.put(context, "entityId", entityId);
                    SnappyDBHelper.put(context, ActivateActivity.ACTIVATION_CODE, activationCode);
                    SnappyDBHelper.put(context, LoginUtil.USERNAME, userId);
                    if (finalKeyPair != null) {
                        SnappyDBHelper.putEncrypted(context, Preferences.ECDSA_PUBLIC_KEY, finalKeyPair[2]);
                        SnappyDBHelper.putEncrypted(context, Preferences.ECDSA_PRIVATE_KEY, finalKeyPair[3]);
                    }
                    onSignupSuccess();
                } else {
                    onSignupFailed();
                }
            } catch (Exception e) {
                onSignupFailed();
                e.printStackTrace();
            }
        };

        Response.ErrorListener errorListener = error -> {
            if (error.getClass().equals(TimeoutError.class)) {
                progressDialog.dismiss();
                // If timeout, show dialog
                showTimeoutDialog(this);
            } else {
                onSignupFailed();
            }
        };

        StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener, errorListener,
                requestBody) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                //params.putEncrypted("Authorization", "Token " + token);
                params.put("Accept-Language", "pt_br");
                //Log.v(TAG, TAG + " Header token: " + token);
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                //Log.v(TAG, TAG + " +parseNetworkError()");
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    try {
                        onErrorResponse(volleyError);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                //Log.v(TAG, TAG + " -parseNetworkError()");
                return volleyError;
            }
        };
        System.setProperty("http.keepAlive", "false");
        postRequest.setRetryPolicy(new DefaultRetryPolicy(15 * 1000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }

    /**
     * Get activation code from textview, get the activation code, device alias, devicepushid and
     * set the generated password on the local database.
     */
    private void getDeviceValues() {
        //Gets text from the textview
        String activationCodeText = _activationCodeText.getText().toString();

        //get the numbers
        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(activationCodeText);
        StringBuilder sb = new StringBuilder();
        while (m.find()) {
            sb.append(m.group());
        }
        activationCode = sb.toString();

        //get the letters
        personalId = activationCodeText.substring(activationCode.length() , activationCodeText.length());

        deviceAlias = Build.MODEL + "_" + Build.VERSION.CODENAME +
                Build.VERSION.RELEASE + "_" + Build.VERSION.SDK_INT;

        Thread t2 = new Thread(() -> {
            //save generated  password to encrypted database
            try {
                SnappyDBHelper.putEncrypted(context, Preferences.GENERATED_PASSWORD, generatedPassword);
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        });
        t2.start(); // spawn thread

        try {
            t2.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // gets device PushId from PushWoosh services and stores it on the local database
        devicePushId = Pushwoosh.getInstance().getHwid();
        try {
            SnappyDBHelper.putEncrypted(context, "devicePushId", devicePushId);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog for when user is being activated
     */
    private void showDialog() {
        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);

        if (calledFromAddCard) {
            //TODO: mudar msg quando for addnewcard
            progressDialog.setMessage(getString(R.string.toast_loading));
        }
        else {
            progressDialog.setMessage(getString(R.string.toast_creating_account));
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    /**
     * Process error on response from server and log or show it to user.
     *
     * @param error Error received from server.
     * @throws UnsupportedEncodingException
     */
    private void onErrorResponse(VolleyError error) throws UnsupportedEncodingException {
        String json;

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    json = changeCharset(json, "message");

                    if (!(json != null && json.isEmpty()))
                        dismissDialog();
                        showMessage(json);
                    break;
            }
            //Additional cases
        }
    }

    /**
     * Change charset to utf-8 then return the string
     *
     * @param json string with json response
     * @param key Key to find what will be changed in the json
     * @return a new string with the message now encoded with UTF-8
     * @throws UnsupportedEncodingException
     */
    private String changeCharset(String json, String key) throws UnsupportedEncodingException {
        String modifiedString;

        try {
            JSONObject obj = new JSONObject(json);
            modifiedString = obj.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return Arrays.toString(modifiedString.getBytes("UTF-8"));
    }



    /**
     * Process success response from server and starts ChangePINCodeActivity.
     */
    private void onSignupSuccess() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        Thread t = new Thread(() -> {
            // do the login
            LoginUtil loginUtil = new LoginUtil();
            //get username and password stored in local database to do the login
            try {
                loginUtil.login(SnappyDBHelper.get(getApplicationContext(), LoginUtil.USERNAME),
                        SnappyDBHelper.getEncrypted(getApplicationContext(), Preferences.GENERATED_PASSWORD),
                        getApplicationContext(), true);

            } catch (JSONException | SnappydbException e) {
                e.printStackTrace();
            }
        });

        t.start(); // spawn thread

        try {
            t.join();  // wait for thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // if origin is from the Add Card process
        if (calledFromAddCard) {
            setResult(RESULT_OK);
            finish();
            // else, if the pin has changed redir to home
        } else if (Preferences.getBoolean(MainActivity.PIN_CHANGED)) {
            ActivityUtils.newActivity(this, new Intent(this, UseActivity.class));
            finish();
            // else, put activated on the prefences but redir to change the pin
        } else {
            String token = null;
            try {
                token = SnappyDBHelper.getEncrypted(context, LoginUtil.TOKEN_KEY);
            } catch (SnappydbException e) {
                e.printStackTrace();
            } //if the login successful
            if (!(token != null && !token.isEmpty())) {
                Preferences.putBoolean(MainActivity.IS_ACTIVATED, true);
                try {
                    SnappyDBHelper.put(context, "personalId", personalId);
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                ActivityUtils.newActivity(this, new Intent(this, ConfirmPINCodeActivity_.class));
                finish();
            } else {
                onSignupFailed();
            }
        }
    }

    /**
     * Process error on SignUp and show it to user.
     */
    private void onSignupFailed() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Toast.makeText(getBaseContext(), R.string.toast_signup_failed, Toast.LENGTH_LONG).show();
    }

    /**
     * Validate if the the activation code is not empty.
     *
     * @return <code>true</code> case valid input
     */
    private boolean validate() {
        boolean valid = true;

        String activationCode = _activationCodeText.getText().toString();

        if (activationCode.isEmpty()) {
            _activationCodeText.requestFocus();
            _activationCodeText.setError(getString(R.string.error_ac_code_signup));
            valid = false;
        } else {
            _activationCodeText.setError(null);
        }

        return valid;
    }

    /**
     * Let the user out of the activation screen ONLY if he already has a card activated and is
     *  adding a new card.
     *  Otherwise force entering the activation code for the first activation
     */
    @Override
    public void onBackPressed() {
        if (calledFromAddCard) {
            setResult(RESULT_CANCELED);
            ActivityUtils.exitActivity(this);
        }
    }

    /**
     * Check if user already chose a new PIN to call the correct activity
     */
    private void onLoginSuccess() {
        //indicates if
        boolean pinChanged = Preferences.getBoolean(MainActivity.PIN_CHANGED, false);
        if (pinChanged) {
            //TODO: verificar se a wallet pertence a esse usuário
            ActivityUtils.newActivity(this, new Intent(this, UseActivity.class));
            finish();

        } else {
            ActivityUtils.newActivity(this, new Intent(this, ConfirmPINCodeActivity_.class));
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityUtils.exitActivity(this, new Intent(this, UseActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

 }