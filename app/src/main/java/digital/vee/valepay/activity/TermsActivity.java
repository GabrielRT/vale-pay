package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import digital.vee.valepay.R;
import digital.vee.valepay.util.Preferences;

/**
 * Class to show terms
 *
 * @author Sérgio Brocchetto
 */
public class TermsActivity extends BaseActivity {
    public static final String TAG = "TermsActivity";

    @Bind(R.id.contract_web_view)
    WebView contractWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);

        Preferences.init(this);

        createProgressDialog(TermsActivity.this, true, dialog -> redirUseOpenMenu());
        //load content of html into WebView
        loadWebView();

        setToolbar();

        showMessage(this.getIntent().getStringExtra("toast"));
    }

    /**
     * load the webview in the screen with the therms
     */
    private void loadWebView() {
        WebView termsWebView = (WebView) findViewById(R.id.contract_web_view);
        WebSettings settings = termsWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        if(Locale.getDefault().getLanguage().equalsIgnoreCase("en")){
            termsWebView.loadUrl("file:///android_asset/terms/terms-en.html");
        }else{
            termsWebView.loadUrl("file:///android_asset/terms/terms.html");
        }

        dismissDialog();
    }

    /**
     * If user clicks to return, it will call redirUseOpenMenu
     * @param item item clicked by the user, usually the back arrow
     * @return true if user chose to return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}