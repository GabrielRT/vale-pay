package digital.vee.valepay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.crash.FirebaseCrash;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.codec.android.CharEncoding;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.TSC.TransactionSignedCode;

/**
 * Control the PIN change by validating user info with the server and sending the new PIN from
 * the user's input
 *
 * @author Rafael Magalhães
 */
@EActivity(R.layout.activity_change_pincode)
public class ChangePINCodeActivity extends BaseActivity {

    public static final String PERSONAL_ID_KEY = "personalID";

    private static final String URL = BuildConfig.SERVER_URL + "rest/private/manager/changepin";
    private static final String TAG = "ChangePINCodeActivity";

    private static final String FONTS_ROBOTO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";
    private static final String FONTS_ROBOTO_LIGHT_TTF = "fonts/Roboto-Light.ttf";
    private static final String FONTS_ROBOTO_BOLD_TTF = "fonts/Roboto-Bold.ttf";

    private static final String CURRENT_PIN_HASH_KEY = "currentPINHash";
    private static final String NEW_PIN_HASH_KEY = "newPINHash";
    public static final String CONFIRMATION_PIN_CODE = "pinCode";

    @ViewById
    Toolbar toolbar;
    @ViewById
    TextView currentPincodeText;

    @ViewById
    EditText newPincodeET;
    @ViewById
    EditText confirmPincodeET;
    @ViewById(R.id.btn_submit)
    Button submitBtn;

    private ProgressDialog progressDialog;
    private String token;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(new IconicsDrawable(this)
                    .icon(GoogleMaterial.Icon.gmd_chevron_left)
                    .color(Color.WHITE)
                    .sizeDp(24));
        }

        setFonts();

        Preferences.init(this);

        doLogin();

        //token = Preferences.getString(LoginUtil.TOKEN_KEY);
        try {
            token = SnappyDBHelper.getEncrypted(getApplicationContext(), LoginUtil.TOKEN_KEY);

            if (token.isEmpty() || token == null) {
                FirebaseCrash.log("---ChangePINCodeActivity: invalid token: " + token);
            }

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Typeface of EditTexts and TextViews
     */
    private void setFonts() {
        AssetManager assets = this.getAssets();
        Typeface typeRobotoRegular = Typeface.createFromAsset(assets, FONTS_ROBOTO_REGULAR_TTF);
        Typeface typeRobotoLight = Typeface.createFromAsset(assets, FONTS_ROBOTO_LIGHT_TTF);
        Typeface typeRobotoBold = Typeface.createFromAsset(assets, FONTS_ROBOTO_BOLD_TTF);

        currentPincodeText.setText(getString(R.string.signup_input_pin));
        currentPincodeText.setTypeface(typeRobotoBold);

        newPincodeET.setTypeface(typeRobotoRegular);
        confirmPincodeET.setTypeface(typeRobotoRegular);
    }

    /**
     * Send a postRequest to confirm the pin with the server
     */
    @Click(R.id.btn_submit)
    void confirmBtnClicked() {
        //Log.d(TAG, "ChangePin Confirm");

        if (validate()) {
            submitBtn.setEnabled(false);

            createProgressDialog(ChangePINCodeActivity.this, getString(R.string.toast_creating_account));

            // getEncrypted data used in post to change the PIN Code
            String entityId = null;
            try {
                entityId = SnappyDBHelper.get(getApplicationContext(), "entityId");
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            final String pinCode = getIntent().getStringExtra(ChangePINCodeActivity_.CONFIRMATION_PIN_CODE);// currentPincodeET.getText().toString();
            final String newPinCode = newPincodeET.getText().toString();

            // create json object to use in post
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put(ActivateActivity.PERSONAL_ID, entityId);
                jsonBody.put(CURRENT_PIN_HASH_KEY, TransactionSignedCode.hashPin(pinCode + entityId));
                jsonBody.put(NEW_PIN_HASH_KEY, TransactionSignedCode.hashPin(newPinCode + entityId));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            String mRequestBody = jsonBody.toString();

            Response.Listener<String> listener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        //save pin
                        try {
                            SnappyDBHelper.putEncrypted(ChangePINCodeActivity.this, "pin", newPinCode);
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                        onChangePinSuccess();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            Response.ErrorListener errorListener = error -> {
                String errorMsg = "";
                if (error != null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        onChangePinFailed(false);
                        showTimeoutDialog(ChangePINCodeActivity.this);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(error.getMessage());
                            errorMsg = jsonObject.optString("error");
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                        }

                        if (errorMsg.equals("br.com.veedigital.exception.security.ConfirmationPINDifferentException: EX_CONFIRMATION_PIN_DIFFERENT")) {
                            Intent i = new Intent(ChangePINCodeActivity.this, ConfirmPINCodeActivity_.class);
                            i.putExtra("toast", "Senha de Transação inválida.");
                            i.putExtra("showCallOption", true);
                            ActivityUtils.exitActivity(this, i);
                            finish();
                        }
                    }
                } else {
                    onChangePinFailed(true);
                }

            };

            StringRequestCustom postRequest = new StringRequestCustom(Request.Method.POST, URL, listener, errorListener,
                    mRequestBody) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();

                    if (token.isEmpty()) {
                        FirebaseCrash.log("---ChangePINCodeActivity: invalid token: " + token);
                    }

                    params.put("Authorization", "Token " + token);

                    params.put("Accept-Language", "pt_br");
                    //Log.v(TAG, " Header token: " + token);
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    //Log.v(TAG, TAG + " +parseNetworkError()");
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                        try {
                            onErrorResponse(volleyError);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    //Log.v(TAG, TAG + " -parseNetworkError()");
                    return volleyError;
                }
            };
            System.setProperty("http.keepAlive", "false");
            postRequest.setRetryPolicy(new DefaultRetryPolicy(15 * 1000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(postRequest);

        } else {
            dismissDialog();
            submitBtn.setEnabled(true);
        }
    }

    /**
     * Validate if the fields of PIN Code are filled correctly
     * @return <code>true</code> if all input fields are correct
     */
    private boolean validate() {
        String currentPinCode = getIntent().getStringExtra(ChangePINCodeActivity_.CONFIRMATION_PIN_CODE);// currentPincodeET.getText().toString();
        String newPinCode = newPincodeET.getText().toString();
        String pinCodeConfirmation = confirmPincodeET.getText().toString();
        boolean valid = true;

        // validate new pin code input
        if (newPinCode.isEmpty() || newPinCode.length() < 4) {
            newPincodeET.requestFocus();
            newPincodeET.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else if (newPinCode.equals(currentPinCode)) {
            newPincodeET.requestFocus();
            newPincodeET.setError(getString(R.string.error_new_pass));
            valid = false;
        }
        else {
            newPincodeET.setError(null);
        }

        // validate confirm new pin code input
        if (pinCodeConfirmation.isEmpty() || pinCodeConfirmation.length() < 4) {
            confirmPincodeET.requestFocus();
            confirmPincodeET.setError(getString(R.string.error_pin_code));
            valid = false;
        }
        else if (!pinCodeConfirmation.equals(newPinCode)) {
            confirmPincodeET.requestFocus();
            confirmPincodeET.setError(getString(R.string.error_pass_confirm_signup));
            valid = false;
        }
        else {
            confirmPincodeET.setError(null);
        }
        return valid;
    }

    /**
     * Process success on Change PIN Code and starts UseActivity.
     */
    private void onChangePinSuccess() {
        dismissDialog();
        Intent intent = new Intent(this, UseActivity.class);
        intent.putExtra("toast", getString(R.string.toast_signup2_success));
        Preferences.putBoolean(MainActivity.PIN_CHANGED, true);
        finish();
        ActivityUtils.newActivity(this, intent);
    }

    /**
     * Process error on change PIN Code and shows message to user.
     */
    private void onChangePinFailed(boolean showToast) {
        dismissDialog();
        submitBtn.setEnabled(true);
        if (showToast) {
            Toast.makeText(getBaseContext(), R.string.toast_signup_failed, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Process error on response from server and log or show it to user.
     *
     * @param error Error received from server.
     * @throws UnsupportedEncodingException
     */
    private void onErrorResponse(VolleyError error) throws UnsupportedEncodingException {
        //Log.v(TAG, TAG + " +onErrorResponse()");
        String json = "";

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    json = trimMessage(json, "message");
            }
            if (!(json != null && json.isEmpty()))
                displayMessage(json);
        }
        //Log.v(TAG, TAG + " -onErrorResponse()");
    }

    /**
     * Get message from JSON response received from server
     *
     * @param json JSON response text
     * @param key Key to find message in JSON response
     * @return Message from JSON or <code>null</code> in case of no message in JSON.
     * @throws UnsupportedEncodingException
     */
    private String trimMessage(String json, String key) throws UnsupportedEncodingException {
        String trimmedString;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        trimmedString = Arrays.toString(trimmedString.getBytes("UTF-8"));
        return trimmedString;
    }

    /**
     * Display a toast message to user in UTF-8 format
     *
     * @param toastString Message to show
     */
    private void displayMessage(String toastString) {
        String s = null;
        try {
            s = new String(toastString.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ConfirmPINCodeActivity_.class);
        finish();
        ActivityUtils.exitActivity(this, intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent intent = new Intent(this, ConfirmPINCodeActivity_.class);
                finish();
                ActivityUtils.exitActivity(this, intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Converts the byte[] message to json object and send the exceptional message
     *
     * @param message error message in byte [] format.
     * @return error message in string format.
     */
    private String exceptionMessage(byte[] message) throws UnsupportedEncodingException {
        String strMessage = new String(new String(message).getBytes(), CharEncoding.UTF_8);
        try {
            JSONObject jsonObject = new JSONObject(strMessage);
            String error = jsonObject.optString("error");
            return !error.isEmpty() ? error : "Erro no servidor";

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
