package digital.vee.valepay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.Volley;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import digital.vee.valepay.BuildConfig;
import digital.vee.valepay.R;
import digital.vee.valepay.adapter.TransactionAdapter;
import digital.vee.valepay.helper.SnappyDBHelper;
import digital.vee.valepay.helper.VolleySingleton;
import digital.vee.valepay.model.Transaction;
import digital.vee.valepay.util.ActivityUtils;
import digital.vee.valepay.util.App;
import digital.vee.valepay.util.LoginUtil;
import digital.vee.valepay.util.Preferences;
import digital.vee.valepay.util.StringRequestCustom;
import digital.vee.valepay.util.crypto.CryptoUtil;

/**
 * Class that controls and shows a list of transactions.
 *
 * @author Sérgio Brocchetto
 */

public class TransactionsListActivity extends BaseActivity {
    public static final int TRANSACTIONS_NUMBER = 100;
    private static final String URL = BuildConfig.SERVER_URL + "rest/private/transaction/movementlist";
    private static final String tokenKey = LoginUtil.TOKEN_KEY;

    private StringRequestCustom postRequest;
    String balance = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.v(TAG, "Starting...");

        initializeKeyStore();

        setContentView(R.layout.activity_transactions_list);

        Preferences.init(getApplicationContext());

        doLogin();

        ButterKnife.bind(this);

        setToolbar();

        setBalance();

        setFonts();

        createProgressDialog(TransactionsListActivity.this, true, dialog -> {
            postRequest.cancel();
            redirUseOpenMenu();
        });

        createTransactionList();
    }


    /**
     * Get the user balance by an Extra String given trough the intent to be shown to the user
     */
    private void setBalance() {
        try {
            // App.BALANCE_HOME is already formatted when its saved on snappy
            balance = SnappyDBHelper.get(getApplicationContext(), App.BALANCE_HOME);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    /**
     * initialize an instance of cryptoUtil, that automatically initializes KeyStore on the constructor
     */
    private void initializeKeyStore() {
        //initialize keystore when starting after activation
        try {
            new CryptoUtil(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the list where the transactions are, get all transactions via Volley request and sets
     * in the adapter to be displayed on screen
     */
    private void createTransactionList() {
        // Construct the data source
        ArrayList<Transaction> arrayOfUsers = new ArrayList<>();
        // Create the adapter to convert the array to views
        final TransactionAdapter adapter = new TransactionAdapter(this, arrayOfUsers);

        // Attach the adapter to a ListView
        ListView listView = findViewById(R.id.lvItems);
        listView.setAdapter(adapter);

        JSONObject jsonBody = new JSONObject();
        Intent intent = getIntent();
        try {
            if (intent.hasExtra("walletFrom")) {
                jsonBody.put("walletIdentifier", intent.getStringExtra("walletFrom"));
            } else {
                jsonBody.put("walletIdentifier", SnappyDBHelper.get(getApplicationContext(), LoginUtil.WALLET_ID));
            }
            //TODO: alterar
            jsonBody.put("qty", TRANSACTIONS_NUMBER);
        } catch (JSONException | SnappydbException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();

        postRequest = new StringRequestCustom(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    String response;

                    @Override
                    public void onResponse(String response) {
                        try {
                            //Log.v(TAG, "*** *** Response: " + response);
                            this.response = response;
                            JSONArray jsonResponse = new JSONArray(response);
                            ArrayList<Transaction> newTransactions = Transaction.fromJson(jsonResponse);
                            adapter.addAll(newTransactions);
                            dismissDialog();

                        } catch (Exception e) {
                            //Log.v(TAG, "2 Exception: " + e.getMessage());
                            e.printStackTrace();
                            dismissDialog();
                        }
                    }
                }, error -> {
                    //Log.v(TAG, "1 Exception: " + error);
                    error.printStackTrace();
                    if (error.networkResponse == null && !postRequest.isCanceled()) {
                        dismissDialog();
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showTimeoutDialog(this, new Intent(TransactionsListActivity.this, UseActivity.class)
                                    .putExtra("OpenDrawer", true));
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            //Log.v(TAG, error.getClass().getName());
                            redirUseOpenMenu();
                            showMessage(getString(R.string.no_connection));
                        } else {
                            //Log.v(TAG, error.getClass().getName());
                            redirUseOpenMenu();
                            showMessage(getString(R.string.unknow_error));
                        }
                    }
                }, null)
        {
            Priority mPriority;

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    //VolleyAndroid.util.Log.wtf("Unsupported Encoding while trying to getEncrypted the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Accept-Language", "pt_br");
                try {
                    params.put("Authorization", "Token " + SnappyDBHelper.getEncrypted(getApplicationContext(), tokenKey));
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
                return params;
            }

            @Override
            public Priority getPriority() {
                return mPriority != null ? mPriority : Priority.NORMAL;
            }

        };

        postRequest.setRetryPolicy(VolleySingleton.noRetryDefaultTimeRequest());
        VolleySingleton.getInstance(this).getRequestQueue(this).add(postRequest);
    }

    /**
     * Set font to the many TextViews
     */
    private void setFonts() {
        TextView tv_balance_menu = this.findViewById(R.id.menu_balance_transactions);
        tv_balance_menu.setText(String.format("$ %s", balance));

        Typeface type_roboto_light = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_LIGHT_TTF);
        Typeface type_roboto_bold = Typeface.createFromAsset(this.getAssets(), LoginUtil.FONTS_ROBOTO_BOLD_TTF);

        TextView tv_balance = this.findViewById(R.id.your_balance_menu);
        tv_balance.setTypeface(type_roboto_light);
        tv_balance_menu.setTypeface(type_roboto_bold);
    }

    /**
     * If user click to go back via Back Arrow, he goes back to the useActivity but with the
     * drawerMenu open, so redirUseOpenMenu is called
     * @param item menu item click by the user, currently can only be the white back arrow on the toolbar
     * @return true after success
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * If user click to go back via Back Arrow, he goes back to the useActivity but with the
     * drawerMenu open, so redirUseOpenMenu is called
     */
    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    /**
     * Call ReversalQRCodeTransaction activity to show a QRCode of a specific transaction on list.
     *
     * @param transaction Transaction to be shown
     */
    public void callQRcode(Transaction transaction) {
        if(transaction.movementType != null &&
                transaction.movementType.equalsIgnoreCase("refund")){
            Intent intent = new Intent(this, TransactionRefund.class);
            intent.putExtra("amount", transaction.amount);
            intent.putExtra("description", transaction.description);

            if(transaction.rejectReason != null){
                intent.putExtra("rejectReason", transaction.rejectReason);
            }

            if(transaction.dtConfirmed != null && !transaction.description.isEmpty()) {
                intent.putExtra("dtConfirmed", transaction.dtConfirmed);
            } else if(transaction.dtCanceled != null && !transaction.description.isEmpty()){
                intent.putExtra("dtCanceled", transaction.dtCanceled);
            }

            ActivityUtils.newActivity(this, intent);
        } else {
            Intent intent = new Intent(this, ReversalQRCodeTransaction_.class);
            intent.putExtra("amount", transaction.amount);
            intent.putExtra("walletTo", transaction.walletIdentifierTo);
            intent.putExtra("dtConfirmed", transaction.dtConfirmed);
            intent.putExtra("uuid", transaction.transactionUUID);

            ActivityUtils.newActivity(this, intent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (postRequest != null && !postRequest.isCanceled()) {
            postRequest.cancel();
        }
    }
}

