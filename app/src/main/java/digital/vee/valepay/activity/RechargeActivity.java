package digital.vee.valepay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import digital.vee.valepay.R;
import digital.vee.valepay.adapter.DrawerMenuAdapter;
import digital.vee.valepay.model.DrawerMenuItem;

/**
 * Created by Silvio Machado on 07/02/2018.
 */


public class RechargeActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);

        setToolbar();

        createDrawerMenu();
    }

    /**
     * Create the menu, sets methods to inflate the menu and control it's behavior.
     * Adds all the options to a list view.
     * The 'view terms' and 'logout' options are outside the list and are set as TextView with
     * custom fonts.
     */
    private void createDrawerMenu() {
        //getEncrypted NavigationView layout inside  navigation drawer
        ListView drawerList = (ListView) this.findViewById(R.id.list_view);

        drawerList.setAdapter(new DrawerMenuAdapter(this, getDrawerMenuItems()));
        drawerList.setOnItemClickListener(new RechargeActivity.DrawerItemClickListener());
    }

    /**
     * Make a list of every item that will show up in the menu to be set in the ListView.
     * the id send to the click method will be based on it's position, so, passing '3' as id
     * changes nothing. the selected id onClick will still be 0.
     *
     * @return ArrayList with the menu items inside
     */
    @NonNull
    private List<DrawerMenuItem> getDrawerMenuItems() {
        List<DrawerMenuItem> itemList = new ArrayList<>();
//        itemList.add(new DrawerMenuItem(1, "Recarga Boleto", R.drawable.ic_my_payments));
//        itemList.add(new DrawerMenuItem(2, "Recarga Automático", R.drawable.ic_voucher));
        itemList.add(new DrawerMenuItem(3, "Recarga Gift Card", R.drawable.ic_my_payments));
        return itemList;
    }

    /**
     * If user clicks to return, it will call redirUseOpenMenu
     * @param item item clicked by the user, usually the back arrow
     * @return true if user chose to return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                redirUseOpenMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        redirUseOpenMenu();
    }

    /**
     * Create mapping of list of items on Navigation Drawer and associate the proper action
     */
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            int selectedId = (int) id;
            switch (selectedId) {
                case 0:
                    startActivity(new Intent(RechargeActivity.this, GiftCardActivity.class));
                    finish();
                    break;
                    //these below will be used only on R18 part 2
                case 2:
                    startActivity(new Intent(RechargeActivity.this, GiftCardActivity.class));
                    finish();
                    break;
                case 3:
                    startActivity(new Intent(RechargeActivity.this, GiftCardActivity.class));
                    finish();
                    break;
            }
        }
    }
}
