package digital.vee.valepay.activity;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import digital.vee.valepay.R;
import digital.vee.valepay.model.Wallet;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.not;

/**
 * Created by Silvio Machado on 11/12/2017.
 */

@RunWith(AndroidJUnit4.class)
public class UseActivityEspressoTest {

    private static final String TAG = "EspressoTest";

    @Rule
    public ActivityTestRule<UseActivity> mActivityRule =
            new ActivityTestRule<>(UseActivity.class);

    /**
     * Check if carousel of cards is being displayed
     */
    @Test
    public void ensureCardsAreThere() {
        onView(withId(R.id.list_vertical)).check(matches(isDisplayed()));
    }

    /**
     * Click the 'addnewcard' Card, that is in the last position, and check if the activationActivity is being called
     * by checking if a View that is only the Activation is visible.
     */
    @Test
    public void addNewCard() {
        int addCardPosition = mActivityRule.getActivity().getAdapter().getItemCount() - 1;
        onView(withId(R.id.list_vertical)).perform(RecyclerViewActions.actionOnItemAtPosition(
                addCardPosition, click()));

        onView(withId(R.id.input_confirmation_code)).check(matches(isDisplayed()));
    }

    /**
     * Get a Jsonobject from when the UseActivity updates itself with the server, and scrolls to every
     * position on the card carousel, except the 'AddnewCard', and compare the value of the TextView
     * with the value of the JSON obtained from the server.
     *
     * @throws JSONException
     */
    @Test
    public void checkBalance() throws JSONException {
        int cardsCount = mActivityRule.getActivity().getAdapter().getItemCount();

        sleep(5000);
        JSONObject jsonObject = mActivityRule.getActivity().getEspressoTeste();
        JSONArray walletList = jsonObject.getJSONObject("abstractEntity").getJSONArray("walletList");

        for (int position = 0; position < cardsCount - 1; position++) {
            onView(withId(R.id.list_vertical)).perform(RecyclerViewActions.scrollToPosition(position));
            sleep(5000);
            Wallet wallet = mActivityRule.getActivity().getAdapter().getItemWallet(position);

            JSONObject ue = walletList.getJSONObject(position);

            String withoutdot = mActivityRule.getActivity().getBalance().getText().toString().replace(".", "");
            float balanceOfTextView = Float.valueOf(withoutdot.replace(',', '.'));
            float balanceOfJSON = Float.valueOf(ue.getString("valueBalance"));

            // Compare the balance showed in the TextView with the balance of the JSON response from the server
            assertTrue(balanceOfJSON == balanceOfTextView);
        }
    }

    /*@Test
    public void checkCardList() {
        Looper.prepare();
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_my_wallet)).perform(click()); // Click on 'my cards'
        onView(withId(R.id.list)).check(matches(isDisplayed())); // check if the list of cards is displayed
        AccountsActivity x = new AccountsActivity();
        int total = x.getAdapter().getCount();

        for (int count = 0; count < total; count++) {
            onData(anything()).inAdapterView(withId(R.id.list)).atPosition(count).perform(click());
            onView(withId(R.id.cardIV)).check(matches(isDisplayed()));
        }
    }*/


    /**
     * Check if useButton is enabled when 'AddNewCard' is in the center of the carousel.
     * Id for useButton is 'btn_change_pin' for some reason.
     */
    @Test
    public void useButton() {
        int addCardPosition = mActivityRule.getActivity().getAdapter().getItemCount() - 1;
        onView(withId(R.id.list_vertical)).perform(RecyclerViewActions.scrollToPosition(addCardPosition));
        onView(withId(R.id.btn_change_pin)).check(matches(not(isEnabled())));
    }

    /**
     * Open and closes the menu via menu buttons located in the toolbar and check if they're behaving
     * correctly and if menu is opening and closing.
     */
    @Test
    public void toolbar() {
        // When opening the menu, the menuRight button ceases to exist, and the menu is displayed
        onView(withId(R.id.menuRight)).perform(click()).check(doesNotExist());
        onView(withId(R.id.nav_view)).check(matches(isDisplayed()));

        // When closing the menu, the back_arrow ceases to exist and the menu is not displayed anymore
        onView(withId(R.id.back_arrow)).perform(click()).check(doesNotExist());
        onView(withId(R.id.nav_view)).check(matches(not(isDisplayed())));
    }

    /*
        Tests below try out all the menu options and then comeback to see if toolbar is working
        and if the buttons are linked to their respective activities
    */
    @Test
    public void transactionList() {
        // Click on transactionlist and check if layout shows
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_payments)).perform(click());
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Check if list is displayed
        onView(withId(R.id.transaction_list_layout)).check(matches(isDisplayed()));
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    @Test
    public void voucherActivity() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_voucher)).perform(click());
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Check if the voucher webview is being displayed
        onView(withId(R.id.wvVouchers)).check(matches(isDisplayed()));
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    @Test
    public void myCards() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_my_wallet)).perform(click()); // Click on 'my cards'
        onView(withId(R.id.list)).check(matches(isDisplayed())); // check if the list of cards is displayed
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    @Test
    public void profile() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_profile)).perform(click());
        onView(withId(R.id.content)).check(matches(isDisplayed()));
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    @Test
    public void contactList() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_contact)).perform(click());
        onView(withId(R.id.header)).check(matches(isDisplayed()));
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    @Test
    public void termsActivity() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withId(R.id.list_view)).perform(swipeUp());
        onView(withText(R.string.menu_text_terms)).perform(click());
        TermsActivity.dismissDialog();
        onView(withId(R.id.contract_web_view)).check(matches(isDisplayed()));
        // Click on the back arrow, had to use code below because it's a google icon from a external library
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        // Check if came back to use Activity with drawer menu opened
        onView(withId(R.id.back_arrow)).check(matches(isDisplayed()));
    }

    /*
    !------------- Menu Tests Ends Here ----------!
     */
    @Test
    public void activate() {
        int addCardPosition = mActivityRule.getActivity().getAdapter().getItemCount() - 1;
        onView(withId(R.id.list_vertical)).perform(RecyclerViewActions.actionOnItemAtPosition(
                addCardPosition, click()));

        onView(withId(R.id.input_confirmation_code)).perform(typeText("fuq12"));
        onView(withId(R.id.btn_submit)).perform(closeSoftKeyboard(), click());
        sleep(5000);
        // if it failed, the input view will still be there
        onView(withId(R.id.input_confirmation_code)).check(matches(isDisplayed()));
    }

    @Test
    public void indicateRestaurant() {
        // Button with text "INDIQUE E GANHE VEES"
        onView(withId(R.id.free_meal_restaurants)).perform(click());
        checkBackArrow();

        onView(withId(R.id.restaurantName_editText)).perform(typeText("Restaurante de Espresso"));
        onView(withHint("E-mail")).perform(typeText("silvio@vee.digital"));
        onView(withHint("Telefone")).perform(typeText("11 947045697"));
        sleep(1000);
        onView(withId(R.id.contact_editText)).perform(closeSoftKeyboard(), typeText("Silveira da Mangueira"));
        closeSoftKeyboard();

        onView(withId(R.id.btn_submit)).perform(closeSoftKeyboard(), click());
        sleep(5000);
        onView(withId(R.id.btn_ok)).perform(click());
    }

    @Test
    public void restaurantList() {
        onView(withId(R.id.find_restaurants)).perform(click());
        sleep(10000);
        checkFindRestaurant();
        checkBackArrow();
    }

    @Test
    public void emailContact() {
        onView(withId(R.id.menuRight)).perform(click());
        onView(withText(R.string.menu_text_contact)).perform(click());
        sleep(1000);
        checkContactViews();
    }

    /**
     * Check if all the views in the contact activity are actually there
     */
    private void checkContactViews() {
        onView(withId(R.id.img_elipse)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.hello_contact)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.hello_text_contact)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.img_phone)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.phone_text)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.phone)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.view2)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.img_whatsapp)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.whatsapp_text)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.whatsapp)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.view)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.img_email)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.email_text)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.email)).perform(scrollTo()).check(matches(isDisplayed()));
    }

    /**
     * Check if all the ids contained in the find restaurant xml are displayed
     */
    private void checkFindRestaurant() {
        onView(withId(R.id.filterCultureButton)).check(matches(isDisplayed()));
        onView(withId(R.id.filterGasStationsButton)).check(matches(isDisplayed()));
        onView(withId(R.id.filterMarketsButton)).check(matches(isDisplayed()));
        onView(withId(R.id.filterRestaurantsButton)).check(matches(isDisplayed()));
        onView(withId(R.id.searchRestText)).check(matches(isDisplayed()));
    }

    private void checkBackArrow() {
        onView(withContentDescription(R.string.abc_action_bar_up_description)).check(matches(isDisplayed()));
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
