package digital.vee.valepay.activity;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import digital.vee.valepay.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class SplashActivityTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Test
    public void splashActivityTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction imageView = onView(
                allOf(withId(R.id.slide1),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.swipeable_view_pager),
                                        0),
                                0),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.button_next),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.navigation_view),
                                        1),
                                3),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.slide2),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.swipeable_view_pager),
                                        1),
                                0),
                        isDisplayed()));
        imageView2.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withId(R.id.button_next),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.navigation_view),
                                        1),
                                3),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.slide3),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.swipeable_view_pager),
                                        2),
                                0),
                        isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withId(R.id.button_next),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.navigation_view),
                                        1),
                                3),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction imageView4 = onView(
                allOf(withId(R.id.slide4),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.swipeable_view_pager),
                                        2),
                                0),
                        isDisplayed()));
        imageView4.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton4 = onView(
                allOf(withId(R.id.button_next),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.navigation_view),
                                        1),
                                3),
                        isDisplayed()));
        appCompatImageButton4.perform(click());

        ViewInteraction imageView5 = onView(
                allOf(withId(R.id.slide5),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.swipeable_view_pager),
                                        2),
                                0),
                        isDisplayed()));
        imageView5.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton5 = onView(
                allOf(withId(R.id.button_next),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.navigation_view),
                                        1),
                                3),
                        isDisplayed()));
        appCompatImageButton5.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btn_accept), withText("Aceito"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatButton.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                0),
                        0),
                        isDisplayed()));
        linearLayout.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
