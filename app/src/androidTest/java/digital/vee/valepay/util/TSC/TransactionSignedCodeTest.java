package digital.vee.valepay.util.TSC;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


class TransactionSignedCodeTest {

	public void generateTSC() {
		TransactionSignedCode tsc = new TransactionSignedCode("entityCodeTeste", "walletCodeTest", new BigDecimal("2122.12"), "9853");
		String signable = tsc.getSignableTransactionString();

		assertNotNull(signable);
		tsc.setSignature("dasiudhaiusdhiaushdiuashdiuashdiuashdiuaasdasd");
		String tscString = tsc.getTSC();
		assertNotNull(tscString);
	}
	
	public void decompressTSC() throws TSCDecodeException {
		TransactionSignedCode tsc = new TransactionSignedCode("entityCodeTeste", "walletCodeTest", new BigDecimal("2122.12"), "9853");
		String signable = tsc.getSignableTransactionString();
		assertNotNull(signable);
		tsc.setSignature("dasiudhaiusdhiaushdiuashdiuashdiuashdiuaasdasd");
		String tscString = tsc.getTSC();
		assertNotNull(tscString);
		TransactionSignedCodeSpec tscSpec = new TransactionSignedCodeSpec(tscString);
		assertNotNull(tscSpec);
		assertEquals(new BigDecimal("2122.12").toString(), tscSpec.getTransactionTSC().getAmount());
	}
}
